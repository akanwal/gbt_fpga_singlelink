--=================================================================================================--
--==================================== Module Information =========================================--
--=================================================================================================--
--																																  	--
-- Company:  					CERN (PH-ESE-BE)																			--
-- Engineer: 					Manoel Barros Marin (manoel.barros.marin@cern.ch) (m.barros@ieee.org)	--
-- 																																--
-- Create Date:		    	10/09/2012 																					--
-- Project Name:				v3_automated_testbench																	--
-- Module Name:   		 	gtx_ber_cntr							 													--
-- 																																--
-- Language:					VHDL'93		                                       							--
--																																	--
-- Target Devices: 			GLIB (Virtex 6)																			--
-- Tool versions: 			ISE 13.2          																		--
--																																	--
-- Revision:		 			1.0 																							--
--																																	--
-- Additional Comments: 																									--
--																																	--
--=================================================================================================--
--=================================================================================================--
-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;
--=================================================================================================--
--======================================= Module Body =============================================-- 
--=================================================================================================--
entity gtx_ber_cntr is
	generic (	
		CNTR_WDTH									: integer range 0 to 128 := 64
	);					
	port (					
		-- Reset:				
		RESET_I										: in  std_logic;
		-- Clock:				
		CLK_I											: in  std_logic;
		-- Input:				
		TRIGGER_I									: in  std_logic;	
		ERROR_FLAG_I								: in  std_logic;
		-- Output:				
		ERROR_CNT_O									: out std_logic_vector(CNTR_WDTH-1 downto 0);
		BITS_CNT_O									: out std_logic_vector(CNTR_WDTH-1 downto 0)
	);
end gtx_ber_cntr;
architecture structural of gtx_ber_cntr is
	--============================ Declarations ===========================--
   signal trigger_rr 							: std_logic;
   signal trigger_r								: std_logic;
	--=====================================================================--	
--========================================================================--
-----		  --===================================================--
begin		--================== Architecture Body ==================-- 
-----		  --===================================================--
--========================================================================--	
	--============================ User Logic =============================--	
	-- Main user logic:
	main_process: process(RESET_I, CLK_I)
		type T_state is (e0, e1);
		variable state 							: T_state;
		variable error_cnt						: unsigned(CNTR_WDTH-1 downto 0);
		variable bits_cnt							: unsigned(CNTR_WDTH-1 downto 0);
	begin
		if RESET_I = '1' then
			state										:= e0;
			trigger_rr  							<= '0';
			trigger_r								<= '0';
			error_cnt								:= (others => '0'); 				
			ERROR_CNT_O								<= (others => '0');
			bits_cnt									:= (others => '0');
			BITS_CNT_O								<= (others => '0');	
		elsif rising_edge(CLK_I) then
			trigger_rr                       <= trigger_r;
         trigger_r								<= TRIGGER_I;
			case state is			
				when e0 =>					
					if trigger_rr = '0' and trigger_r = '1' then
						state							:= e1;
					end if;
				when e1 =>
					if ERROR_FLAG_I  = '1' then
						error_cnt					:= error_cnt + 1; 
					end if;
					bits_cnt							:= bits_cnt + 1;
			end case;
			ERROR_CNT_O								<= std_logic_vector(error_cnt);
			BITS_CNT_O								<= std_logic_vector(bits_cnt);	
		end if;
	end process;	
	--=====================================================================--	
end structural;
--=================================================================================================--
--=================================================================================================--