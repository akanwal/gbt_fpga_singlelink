
--=================================================================================================--
--==================================== Module Information =========================================--
--=================================================================================================--
--																																  	--
-- Company:  					CERN (PH-ESE-BE)																			--
-- Engineer: 					Manoel Barros Marin (manoel.barros.marin@cern.ch) (m.barros@ieee.org)	--
-- 																																--
-- Create Date:		    	12/03/2012		 																			--
-- Project Name:				GLIB_Lanes_for_data_transmission														--
-- Module Name:   		 	Boundary Scan emulator           													--
-- 																																--
-- Language:					VHDL'93																						--
--																																	--
-- Target Devices: 			GLIB (Virtex 6)																			--
-- Tool versions: 			ISE 13.2           																		--
--																																	--
-- Revision:		 			1.0 																							--
--																																	--
-- Additional Comments: 																									--
--																																	--
--=================================================================================================--
--=================================================================================================--
-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;
-- User libraries and packages:
--use work.xxxxxxx.all;
--=================================================================================================--
--======================================= Module Body =============================================-- 
--=================================================================================================--
entity bscan_emu is
	generic (	
		BUS_WIDTH												: integer := 80;
		READ_DELAY												: integer := 10;	-- 10cycles@40MHz = 250ns 
		TEST_DELAY												: integer := 2
	);	
	port (	
		RESET_I													: in  std_logic;
		CLK_I														: in  std_logic;
		TRIGGER_I												: in  std_logic;
		DATA_O													: out std_logic_vector(BUS_WIDTH-1 downto 0);
		DATA_I													: in  std_logic_vector(BUS_WIDTH-1 downto 0);
		BUSY_O													: out std_logic;
		BUS_ERRORS_O											: out std_logic_vector(BUS_WIDTH-1 downto 0);
		BUS_CORRECT_O											: out std_logic		
	);
end bscan_emu;
architecture structural of bscan_emu is
	--============================ Declarations ===========================--
	-- Attributes:
	attribute S		: string;
	attribute keep : string;
	--Signals:	
	signal data_i_r											: std_logic_vector(BUS_WIDTH-1 downto 0);	
	signal bus_zerosErrors									: std_logic_vector(BUS_WIDTH-1 downto 0);
	signal bus_onesErrors									: std_logic_vector(BUS_WIDTH-1 downto 0);	
	signal bus_errors											: std_logic_vector(BUS_WIDTH-1 downto 0);
	signal testDone											: std_logic;	
	--=====================================================================--
	--======================= Constant Declarations =======================--
	constant NO_ERRORS			 : std_logic_vector(BUS_WIDTH-1 downto 0)	:= (others => '0');	
	--=====================================================================--	
	--=====================================================================--
-----		  --===================================================--
begin		--================== Architecture Body ==================-- 
-----		  --===================================================--
--========================================================================--
	--============================ User Logic =============================--
	main_process: process(RESET_I, CLK_I)
		type stateT is (s0_idle, s1_sendMarchingZero, s2_checkMarchingZero,
							 s3_sendMarchingOne,	s4_checkMarchingOne, s5_testDone);
		variable state											: stateT;
		variable i												: integer range 0 to BUS_WIDTH-1;	
		variable data											: std_logic_vector(BUS_WIDTH-1 downto 0);	
		variable count											: integer range 0 to READ_DELAY-1;		
	begin
		if RESET_I = '1' then
			state													:= s0_idle;
			BUSY_O												<= '0';		
			data_i_r												<= (others => '0');	
			i														:= 0;		
			data													:= (others => '1');
			DATA_O												<= (others => '1');
			count													:= 0;		
			bus_zerosErrors									<= (others => '0');					
			bus_onesErrors										<= (others => '0');			
			testDone												<= '0';
			BUS_ERRORS_O										<= (others => '0');
			BUS_CORRECT_O										<= '0';
		elsif rising_edge(CLK_I) then
			-- Input register:
			data_i_r												<= DATA_I; 			
			-- Finites State Machine:
			case state is 
				when s0_idle =>
					if TRIGGER_I = '1' then
						state										:= s1_sendMarchingZero;
						BUSY_O									<= '1';							
					end if;
				-- Marching 0s:
				when s1_sendMarchingZero =>
					state											:= s2_checkMarchingZero;
					if i > 0 then
						data(i-1)								:= '1';
					end if;	
					data(i)										:= '0';
					DATA_O										<= data;	
				when s2_checkMarchingZero =>
					if count = READ_DELAY-1 then						
						if data_i_r(i) /= data(i) then							
							bus_zerosErrors(i)				<= '1';
						end if;
						if i = BUS_WIDTH-1 then
							state									:= s3_sendMarchingOne;
							data									:= (others => '0');
							DATA_O								<= (others => '0');								
							i									 	:= 0;	
						else
							state									:= s1_sendMarchingZero;
							i									 	:= i+1;				
						end if;
						count 									:= 0;							
					else
						count 									:= count+1;
					end if;				
				-- Marching 1s:
				when s3_sendMarchingOne =>
					state											:= s4_checkMarchingOne;							
					if i > 0 then
						data(i-1)								:= '0';
					end if;	
					data(i)										:= '1';
					DATA_O										<= data;		
				when s4_checkMarchingOne =>
					if count = READ_DELAY-1 then
						if data_i_r(i) /= data(i) then
							bus_onesErrors(i)					<= '1';
						end if;
						if i = BUS_WIDTH-1 then
							state									:= s5_testDone;
							i									 	:= 0;	
							DATA_O								<= (others => '0');	
						else
							state									:= s3_sendMarchingOne;
							i									 	:= i+1;				
						end if;
						count 									:= 0;							
					else
						count 									:= count+1;
					end if;		
				-- Test done:				
				when s5_testDone =>					
					testDone										<= '1';												
			end case;
			-- Test Result:
			if testDone = '1' then 
				if count = TEST_DELAY-1 then
					BUSY_O										<= '0';	
					if bus_errors /= NO_ERRORS then
						BUS_ERRORS_O							<= bus_errors;	
					else
						BUS_CORRECT_O							<= '1';	
					end if;
				else
					count											:= count+1;
				end if;				
			end if;	
		end if;	
	end process;	
	testResultMask_generate: for i in 0 to BUS_WIDTH-1 generate
		testResultMask_process: process(RESET_I, CLK_I)			
		begin
			if RESET_I = '1' then				
				bus_errors(i)									<= '0';
			elsif rising_edge(CLK_I) then
				if testDone	= '1' then
					bus_errors(i)								<= bus_zerosErrors(i) or bus_onesErrors(i);					
				else					
					bus_errors(i)								<= '0';
				end if;
			end if;
		end process;
	end generate;
	--=====================================================================--	
end structural;
--=================================================================================================--
--=================================================================================================--
