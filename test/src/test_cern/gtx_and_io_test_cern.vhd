library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.system_package.all;

library unisim;
use unisim.vcomponents.all;

use work.user_package.all;

entity gtx_and_io_test_cern is 
port
(	

   --==========================--
   -- Clocks scheme
   --==========================--
   
	osc125_b_bufg_i					: in   	std_logic;
	osc125_b_mgtrefclk_i			   : in	   std_logic;
	--		
	osc_xpoint_a_p						: in		std_logic;
	osc_xpoint_a_n						: in		std_logic;
	osc_xpoint_b_p						: in		std_logic;
	osc_xpoint_b_n						: in		std_logic;
	osc_xpoint_c_p						: in		std_logic;
	osc_xpoint_c_n						: in		std_logic;
	osc_xpoint_d_p						: in		std_logic;
	osc_xpoint_d_n						: in		std_logic;
   
   --==========================--
   -- High-Speed test
   --==========================--
   
	fmc_l12_dp_c2m_p					: out		std_logic_vector(11 downto 0);
	fmc_l12_dp_c2m_n					: out		std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_p					: in		std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_n					: in		std_logic_vector(11 downto 0);
	--
	fmc_l8_dp_c2m_p					: out		std_logic_vector( 7 downto 0);
	fmc_l8_dp_c2m_n					: out		std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_p					: in		std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_n					: in		std_logic_vector( 7 downto 0);
	--		
	k7_amc_rx_p							: in		std_logic_vector(15 downto 1);
	k7_amc_rx_n							: in		std_logic_vector(15 downto 1);
	amc_tx_p								: out		std_logic_vector(15 downto 1);
	amc_tx_n								: out		std_logic_vector(15 downto 1);
   
   --==========================--
   -- User IO test
   --==========================--
   
   fmc_l8_la_p							: inout	std_logic_vector(33 downto 0);
	fmc_l8_la_n							: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_p						: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_n						: inout	std_logic_vector(33 downto 0);
   
   --==========================--
   -- Control
   --==========================--

	ipb_rst_i				         : in	  std_logic;
	ipb_clk_i				         : in	  std_logic;
   ------------------------------
	ipb_stat_reg_o			         : out	  array_192x32bit;
	ipb_ctrl_reg_i			         : in	  array_32x32bit 	
   
);
end gtx_and_io_test_cern;

architecture structural of gtx_and_io_test_cern is
	
   --===========================================--
	-- High-Speed test
	--===========================================--
   
   -- Clocks scheme:
   -----------------
   
   signal osc125_b		                   : std_logic;
   signal osc125_b_bufg 		             : std_logic;
   --		                                  
   signal osc_xpoint_a_bufg                : std_logic;
   signal osc_xpoint_a                     : std_logic;
   signal osc_xpoint_b                     : std_logic;
   signal osc_xpoint_b_bufg                : std_logic;
   signal osc_xpoint_c                     : std_logic;
   signal osc_xpoint_c_bufg                : std_logic;
   signal osc_xpoint_d                     : std_logic;
   signal osc_xpoint_d_bufg                : std_logic;
                                           
   signal txOutClk_from_fmcL8_dp           : std_logic_vector(0 to   7);
   signal txOutClk_from_fmcL8_dp_buff      : std_logic_vector(0 to   7);
   signal txOutClk_from_fmcL12_dp          : std_logic_vector(0 to  11);
   signal txOutClk_from_fmcL12_dp_buff     : std_logic_vector(0 to  11);
   signal txOutClk_from_amc                : std_logic_vector(1 to  11);
   signal txOutClk_from_amc_buff           : std_logic_vector(1 to  11);
                                           
   -- Status:                              
   ----------                              
                                           
   signal txReady_from_fmcL8_dp            : std_logic_vector(0 to  7);
   signal rxReady_from_fmcL8_dp            : std_logic_vector(0 to  7);
   --                                      
   signal txReady_from_fmcL12_dp           : std_logic_vector(0 to 11);
   signal rxReady_from_fmcL12_dp           : std_logic_vector(0 to 11);
   --                                      
   signal txReady_from_amc                 : std_logic_vector(1 to 11);
   signal rxReady_from_amc                 : std_logic_vector(1 to 11);  
	                                        
   signal rxPrbsErr_from_fmcL8_dp          : std_logic_vector(0 to  7);
   signal errCnt_from_fmcL8_berCnt         : array_nx64bit(0 to  7);
   signal bitsCnt_from_fmcL8_berCnt        : array_nx64bit(0 to  7);
   signal errCnt_from_fmcL8_cdc            : array_nx64bit(0 to  7);
   signal bitsCnt_from_fmcL8_cdc           : array_nx64bit(0 to  7);
   --                                      
   signal rxPrbsErr_from_fmcL12_dp         : std_logic_vector(0 to 11);
   signal errCnt_from_fmcL12_berCnt        : array_nx64bit(0 to 11);
   signal bitsCnt_from_fmcL12_berCnt       : array_nx64bit(0 to 11);
   signal errCnt_from_fmcL12_cdc           : array_nx64bit(0 to 11);
   signal bitsCnt_from_fmcL12_cdc          : array_nx64bit(0 to 11);
   --                                      
   signal rxPrbsErr_from_amc               : std_logic_vector(1 to 11);
   signal errCnt_from_amc_berCnt           : array_nx64bit(1 to 11);
   signal bitsCnt_from_amc_berCnt          : array_nx64bit(1 to 11);  
   signal errCnt_from_amc_cdc              : array_nx64bit(1 to 11);
   signal bitsCnt_from_amc_cdc             : array_nx64bit(1 to 11);
   
   -- Control:
   -----------
   
   signal gtx_loopback                     : std_logic_vector(2 downto 0);
                                           
   signal fmc_l8_dp_gtx_tx_reset           : std_logic_vector(0 to  7);
   signal fmc_l8_dp_gtx_rx_reset           : std_logic_vector(0 to  7);
   --                                      
   signal fmc_l12_dp_gtx_tx_reset          : std_logic_vector(0 to 11);
   signal fmc_l12_dp_gtx_rx_reset          : std_logic_vector(0 to 11);
   --                                      
   signal amc_gtx_tx_reset                 : std_logic_vector(1 to 11);
   signal amc_gtx_rx_reset                 : std_logic_vector(1 to 11);
   
   signal fmcL8Bank112_gtx_qpll_powerDown  : std_logic;
   signal fmcL8Bank111_gtx_qpll_powerDown  : std_logic;
   --
   signal fmcL12Bank118_gtx_qpll_powerDown : std_logic;
   signal fmcL12Bank117_gtx_qpll_powerDown : std_logic;
   signal fmcL12Bank116_gtx_qpll_powerDown : std_logic;
   --
   signal amc_gtx_cpll_powerDown           : std_logic_vector(1 to 11);
   
   signal fmc_l8_dp_gtx_tx_powerDown       : std_logic_vector(0 to  7);
   signal fmc_l8_dp_gtx_rx_powerDown       : std_logic_vector(0 to  7);
   --                                      
   signal fmc_l12_dp_gtx_tx_powerDown      : std_logic_vector(0 to 11);
   signal fmc_l12_dp_gtx_rx_powerDown      : std_logic_vector(0 to 11);
   --                                      
   signal amc_gtx_tx_powerDown             : std_logic_vector(1 to 11);
   signal amc_gtx_rx_powerDown             : std_logic_vector(1 to 11);
                                           
   signal gtx_bert_trigger	                : std_logic;
   signal gtx_bert_reset	                : std_logic;	
                                           
   --===========================================--
	-- User I/O test
	--===========================================--
   
	signal io_test_trigger         			 : std_logic;
	signal io_test_reset	          			 : std_logic;	
	                                        
	-- FMC L8:                              
	----------                              
	                                        
	signal fmcL8_p_pattern_out 			    : std_logic_vector(16 downto 0);
	signal fmcL8_n_pattern_out 			    : std_logic_vector(16 downto 0);
	-- 
	signal fmcL8_p_pattern_in			       : std_logic_vector(16 downto 0);
	signal fmcL8_n_pattern_in			       : std_logic_vector(16 downto 0);
	                                        
	signal fmcL8_p_user_io_errors   			 : std_logic_vector(16 downto 0);
   signal fmcL8_p_user_io_correct       	 : std_logic;	
	-- 
	signal fmcL8_n_user_io_errors   			 : std_logic_vector(16 downto 0);
   signal fmcL8_n_user_io_correct       	 : std_logic;		
	                                        
	-- FMC L12:                             
	-----------	                            
	                                        
	signal fmcL12_p_pattern_out 			    : std_logic_vector(16 downto 0);
	signal fmcL12_n_pattern_out 			    : std_logic_vector(16 downto 0);
	-- 
	signal fmcL12_p_pattern_in			       : std_logic_vector(16 downto 0);
	signal fmcL12_n_pattern_in			       : std_logic_vector(16 downto 0);
	                                        
	signal fmcL12_p_user_io_errors 			 : std_logic_vector(16 downto 0);
   signal fmcL12_p_user_io_correct       	 : std_logic;		
	-- 
	signal fmcL12_n_user_io_errors  			 : std_logic_vector(16 downto 0);
   signal fmcL12_n_user_io_correct       	 : std_logic;		
	                                        
	-- AMC:                                 
	-----------	                            
	                                        
	signal amc_p_pattern_out 			   	 : std_logic_vector(15 downto 12);
	signal amc_n_pattern_out 			   	 : std_logic_vector(15 downto 12);
	-- 
	signal amc_p_pattern_in			      	 : std_logic_vector(15 downto 12);
	signal amc_n_pattern_in			      	 : std_logic_vector(15 downto 12);
		                                     
	signal amc_p_user_io_errors 				 : std_logic_vector(15 downto 12);
   signal amc_p_user_io_correct       		 : std_logic;		
	-- 
	signal amc_n_user_io_errors  				 : std_logic_vector(15 downto 12);
   signal amc_n_user_io_correct       		 : std_logic;		
	                                        
                                           
begin                                      



   --===========================================--
	-- Status registers mapping
	--===========================================--
   
   -- High-Speed lanes:
   --------------------
   
   -- FMC L8:
                                       ipb_stat_reg_o(  0)( 0)           <= txReady_from_fmcL8_dp( 0);
                                       ipb_stat_reg_o(  0)( 1)           <= rxReady_from_fmcL8_dp( 0);
                                       ipb_stat_reg_o(  0)( 2)           <= txReady_from_fmcL8_dp( 1);
                                       ipb_stat_reg_o(  0)( 3)           <= rxReady_from_fmcL8_dp( 1);	                	
                                       ipb_stat_reg_o(  0)( 4)           <= txReady_from_fmcL8_dp( 2);
                                       ipb_stat_reg_o(  0)( 5)           <= rxReady_from_fmcL8_dp( 2);	
                                       ipb_stat_reg_o(  0)( 6)           <= txReady_from_fmcL8_dp( 3);
                                       ipb_stat_reg_o(  0)( 7)           <= rxReady_from_fmcL8_dp( 3);	
                                       ipb_stat_reg_o(  0)( 8)           <= txReady_from_fmcL8_dp( 4);
                                       ipb_stat_reg_o(  0)( 9)           <= rxReady_from_fmcL8_dp( 4);
                                       ipb_stat_reg_o(  0)(10)           <= txReady_from_fmcL8_dp( 5);
                                       ipb_stat_reg_o(  0)(11)           <= rxReady_from_fmcL8_dp( 5);	                	
                                       ipb_stat_reg_o(  0)(12)           <= txReady_from_fmcL8_dp( 6);
                                       ipb_stat_reg_o(  0)(13)           <= rxReady_from_fmcL8_dp( 6);	
                                       ipb_stat_reg_o(  0)(14)           <= txReady_from_fmcL8_dp( 7);
                                       ipb_stat_reg_o(  0)(15)           <= rxReady_from_fmcL8_dp( 7);	
                                       ipb_stat_reg_o(  0)(31 downto 16) <= (others => '0');
                                       
   -- FMC L12:                         
                                       ipb_stat_reg_o(  1)( 0)           <= txReady_from_fmcL12_dp( 0);
                                       ipb_stat_reg_o(  1)( 1)           <= rxReady_from_fmcL12_dp( 0);
                                       ipb_stat_reg_o(  1)( 2)           <= txReady_from_fmcL12_dp( 1);
                                       ipb_stat_reg_o(  1)( 3)           <= rxReady_from_fmcL12_dp( 1);	                	
                                       ipb_stat_reg_o(  1)( 4)           <= txReady_from_fmcL12_dp( 2);
                                       ipb_stat_reg_o(  1)( 5)           <= rxReady_from_fmcL12_dp( 2);	
                                       ipb_stat_reg_o(  1)( 6)           <= txReady_from_fmcL12_dp( 3);
                                       ipb_stat_reg_o(  1)( 7)           <= rxReady_from_fmcL12_dp( 3);	
                                       ipb_stat_reg_o(  1)( 8)           <= txReady_from_fmcL12_dp( 4);
                                       ipb_stat_reg_o(  1)( 9)           <= rxReady_from_fmcL12_dp( 4);
                                       ipb_stat_reg_o(  1)(10)           <= txReady_from_fmcL12_dp( 5);
                                       ipb_stat_reg_o(  1)(11)           <= rxReady_from_fmcL12_dp( 5);	                	
                                       ipb_stat_reg_o(  1)(12)           <= txReady_from_fmcL12_dp( 6);
                                       ipb_stat_reg_o(  1)(13)           <= rxReady_from_fmcL12_dp( 6);	
                                       ipb_stat_reg_o(  1)(14)           <= txReady_from_fmcL12_dp( 7);
                                       ipb_stat_reg_o(  1)(15)           <= rxReady_from_fmcL12_dp( 7);	
                                       ipb_stat_reg_o(  1)(16)           <= txReady_from_fmcL12_dp( 8);
                                       ipb_stat_reg_o(  1)(17)           <= rxReady_from_fmcL12_dp( 8);	
                                       ipb_stat_reg_o(  1)(18)           <= txReady_from_fmcL12_dp( 9);
                                       ipb_stat_reg_o(  1)(19)           <= rxReady_from_fmcL12_dp( 9);
                                       ipb_stat_reg_o(  1)(20)           <= txReady_from_fmcL12_dp(10);
                                       ipb_stat_reg_o(  1)(21)           <= rxReady_from_fmcL12_dp(10);	   
                                       ipb_stat_reg_o(  1)(22)           <= txReady_from_fmcL12_dp(11);   
                                       ipb_stat_reg_o(  1)(23)           <= rxReady_from_fmcL12_dp(11);
                                       ipb_stat_reg_o(  1)(31 downto 24) <= (others => '0');
                                       
   -- AMC:                             
                                       ipb_stat_reg_o(  2)( 0)           <= txReady_from_amc( 1);
                                       ipb_stat_reg_o(  2)( 1)           <= rxReady_from_amc( 1);
                                       ipb_stat_reg_o(  2)( 2)           <= txReady_from_amc( 2);
                                       ipb_stat_reg_o(  2)( 3)           <= rxReady_from_amc( 2);	                	
                                       ipb_stat_reg_o(  2)( 4)           <= txReady_from_amc( 3);
                                       ipb_stat_reg_o(  2)( 5)           <= rxReady_from_amc( 3);	
                                       ipb_stat_reg_o(  2)( 6)           <= txReady_from_amc( 4);
                                       ipb_stat_reg_o(  2)( 7)           <= rxReady_from_amc( 4);	
                                       ipb_stat_reg_o(  2)( 8)           <= txReady_from_amc( 5);
                                       ipb_stat_reg_o(  2)( 9)           <= rxReady_from_amc( 5);
                                       ipb_stat_reg_o(  2)(10)           <= txReady_from_amc( 6);
                                       ipb_stat_reg_o(  2)(11)           <= rxReady_from_amc( 6);	                	
                                       ipb_stat_reg_o(  2)(12)           <= txReady_from_amc( 7);
                                       ipb_stat_reg_o(  2)(13)           <= rxReady_from_amc( 7);	
                                       ipb_stat_reg_o(  2)(14)           <= txReady_from_amc( 8);
                                       ipb_stat_reg_o(  2)(15)           <= rxReady_from_amc( 8);	
                                       ipb_stat_reg_o(  2)(16)           <= txReady_from_amc( 9);
                                       ipb_stat_reg_o(  2)(17)           <= rxReady_from_amc( 9);	
                                       ipb_stat_reg_o(  2)(18)           <= txReady_from_amc(10);
                                       ipb_stat_reg_o(  2)(19)           <= rxReady_from_amc(10);
                                       ipb_stat_reg_o(  2)(20)           <= txReady_from_amc(11);
                                       ipb_stat_reg_o(  2)(21)           <= rxReady_from_amc(11);	   
                                       ipb_stat_reg_o(  2)(31 downto 22) <= (others => '0');
                                                                            
   -- Word Error Test:                                     
                                       ipb_stat_reg_o(  3) 		          <= errCnt_from_fmcL8_berCnt  ( 0)(31 downto  0);
                                       ipb_stat_reg_o(  4) 		          <= errCnt_from_fmcL8_berCnt  ( 0)(63 downto 32);
                                       ipb_stat_reg_o(  5) 		          <= bitsCnt_from_fmcL8_berCnt ( 0)(31 downto  0);
                                       ipb_stat_reg_o(  6) 		          <= bitsCnt_from_fmcL8_berCnt ( 0)(63 downto 32);
                                       ipb_stat_reg_o(  7) 		          <= errCnt_from_fmcL8_berCnt  ( 1)(31 downto  0);
                                       ipb_stat_reg_o(  8) 		          <= errCnt_from_fmcL8_berCnt  ( 1)(63 downto 32);
                                       ipb_stat_reg_o(  9) 		          <= bitsCnt_from_fmcL8_berCnt ( 1)(31 downto  0);
                                       ipb_stat_reg_o( 10) 		          <= bitsCnt_from_fmcL8_berCnt ( 1)(63 downto 32);
                                       ipb_stat_reg_o( 11) 		          <= errCnt_from_fmcL8_berCnt  ( 2)(31 downto  0);
                                       ipb_stat_reg_o( 12) 		          <= errCnt_from_fmcL8_berCnt  ( 2)(63 downto 32);
                                       ipb_stat_reg_o( 13) 		          <= bitsCnt_from_fmcL8_berCnt ( 2)(31 downto  0);
                                       ipb_stat_reg_o( 14) 		          <= bitsCnt_from_fmcL8_berCnt ( 2)(63 downto 32);
                                       ipb_stat_reg_o( 15) 		          <= errCnt_from_fmcL8_berCnt  ( 3)(31 downto  0);
                                       ipb_stat_reg_o( 16) 		          <= errCnt_from_fmcL8_berCnt  ( 3)(63 downto 32);
                                       ipb_stat_reg_o( 17) 		          <= bitsCnt_from_fmcL8_berCnt ( 3)(31 downto  0);
                                       ipb_stat_reg_o( 18) 		          <= bitsCnt_from_fmcL8_berCnt ( 3)(63 downto 32);
                                       ipb_stat_reg_o( 19) 		          <= errCnt_from_fmcL8_berCnt  ( 4)(31 downto  0);
                                       ipb_stat_reg_o( 20) 		          <= errCnt_from_fmcL8_berCnt  ( 4)(63 downto 32);
                                       ipb_stat_reg_o( 21) 		          <= bitsCnt_from_fmcL8_berCnt ( 4)(31 downto  0);
                                       ipb_stat_reg_o( 22) 		          <= bitsCnt_from_fmcL8_berCnt ( 4)(63 downto 32);
                                       ipb_stat_reg_o( 23) 		          <= errCnt_from_fmcL8_berCnt  ( 5)(31 downto  0);
                                       ipb_stat_reg_o( 24) 		          <= errCnt_from_fmcL8_berCnt  ( 5)(63 downto 32);
                                       ipb_stat_reg_o( 25) 		          <= bitsCnt_from_fmcL8_berCnt ( 5)(31 downto  0);
                                       ipb_stat_reg_o( 26) 		          <= bitsCnt_from_fmcL8_berCnt ( 5)(63 downto 32);
                                       ipb_stat_reg_o( 27) 		          <= errCnt_from_fmcL8_berCnt  ( 6)(31 downto  0);
                                       ipb_stat_reg_o( 28) 		          <= errCnt_from_fmcL8_berCnt  ( 6)(63 downto 32);
                                       ipb_stat_reg_o( 29) 		          <= bitsCnt_from_fmcL8_berCnt ( 6)(31 downto  0);
                                       ipb_stat_reg_o( 30) 		          <= bitsCnt_from_fmcL8_berCnt ( 6)(63 downto 32);
                                       ipb_stat_reg_o( 31) 		          <= errCnt_from_fmcL8_berCnt  ( 7)(31 downto  0);
                                       ipb_stat_reg_o( 32) 		          <= errCnt_from_fmcL8_berCnt  ( 7)(63 downto 32);
                                       ipb_stat_reg_o( 33) 		          <= bitsCnt_from_fmcL8_berCnt ( 7)(31 downto  0);
                                       ipb_stat_reg_o( 34) 		          <= bitsCnt_from_fmcL8_berCnt ( 7)(63 downto 32);
                                       
                                       ipb_stat_reg_o( 35) 		          <= errCnt_from_fmcL12_berCnt ( 0)(31 downto  0);
                                       ipb_stat_reg_o( 36) 		          <= errCnt_from_fmcL12_berCnt ( 0)(63 downto 32);
                                       ipb_stat_reg_o( 37) 		          <= bitsCnt_from_fmcL12_berCnt( 0)(31 downto  0);
                                       ipb_stat_reg_o( 38) 		          <= bitsCnt_from_fmcL12_berCnt( 0)(63 downto 32);
                                       ipb_stat_reg_o( 39) 		          <= errCnt_from_fmcL12_berCnt ( 1)(31 downto  0);
                                       ipb_stat_reg_o( 40) 		          <= errCnt_from_fmcL12_berCnt ( 1)(63 downto 32);
                                       ipb_stat_reg_o( 41) 		          <= bitsCnt_from_fmcL12_berCnt( 1)(31 downto  0);
                                       ipb_stat_reg_o( 42) 		          <= bitsCnt_from_fmcL12_berCnt( 1)(63 downto 32); 
                                       ipb_stat_reg_o( 43) 		          <= errCnt_from_fmcL12_berCnt ( 2)(31 downto  0);
                                       ipb_stat_reg_o( 44) 		          <= errCnt_from_fmcL12_berCnt ( 2)(63 downto 32);
                                       ipb_stat_reg_o( 45) 		          <= bitsCnt_from_fmcL12_berCnt( 2)(31 downto  0);
                                       ipb_stat_reg_o( 46) 		          <= bitsCnt_from_fmcL12_berCnt( 2)(63 downto 32); 
                                       ipb_stat_reg_o( 47) 		          <= errCnt_from_fmcL12_berCnt ( 3)(31 downto  0);
                                       ipb_stat_reg_o( 48) 		          <= errCnt_from_fmcL12_berCnt ( 3)(63 downto 32);
                                       ipb_stat_reg_o( 49) 		          <= bitsCnt_from_fmcL12_berCnt( 3)(31 downto  0);
                                       ipb_stat_reg_o( 50) 		          <= bitsCnt_from_fmcL12_berCnt( 3)(63 downto 32);
                                       ipb_stat_reg_o( 51) 		          <= errCnt_from_fmcL12_berCnt ( 4)(31 downto  0);
                                       ipb_stat_reg_o( 52) 		          <= errCnt_from_fmcL12_berCnt ( 4)(63 downto 32);
                                       ipb_stat_reg_o( 53) 		          <= bitsCnt_from_fmcL12_berCnt( 4)(31 downto  0);
                                       ipb_stat_reg_o( 54) 		          <= bitsCnt_from_fmcL12_berCnt( 4)(63 downto 32); 
                                       ipb_stat_reg_o( 55) 		          <= errCnt_from_fmcL12_berCnt ( 5)(31 downto  0);
                                       ipb_stat_reg_o( 56) 		          <= errCnt_from_fmcL12_berCnt ( 5)(63 downto 32);
                                       ipb_stat_reg_o( 57) 		          <= bitsCnt_from_fmcL12_berCnt( 5)(31 downto  0);
                                       ipb_stat_reg_o( 58) 		          <= bitsCnt_from_fmcL12_berCnt( 5)(63 downto 32); 
                                       ipb_stat_reg_o( 59) 		          <= errCnt_from_fmcL12_berCnt ( 6)(31 downto  0);
                                       ipb_stat_reg_o( 60) 		          <= errCnt_from_fmcL12_berCnt ( 6)(63 downto 32);
                                       ipb_stat_reg_o( 61) 		          <= bitsCnt_from_fmcL12_berCnt( 6)(31 downto  0);
                                       ipb_stat_reg_o( 62) 		          <= bitsCnt_from_fmcL12_berCnt( 6)(63 downto 32);
                                       ipb_stat_reg_o( 63) 		          <= errCnt_from_fmcL12_berCnt ( 7)(31 downto  0);
                                       ipb_stat_reg_o( 64) 		          <= errCnt_from_fmcL12_berCnt ( 7)(63 downto 32);
                                       ipb_stat_reg_o( 65) 		          <= bitsCnt_from_fmcL12_berCnt( 7)(31 downto  0);
                                       ipb_stat_reg_o( 66) 		          <= bitsCnt_from_fmcL12_berCnt( 7)(63 downto 32); 
                                       ipb_stat_reg_o( 67) 		          <= errCnt_from_fmcL12_berCnt ( 8)(31 downto  0);
                                       ipb_stat_reg_o( 68) 		          <= errCnt_from_fmcL12_berCnt ( 8)(63 downto 32);
                                       ipb_stat_reg_o( 69) 		          <= bitsCnt_from_fmcL12_berCnt( 8)(31 downto  0);
                                       ipb_stat_reg_o( 70) 		          <= bitsCnt_from_fmcL12_berCnt( 8)(63 downto 32); 
                                       ipb_stat_reg_o( 71) 		          <= errCnt_from_fmcL12_berCnt ( 9)(31 downto  0);
                                       ipb_stat_reg_o( 72) 		          <= errCnt_from_fmcL12_berCnt ( 9)(63 downto 32);
                                       ipb_stat_reg_o( 73) 		          <= bitsCnt_from_fmcL12_berCnt( 9)(31 downto  0);
                                       ipb_stat_reg_o( 74) 		          <= bitsCnt_from_fmcL12_berCnt( 9)(63 downto 32);
                                       ipb_stat_reg_o( 75) 		          <= errCnt_from_fmcL12_berCnt (10)(31 downto  0);
                                       ipb_stat_reg_o( 76) 		          <= errCnt_from_fmcL12_berCnt (10)(63 downto 32);
                                       ipb_stat_reg_o( 77) 		          <= bitsCnt_from_fmcL12_berCnt(10)(31 downto  0);
                                       ipb_stat_reg_o( 78) 		          <= bitsCnt_from_fmcL12_berCnt(10)(63 downto 32); 
                                       ipb_stat_reg_o( 79) 		          <= errCnt_from_fmcL12_berCnt (11)(31 downto  0);
                                       ipb_stat_reg_o( 80) 		          <= errCnt_from_fmcL12_berCnt (11)(63 downto 32);
                                       ipb_stat_reg_o( 81) 		          <= bitsCnt_from_fmcL12_berCnt(11)(31 downto  0);
                                       ipb_stat_reg_o( 82) 		          <= bitsCnt_from_fmcL12_berCnt(11)(63 downto 32);
                                       
                                       ipb_stat_reg_o( 83) 		          <= errCnt_from_amc_berCnt    ( 1)(31 downto  0);
                                       ipb_stat_reg_o( 84) 		          <= errCnt_from_amc_berCnt    ( 1)(63 downto 32);
                                       ipb_stat_reg_o( 85) 		          <= bitsCnt_from_amc_berCnt   ( 1)(31 downto  0);
                                       ipb_stat_reg_o( 86) 		          <= bitsCnt_from_amc_berCnt   ( 1)(63 downto 32);
                                       ipb_stat_reg_o( 87) 		          <= errCnt_from_amc_berCnt    ( 2)(31 downto  0);
                                       ipb_stat_reg_o( 88) 		          <= errCnt_from_amc_berCnt    ( 2)(63 downto 32);
                                       ipb_stat_reg_o( 89) 		          <= bitsCnt_from_amc_berCnt   ( 2)(31 downto  0);
                                       ipb_stat_reg_o( 90) 		          <= bitsCnt_from_amc_berCnt   ( 2)(63 downto 32); 
                                       ipb_stat_reg_o( 91) 		          <= errCnt_from_amc_berCnt    ( 3)(31 downto  0);
                                       ipb_stat_reg_o( 92) 		          <= errCnt_from_amc_berCnt    ( 3)(63 downto 32);
                                       ipb_stat_reg_o( 93) 		          <= bitsCnt_from_amc_berCnt   ( 3)(31 downto  0);
                                       ipb_stat_reg_o( 94) 		          <= bitsCnt_from_amc_berCnt   ( 3)(63 downto 32); 
                                       ipb_stat_reg_o( 95) 		          <= errCnt_from_amc_berCnt    ( 4)(31 downto  0);
                                       ipb_stat_reg_o( 96) 		          <= errCnt_from_amc_berCnt    ( 4)(63 downto 32);
                                       ipb_stat_reg_o( 97) 		          <= bitsCnt_from_amc_berCnt   ( 4)(31 downto  0);
                                       ipb_stat_reg_o( 98) 		          <= bitsCnt_from_amc_berCnt   ( 4)(63 downto 32);
                                       ipb_stat_reg_o( 99) 		          <= errCnt_from_amc_berCnt    ( 5)(31 downto  0);
                                       ipb_stat_reg_o(100) 		          <= errCnt_from_amc_berCnt    ( 5)(63 downto 32);
                                       ipb_stat_reg_o(101) 		          <= bitsCnt_from_amc_berCnt   ( 5)(31 downto  0);
                                       ipb_stat_reg_o(102) 		          <= bitsCnt_from_amc_berCnt   ( 5)(63 downto 32); 
                                       ipb_stat_reg_o(103) 		          <= errCnt_from_amc_berCnt    ( 6)(31 downto  0);
                                       ipb_stat_reg_o(104) 		          <= errCnt_from_amc_berCnt    ( 6)(63 downto 32);
                                       ipb_stat_reg_o(105) 		          <= bitsCnt_from_amc_berCnt   ( 6)(31 downto  0);
                                       ipb_stat_reg_o(106) 		          <= bitsCnt_from_amc_berCnt   ( 6)(63 downto 32);
                                       ipb_stat_reg_o(107) 		          <= errCnt_from_amc_berCnt    ( 7)(31 downto  0);
                                       ipb_stat_reg_o(108) 		          <= errCnt_from_amc_berCnt    ( 7)(63 downto 32);
                                       ipb_stat_reg_o(109) 		          <= bitsCnt_from_amc_berCnt   ( 7)(31 downto  0);
                                       ipb_stat_reg_o(110) 		          <= bitsCnt_from_amc_berCnt   ( 7)(63 downto 32);
                                       ipb_stat_reg_o(111) 		          <= errCnt_from_amc_berCnt    ( 8)(31 downto  0);
                                       ipb_stat_reg_o(112) 		          <= errCnt_from_amc_berCnt    ( 8)(63 downto 32);
                                       ipb_stat_reg_o(113) 		          <= bitsCnt_from_amc_berCnt   ( 8)(31 downto  0);
                                       ipb_stat_reg_o(114) 		          <= bitsCnt_from_amc_berCnt   ( 8)(63 downto 32); 
                                       ipb_stat_reg_o(115) 		          <= errCnt_from_amc_berCnt    ( 9)(31 downto  0);
                                       ipb_stat_reg_o(116) 		          <= errCnt_from_amc_berCnt    ( 9)(63 downto 32);
                                       ipb_stat_reg_o(117) 		          <= bitsCnt_from_amc_berCnt   ( 9)(31 downto  0);
                                       ipb_stat_reg_o(118) 		          <= bitsCnt_from_amc_berCnt   ( 9)(63 downto 32); 
                                       ipb_stat_reg_o(119) 		          <= errCnt_from_amc_berCnt    (10)(31 downto  0);
                                       ipb_stat_reg_o(120) 		          <= errCnt_from_amc_berCnt    (10)(63 downto 32);
                                       ipb_stat_reg_o(121) 		          <= bitsCnt_from_amc_berCnt   (10)(31 downto  0);
                                       ipb_stat_reg_o(122) 		          <= bitsCnt_from_amc_berCnt   (10)(63 downto 32); 
                                       ipb_stat_reg_o(123) 		          <= errCnt_from_amc_berCnt    (11)(31 downto  0);
                                       ipb_stat_reg_o(124) 		          <= errCnt_from_amc_berCnt    (11)(63 downto 32); 
                                       ipb_stat_reg_o(125) 		          <= bitsCnt_from_amc_berCnt   (11)(31 downto  0);
                                       ipb_stat_reg_o(126) 		          <= bitsCnt_from_amc_berCnt   (11)(63 downto 32);                            
                                                                                                                
   -- User I/O:                                                          
   ------------      
      
   -- FMC L8:     
                                       ipb_stat_reg_o(127)(16 downto  0) <= fmcL8_p_user_io_errors;
                                       ipb_stat_reg_o(127)(17)           <= fmcL8_p_user_io_correct;
                                       ipb_stat_reg_o(127)(31 downto 18) <= (others => '0');										
                                                                         
                                       ipb_stat_reg_o(128)(16 downto  0) <= fmcL8_n_user_io_errors;
                                       ipb_stat_reg_o(128)(17)           <= fmcL8_n_user_io_correct;
                                       ipb_stat_reg_o(128)(31 downto 18) <= (others => '0');
                                                                         
   -- FMC L12:   	                                                       
                                       ipb_stat_reg_o(129)(16 downto  0) <= fmcL12_p_user_io_errors;
                                       ipb_stat_reg_o(129)(17)           <= fmcL12_p_user_io_correct;
                                       ipb_stat_reg_o(129)(31 downto 18) <= (others => '0');										
                                                                         
                                       ipb_stat_reg_o(130)(16 downto  0) <= fmcL12_n_user_io_errors;
                                       ipb_stat_reg_o(130)(17)           <= fmcL12_n_user_io_correct;
                                       ipb_stat_reg_o(130)(31 downto 18) <= (others => '0');
      
   -- AMC:   	                     
                                       ipb_stat_reg_o(131)( 3 downto  0) <= amc_p_user_io_errors;
                                       ipb_stat_reg_o(131)( 4)           <= amc_p_user_io_correct;
                                       ipb_stat_reg_o(131)(31 downto  5) <= (others => '0');										
                                       
                                       ipb_stat_reg_o(132)( 3 downto  0) <= amc_n_user_io_errors;
                                       ipb_stat_reg_o(132)( 4)           <= amc_n_user_io_correct;
                                       ipb_stat_reg_o(132)(31 downto  5) <= (others => '0');

											  
   --===========================================--
	-- Control registers mapping
	--===========================================--
      
   -- High-Speed lanes:
   --------------------
   
   gtx_loopback				          <= ipb_ctrl_reg_i(0)(2 downto 0);
                                     
   fmcL8Bank112_gtx_qpll_powerDown   <= not ipb_ctrl_reg_i(1)( 0); 
   fmcL8Bank111_gtx_qpll_powerDown   <= not ipb_ctrl_reg_i(1)( 1); 
   --                                
   fmcL12Bank118_gtx_qpll_powerDown  <= not ipb_ctrl_reg_i(1)( 2);
   fmcL12Bank117_gtx_qpll_powerDown  <= not ipb_ctrl_reg_i(1)( 3);
   fmcL12Bank116_gtx_qpll_powerDown  <= not ipb_ctrl_reg_i(1)( 4);
   --
   amc_gtx_cpll_powerDown       (1)  <= not ipb_ctrl_reg_i(1)( 5);        
   amc_gtx_cpll_powerDown       (2)  <= not ipb_ctrl_reg_i(1)( 6);
   amc_gtx_cpll_powerDown       (3)  <= not ipb_ctrl_reg_i(1)( 7);
   amc_gtx_cpll_powerDown       (4)  <= not ipb_ctrl_reg_i(1)( 8); 
   amc_gtx_cpll_powerDown       (5)  <= not ipb_ctrl_reg_i(1)( 9);
   amc_gtx_cpll_powerDown       (6)  <= not ipb_ctrl_reg_i(1)(10); 
   amc_gtx_cpll_powerDown       (7)  <= not ipb_ctrl_reg_i(1)(11);
   amc_gtx_cpll_powerDown       (8)  <= not ipb_ctrl_reg_i(1)(12); 
   amc_gtx_cpll_powerDown       (9)  <= not ipb_ctrl_reg_i(1)(13);
   amc_gtx_cpll_powerDown       (10) <= not ipb_ctrl_reg_i(1)(14); 
   amc_gtx_cpll_powerDown       (11) <= not ipb_ctrl_reg_i(1)(15); 
                                
   fmc_l8_dp_gtx_tx_powerDown   ( 0) <= not ipb_ctrl_reg_i(2)( 0);
	fmc_l8_dp_gtx_rx_powerDown   ( 0) <= not ipb_ctrl_reg_i(2)( 1);
   fmc_l8_dp_gtx_tx_powerDown   ( 1) <= not ipb_ctrl_reg_i(2)( 2);
	fmc_l8_dp_gtx_rx_powerDown   ( 1) <= not ipb_ctrl_reg_i(2)( 3);     
   fmc_l8_dp_gtx_tx_powerDown   ( 2) <= not ipb_ctrl_reg_i(2)( 4);
	fmc_l8_dp_gtx_rx_powerDown   ( 2) <= not ipb_ctrl_reg_i(2)( 5);     
   fmc_l8_dp_gtx_tx_powerDown   ( 3) <= not ipb_ctrl_reg_i(2)( 6);
	fmc_l8_dp_gtx_rx_powerDown   ( 3) <= not ipb_ctrl_reg_i(2)( 7);     
   fmc_l8_dp_gtx_tx_powerDown   ( 4) <= not ipb_ctrl_reg_i(2)( 8);
	fmc_l8_dp_gtx_rx_powerDown   ( 4) <= not ipb_ctrl_reg_i(2)( 9);     
   fmc_l8_dp_gtx_tx_powerDown   ( 5) <= not ipb_ctrl_reg_i(2)(10);
	fmc_l8_dp_gtx_rx_powerDown   ( 5) <= not ipb_ctrl_reg_i(2)(11);     
   fmc_l8_dp_gtx_tx_powerDown   ( 6) <= not ipb_ctrl_reg_i(2)(12);
	fmc_l8_dp_gtx_rx_powerDown   ( 6) <= not ipb_ctrl_reg_i(2)(13);     
   fmc_l8_dp_gtx_tx_powerDown   ( 7) <= not ipb_ctrl_reg_i(2)(14);
	fmc_l8_dp_gtx_rx_powerDown   ( 7) <= not ipb_ctrl_reg_i(2)(15);     
   --                           
   fmc_l12_dp_gtx_tx_powerDown  ( 0) <= not ipb_ctrl_reg_i(3)( 0);
	fmc_l12_dp_gtx_rx_powerDown  ( 0) <= not ipb_ctrl_reg_i(3)( 1);
   fmc_l12_dp_gtx_tx_powerDown  ( 1) <= not ipb_ctrl_reg_i(3)( 2);
	fmc_l12_dp_gtx_rx_powerDown  ( 1) <= not ipb_ctrl_reg_i(3)( 3);     
   fmc_l12_dp_gtx_tx_powerDown  ( 2) <= not ipb_ctrl_reg_i(3)( 4);
	fmc_l12_dp_gtx_rx_powerDown  ( 2) <= not ipb_ctrl_reg_i(3)( 5);     
   fmc_l12_dp_gtx_tx_powerDown  ( 3) <= not ipb_ctrl_reg_i(3)( 6);
	fmc_l12_dp_gtx_rx_powerDown  ( 3) <= not ipb_ctrl_reg_i(3)( 7);     
   fmc_l12_dp_gtx_tx_powerDown  ( 4) <= not ipb_ctrl_reg_i(3)( 8);
	fmc_l12_dp_gtx_rx_powerDown  ( 4) <= not ipb_ctrl_reg_i(3)( 9);     
   fmc_l12_dp_gtx_tx_powerDown  ( 5) <= not ipb_ctrl_reg_i(3)(10);
	fmc_l12_dp_gtx_rx_powerDown  ( 5) <= not ipb_ctrl_reg_i(3)(11);     
   fmc_l12_dp_gtx_tx_powerDown  ( 6) <= not ipb_ctrl_reg_i(3)(12);
	fmc_l12_dp_gtx_rx_powerDown  ( 6) <= not ipb_ctrl_reg_i(3)(13);     
   fmc_l12_dp_gtx_tx_powerDown  ( 7) <= not ipb_ctrl_reg_i(3)(14);
	fmc_l12_dp_gtx_rx_powerDown  ( 7) <= not ipb_ctrl_reg_i(3)(15);       
   fmc_l12_dp_gtx_tx_powerDown  ( 8) <= not ipb_ctrl_reg_i(3)(16);        
	fmc_l12_dp_gtx_rx_powerDown  ( 8) <= not ipb_ctrl_reg_i(3)(17);      
   fmc_l12_dp_gtx_tx_powerDown  ( 9) <= not ipb_ctrl_reg_i(3)(18);     
	fmc_l12_dp_gtx_rx_powerDown  ( 9) <= not ipb_ctrl_reg_i(3)(19);      
   fmc_l12_dp_gtx_tx_powerDown  (10) <= not ipb_ctrl_reg_i(3)(20);     
	fmc_l12_dp_gtx_rx_powerDown  (10) <= not ipb_ctrl_reg_i(3)(21);
   fmc_l12_dp_gtx_tx_powerDown  (11) <= not ipb_ctrl_reg_i(3)(22);     
	fmc_l12_dp_gtx_rx_powerDown  (11) <= not ipb_ctrl_reg_i(3)(23);
   --	                          
   amc_gtx_tx_powerDown         ( 1) <= not ipb_ctrl_reg_i(4)( 0);
	amc_gtx_rx_powerDown         ( 1) <= not ipb_ctrl_reg_i(4)( 1);
   amc_gtx_tx_powerDown         ( 2) <= not ipb_ctrl_reg_i(4)( 2);
	amc_gtx_rx_powerDown         ( 2) <= not ipb_ctrl_reg_i(4)( 3);     
   amc_gtx_tx_powerDown         ( 3) <= not ipb_ctrl_reg_i(4)( 4);
	amc_gtx_rx_powerDown         ( 3) <= not ipb_ctrl_reg_i(4)( 5);     
   amc_gtx_tx_powerDown         ( 4) <= not ipb_ctrl_reg_i(4)( 6);
	amc_gtx_rx_powerDown         ( 4) <= not ipb_ctrl_reg_i(4)( 7);     
   amc_gtx_tx_powerDown         ( 5) <= not ipb_ctrl_reg_i(4)( 8);
	amc_gtx_rx_powerDown         ( 5) <= not ipb_ctrl_reg_i(4)( 9);     
   amc_gtx_tx_powerDown         ( 6) <= not ipb_ctrl_reg_i(4)(10);
	amc_gtx_rx_powerDown         ( 6) <= not ipb_ctrl_reg_i(4)(11);     
   amc_gtx_tx_powerDown         ( 7) <= not ipb_ctrl_reg_i(4)(12);
	amc_gtx_rx_powerDown         ( 7) <= not ipb_ctrl_reg_i(4)(13);     
   amc_gtx_tx_powerDown         ( 8) <= not ipb_ctrl_reg_i(4)(14);
	amc_gtx_rx_powerDown         ( 8) <= not ipb_ctrl_reg_i(4)(15);       
   amc_gtx_tx_powerDown         ( 9) <= not ipb_ctrl_reg_i(4)(16);     
	amc_gtx_rx_powerDown         ( 9) <= not ipb_ctrl_reg_i(4)(17);      
   amc_gtx_tx_powerDown         (10) <= not ipb_ctrl_reg_i(4)(18);     
	amc_gtx_rx_powerDown         (10) <= not ipb_ctrl_reg_i(4)(19);
   amc_gtx_tx_powerDown         (11) <= not ipb_ctrl_reg_i(4)(20);     
	amc_gtx_rx_powerDown         (11) <= not ipb_ctrl_reg_i(4)(21);        
                                
   fmc_l8_dp_gtx_tx_reset       ( 0) <= not ipb_ctrl_reg_i(5)( 0);
	fmc_l8_dp_gtx_rx_reset       ( 0) <= not ipb_ctrl_reg_i(5)( 1);
   fmc_l8_dp_gtx_tx_reset       ( 1) <= not ipb_ctrl_reg_i(5)( 2);
	fmc_l8_dp_gtx_rx_reset       ( 1) <= not ipb_ctrl_reg_i(5)( 3);     
   fmc_l8_dp_gtx_tx_reset       ( 2) <= not ipb_ctrl_reg_i(5)( 4);
	fmc_l8_dp_gtx_rx_reset       ( 2) <= not ipb_ctrl_reg_i(5)( 5);     
   fmc_l8_dp_gtx_tx_reset       ( 3) <= not ipb_ctrl_reg_i(5)( 6);
	fmc_l8_dp_gtx_rx_reset       ( 3) <= not ipb_ctrl_reg_i(5)( 7);     
   fmc_l8_dp_gtx_tx_reset       ( 4) <= not ipb_ctrl_reg_i(5)( 8);
	fmc_l8_dp_gtx_rx_reset       ( 4) <= not ipb_ctrl_reg_i(5)( 9);     
   fmc_l8_dp_gtx_tx_reset       ( 5) <= not ipb_ctrl_reg_i(5)(10);
	fmc_l8_dp_gtx_rx_reset       ( 5) <= not ipb_ctrl_reg_i(5)(11);     
   fmc_l8_dp_gtx_tx_reset       ( 6) <= not ipb_ctrl_reg_i(5)(12);
	fmc_l8_dp_gtx_rx_reset       ( 6) <= not ipb_ctrl_reg_i(5)(13);     
   fmc_l8_dp_gtx_tx_reset       ( 7) <= not ipb_ctrl_reg_i(5)(14);
	fmc_l8_dp_gtx_rx_reset       ( 7) <= not ipb_ctrl_reg_i(5)(15);     
   --                           
   fmc_l12_dp_gtx_tx_reset      ( 0) <= not ipb_ctrl_reg_i(6)( 0);
	fmc_l12_dp_gtx_rx_reset      ( 0) <= not ipb_ctrl_reg_i(6)( 1);
   fmc_l12_dp_gtx_tx_reset      ( 1) <= not ipb_ctrl_reg_i(6)( 2);
	fmc_l12_dp_gtx_rx_reset      ( 1) <= not ipb_ctrl_reg_i(6)( 3);     
   fmc_l12_dp_gtx_tx_reset      ( 2) <= not ipb_ctrl_reg_i(6)( 4);
	fmc_l12_dp_gtx_rx_reset      ( 2) <= not ipb_ctrl_reg_i(6)( 5);     
   fmc_l12_dp_gtx_tx_reset      ( 3) <= not ipb_ctrl_reg_i(6)( 6);
	fmc_l12_dp_gtx_rx_reset      ( 3) <= not ipb_ctrl_reg_i(6)( 7);     
   fmc_l12_dp_gtx_tx_reset      ( 4) <= not ipb_ctrl_reg_i(6)( 8);
	fmc_l12_dp_gtx_rx_reset      ( 4) <= not ipb_ctrl_reg_i(6)( 9);     
   fmc_l12_dp_gtx_tx_reset      ( 5) <= not ipb_ctrl_reg_i(6)(10);
	fmc_l12_dp_gtx_rx_reset      ( 5) <= not ipb_ctrl_reg_i(6)(11);     
   fmc_l12_dp_gtx_tx_reset      ( 6) <= not ipb_ctrl_reg_i(6)(12);
	fmc_l12_dp_gtx_rx_reset      ( 6) <= not ipb_ctrl_reg_i(6)(13);     
   fmc_l12_dp_gtx_tx_reset      ( 7) <= not ipb_ctrl_reg_i(6)(14);
	fmc_l12_dp_gtx_rx_reset      ( 7) <= not ipb_ctrl_reg_i(6)(15);       
   fmc_l12_dp_gtx_tx_reset      ( 8) <= not ipb_ctrl_reg_i(6)(16);        
	fmc_l12_dp_gtx_rx_reset      ( 8) <= not ipb_ctrl_reg_i(6)(17);      
   fmc_l12_dp_gtx_tx_reset      ( 9) <= not ipb_ctrl_reg_i(6)(18);     
	fmc_l12_dp_gtx_rx_reset      ( 9) <= not ipb_ctrl_reg_i(6)(19);      
   fmc_l12_dp_gtx_tx_reset      (10) <= not ipb_ctrl_reg_i(6)(20);     
	fmc_l12_dp_gtx_rx_reset      (10) <= not ipb_ctrl_reg_i(6)(21);
   fmc_l12_dp_gtx_tx_reset      (11) <= not ipb_ctrl_reg_i(6)(22);     
	fmc_l12_dp_gtx_rx_reset      (11) <= not ipb_ctrl_reg_i(6)(23);
   --	                          
   amc_gtx_tx_reset             ( 1) <= not ipb_ctrl_reg_i(7)( 0);
	amc_gtx_rx_reset             ( 1) <= not ipb_ctrl_reg_i(7)( 1);
   amc_gtx_tx_reset             ( 2) <= not ipb_ctrl_reg_i(7)( 2);
	amc_gtx_rx_reset             ( 2) <= not ipb_ctrl_reg_i(7)( 3);     
   amc_gtx_tx_reset             ( 3) <= not ipb_ctrl_reg_i(7)( 4);
	amc_gtx_rx_reset             ( 3) <= not ipb_ctrl_reg_i(7)( 5);     
   amc_gtx_tx_reset             ( 4) <= not ipb_ctrl_reg_i(7)( 6);
	amc_gtx_rx_reset             ( 4) <= not ipb_ctrl_reg_i(7)( 7);     
   amc_gtx_tx_reset             ( 5) <= not ipb_ctrl_reg_i(7)( 8);
	amc_gtx_rx_reset             ( 5) <= not ipb_ctrl_reg_i(7)( 9);     
   amc_gtx_tx_reset             ( 6) <= not ipb_ctrl_reg_i(7)(10);
	amc_gtx_rx_reset             ( 6) <= not ipb_ctrl_reg_i(7)(11);     
   amc_gtx_tx_reset             ( 7) <= not ipb_ctrl_reg_i(7)(12);
	amc_gtx_rx_reset             ( 7) <= not ipb_ctrl_reg_i(7)(13);     
   amc_gtx_tx_reset             ( 8) <= not ipb_ctrl_reg_i(7)(14);
	amc_gtx_rx_reset             ( 8) <= not ipb_ctrl_reg_i(7)(15);       
   amc_gtx_tx_reset             ( 9) <= not ipb_ctrl_reg_i(7)(16);     
	amc_gtx_rx_reset             ( 9) <= not ipb_ctrl_reg_i(7)(17);      
   amc_gtx_tx_reset             (10) <= not ipb_ctrl_reg_i(7)(18);     
	amc_gtx_rx_reset             (10) <= not ipb_ctrl_reg_i(7)(19);
   amc_gtx_tx_reset             (11) <= not ipb_ctrl_reg_i(7)(20);     
	amc_gtx_rx_reset             (11) <= not ipb_ctrl_reg_i(7)(21);  
   
   gtx_bert_trigger			          <= ipb_ctrl_reg_i    (8)( 0);
	gtx_bert_reset				          <= ipb_ctrl_reg_i    (8)( 1);
			
   -- User I/O:  		
   ------------  		
			
   io_test_trigger			          <= ipb_ctrl_reg_i    (9)( 0);
	io_test_reset		   	          <= ipb_ctrl_reg_i    (9)( 1);	
   
   --===========================================--
   -- MGT reference clocks scheme
   --===========================================--  

--   -- osc125_b:
--   ----------------
--   
--   osc125_b_mgtRefClk: ibufds_gte2
--      port map (
--         I                                  => osc125_b_p,
--         IB                                 => osc125_b_n,
--         O                                  => osc125_b,     -- 125MHz
--         CEB                                => '0'
--      );
--	
--	osc125_b_mgtRefClkBufg: bufg
--      port map (
--         I                                  => osc125_b,
--         O                                  => osc125_b_bufg
--      );



	osc125_b_bufg <= osc125_b_bufg_i;
	osc125_b      <= osc125_b_mgtrefclk_i;







   
   -- osc_xpoint_a:
   ----------------
   
   osc_xpoint_a_mgtRefClk: ibufds_gte2
      port map (
         I                                  => osc_xpoint_a_p,
         IB                                 => osc_xpoint_a_n,
         O                                  => osc_xpoint_a,     -- 125MHz
         CEB                                => '0'
      );
	
	osc_xpoint_a_mgtRefClkBufg: bufg
      port map (
         I                                  => osc_xpoint_a,
         O                                  => osc_xpoint_a_bufg
      );
   
   -- osc_xpoint_b:
   ----------------
   
   osc_xpoint_b_mgtRefClk: ibufds_gte2
      port map (
         I                                  => osc_xpoint_b_p,
         IB                                 => osc_xpoint_b_n,
         O                                  => osc_xpoint_b,     -- 125MHz
         CEB                                => '0'
      );
	
	osc_xpoint_b_mgtRefClkBufg: bufg
      port map (
         I                                  => osc_xpoint_b,
         O                                  => osc_xpoint_b_bufg
      );
   
   -- osc_xpoint_c:
   ----------------
   
   osc_xpoint_c_mgtRefClk: ibufds_gte2
      port map (
         I                                  => osc_xpoint_c_p,
         IB                                 => osc_xpoint_c_n,
         O                                  => osc_xpoint_c,     -- 125MHz
         CEB                                => '0'
      );
	
	osc_xpoint_c_mgtRefClkBufg: bufg
      port map (
         I                                  => osc_xpoint_c,
         O                                  => osc_xpoint_c_bufg
      );   
   
   -- osc_xpoint_d:
   ----------------
   
   osc_xpoint_d_mgtRefClk: ibufds_gte2
      port map (
         I                                  => osc_xpoint_d_p,
         IB                                 => osc_xpoint_d_n,
         O                                  => osc_xpoint_d,     -- 125MHz
         CEB                                => '0'
      );
	
	osc_xpoint_d_mgtRefClkBufg: bufg
      port map (
         I                                  => osc_xpoint_d,
         O                                  => osc_xpoint_d_bufg
      );
   
   --===========================================--
   -- High-Speed test
   --===========================================-- 
      
   -- FMC L8:
   ----------
   
   -- Bank 112 (osc_xpoint_a -> 125MHz):   -- Note!! GTX mapping: gtx_0 -> fmc_l8_dp_3
                                           --                     gtx_1 -> fmc_l8_dp_2
                                           --                     gtx_2 -> fmc_l8_dp_1
                                           --                     gtx_3 -> fmc_l8_dp_0
                                           
   fmcL8Bank112_txOutClkBufg: bufg
      port map (
         I                                  => txOutClk_from_fmcL8_dp(3),
         O                                  => txOutClk_from_fmcL8_dp_buff(3)
      );
   
   fmcL8Bank112_gtx: entity work.gtx_10gbps_125mhz_x4_init_mbm
      generic map (
         EXAMPLE_SIM_GTRESET_SPEEDUP        => "TRUE",     
         EXAMPLE_SIMULATION                 => 0,          
         EXAMPLE_USE_CHIPSCOPE              => 0)
      port map (
         SYSCLK_IN                          => osc_xpoint_a_bufg,                         
         SOFT_RESET_IN                      => ipb_rst_i,              
         DONT_RESET_ON_DATA_ERROR_IN        => '1',             
         GT0_TX_FSM_RESET_DONE_OUT          => txReady_from_fmcL8_dp(3),             
         GT0_RX_FSM_RESET_DONE_OUT          => rxReady_from_fmcL8_dp(3),             
         GT0_DATA_VALID_IN                  => '1',                 
         GT1_TX_FSM_RESET_DONE_OUT          => txReady_from_fmcL8_dp(2),                
         GT1_RX_FSM_RESET_DONE_OUT          => rxReady_from_fmcL8_dp(2),                
         GT1_DATA_VALID_IN                  => '1',                             
         GT2_TX_FSM_RESET_DONE_OUT          => txReady_from_fmcL8_dp(1),                
         GT2_RX_FSM_RESET_DONE_OUT          => rxReady_from_fmcL8_dp(1),                
         GT2_DATA_VALID_IN                  => '1',                             
         GT3_TX_FSM_RESET_DONE_OUT          => txReady_from_fmcL8_dp(0),                
         GT3_RX_FSM_RESET_DONE_OUT          => rxReady_from_fmcL8_dp(0),                
         GT3_DATA_VALID_IN                  => '1',                             
         -----------------------------------
         GT0_DRPADDR_IN                     => (others => '0'),                     
         GT0_DRPCLK_IN                      => '0',             
         GT0_DRPDI_IN                       => (others => '0'),               
         GT0_DRPDO_OUT                      => open,             
         GT0_DRPEN_IN                       => '0',              
         GT0_DRPRDY_OUT                     => open,             
         GT0_DRPWE_IN                       => '0',              
         GT0_LOOPBACK_IN                    => gtx_loopback,
         GT0_RXPD_IN                        => (fmc_l8_dp_gtx_rx_powerDown(3),fmc_l8_dp_gtx_rx_powerDown(3)),
         GT0_TXPD_IN                        => (fmc_l8_dp_gtx_tx_powerDown(3),fmc_l8_dp_gtx_tx_powerDown(3)),               
         GT0_RXUSERRDY_IN                   => '1',             
         GT0_EYESCANDATAERROR_OUT           => open,               
         GT0_RXCDRLOCK_OUT                  => open,               
         GT0_RXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(3),                   
         GT0_RXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(3),                       
         GT0_RXDATA_OUT                     => open,                   
         GT0_RXPRBSERR_OUT                  => rxPrbsErr_from_fmcL8_dp(3),                 
         GT0_RXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 137)                   
         GT0_RXPRBSCNTRESET_IN              => '0',                 
         GT0_GTXRXP_IN                      => fmc_l8_dp_m2c_n(3),   -- Note!! Inverted                 
         GT0_GTXRXN_IN                      => fmc_l8_dp_m2c_p(3),   --               
         GT0_GTRXRESET_IN                   => fmc_l8_dp_gtx_rx_reset(3),                 
         GT0_RXPMARESET_IN                  => '0',                 
         GT0_RXPOLARITY_IN                  => '1',                 
         GT0_RXRESETDONE_OUT                => open,                   
         GT0_GTTXRESET_IN                   => fmc_l8_dp_gtx_tx_reset(3),                 
         GT0_TXUSERRDY_IN                   => '1',              
         GT0_TXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(3),                    
         GT0_TXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(3),              
         GT0_TXPRBSFORCEERR_IN              => '0',              
         GT0_TXDIFFCTRL_IN                  => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)              
         GT0_TXDATA_IN                      => (others => '0'),                 
         GT0_GTXTXN_OUT                     => fmc_l8_dp_c2m_p(3),   -- Note!! Inverted             
         GT0_GTXTXP_OUT                     => fmc_l8_dp_c2m_n(3),   --                            
         GT0_TXOUTCLK_OUT                   => txOutClk_from_fmcL8_dp(3),              
         GT0_TXOUTCLKFABRIC_OUT             => open,                
         GT0_TXOUTCLKPCS_OUT                => open,                
         GT0_TXRESETDONE_OUT                => open,                
         GT0_TXPOLARITY_IN                  => '1',              
         GT0_TXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 213)                     
         -----------------------------------
         GT1_DRPADDR_IN                     => (others => '0'),                     
         GT1_DRPCLK_IN                      => '0',             
         GT1_DRPDI_IN                       => (others => '0'),               
         GT1_DRPDO_OUT                      => open,             
         GT1_DRPEN_IN                       => '0',              
         GT1_DRPRDY_OUT                     => open,             
         GT1_DRPWE_IN                       => '0',              
         GT1_LOOPBACK_IN                    => gtx_loopback,
         GT1_RXPD_IN                        => (fmc_l8_dp_gtx_rx_powerDown(2),fmc_l8_dp_gtx_rx_powerDown(2)),
         GT1_TXPD_IN                        => (fmc_l8_dp_gtx_tx_powerDown(2),fmc_l8_dp_gtx_tx_powerDown(2)),          
         GT1_RXUSERRDY_IN                   => '1',             
         GT1_EYESCANDATAERROR_OUT           => open,               
         GT1_RXCDRLOCK_OUT                  => open,               
         GT1_RXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(3),             
         GT1_RXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(3),             
         GT1_RXDATA_OUT                     => open,                   
         GT1_RXPRBSERR_OUT                  => rxPrbsErr_from_fmcL8_dp(2),               
         GT1_RXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT1_RXPRBSCNTRESET_IN              => '0',                 
         GT1_GTXRXP_IN                      => fmc_l8_dp_m2c_n(2),   -- Note!! Inverted                 
         GT1_GTXRXN_IN                      => fmc_l8_dp_m2c_p(2),   --                                
         GT1_GTRXRESET_IN                   => fmc_l8_dp_gtx_rx_reset(2),                 
         GT1_RXPMARESET_IN                  => '0',                 
         GT1_RXPOLARITY_IN                  => '1',                 
         GT1_RXRESETDONE_OUT                => open,                   
         GT1_GTTXRESET_IN                   => fmc_l8_dp_gtx_tx_reset(2),                 
         GT1_TXUSERRDY_IN                   => '1',              
         GT1_TXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(3),             
         GT1_TXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(3),             
         GT1_TXPRBSFORCEERR_IN              => '0',              
         GT1_TXDIFFCTRL_IN                  => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT1_TXDATA_IN                      => (others => '0'),                 
         GT1_GTXTXN_OUT                     => fmc_l8_dp_c2m_p(2),   -- Note!! Inverted             
         GT1_GTXTXP_OUT                     => fmc_l8_dp_c2m_n(2),   --                            
         GT1_TXOUTCLK_OUT                   => open,              
         GT1_TXOUTCLKFABRIC_OUT             => open,                
         GT1_TXOUTCLKPCS_OUT                => open,                
         GT1_TXRESETDONE_OUT                => open,                
         GT1_TXPOLARITY_IN                  => '1',              
         GT1_TXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         -----------------------------------
         GT2_DRPADDR_IN                     => (others => '0'),                     
         GT2_DRPCLK_IN                      => '0',             
         GT2_DRPDI_IN                       => (others => '0'),               
         GT2_DRPDO_OUT                      => open,             
         GT2_DRPEN_IN                       => '0',              
         GT2_DRPRDY_OUT                     => open,             
         GT2_DRPWE_IN                       => '0',              
         GT2_LOOPBACK_IN                    => gtx_loopback,
         GT2_RXPD_IN                        => (fmc_l8_dp_gtx_rx_powerDown(1),fmc_l8_dp_gtx_rx_powerDown(1)),
         GT2_TXPD_IN                        => (fmc_l8_dp_gtx_tx_powerDown(1),fmc_l8_dp_gtx_tx_powerDown(1)),          
         GT2_RXUSERRDY_IN                   => '1',             
         GT2_EYESCANDATAERROR_OUT           => open,               
         GT2_RXCDRLOCK_OUT                  => open,               
         GT2_RXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(3),             
         GT2_RXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(3),             
         GT2_RXDATA_OUT                     => open,                   
         GT2_RXPRBSERR_OUT                  => rxPrbsErr_from_fmcL8_dp(1),               
         GT2_RXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT2_RXPRBSCNTRESET_IN              => '0',                 
         GT2_GTXRXP_IN                      => fmc_l8_dp_m2c_n(1),   -- Note!! Inverted                 
         GT2_GTXRXN_IN                      => fmc_l8_dp_m2c_p(1),   --                                
         GT2_GTRXRESET_IN                   => fmc_l8_dp_gtx_rx_reset(1),                 
         GT2_RXPMARESET_IN                  => '0',                 
         GT2_RXPOLARITY_IN                  => '1',                 
         GT2_RXRESETDONE_OUT                => open,                   
         GT2_GTTXRESET_IN                   => fmc_l8_dp_gtx_tx_reset(1),                 
         GT2_TXUSERRDY_IN                   => '1',              
         GT2_TXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(3),             
         GT2_TXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(3),             
         GT2_TXPRBSFORCEERR_IN              => '0',              
         GT2_TXDIFFCTRL_IN                  => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT2_TXDATA_IN                      => (others => '0'),                 
         GT2_GTXTXN_OUT                     => fmc_l8_dp_c2m_p(1),   -- Note!! Inverted             
         GT2_GTXTXP_OUT                     => fmc_l8_dp_c2m_n(1),   --                            
         GT2_TXOUTCLK_OUT                   => open,              
         GT2_TXOUTCLKFABRIC_OUT             => open,                
         GT2_TXOUTCLKPCS_OUT                => open,                
         GT2_TXRESETDONE_OUT                => open,                
         GT2_TXPOLARITY_IN                  => '1',              
         GT2_TXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         -----------------------------------
         GT3_DRPADDR_IN                     => (others => '0'),                     
         GT3_DRPCLK_IN                      => '0',             
         GT3_DRPDI_IN                       => (others => '0'),               
         GT3_DRPDO_OUT                      => open,             
         GT3_DRPEN_IN                       => '0',              
         GT3_DRPRDY_OUT                     => open,             
         GT3_DRPWE_IN                       => '0',              
         GT3_LOOPBACK_IN                    => gtx_loopback,
         GT3_RXPD_IN                        => (fmc_l8_dp_gtx_rx_powerDown(0),fmc_l8_dp_gtx_rx_powerDown(0)),
         GT3_TXPD_IN                        => (fmc_l8_dp_gtx_tx_powerDown(0),fmc_l8_dp_gtx_tx_powerDown(0)),          
         GT3_RXUSERRDY_IN                   => '1',             
         GT3_EYESCANDATAERROR_OUT           => open,               
         GT3_RXCDRLOCK_OUT                  => open,               
         GT3_RXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(3),             
         GT3_RXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(3),             
         GT3_RXDATA_OUT                     => open,                   
         GT3_RXPRBSERR_OUT                  => rxPrbsErr_from_fmcL8_dp(0),               
         GT3_RXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT3_RXPRBSCNTRESET_IN              => '0',                 
         GT3_GTXRXP_IN                      => fmc_l8_dp_m2c_n(0),   -- Note!! Inverted                 
         GT3_GTXRXN_IN                      => fmc_l8_dp_m2c_p(0),   --                                
         GT3_GTRXRESET_IN                   => fmc_l8_dp_gtx_rx_reset(0),                 
         GT3_RXPMARESET_IN                  => '0',                 
         GT3_RXPOLARITY_IN                  => '1',                 
         GT3_RXRESETDONE_OUT                => open,                   
         GT3_GTTXRESET_IN                   => fmc_l8_dp_gtx_tx_reset(0),                 
         GT3_TXUSERRDY_IN                   => '1',              
         GT3_TXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(3),             
         GT3_TXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(3),             
         GT3_TXPRBSFORCEERR_IN              => '0',              
         GT3_TXDIFFCTRL_IN                  => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT3_TXDATA_IN                      => (others => '0'),                 
         GT3_GTXTXN_OUT                     => fmc_l8_dp_c2m_p(0),   -- Note!! Inverted             
         GT3_GTXTXP_OUT                     => fmc_l8_dp_c2m_n(0),   --                            
         GT3_TXOUTCLK_OUT                   => open,              
         GT3_TXOUTCLKFABRIC_OUT             => open,                
         GT3_TXOUTCLKPCS_OUT                => open,                
         GT3_TXRESETDONE_OUT                => open,                
         GT3_TXPOLARITY_IN                  => '1',              
         GT3_TXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         -----------------------------------
         GT0_GTREFCLK0_COMMON_IN            => osc_xpoint_a,              
         GT0_QPLLLOCK_OUT                   => open,              
         GT0_QPLLLOCKDETCLK_IN              => osc_xpoint_a_bufg,              
         GT0_QPLLPD_IN                      => fmcL8Bank112_gtx_qpll_powerDown,
         GT0_QPLLRESET_IN                   => '0'              
      );                               
                                       
   fmcL8Bank112_ber_gen: for i in 0 to 3 generate
   
      fmcL8Bank112_berCnt: entity work.gtx_ber_cntr
         port map (
            RESET_I 								  => (not rxReady_from_fmcL8_dp(BANK112(i))) or gtx_bert_reset,
            CLK_I 								  => txOutClk_from_fmcL8_dp_buff(3),
            TRIGGER_I 							  => gtx_bert_trigger,
            ERROR_FLAG_I	 					  => rxPrbsErr_from_fmcL8_dp(BANK112(i)),
            ERROR_CNT_O 						  => errCnt_from_fmcL8_berCnt(BANK112(i)),
            BITS_CNT_O 							  => bitsCnt_from_fmcL8_berCnt(BANK112(i))	
         );		
         
      fmcL8Bank112_cdc: entity work.clk_domain_bridge			
         port map (
            WRCLK_I 								  => txOutClk_from_fmcL8_dp_buff(3),
            RDCLK_I 								  => ipb_clk_i,
            WDATA_I( 63 downto  0)			  => errCnt_from_fmcL8_berCnt(BANK112(i)),
            WDATA_I(127 downto 64)			  => bitsCnt_from_fmcL8_berCnt(BANK112(i)),
            RDATA_O( 63 downto  0) 			  => errCnt_from_fmcL8_cdc(BANK112(i)),
            RDATA_O(127 downto 64) 			  => bitsCnt_from_fmcL8_cdc(BANK112(i))					
         );
   
   end generate;
   
   -- Bank 111 (osc_xpoint_a -> 125MHz):   -- Note!! GTX mapping: gtx_0 -> fmc_l8_dp_5
                                           --                     gtx_1 -> fmc_l8_dp_6
                                           --                     gtx_2 -> fmc_l8_dp_7
                                           --                     gtx_3 -> fmc_l8_dp_4
                                           
   fmcL8Bank111_txOutClkBufg: bufg
      port map (                            
         I                                  => txOutClk_from_fmcL8_dp(5),
         O                                  => txOutClk_from_fmcL8_dp_buff(5)
      );
   
   fmcL8Bank111_gtx: entity work.gtx_10gbps_125mhz_x4_init_mbm
      generic map (
         EXAMPLE_SIM_GTRESET_SPEEDUP        => "TRUE",     
         EXAMPLE_SIMULATION                 => 0,          
         EXAMPLE_USE_CHIPSCOPE              => 0)
      port map (
         SYSCLK_IN                          => osc_xpoint_a_bufg,                         
         SOFT_RESET_IN                      => ipb_rst_i,              
         DONT_RESET_ON_DATA_ERROR_IN        => '1',             
         GT0_TX_FSM_RESET_DONE_OUT          => txReady_from_fmcL8_dp(5),             
         GT0_RX_FSM_RESET_DONE_OUT          => rxReady_from_fmcL8_dp(5),             
         GT0_DATA_VALID_IN                  => '1',                 
         GT1_TX_FSM_RESET_DONE_OUT          => txReady_from_fmcL8_dp(6),                
         GT1_RX_FSM_RESET_DONE_OUT          => rxReady_from_fmcL8_dp(6),                
         GT1_DATA_VALID_IN                  => '1',                             
         GT2_TX_FSM_RESET_DONE_OUT          => txReady_from_fmcL8_dp(7),                
         GT2_RX_FSM_RESET_DONE_OUT          => rxReady_from_fmcL8_dp(7),                
         GT2_DATA_VALID_IN                  => '1',                             
         GT3_TX_FSM_RESET_DONE_OUT          => txReady_from_fmcL8_dp(4),                
         GT3_RX_FSM_RESET_DONE_OUT          => rxReady_from_fmcL8_dp(4),                
         GT3_DATA_VALID_IN                  => '1',                             
         -----------------------------------
         GT0_DRPADDR_IN                     => (others => '0'),                     
         GT0_DRPCLK_IN                      => '0',             
         GT0_DRPDI_IN                       => (others => '0'),               
         GT0_DRPDO_OUT                      => open,             
         GT0_DRPEN_IN                       => '0',              
         GT0_DRPRDY_OUT                     => open,             
         GT0_DRPWE_IN                       => '0',              
         GT0_LOOPBACK_IN                    => gtx_loopback,
         GT0_RXPD_IN                        => (fmc_l8_dp_gtx_rx_powerDown(5),fmc_l8_dp_gtx_rx_powerDown(5)),
         GT0_TXPD_IN                        => (fmc_l8_dp_gtx_tx_powerDown(5),fmc_l8_dp_gtx_tx_powerDown(5)),          
         GT0_RXUSERRDY_IN                   => '1',             
         GT0_EYESCANDATAERROR_OUT           => open,               
         GT0_RXCDRLOCK_OUT                  => open,               
         GT0_RXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(5),                   
         GT0_RXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(5),                       
         GT0_RXDATA_OUT                     => open,                   
         GT0_RXPRBSERR_OUT                  => rxPrbsErr_from_fmcL8_dp(5),                 
         GT0_RXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 137)                   
         GT0_RXPRBSCNTRESET_IN              => '0',                 
         GT0_GTXRXP_IN                      => fmc_l8_dp_m2c_n(5),   -- Note!! Inverted                 
         GT0_GTXRXN_IN                      => fmc_l8_dp_m2c_p(5),   --                                
         GT0_GTRXRESET_IN                   => fmc_l8_dp_gtx_rx_reset(5),                 
         GT0_RXPMARESET_IN                  => '0',                 
         GT0_RXPOLARITY_IN                  => '1',                 
         GT0_RXRESETDONE_OUT                => open,                   
         GT0_GTTXRESET_IN                   => fmc_l8_dp_gtx_tx_reset(5),                 
         GT0_TXUSERRDY_IN                   => '1',              
         GT0_TXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(5),                    
         GT0_TXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(5),              
         GT0_TXPRBSFORCEERR_IN              => '0',              
         GT0_TXDIFFCTRL_IN                  => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)              
         GT0_TXDATA_IN                      => (others => '0'),                 
         GT0_GTXTXN_OUT                     => fmc_l8_dp_c2m_p(5),   -- Note!! Inverted             
         GT0_GTXTXP_OUT                     => fmc_l8_dp_c2m_n(5),   --                            
         GT0_TXOUTCLK_OUT                   => txOutClk_from_fmcL8_dp(5),              
         GT0_TXOUTCLKFABRIC_OUT             => open,                
         GT0_TXOUTCLKPCS_OUT                => open,                
         GT0_TXRESETDONE_OUT                => open,                
         GT0_TXPOLARITY_IN                  => '1',              
         GT0_TXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 213)                     
         -----------------------------------
         GT1_DRPADDR_IN                     => (others => '0'),                     
         GT1_DRPCLK_IN                      => '0',             
         GT1_DRPDI_IN                       => (others => '0'),               
         GT1_DRPDO_OUT                      => open,             
         GT1_DRPEN_IN                       => '0',              
         GT1_DRPRDY_OUT                     => open,             
         GT1_DRPWE_IN                       => '0',              
         GT1_LOOPBACK_IN                    => gtx_loopback,
         GT1_RXPD_IN                        => (fmc_l8_dp_gtx_rx_powerDown(6),fmc_l8_dp_gtx_rx_powerDown(6)),
         GT1_TXPD_IN                        => (fmc_l8_dp_gtx_tx_powerDown(6),fmc_l8_dp_gtx_tx_powerDown(6)),          
         GT1_RXUSERRDY_IN                   => '1',             
         GT1_EYESCANDATAERROR_OUT           => open,               
         GT1_RXCDRLOCK_OUT                  => open,               
         GT1_RXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(5),             
         GT1_RXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(5),             
         GT1_RXDATA_OUT                     => open,                   
         GT1_RXPRBSERR_OUT                  => rxPrbsErr_from_fmcL8_dp(6),               
         GT1_RXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT1_RXPRBSCNTRESET_IN              => '0',                 
         GT1_GTXRXP_IN                      => fmc_l8_dp_m2c_n(6),   -- Note!! Inverted                 
         GT1_GTXRXN_IN                      => fmc_l8_dp_m2c_p(6),   --                                
         GT1_GTRXRESET_IN                   => fmc_l8_dp_gtx_rx_reset(6),                 
         GT1_RXPMARESET_IN                  => '0',                 
         GT1_RXPOLARITY_IN                  => '1',                 
         GT1_RXRESETDONE_OUT                => open,                   
         GT1_GTTXRESET_IN                   => fmc_l8_dp_gtx_tx_reset(6),                 
         GT1_TXUSERRDY_IN                   => '1',              
         GT1_TXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(5),             
         GT1_TXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(5),             
         GT1_TXPRBSFORCEERR_IN              => '0',              
         GT1_TXDIFFCTRL_IN                  => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT1_TXDATA_IN                      => (others => '0'),                 
         GT1_GTXTXN_OUT                     => fmc_l8_dp_c2m_p(6),   -- Note!! Inverted             
         GT1_GTXTXP_OUT                     => fmc_l8_dp_c2m_n(6),   --                            
         GT1_TXOUTCLK_OUT                   => open,              
         GT1_TXOUTCLKFABRIC_OUT             => open,                
         GT1_TXOUTCLKPCS_OUT                => open,                
         GT1_TXRESETDONE_OUT                => open,                
         GT1_TXPOLARITY_IN                  => '1',              
         GT1_TXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         -----------------------------------
         GT2_DRPADDR_IN                     => (others => '0'),                     
         GT2_DRPCLK_IN                      => '0',             
         GT2_DRPDI_IN                       => (others => '0'),               
         GT2_DRPDO_OUT                      => open,             
         GT2_DRPEN_IN                       => '0',              
         GT2_DRPRDY_OUT                     => open,             
         GT2_DRPWE_IN                       => '0',              
         GT2_LOOPBACK_IN                    => gtx_loopback,
         GT2_RXPD_IN                        => (fmc_l8_dp_gtx_rx_powerDown(7),fmc_l8_dp_gtx_rx_powerDown(7)),
         GT2_TXPD_IN                        => (fmc_l8_dp_gtx_tx_powerDown(7),fmc_l8_dp_gtx_tx_powerDown(7)),          
         GT2_RXUSERRDY_IN                   => '1',             
         GT2_EYESCANDATAERROR_OUT           => open,               
         GT2_RXCDRLOCK_OUT                  => open,               
         GT2_RXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(5),             
         GT2_RXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(5),             
         GT2_RXDATA_OUT                     => open,                   
         GT2_RXPRBSERR_OUT                  => rxPrbsErr_from_fmcL8_dp(7),               
         GT2_RXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT2_RXPRBSCNTRESET_IN              => '0',                 
         GT2_GTXRXP_IN                      => fmc_l8_dp_m2c_n(7),   -- Note!! Inverted                 
         GT2_GTXRXN_IN                      => fmc_l8_dp_m2c_p(7),   --                                
         GT2_GTRXRESET_IN                   => fmc_l8_dp_gtx_rx_reset(7),                 
         GT2_RXPMARESET_IN                  => '0',                 
         GT2_RXPOLARITY_IN                  => '1',                 
         GT2_RXRESETDONE_OUT                => open,                   
         GT2_GTTXRESET_IN                   => fmc_l8_dp_gtx_tx_reset(7),                 
         GT2_TXUSERRDY_IN                   => '1',              
         GT2_TXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(5),             
         GT2_TXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(5),             
         GT2_TXPRBSFORCEERR_IN              => '0',              
         GT2_TXDIFFCTRL_IN                  => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT2_TXDATA_IN                      => (others => '0'),                 
         GT2_GTXTXN_OUT                     => fmc_l8_dp_c2m_p(7),   -- Note!! Inverted             
         GT2_GTXTXP_OUT                     => fmc_l8_dp_c2m_n(7),   --                            
         GT2_TXOUTCLK_OUT                   => open,              
         GT2_TXOUTCLKFABRIC_OUT             => open,                
         GT2_TXOUTCLKPCS_OUT                => open,                
         GT2_TXRESETDONE_OUT                => open,                
         GT2_TXPOLARITY_IN                  => '1',              
         GT2_TXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         -----------------------------------
         GT3_DRPADDR_IN                     => (others => '0'),                     
         GT3_DRPCLK_IN                      => '0',             
         GT3_DRPDI_IN                       => (others => '0'),               
         GT3_DRPDO_OUT                      => open,             
         GT3_DRPEN_IN                       => '0',              
         GT3_DRPRDY_OUT                     => open,             
         GT3_DRPWE_IN                       => '0',              
         GT3_LOOPBACK_IN                    => gtx_loopback,
         GT3_RXPD_IN                        => (fmc_l8_dp_gtx_rx_powerDown(4),fmc_l8_dp_gtx_rx_powerDown(4)),
         GT3_TXPD_IN                        => (fmc_l8_dp_gtx_tx_powerDown(4),fmc_l8_dp_gtx_tx_powerDown(4)),          
         GT3_RXUSERRDY_IN                   => '1',             
         GT3_EYESCANDATAERROR_OUT           => open,               
         GT3_RXCDRLOCK_OUT                  => open,               
         GT3_RXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(5),             
         GT3_RXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(5),             
         GT3_RXDATA_OUT                     => open,                   
         GT3_RXPRBSERR_OUT                  => rxPrbsErr_from_fmcL8_dp(4),               
         GT3_RXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT3_RXPRBSCNTRESET_IN              => '0',                 
         GT3_GTXRXP_IN                      => fmc_l8_dp_m2c_n(4),   -- Note!! Inverted                 
         GT3_GTXRXN_IN                      => fmc_l8_dp_m2c_p(4),   --                                
         GT3_GTRXRESET_IN                   => fmc_l8_dp_gtx_rx_reset(4),                 
         GT3_RXPMARESET_IN                  => '0',                 
         GT3_RXPOLARITY_IN                  => '1',                 
         GT3_RXRESETDONE_OUT                => open,                   
         GT3_GTTXRESET_IN                   => fmc_l8_dp_gtx_tx_reset(4),                 
         GT3_TXUSERRDY_IN                   => '1',              
         GT3_TXUSRCLK_IN                    => txOutClk_from_fmcL8_dp_buff(5),             
         GT3_TXUSRCLK2_IN                   => txOutClk_from_fmcL8_dp_buff(5),             
         GT3_TXPRBSFORCEERR_IN              => '0',              
         GT3_TXDIFFCTRL_IN                  => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT3_TXDATA_IN                      => (others => '0'),                 
         GT3_GTXTXN_OUT                     => fmc_l8_dp_c2m_p(4),   -- Note!! Inverted             
         GT3_GTXTXP_OUT                     => fmc_l8_dp_c2m_n(4),   --                            
         GT3_TXOUTCLK_OUT                   => open,              
         GT3_TXOUTCLKFABRIC_OUT             => open,                
         GT3_TXOUTCLKPCS_OUT                => open,                
         GT3_TXRESETDONE_OUT                => open,                
         GT3_TXPOLARITY_IN                  => '1',              
         GT3_TXPRBSSEL_IN                   => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         -----------------------------------
         GT0_GTREFCLK0_COMMON_IN            => osc_xpoint_a,              
         GT0_QPLLLOCK_OUT                   => open,              
         GT0_QPLLLOCKDETCLK_IN              => osc_xpoint_a_bufg, 
         GT0_QPLLPD_IN                      => fmcL8Bank111_gtx_qpll_powerDown,         
         GT0_QPLLRESET_IN                   => '0'              
      );                               
                                       
   fmcL8Bank111_ber_gen: for i in 0 to 3 generate
   
      fmcL8Bank111_berCnt: entity work.gtx_ber_cntr
         port map (
            RESET_I 								 => (not rxReady_from_fmcL8_dp(BANK111(i))) or gtx_bert_reset,
            CLK_I 								 => txOutClk_from_fmcL8_dp_buff(5),
            TRIGGER_I 							 => gtx_bert_trigger,
            ERROR_FLAG_I	 					 => rxPrbsErr_from_fmcL8_dp(BANK111(i)),
            ERROR_CNT_O 						 => errCnt_from_fmcL8_berCnt(BANK111(i)),
            BITS_CNT_O 							 => bitsCnt_from_fmcL8_berCnt(BANK111(i))	
         );		
         
      fmcL8Bank111_cdc: entity work.clk_domain_bridge			
         port map (
            WRCLK_I 								 => txOutClk_from_fmcL8_dp_buff(5),
            RDCLK_I 								 => ipb_clk_i,
            WDATA_I( 63 downto  0)			 => errCnt_from_fmcL8_berCnt(BANK111(i)),
            WDATA_I(127 downto 64)			 => bitsCnt_from_fmcL8_berCnt(BANK111(i)),
            RDATA_O( 63 downto  0) 			 => errCnt_from_fmcL8_cdc(BANK111(i)),
            RDATA_O(127 downto 64) 			 => bitsCnt_from_fmcL8_cdc(BANK111(i))					
         );
   
   end generate;
   
   -- FMC L12:
   -----------
   
   -- Bank 118 (osc_xpoint_c -> 125MHz):   -- Note!! GTX mapping: gtx_0 -> fmc_l12_dp_2
                                           --                     gtx_1 -> fmc_l12_dp_9
                                           --                     gtx_2 -> fmc_l12_dp_0
                                           --                     gtx_3 -> fmc_l12_dp_1
   
   fmcL12Bank118_txOutClkBufg: bufg
      port map (
         I                                 => txOutClk_from_fmcL12_dp(2),
         O                                 => txOutClk_from_fmcL12_dp_buff(2)
      );
   
   fmcL12Bank118_gtx: entity work.gtx_10gbps_125mhz_x4_init_mbm
      generic map (
         EXAMPLE_SIM_GTRESET_SPEEDUP       => "TRUE",     
         EXAMPLE_SIMULATION                => 0,          
         EXAMPLE_USE_CHIPSCOPE             => 0)
      port map (
         SYSCLK_IN                         => osc_xpoint_c_bufg,                         
         SOFT_RESET_IN                     => ipb_rst_i,              
         DONT_RESET_ON_DATA_ERROR_IN       => '1',             
         GT0_TX_FSM_RESET_DONE_OUT         => txReady_from_fmcL12_dp(2),             
         GT0_RX_FSM_RESET_DONE_OUT         => rxReady_from_fmcL12_dp(2),             
         GT0_DATA_VALID_IN                 => '1',                 
         GT1_TX_FSM_RESET_DONE_OUT         => txReady_from_fmcL12_dp(9),                
         GT1_RX_FSM_RESET_DONE_OUT         => rxReady_from_fmcL12_dp(9),                
         GT1_DATA_VALID_IN                 => '1',                             
         GT2_TX_FSM_RESET_DONE_OUT         => txReady_from_fmcL12_dp(0),                
         GT2_RX_FSM_RESET_DONE_OUT         => rxReady_from_fmcL12_dp(0),                
         GT2_DATA_VALID_IN                 => '1',                             
         GT3_TX_FSM_RESET_DONE_OUT         => txReady_from_fmcL12_dp(1),                
         GT3_RX_FSM_RESET_DONE_OUT         => rxReady_from_fmcL12_dp(1),                
         GT3_DATA_VALID_IN                 => '1',                             
         ----------------------------------
         GT0_DRPADDR_IN                    => (others => '0'),                     
         GT0_DRPCLK_IN                     => '0',             
         GT0_DRPDI_IN                      => (others => '0'),               
         GT0_DRPDO_OUT                     => open,             
         GT0_DRPEN_IN                      => '0',              
         GT0_DRPRDY_OUT                    => open,             
         GT0_DRPWE_IN                      => '0',              
         GT0_LOOPBACK_IN                   => gtx_loopback,
         GT0_RXPD_IN                       => (fmc_l12_dp_gtx_rx_powerDown(2),fmc_l12_dp_gtx_rx_powerDown(2)),
         GT0_TXPD_IN                       => (fmc_l12_dp_gtx_tx_powerDown(2),fmc_l12_dp_gtx_tx_powerDown(2)),             
         GT0_RXUSERRDY_IN                  => '1',             
         GT0_EYESCANDATAERROR_OUT          => open,               
         GT0_RXCDRLOCK_OUT                 => open,               
         GT0_RXUSRCLK_IN                   => txOutClk_from_fmcL12_dp_buff(2),                   
         GT0_RXUSRCLK2_IN                  => txOutClk_from_fmcL12_dp_buff(2),                       
         GT0_RXDATA_OUT                    => open,                   
         GT0_RXPRBSERR_OUT                 => rxPrbsErr_from_fmcL12_dp(2),                 
         GT0_RXPRBSSEL_IN                  => "100",   -- PRBS-31 (Xilinx UG476 page 137)                   
         GT0_RXPRBSCNTRESET_IN             => '0',                 
         GT0_GTXRXP_IN                     => fmc_l12_dp_m2c_p(2),                 
         GT0_GTXRXN_IN                     => fmc_l12_dp_m2c_n(2),                 
         GT0_GTRXRESET_IN                  => fmc_l12_dp_gtx_rx_reset(2),                 
         GT0_RXPMARESET_IN                 => '0',                 
         GT0_RXPOLARITY_IN                 => '0',                 
         GT0_RXRESETDONE_OUT               => open,                   
         GT0_GTTXRESET_IN                  => fmc_l12_dp_gtx_tx_reset(2),                 
         GT0_TXUSERRDY_IN                  => '1',              
         GT0_TXUSRCLK_IN                   => txOutClk_from_fmcL12_dp_buff(2),                    
         GT0_TXUSRCLK2_IN                  => txOutClk_from_fmcL12_dp_buff(2),              
         GT0_TXPRBSFORCEERR_IN             => '0',              
         GT0_TXDIFFCTRL_IN                 => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)              
         GT0_TXDATA_IN                     => (others => '0'),                 
         GT0_GTXTXN_OUT                    => fmc_l12_dp_c2m_n(2),             
         GT0_GTXTXP_OUT                    => fmc_l12_dp_c2m_p(2),             
         GT0_TXOUTCLK_OUT                  => txOutClk_from_fmcL12_dp(2),              
         GT0_TXOUTCLKFABRIC_OUT            => open,                
         GT0_TXOUTCLKPCS_OUT               => open,                
         GT0_TXRESETDONE_OUT               => open,                
         GT0_TXPOLARITY_IN                 => '0',              
         GT0_TXPRBSSEL_IN                  => "100",   -- PRBS-31 (Xilinx UG476 page 213)                     
         ----------------------------------
         GT1_DRPADDR_IN                    => (others => '0'),                     
         GT1_DRPCLK_IN                     => '0',             
         GT1_DRPDI_IN                      => (others => '0'),               
         GT1_DRPDO_OUT                     => open,             
         GT1_DRPEN_IN                      => '0',              
         GT1_DRPRDY_OUT                    => open,             
         GT1_DRPWE_IN                      => '0',              
         GT1_LOOPBACK_IN                   => gtx_loopback,
         GT1_RXPD_IN                       => (fmc_l12_dp_gtx_rx_powerDown(9),fmc_l12_dp_gtx_rx_powerDown(9)),
         GT1_TXPD_IN                       => (fmc_l12_dp_gtx_tx_powerDown(9),fmc_l12_dp_gtx_tx_powerDown(9)),             
         GT1_RXUSERRDY_IN                  => '1',             
         GT1_EYESCANDATAERROR_OUT          => open,               
         GT1_RXCDRLOCK_OUT                 => open,               
         GT1_RXUSRCLK_IN                   => txOutClk_from_fmcL12_dp_buff(2),             
         GT1_RXUSRCLK2_IN                  => txOutClk_from_fmcL12_dp_buff(2),             
         GT1_RXDATA_OUT                    => open,                   
         GT1_RXPRBSERR_OUT                 => rxPrbsErr_from_fmcL12_dp(9),               
         GT1_RXPRBSSEL_IN                  => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT1_RXPRBSCNTRESET_IN             => '0',                 
         GT1_GTXRXP_IN                     => fmc_l12_dp_m2c_p(9),                 
         GT1_GTXRXN_IN                     => fmc_l12_dp_m2c_n(9),                 
         GT1_GTRXRESET_IN                  => fmc_l12_dp_gtx_rx_reset(9),                 
         GT1_RXPMARESET_IN                 => '0',                 
         GT1_RXPOLARITY_IN                 => '0',                 
         GT1_RXRESETDONE_OUT               => open,                   
         GT1_GTTXRESET_IN                  => fmc_l12_dp_gtx_tx_reset(9),                 
         GT1_TXUSERRDY_IN                  => '1',              
         GT1_TXUSRCLK_IN                   => txOutClk_from_fmcL12_dp_buff(2),             
         GT1_TXUSRCLK2_IN                  => txOutClk_from_fmcL12_dp_buff(2),             
         GT1_TXPRBSFORCEERR_IN             => '0',              
         GT1_TXDIFFCTRL_IN                 => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT1_TXDATA_IN                     => (others => '0'),                 
         GT1_GTXTXN_OUT                    => fmc_l12_dp_c2m_n(9),             
         GT1_GTXTXP_OUT                    => fmc_l12_dp_c2m_p(9),             
         GT1_TXOUTCLK_OUT                  => open,              
         GT1_TXOUTCLKFABRIC_OUT            => open,                
         GT1_TXOUTCLKPCS_OUT               => open,                
         GT1_TXRESETDONE_OUT               => open,                
         GT1_TXPOLARITY_IN                 => '0',              
         GT1_TXPRBSSEL_IN                  => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ----------------------------------
         GT2_DRPADDR_IN                    => (others => '0'),                     
         GT2_DRPCLK_IN                     => '0',             
         GT2_DRPDI_IN                      => (others => '0'),               
         GT2_DRPDO_OUT                     => open,             
         GT2_DRPEN_IN                      => '0',              
         GT2_DRPRDY_OUT                    => open,             
         GT2_DRPWE_IN                      => '0',              
         GT2_LOOPBACK_IN                   => gtx_loopback,
         GT2_RXPD_IN                       => (fmc_l12_dp_gtx_rx_powerDown(0),fmc_l12_dp_gtx_rx_powerDown(0)),
         GT2_TXPD_IN                       => (fmc_l12_dp_gtx_tx_powerDown(0),fmc_l12_dp_gtx_tx_powerDown(0)),             
         GT2_RXUSERRDY_IN                  => '1',             
         GT2_EYESCANDATAERROR_OUT          => open,               
         GT2_RXCDRLOCK_OUT                 => open,               
         GT2_RXUSRCLK_IN                   => txOutClk_from_fmcL12_dp_buff(2),             
         GT2_RXUSRCLK2_IN                  => txOutClk_from_fmcL12_dp_buff(2),             
         GT2_RXDATA_OUT                    => open,                   
         GT2_RXPRBSERR_OUT                 => rxPrbsErr_from_fmcL12_dp(0),               
         GT2_RXPRBSSEL_IN                  => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT2_RXPRBSCNTRESET_IN             => '0',                 
         GT2_GTXRXP_IN                     => fmc_l12_dp_m2c_p(0),                 
         GT2_GTXRXN_IN                     => fmc_l12_dp_m2c_n(0),                 
         GT2_GTRXRESET_IN                  => fmc_l12_dp_gtx_rx_reset(0),                 
         GT2_RXPMARESET_IN                 => '0',                 
         GT2_RXPOLARITY_IN                 => '0',                 
         GT2_RXRESETDONE_OUT               => open,                   
         GT2_GTTXRESET_IN                  => fmc_l12_dp_gtx_tx_reset(0),                 
         GT2_TXUSERRDY_IN                  => '1',              
         GT2_TXUSRCLK_IN                   => txOutClk_from_fmcL12_dp_buff(2),             
         GT2_TXUSRCLK2_IN                  => txOutClk_from_fmcL12_dp_buff(2),             
         GT2_TXPRBSFORCEERR_IN             => '0',              
         GT2_TXDIFFCTRL_IN                 => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT2_TXDATA_IN                     => (others => '0'),                 
         GT2_GTXTXN_OUT                    => fmc_l12_dp_c2m_n(0),             
         GT2_GTXTXP_OUT                    => fmc_l12_dp_c2m_p(0),             
         GT2_TXOUTCLK_OUT                  => open,              
         GT2_TXOUTCLKFABRIC_OUT            => open,                
         GT2_TXOUTCLKPCS_OUT               => open,                
         GT2_TXRESETDONE_OUT               => open,                
         GT2_TXPOLARITY_IN                 => '0',              
         GT2_TXPRBSSEL_IN                  => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ----------------------------------
         GT3_DRPADDR_IN                    => (others => '0'),                     
         GT3_DRPCLK_IN                     => '0',             
         GT3_DRPDI_IN                      => (others => '0'),               
         GT3_DRPDO_OUT                     => open,             
         GT3_DRPEN_IN                      => '0',              
         GT3_DRPRDY_OUT                    => open,             
         GT3_DRPWE_IN                      => '0',              
         GT3_LOOPBACK_IN                   => gtx_loopback,
         GT3_RXPD_IN                       => (fmc_l12_dp_gtx_rx_powerDown(1),fmc_l12_dp_gtx_rx_powerDown(1)),
         GT3_TXPD_IN                       => (fmc_l12_dp_gtx_tx_powerDown(1),fmc_l12_dp_gtx_tx_powerDown(1)),             
         GT3_RXUSERRDY_IN                  => '1',             
         GT3_EYESCANDATAERROR_OUT          => open,               
         GT3_RXCDRLOCK_OUT                 => open,               
         GT3_RXUSRCLK_IN                   => txOutClk_from_fmcL12_dp_buff(2),             
         GT3_RXUSRCLK2_IN                  => txOutClk_from_fmcL12_dp_buff(2),             
         GT3_RXDATA_OUT                    => open,                   
         GT3_RXPRBSERR_OUT                 => rxPrbsErr_from_fmcL12_dp(1),               
         GT3_RXPRBSSEL_IN                  => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT3_RXPRBSCNTRESET_IN             => '0',                 
         GT3_GTXRXP_IN                     => fmc_l12_dp_m2c_p(1),                 
         GT3_GTXRXN_IN                     => fmc_l12_dp_m2c_n(1),                 
         GT3_GTRXRESET_IN                  => fmc_l12_dp_gtx_rx_reset(1),                 
         GT3_RXPMARESET_IN                 => '0',                 
         GT3_RXPOLARITY_IN                 => '0',                 
         GT3_RXRESETDONE_OUT               => open,                   
         GT3_GTTXRESET_IN                  => fmc_l12_dp_gtx_tx_reset(1),                 
         GT3_TXUSERRDY_IN                  => '1',              
         GT3_TXUSRCLK_IN                   => txOutClk_from_fmcL12_dp_buff(2),             
         GT3_TXUSRCLK2_IN                  => txOutClk_from_fmcL12_dp_buff(2),             
         GT3_TXPRBSFORCEERR_IN             => '0',              
         GT3_TXDIFFCTRL_IN                 => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT3_TXDATA_IN                     => (others => '0'),                 
         GT3_GTXTXN_OUT                    => fmc_l12_dp_c2m_n(1),             
         GT3_GTXTXP_OUT                    => fmc_l12_dp_c2m_p(1),             
         GT3_TXOUTCLK_OUT                  => open,              
         GT3_TXOUTCLKFABRIC_OUT            => open,                
         GT3_TXOUTCLKPCS_OUT               => open,                
         GT3_TXRESETDONE_OUT               => open,                
         GT3_TXPOLARITY_IN                 => '0',              
         GT3_TXPRBSSEL_IN                  => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ----------------------------------
         GT0_GTREFCLK0_COMMON_IN           => osc_xpoint_c,              
         GT0_QPLLLOCK_OUT                  => open,              
         GT0_QPLLLOCKDETCLK_IN             => osc_xpoint_c_bufg,
         GT0_QPLLPD_IN                     => fmcL12Bank118_gtx_qpll_powerDown,         
         GT0_QPLLRESET_IN                  => '0'              
      );                               
                                       
   fmcL12Bank118_ber_gen: for i in 0 to 3 generate
      
      -- BANK118 = (2,9,0,1)
      
      fmcL12Bank118_berCnt: entity work.gtx_ber_cntr
         port map (
            RESET_I 								=> (not rxReady_from_fmcL12_dp(BANK118(i))) or gtx_bert_reset,
            CLK_I 								=> txOutClk_from_fmcL12_dp_buff(2),
            TRIGGER_I 							=> gtx_bert_trigger,
            ERROR_FLAG_I	 					=> rxPrbsErr_from_fmcL12_dp(BANK118(i)),
            ERROR_CNT_O 						=> errCnt_from_fmcL12_berCnt(BANK118(i)),
            BITS_CNT_O 							=> bitsCnt_from_fmcL12_berCnt(BANK118(i))
         );		
         
      fmcL12Bank118_cdc: entity work.clk_domain_bridge			
         port map (
            WRCLK_I 								=> txOutClk_from_fmcL12_dp_buff(2),
            RDCLK_I 								=> ipb_clk_i,
            WDATA_I( 63 downto  0)			=> errCnt_from_fmcL12_berCnt(BANK118(i)),
            WDATA_I(127 downto 64)			=> bitsCnt_from_fmcL12_berCnt(BANK118(i)),
            RDATA_O( 63 downto  0) 			=> errCnt_from_fmcL12_cdc(BANK118(i)),
            RDATA_O(127 downto 64) 			=> bitsCnt_from_fmcL12_cdc(BANK118(i))					
         );
   
   end generate;
 
   -- Bank 117 (osc_xpoint_c -> 125MHz):   -- Note!! GTX mapping: gtx_0 -> fmc_l12_dp_7
                                           --                     gtx_1 -> fmc_l12_dp_3
                                           --                     gtx_2 -> fmc_l12_dp_8
                                           --                     gtx_3 -> fmc_l12_dp_11
   
   fmcL12Bank117_txOutClkBufg: bufg
      port map (
         I                                => txOutClk_from_fmcL12_dp(7),
         O                                => txOutClk_from_fmcL12_dp_buff(7)
      );
   
   fmcL12Bank117_gtx: entity work.gtx_10gbps_125mhz_x4_init_mbm
      generic map (
         EXAMPLE_SIM_GTRESET_SPEEDUP      => "TRUE",     
         EXAMPLE_SIMULATION               => 0,          
         EXAMPLE_USE_CHIPSCOPE            => 0)
      port map (
         SYSCLK_IN                        => osc_xpoint_c_bufg,                         
         SOFT_RESET_IN                    => ipb_rst_i,              
         DONT_RESET_ON_DATA_ERROR_IN      => '1',             
         GT0_TX_FSM_RESET_DONE_OUT        => txReady_from_fmcL12_dp(7),             
         GT0_RX_FSM_RESET_DONE_OUT        => rxReady_from_fmcL12_dp(7),             
         GT0_DATA_VALID_IN                => '1',                 
         GT1_TX_FSM_RESET_DONE_OUT        => txReady_from_fmcL12_dp(3),                
         GT1_RX_FSM_RESET_DONE_OUT        => rxReady_from_fmcL12_dp(3),                
         GT1_DATA_VALID_IN                => '1',                             
         GT2_TX_FSM_RESET_DONE_OUT        => txReady_from_fmcL12_dp(8),                
         GT2_RX_FSM_RESET_DONE_OUT        => rxReady_from_fmcL12_dp(8),                
         GT2_DATA_VALID_IN                => '1',                             
         GT3_TX_FSM_RESET_DONE_OUT        => txReady_from_fmcL12_dp(11),                
         GT3_RX_FSM_RESET_DONE_OUT        => rxReady_from_fmcL12_dp(11),                
         GT3_DATA_VALID_IN                => '1',                             
         ---------------------------------
         GT0_DRPADDR_IN                   => (others => '0'),                     
         GT0_DRPCLK_IN                    => '0',             
         GT0_DRPDI_IN                     => (others => '0'),               
         GT0_DRPDO_OUT                    => open,             
         GT0_DRPEN_IN                     => '0',              
         GT0_DRPRDY_OUT                   => open,             
         GT0_DRPWE_IN                     => '0',              
         GT0_LOOPBACK_IN                  => gtx_loopback,
         GT0_RXPD_IN                      => (fmc_l12_dp_gtx_rx_powerDown(7),fmc_l12_dp_gtx_rx_powerDown(7)),
         GT0_TXPD_IN                      => (fmc_l12_dp_gtx_tx_powerDown(7),fmc_l12_dp_gtx_tx_powerDown(7)),                      
         GT0_RXUSERRDY_IN                 => '1',             
         GT0_EYESCANDATAERROR_OUT         => open,               
         GT0_RXCDRLOCK_OUT                => open,               
         GT0_RXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(7),                   
         GT0_RXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(7),                       
         GT0_RXDATA_OUT                   => open,                   
         GT0_RXPRBSERR_OUT                => rxPrbsErr_from_fmcL12_dp(7),                 
         GT0_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)                   
         GT0_RXPRBSCNTRESET_IN            => '0',                 
         GT0_GTXRXP_IN                    => fmc_l12_dp_m2c_p(7),                 
         GT0_GTXRXN_IN                    => fmc_l12_dp_m2c_n(7),                 
         GT0_GTRXRESET_IN                 => fmc_l12_dp_gtx_rx_reset(7),                 
         GT0_RXPMARESET_IN                => '0',                 
         GT0_RXPOLARITY_IN                => '0',                 
         GT0_RXRESETDONE_OUT              => open,                   
         GT0_GTTXRESET_IN                 => fmc_l12_dp_gtx_tx_reset(7),                 
         GT0_TXUSERRDY_IN                 => '1',              
         GT0_TXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(7),                    
         GT0_TXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(7),              
         GT0_TXPRBSFORCEERR_IN            => '0',              
         GT0_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)              
         GT0_TXDATA_IN                    => (others => '0'),                 
         GT0_GTXTXN_OUT                   => fmc_l12_dp_c2m_n(7),             
         GT0_GTXTXP_OUT                   => fmc_l12_dp_c2m_p(7),             
         GT0_TXOUTCLK_OUT                 => txOutClk_from_fmcL12_dp(7),              
         GT0_TXOUTCLKFABRIC_OUT           => open,                
         GT0_TXOUTCLKPCS_OUT              => open,                
         GT0_TXRESETDONE_OUT              => open,                
         GT0_TXPOLARITY_IN                => '0',              
         GT0_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)                     
         ---------------------------------
         GT1_DRPADDR_IN                   => (others => '0'),                     
         GT1_DRPCLK_IN                    => '0',             
         GT1_DRPDI_IN                     => (others => '0'),               
         GT1_DRPDO_OUT                    => open,             
         GT1_DRPEN_IN                     => '0',              
         GT1_DRPRDY_OUT                   => open,             
         GT1_DRPWE_IN                     => '0',              
         GT1_LOOPBACK_IN                  => gtx_loopback,
         GT1_RXPD_IN                      => (fmc_l12_dp_gtx_rx_powerDown(3),fmc_l12_dp_gtx_rx_powerDown(3)),
         GT1_TXPD_IN                      => (fmc_l12_dp_gtx_tx_powerDown(3),fmc_l12_dp_gtx_tx_powerDown(3)),                      
         GT1_RXUSERRDY_IN                 => '1',             
         GT1_EYESCANDATAERROR_OUT         => open,               
         GT1_RXCDRLOCK_OUT                => open,               
         GT1_RXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(7),             
         GT1_RXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(7),             
         GT1_RXDATA_OUT                   => open,                   
         GT1_RXPRBSERR_OUT                => rxPrbsErr_from_fmcL12_dp(3),               
         GT1_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT1_RXPRBSCNTRESET_IN            => '0',                 
         GT1_GTXRXP_IN                    => fmc_l12_dp_m2c_p(3),                 
         GT1_GTXRXN_IN                    => fmc_l12_dp_m2c_n(3),                 
         GT1_GTRXRESET_IN                 => fmc_l12_dp_gtx_rx_reset(3),                 
         GT1_RXPMARESET_IN                => '0',                 
         GT1_RXPOLARITY_IN                => '0',                 
         GT1_RXRESETDONE_OUT              => open,                   
         GT1_GTTXRESET_IN                 => fmc_l12_dp_gtx_tx_reset(3),                 
         GT1_TXUSERRDY_IN                 => '1',              
         GT1_TXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(7),             
         GT1_TXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(7),             
         GT1_TXPRBSFORCEERR_IN            => '0',              
         GT1_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT1_TXDATA_IN                    => (others => '0'),                 
         GT1_GTXTXN_OUT                   => fmc_l12_dp_c2m_n(3),             
         GT1_GTXTXP_OUT                   => fmc_l12_dp_c2m_p(3),             
         GT1_TXOUTCLK_OUT                 => open,              
         GT1_TXOUTCLKFABRIC_OUT           => open,                
         GT1_TXOUTCLKPCS_OUT              => open,                
         GT1_TXRESETDONE_OUT              => open,                
         GT1_TXPOLARITY_IN                => '0',              
         GT1_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT2_DRPADDR_IN                   => (others => '0'),                     
         GT2_DRPCLK_IN                    => '0',             
         GT2_DRPDI_IN                     => (others => '0'),               
         GT2_DRPDO_OUT                    => open,             
         GT2_DRPEN_IN                     => '0',              
         GT2_DRPRDY_OUT                   => open,             
         GT2_DRPWE_IN                     => '0',              
         GT2_LOOPBACK_IN                  => gtx_loopback,
         GT2_RXPD_IN                      => (fmc_l12_dp_gtx_rx_powerDown(8),fmc_l12_dp_gtx_rx_powerDown(8)),
         GT2_TXPD_IN                      => (fmc_l12_dp_gtx_tx_powerDown(8),fmc_l12_dp_gtx_tx_powerDown(8)),                      
         GT2_RXUSERRDY_IN                 => '1',             
         GT2_EYESCANDATAERROR_OUT         => open,               
         GT2_RXCDRLOCK_OUT                => open,               
         GT2_RXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(7),             
         GT2_RXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(7),             
         GT2_RXDATA_OUT                   => open,                   
         GT2_RXPRBSERR_OUT                => rxPrbsErr_from_fmcL12_dp(8),               
         GT2_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT2_RXPRBSCNTRESET_IN            => '0',                 
         GT2_GTXRXP_IN                    => fmc_l12_dp_m2c_p(8),                 
         GT2_GTXRXN_IN                    => fmc_l12_dp_m2c_n(8),                 
         GT2_GTRXRESET_IN                 => fmc_l12_dp_gtx_rx_reset(8),                 
         GT2_RXPMARESET_IN                => '0',                 
         GT2_RXPOLARITY_IN                => '0',                 
         GT2_RXRESETDONE_OUT              => open,                   
         GT2_GTTXRESET_IN                 => fmc_l12_dp_gtx_tx_reset(8),                 
         GT2_TXUSERRDY_IN                 => '1',              
         GT2_TXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(7),             
         GT2_TXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(7),             
         GT2_TXPRBSFORCEERR_IN            => '0',              
         GT2_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT2_TXDATA_IN                    => (others => '0'),                 
         GT2_GTXTXN_OUT                   => fmc_l12_dp_c2m_n(8),             
         GT2_GTXTXP_OUT                   => fmc_l12_dp_c2m_p(8),             
         GT2_TXOUTCLK_OUT                 => open,              
         GT2_TXOUTCLKFABRIC_OUT           => open,                
         GT2_TXOUTCLKPCS_OUT              => open,                
         GT2_TXRESETDONE_OUT              => open,                
         GT2_TXPOLARITY_IN                => '0',              
         GT2_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT3_DRPADDR_IN                   => (others => '0'),                     
         GT3_DRPCLK_IN                    => '0',             
         GT3_DRPDI_IN                     => (others => '0'),               
         GT3_DRPDO_OUT                    => open,             
         GT3_DRPEN_IN                     => '0',              
         GT3_DRPRDY_OUT                   => open,             
         GT3_DRPWE_IN                     => '0',              
         GT3_LOOPBACK_IN                  => gtx_loopback,
         GT3_RXPD_IN                      => (fmc_l12_dp_gtx_rx_powerDown(11),fmc_l12_dp_gtx_rx_powerDown(11)),
         GT3_TXPD_IN                      => (fmc_l12_dp_gtx_tx_powerDown(11),fmc_l12_dp_gtx_tx_powerDown(11)),                      
         GT3_RXUSERRDY_IN                 => '1',             
         GT3_EYESCANDATAERROR_OUT         => open,               
         GT3_RXCDRLOCK_OUT                => open,               
         GT3_RXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(7),             
         GT3_RXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(7),             
         GT3_RXDATA_OUT                   => open,                   
         GT3_RXPRBSERR_OUT                => rxPrbsErr_from_fmcL12_dp(11),               
         GT3_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT3_RXPRBSCNTRESET_IN            => '0',                 
         GT3_GTXRXP_IN                    => fmc_l12_dp_m2c_p(11),                 
         GT3_GTXRXN_IN                    => fmc_l12_dp_m2c_n(11),                 
         GT3_GTRXRESET_IN                 => fmc_l12_dp_gtx_rx_reset(11),                 
         GT3_RXPMARESET_IN                => '0',                 
         GT3_RXPOLARITY_IN                => '0',                 
         GT3_RXRESETDONE_OUT              => open,                   
         GT3_GTTXRESET_IN                 => fmc_l12_dp_gtx_tx_reset(11),                 
         GT3_TXUSERRDY_IN                 => '1',              
         GT3_TXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(7),             
         GT3_TXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(7),             
         GT3_TXPRBSFORCEERR_IN            => '0',              
         GT3_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT3_TXDATA_IN                    => (others => '0'),                 
         GT3_GTXTXN_OUT                   => fmc_l12_dp_c2m_n(11),             
         GT3_GTXTXP_OUT                   => fmc_l12_dp_c2m_p(11),             
         GT3_TXOUTCLK_OUT                 => open,              
         GT3_TXOUTCLKFABRIC_OUT           => open,                
         GT3_TXOUTCLKPCS_OUT              => open,                
         GT3_TXRESETDONE_OUT              => open,                
         GT3_TXPOLARITY_IN                => '0',              
         GT3_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT0_GTREFCLK0_COMMON_IN          => osc_xpoint_c,              
         GT0_QPLLLOCK_OUT                 => open,              
         GT0_QPLLLOCKDETCLK_IN            => osc_xpoint_c_bufg, 
         GT0_QPLLPD_IN                    => fmcL12Bank117_gtx_qpll_powerDown,         
         GT0_QPLLRESET_IN                 => '0'              
      );                               
                                       
   fmcL12Bank117_ber_gen: for i in 0 to 3 generate
      
      -- BANK117 = (7,3,8,11)
      
      fmcL12Bank117_berCnt: entity work.gtx_ber_cntr
         port map (
            RESET_I 								=> (not rxReady_from_fmcL12_dp(BANK117(i))) or gtx_bert_reset,
            CLK_I 								=> txOutClk_from_fmcL12_dp_buff(7),
            TRIGGER_I 							=> gtx_bert_trigger,
            ERROR_FLAG_I	 					=> rxPrbsErr_from_fmcL12_dp(BANK117(i)),
            ERROR_CNT_O 						=> errCnt_from_fmcL12_berCnt(BANK117(i)),
            BITS_CNT_O 							=> bitsCnt_from_fmcL12_berCnt(BANK117(i))	
         );		
         
      fmcL12Bank117_cdc: entity work.clk_domain_bridge			
         port map (
            WRCLK_I 								=> txOutClk_from_fmcL12_dp_buff(7),
            RDCLK_I 								=> ipb_clk_i,
            WDATA_I( 63 downto  0)			=> errCnt_from_fmcL12_berCnt(BANK117(i)),
            WDATA_I(127 downto 64)			=> bitsCnt_from_fmcL12_berCnt(BANK117(i)),
            RDATA_O( 63 downto  0) 			=> errCnt_from_fmcL12_cdc(BANK117(i)),
            RDATA_O(127 downto 64) 			=> bitsCnt_from_fmcL12_cdc(BANK117(i))					
         );
   
   end generate;
   
   -- Bank 116 (osc_xpoint_c -> 125MHz):   -- Note!! GTX mapping: gtx_0 -> fmc_l12_dp_5
                                           --                     gtx_1 -> fmc_l12_dp_6
                                           --                     gtx_2 -> fmc_l12_dp_10
                                           --                     gtx_3 -> fmc_l12_dp_4
   
   fmcL12Bank116_txOutClkBufg: bufg
      port map (
         I                                => txOutClk_from_fmcL12_dp(5),
         O                                => txOutClk_from_fmcL12_dp_buff(5)
      );
   
   fmcL12Bank116_gtx: entity work.gtx_10gbps_125mhz_x4_init_mbm
      generic map (
         EXAMPLE_SIM_GTRESET_SPEEDUP      => "TRUE",     
         EXAMPLE_SIMULATION               => 0,          
         EXAMPLE_USE_CHIPSCOPE            => 0)
      port map (
         SYSCLK_IN                        => osc_xpoint_c_bufg,                         
         SOFT_RESET_IN                    => ipb_rst_i,              
         DONT_RESET_ON_DATA_ERROR_IN      => '1',             
         GT0_TX_FSM_RESET_DONE_OUT        => txReady_from_fmcL12_dp(5),             
         GT0_RX_FSM_RESET_DONE_OUT        => rxReady_from_fmcL12_dp(5),             
         GT0_DATA_VALID_IN                => '1',                 
         GT1_TX_FSM_RESET_DONE_OUT        => txReady_from_fmcL12_dp(6),                
         GT1_RX_FSM_RESET_DONE_OUT        => rxReady_from_fmcL12_dp(6),                
         GT1_DATA_VALID_IN                => '1',                             
         GT2_TX_FSM_RESET_DONE_OUT        => txReady_from_fmcL12_dp(10),                
         GT2_RX_FSM_RESET_DONE_OUT        => rxReady_from_fmcL12_dp(10),                
         GT2_DATA_VALID_IN                => '1',                             
         GT3_TX_FSM_RESET_DONE_OUT        => txReady_from_fmcL12_dp(4),                
         GT3_RX_FSM_RESET_DONE_OUT        => rxReady_from_fmcL12_dp(4),                
         GT3_DATA_VALID_IN                => '1',                             
         ---------------------------------
         GT0_DRPADDR_IN                   => (others => '0'),                     
         GT0_DRPCLK_IN                    => '0',             
         GT0_DRPDI_IN                     => (others => '0'),               
         GT0_DRPDO_OUT                    => open,             
         GT0_DRPEN_IN                     => '0',              
         GT0_DRPRDY_OUT                   => open,             
         GT0_DRPWE_IN                     => '0',              
         GT0_LOOPBACK_IN                  => gtx_loopback,
         GT0_RXPD_IN                      => (fmc_l12_dp_gtx_rx_powerDown(5),fmc_l12_dp_gtx_rx_powerDown(5)),
         GT0_TXPD_IN                      => (fmc_l12_dp_gtx_tx_powerDown(5),fmc_l12_dp_gtx_tx_powerDown(5)),                      
         GT0_RXUSERRDY_IN                 => '1',             
         GT0_EYESCANDATAERROR_OUT         => open,               
         GT0_RXCDRLOCK_OUT                => open,               
         GT0_RXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(5),                   
         GT0_RXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(5),                       
         GT0_RXDATA_OUT                   => open,                   
         GT0_RXPRBSERR_OUT                => rxPrbsErr_from_fmcL12_dp(5),                 
         GT0_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)                   
         GT0_RXPRBSCNTRESET_IN            => '0',                 
         GT0_GTXRXP_IN                    => fmc_l12_dp_m2c_p(5),                 
         GT0_GTXRXN_IN                    => fmc_l12_dp_m2c_n(5),                 
         GT0_GTRXRESET_IN                 => fmc_l12_dp_gtx_rx_reset(5),                 
         GT0_RXPMARESET_IN                => '0',                 
         GT0_RXPOLARITY_IN                => '0',                 
         GT0_RXRESETDONE_OUT              => open,                   
         GT0_GTTXRESET_IN                 => fmc_l12_dp_gtx_tx_reset(5),                 
         GT0_TXUSERRDY_IN                 => '1',              
         GT0_TXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(5),                    
         GT0_TXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(5),              
         GT0_TXPRBSFORCEERR_IN            => '0',              
         GT0_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)              
         GT0_TXDATA_IN                    => (others => '0'),                 
         GT0_GTXTXN_OUT                   => fmc_l12_dp_c2m_n(5),             
         GT0_GTXTXP_OUT                   => fmc_l12_dp_c2m_p(5),             
         GT0_TXOUTCLK_OUT                 => txOutClk_from_fmcL12_dp(5),              
         GT0_TXOUTCLKFABRIC_OUT           => open,                
         GT0_TXOUTCLKPCS_OUT              => open,                
         GT0_TXRESETDONE_OUT              => open,                
         GT0_TXPOLARITY_IN                => '0',              
         GT0_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)                     
         ---------------------------------
         GT1_DRPADDR_IN                   => (others => '0'),                     
         GT1_DRPCLK_IN                    => '0',             
         GT1_DRPDI_IN                     => (others => '0'),               
         GT1_DRPDO_OUT                    => open,             
         GT1_DRPEN_IN                     => '0',              
         GT1_DRPRDY_OUT                   => open,             
         GT1_DRPWE_IN                     => '0',              
         GT1_LOOPBACK_IN                  => gtx_loopback,
         GT1_RXPD_IN                      => (fmc_l12_dp_gtx_rx_powerDown(6),fmc_l12_dp_gtx_rx_powerDown(6)),
         GT1_TXPD_IN                      => (fmc_l12_dp_gtx_tx_powerDown(6),fmc_l12_dp_gtx_tx_powerDown(6)),                      
         GT1_RXUSERRDY_IN                 => '1',             
         GT1_EYESCANDATAERROR_OUT         => open,               
         GT1_RXCDRLOCK_OUT                => open,               
         GT1_RXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(5),             
         GT1_RXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(5),             
         GT1_RXDATA_OUT                   => open,                   
         GT1_RXPRBSERR_OUT                => rxPrbsErr_from_fmcL12_dp(6),               
         GT1_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT1_RXPRBSCNTRESET_IN            => '0',                 
         GT1_GTXRXP_IN                    => fmc_l12_dp_m2c_p(6),                 
         GT1_GTXRXN_IN                    => fmc_l12_dp_m2c_n(6),                 
         GT1_GTRXRESET_IN                 => fmc_l12_dp_gtx_rx_reset(6),                 
         GT1_RXPMARESET_IN                => '0',                 
         GT1_RXPOLARITY_IN                => '0',                 
         GT1_RXRESETDONE_OUT              => open,                   
         GT1_GTTXRESET_IN                 => fmc_l12_dp_gtx_tx_reset(6),                 
         GT1_TXUSERRDY_IN                 => '1',              
         GT1_TXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(5),             
         GT1_TXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(5),             
         GT1_TXPRBSFORCEERR_IN            => '0',              
         GT1_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT1_TXDATA_IN                    => (others => '0'),                 
         GT1_GTXTXN_OUT                   => fmc_l12_dp_c2m_n(6),             
         GT1_GTXTXP_OUT                   => fmc_l12_dp_c2m_p(6),             
         GT1_TXOUTCLK_OUT                 => open,              
         GT1_TXOUTCLKFABRIC_OUT           => open,                
         GT1_TXOUTCLKPCS_OUT              => open,                
         GT1_TXRESETDONE_OUT              => open,                
         GT1_TXPOLARITY_IN                => '0',              
         GT1_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT2_DRPADDR_IN                   => (others => '0'),                     
         GT2_DRPCLK_IN                    => '0',             
         GT2_DRPDI_IN                     => (others => '0'),               
         GT2_DRPDO_OUT                    => open,             
         GT2_DRPEN_IN                     => '0',              
         GT2_DRPRDY_OUT                   => open,             
         GT2_DRPWE_IN                     => '0',              
         GT2_LOOPBACK_IN                  => gtx_loopback, 
         GT2_RXPD_IN                       => (fmc_l12_dp_gtx_rx_powerDown(10),fmc_l12_dp_gtx_rx_powerDown(10)),
         GT2_TXPD_IN                       => (fmc_l12_dp_gtx_tx_powerDown(10),fmc_l12_dp_gtx_tx_powerDown(10)),
         GT2_RXUSERRDY_IN                 => '1',             
         GT2_EYESCANDATAERROR_OUT         => open,               
         GT2_RXCDRLOCK_OUT                => open,               
         GT2_RXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(5),             
         GT2_RXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(5),             
         GT2_RXDATA_OUT                   => open,                   
         GT2_RXPRBSERR_OUT                => rxPrbsErr_from_fmcL12_dp(10),               
         GT2_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT2_RXPRBSCNTRESET_IN            => '0',                 
         GT2_GTXRXP_IN                    => fmc_l12_dp_m2c_p(10),                 
         GT2_GTXRXN_IN                    => fmc_l12_dp_m2c_n(10),                 
         GT2_GTRXRESET_IN                 => fmc_l12_dp_gtx_rx_reset(10),                 
         GT2_RXPMARESET_IN                => '0',                 
         GT2_RXPOLARITY_IN                => '0',                 
         GT2_RXRESETDONE_OUT              => open,                   
         GT2_GTTXRESET_IN                 => fmc_l12_dp_gtx_tx_reset(10),                 
         GT2_TXUSERRDY_IN                 => '1',              
         GT2_TXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(5),             
         GT2_TXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(5),             
         GT2_TXPRBSFORCEERR_IN            => '0',              
         GT2_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT2_TXDATA_IN                    => (others => '0'),                 
         GT2_GTXTXN_OUT                   => fmc_l12_dp_c2m_n(10),             
         GT2_GTXTXP_OUT                   => fmc_l12_dp_c2m_p(10),             
         GT2_TXOUTCLK_OUT                 => open,              
         GT2_TXOUTCLKFABRIC_OUT           => open,                
         GT2_TXOUTCLKPCS_OUT              => open,                
         GT2_TXRESETDONE_OUT              => open,                
         GT2_TXPOLARITY_IN                => '0',              
         GT2_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT3_DRPADDR_IN                   => (others => '0'),                     
         GT3_DRPCLK_IN                    => '0',             
         GT3_DRPDI_IN                     => (others => '0'),               
         GT3_DRPDO_OUT                    => open,             
         GT3_DRPEN_IN                     => '0',              
         GT3_DRPRDY_OUT                   => open,             
         GT3_DRPWE_IN                     => '0',              
         GT3_LOOPBACK_IN                  => gtx_loopback,
         GT3_RXPD_IN                      => (fmc_l12_dp_gtx_rx_powerDown(4),fmc_l12_dp_gtx_rx_powerDown(4)),
         GT3_TXPD_IN                      => (fmc_l12_dp_gtx_tx_powerDown(4),fmc_l12_dp_gtx_tx_powerDown(4)),                      
         GT3_RXUSERRDY_IN                 => '1',             
         GT3_EYESCANDATAERROR_OUT         => open,               
         GT3_RXCDRLOCK_OUT                => open,               
         GT3_RXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(5),             
         GT3_RXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(5),             
         GT3_RXDATA_OUT                   => open,                   
         GT3_RXPRBSERR_OUT                => rxPrbsErr_from_fmcL12_dp(4),               
         GT3_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT3_RXPRBSCNTRESET_IN            => '0',                 
         GT3_GTXRXP_IN                    => fmc_l12_dp_m2c_p(4),                 
         GT3_GTXRXN_IN                    => fmc_l12_dp_m2c_n(4),                 
         GT3_GTRXRESET_IN                 => fmc_l12_dp_gtx_rx_reset(4),                 
         GT3_RXPMARESET_IN                => '0',                 
         GT3_RXPOLARITY_IN                => '0',                 
         GT3_RXRESETDONE_OUT              => open,                   
         GT3_GTTXRESET_IN                 => fmc_l12_dp_gtx_tx_reset(4),                 
         GT3_TXUSERRDY_IN                 => '1',              
         GT3_TXUSRCLK_IN                  => txOutClk_from_fmcL12_dp_buff(5),             
         GT3_TXUSRCLK2_IN                 => txOutClk_from_fmcL12_dp_buff(5),             
         GT3_TXPRBSFORCEERR_IN            => '0',              
         GT3_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT3_TXDATA_IN                    => (others => '0'),                 
         GT3_GTXTXN_OUT                   => fmc_l12_dp_c2m_n(4),             
         GT3_GTXTXP_OUT                   => fmc_l12_dp_c2m_p(4),             
         GT3_TXOUTCLK_OUT                 => open,              
         GT3_TXOUTCLKFABRIC_OUT           => open,                
         GT3_TXOUTCLKPCS_OUT              => open,                
         GT3_TXRESETDONE_OUT              => open,                
         GT3_TXPOLARITY_IN                => '0',              
         GT3_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT0_GTREFCLK0_COMMON_IN          => osc_xpoint_c,              
         GT0_QPLLLOCK_OUT                 => open,              
         GT0_QPLLLOCKDETCLK_IN            => osc_xpoint_c_bufg, 
         GT0_QPLLPD_IN                    => fmcL12Bank116_gtx_qpll_powerDown,         
         GT0_QPLLRESET_IN                 => '0'              
      );                               
                                       
   fmcL12Bank116_ber_gen: for i in 0 to 3 generate
      
      -- BANK116 = (5,6,10,4)
      
      fmcL12Bank116_berCnt: entity work.gtx_ber_cntr
         port map (
            RESET_I 								=> (not rxReady_from_fmcL12_dp(BANK116(i))) or gtx_bert_reset,
            CLK_I 								=> txOutClk_from_fmcL12_dp_buff(5),
            TRIGGER_I 							=> gtx_bert_trigger,
            ERROR_FLAG_I	 					=> rxPrbsErr_from_fmcL12_dp(BANK116(i)),
            ERROR_CNT_O 						=> errCnt_from_fmcL12_berCnt(BANK116(i)),
            BITS_CNT_O 							=> bitsCnt_from_fmcL12_berCnt(BANK116(i))	
         );		
         
      fmcL12Bank116_cdc: entity work.clk_domain_bridge			
         port map (
            WRCLK_I 								=> txOutClk_from_fmcL12_dp_buff(5),
            RDCLK_I 								=> ipb_clk_i,
            WDATA_I( 63 downto  0)			=> errCnt_from_fmcL12_berCnt(BANK116(i)),
            WDATA_I(127 downto 64)			=> bitsCnt_from_fmcL12_berCnt(BANK116(i)),
            RDATA_O( 63 downto  0) 			=> errCnt_from_fmcL12_cdc(BANK116(i)),
            RDATA_O(127 downto 64) 			=> bitsCnt_from_fmcL12_cdc(BANK116(i))					
         );
   
   end generate;   

   -- AMC:
   -------
   
   -- Bank 115 (osc125_b -> 125MHz):   -- Note!! GTX mapping: gtx_0 -> amc_7
                                       --                     gtx_1 -> amc_6
                                       --                     gtx_2 -> amc_5
                                       --                     gtx_3 -> amc_4
   
   amcBank115_txOutClkBufg: bufg
      port map (
         I                                => txOutClk_from_amc(7),
         O                                => txOutClk_from_amc_buff(7)
      );
   
   amcBank115_gtx: entity work.gtx_5gbps_125mhz_x4_init_mbm
      generic map (
         EXAMPLE_SIM_GTRESET_SPEEDUP      => "TRUE",     
         EXAMPLE_SIMULATION               => 0,          
         EXAMPLE_USE_CHIPSCOPE            => 0)
      port map (
         SYSCLK_IN                        => osc125_b_bufg,                         
         SOFT_RESET_IN                    => ipb_rst_i,              
         DONT_RESET_ON_DATA_ERROR_IN      => '1',             
         GT0_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(7),             
         GT0_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(7),             
         GT0_DATA_VALID_IN                => '1',                 
         GT1_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(6),                
         GT1_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(6),                
         GT1_DATA_VALID_IN                => '1',                             
         GT2_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(5),                
         GT2_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(5),                
         GT2_DATA_VALID_IN                => '1',                             
         GT3_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(4),                
         GT3_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(4),                
         GT3_DATA_VALID_IN                => '1',                             
         ---------------------------------
         GT0_CPLLFBCLKLOST_OUT            => open,  
         GT0_CPLLLOCK_OUT                 => open,  
         GT0_CPLLLOCKDETCLK_IN            => osc125_b_bufg, 
         GT0_CPLLPD_IN                    => amc_gtx_cpll_powerDown(7),         
         GT0_CPLLRESET_IN                 => '0',  
         GT0_GTREFCLK0_IN                 => osc125_b,
         GT0_DRPADDR_IN                   => (others => '0'),                     
         GT0_DRPCLK_IN                    => '0',             
         GT0_DRPDI_IN                     => (others => '0'),               
         GT0_DRPDO_OUT                    => open,             
         GT0_DRPEN_IN                     => '0',              
         GT0_DRPRDY_OUT                   => open,             
         GT0_DRPWE_IN                     => '0',              
         GT0_LOOPBACK_IN                  => gtx_loopback,
         GT0_RXPD_IN                      => (amc_gtx_rx_powerDown(7),amc_gtx_rx_powerDown(7)),
         GT0_TXPD_IN                      => (amc_gtx_tx_powerDown(7),amc_gtx_tx_powerDown(7)),             
         GT0_RXUSERRDY_IN                 => '1',             
         GT0_EYESCANDATAERROR_OUT         => open,               
         GT0_RXCDRLOCK_OUT                => open,               
         GT0_RXUSRCLK_IN                  => txOutClk_from_amc_buff(7),                   
         GT0_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(7),                       
         GT0_RXDATA_OUT                   => open,                   
         GT0_RXPRBSERR_OUT                => rxPrbsErr_from_amc(7),                 
         GT0_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)                   
         GT0_RXPRBSCNTRESET_IN            => '0',                 
         GT0_GTXRXP_IN                    => k7_amc_rx_p(7),                 
         GT0_GTXRXN_IN                    => k7_amc_rx_n(7),                 
         GT0_GTRXRESET_IN                 => amc_gtx_rx_reset(7),                 
         GT0_RXPMARESET_IN                => '0',                 
         GT0_RXPOLARITY_IN                => '0',                 
         GT0_RXRESETDONE_OUT              => open,                   
         GT0_GTTXRESET_IN                 => amc_gtx_tx_reset(7),                 
         GT0_TXUSERRDY_IN                 => '1',              
         GT0_TXUSRCLK_IN                  => txOutClk_from_amc_buff(7),                    
         GT0_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(7),              
         GT0_TXPRBSFORCEERR_IN            => '0',              
         GT0_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)              
         GT0_TXDATA_IN                    => (others => '0'),                 
         GT0_GTXTXN_OUT                   => amc_tx_n(7),             
         GT0_GTXTXP_OUT                   => amc_tx_p(7),             
         GT0_TXOUTCLK_OUT                 => txOutClk_from_amc(7),              
         GT0_TXOUTCLKFABRIC_OUT           => open,                
         GT0_TXOUTCLKPCS_OUT              => open,                
         GT0_TXRESETDONE_OUT              => open,                
         GT0_TXPOLARITY_IN                => '0',              
         GT0_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)                     
         ---------------------------------
         GT1_CPLLFBCLKLOST_OUT            => open,  
         GT1_CPLLLOCK_OUT                 => open,  
         GT1_CPLLLOCKDETCLK_IN            => osc125_b_bufg,  
         GT1_CPLLPD_IN                    => amc_gtx_cpll_powerDown(6),         
         GT1_CPLLRESET_IN                 => '0',  
         GT1_GTREFCLK0_IN                 => osc125_b,
         GT1_DRPADDR_IN                   => (others => '0'),                     
         GT1_DRPCLK_IN                    => '0',             
         GT1_DRPDI_IN                     => (others => '0'),               
         GT1_DRPDO_OUT                    => open,             
         GT1_DRPEN_IN                     => '0',              
         GT1_DRPRDY_OUT                   => open,             
         GT1_DRPWE_IN                     => '0',              
         GT1_LOOPBACK_IN                  => gtx_loopback,
         GT1_RXPD_IN                      => (amc_gtx_rx_powerDown(6),amc_gtx_rx_powerDown(6)),
         GT1_TXPD_IN                      => (amc_gtx_tx_powerDown(6),amc_gtx_tx_powerDown(6)),             
         GT1_RXUSERRDY_IN                 => '1',             
         GT1_EYESCANDATAERROR_OUT         => open,               
         GT1_RXCDRLOCK_OUT                => open,               
         GT1_RXUSRCLK_IN                  => txOutClk_from_amc_buff(7),             
         GT1_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(7),             
         GT1_RXDATA_OUT                   => open,                   
         GT1_RXPRBSERR_OUT                => rxPrbsErr_from_amc(6),               
         GT1_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT1_RXPRBSCNTRESET_IN            => '0',                 
         GT1_GTXRXP_IN                    => k7_amc_rx_p(6),                 
         GT1_GTXRXN_IN                    => k7_amc_rx_n(6),                 
         GT1_GTRXRESET_IN                 => amc_gtx_rx_reset(6),                 
         GT1_RXPMARESET_IN                => '0',                 
         GT1_RXPOLARITY_IN                => '0',                 
         GT1_RXRESETDONE_OUT              => open,                   
         GT1_GTTXRESET_IN                 => amc_gtx_tx_reset(6),                 
         GT1_TXUSERRDY_IN                 => '1',              
         GT1_TXUSRCLK_IN                  => txOutClk_from_amc_buff(7),             
         GT1_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(7),             
         GT1_TXPRBSFORCEERR_IN            => '0',              
         GT1_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT1_TXDATA_IN                    => (others => '0'),                 
         GT1_GTXTXN_OUT                   => amc_tx_n(6),             
         GT1_GTXTXP_OUT                   => amc_tx_p(6),             
         GT1_TXOUTCLK_OUT                 => open,              
         GT1_TXOUTCLKFABRIC_OUT           => open,                
         GT1_TXOUTCLKPCS_OUT              => open,                
         GT1_TXRESETDONE_OUT              => open,                
         GT1_TXPOLARITY_IN                => '0',              
         GT1_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT2_CPLLFBCLKLOST_OUT            => open,  
         GT2_CPLLLOCK_OUT                 => open,  
         GT2_CPLLLOCKDETCLK_IN            => osc125_b_bufg,  
         GT2_CPLLPD_IN                    => amc_gtx_cpll_powerDown(5),         
         GT2_CPLLRESET_IN                 => '0',  
         GT2_GTREFCLK0_IN                 => osc125_b,
         GT2_DRPADDR_IN                   => (others => '0'),                     
         GT2_DRPCLK_IN                    => '0',             
         GT2_DRPDI_IN                     => (others => '0'),               
         GT2_DRPDO_OUT                    => open,             
         GT2_DRPEN_IN                     => '0',              
         GT2_DRPRDY_OUT                   => open,             
         GT2_DRPWE_IN                     => '0',              
         GT2_LOOPBACK_IN                  => gtx_loopback,
         GT2_RXPD_IN                      => (amc_gtx_rx_powerDown(5),amc_gtx_rx_powerDown(4)),
         GT2_TXPD_IN                      => (amc_gtx_tx_powerDown(5),amc_gtx_tx_powerDown(4)),             
         GT2_RXUSERRDY_IN                 => '1',             
         GT2_EYESCANDATAERROR_OUT         => open,               
         GT2_RXCDRLOCK_OUT                => open,               
         GT2_RXUSRCLK_IN                  => txOutClk_from_amc_buff(7),             
         GT2_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(7),             
         GT2_RXDATA_OUT                   => open,                   
         GT2_RXPRBSERR_OUT                => rxPrbsErr_from_amc(5),               
         GT2_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT2_RXPRBSCNTRESET_IN            => '0',                 
         GT2_GTXRXP_IN                    => k7_amc_rx_p(5),                 
         GT2_GTXRXN_IN                    => k7_amc_rx_n(5),                 
         GT2_GTRXRESET_IN                 => amc_gtx_rx_reset(5),                 
         GT2_RXPMARESET_IN                => '0',                 
         GT2_RXPOLARITY_IN                => '0',                 
         GT2_RXRESETDONE_OUT              => open,                   
         GT2_GTTXRESET_IN                 => amc_gtx_tx_reset(5),                 
         GT2_TXUSERRDY_IN                 => '1',              
         GT2_TXUSRCLK_IN                  => txOutClk_from_amc_buff(7),             
         GT2_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(7),             
         GT2_TXPRBSFORCEERR_IN            => '0',              
         GT2_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT2_TXDATA_IN                    => (others => '0'),                 
         GT2_GTXTXN_OUT                   => amc_tx_n(5),             
         GT2_GTXTXP_OUT                   => amc_tx_p(5),             
         GT2_TXOUTCLK_OUT                 => open,              
         GT2_TXOUTCLKFABRIC_OUT           => open,                
         GT2_TXOUTCLKPCS_OUT              => open,                
         GT2_TXRESETDONE_OUT              => open,                
         GT2_TXPOLARITY_IN                => '0',              
         GT2_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT3_CPLLFBCLKLOST_OUT            => open,  
         GT3_CPLLLOCK_OUT                 => open,  
         GT3_CPLLLOCKDETCLK_IN            => osc125_b_bufg,  
         GT3_CPLLPD_IN                    => amc_gtx_cpll_powerDown(4),         
         GT3_CPLLRESET_IN                 => '0',  
         GT3_GTREFCLK0_IN                 => osc125_b,
         GT3_DRPADDR_IN                   => (others => '0'),                     
         GT3_DRPCLK_IN                    => '0',             
         GT3_DRPDI_IN                     => (others => '0'),               
         GT3_DRPDO_OUT                    => open,             
         GT3_DRPEN_IN                     => '0',              
         GT3_DRPRDY_OUT                   => open,             
         GT3_DRPWE_IN                     => '0',              
         GT3_LOOPBACK_IN                  => gtx_loopback,
         GT3_RXPD_IN                      => (amc_gtx_rx_powerDown(4),amc_gtx_rx_powerDown(4)),
         GT3_TXPD_IN                      => (amc_gtx_tx_powerDown(4),amc_gtx_tx_powerDown(4)),             
         GT3_RXUSERRDY_IN                 => '1',             
         GT3_EYESCANDATAERROR_OUT         => open,               
         GT3_RXCDRLOCK_OUT                => open,               
         GT3_RXUSRCLK_IN                  => txOutClk_from_amc_buff(7),             
         GT3_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(7),             
         GT3_RXDATA_OUT                   => open,                   
         GT3_RXPRBSERR_OUT                => rxPrbsErr_from_amc(4),               
         GT3_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT3_RXPRBSCNTRESET_IN            => '0',                 
         GT3_GTXRXP_IN                    => k7_amc_rx_p(4),                 
         GT3_GTXRXN_IN                    => k7_amc_rx_n(4),                 
         GT3_GTRXRESET_IN                 => amc_gtx_rx_reset(4),                 
         GT3_RXPMARESET_IN                => '0',                 
         GT3_RXPOLARITY_IN                => '0',                 
         GT3_RXRESETDONE_OUT              => open,                   
         GT3_GTTXRESET_IN                 => amc_gtx_tx_reset(4),                 
         GT3_TXUSERRDY_IN                 => '1',              
         GT3_TXUSRCLK_IN                  => txOutClk_from_amc_buff(7),             
         GT3_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(7),             
         GT3_TXPRBSFORCEERR_IN            => '0',              
         GT3_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT3_TXDATA_IN                    => (others => '0'),                 
         GT3_GTXTXN_OUT                   => amc_tx_n(4),             
         GT3_GTXTXP_OUT                   => amc_tx_p(4),             
         GT3_TXOUTCLK_OUT                 => open,              
         GT3_TXOUTCLKFABRIC_OUT           => open,                
         GT3_TXOUTCLKPCS_OUT              => open,                
         GT3_TXRESETDONE_OUT              => open,                
         GT3_TXPOLARITY_IN                => '0',              
         GT3_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT0_GTREFCLK0_COMMON_IN          => '0',              
         GT0_QPLLLOCK_OUT                 => open,              
         GT0_QPLLLOCKDETCLK_IN            => '0',              
         GT0_QPLLRESET_IN                 => '0'              
      );                               
                                       
   amcBank115_ber_gen: for i in 0 to 3 generate
      
      -- BANK115 = (7,6,5,4)
      
      amcBank115_berCnt: entity work.gtx_ber_cntr
         port map (
            RESET_I 								=> (not rxReady_from_amc(BANK115(i))) or gtx_bert_reset,
            CLK_I 								=> txOutClk_from_amc_buff(7),
            TRIGGER_I 							=> gtx_bert_trigger,
            ERROR_FLAG_I	 					=> rxPrbsErr_from_amc(BANK115(i)),
            ERROR_CNT_O 						=> errCnt_from_amc_berCnt(BANK115(i)),
            BITS_CNT_O 							=> bitsCnt_from_amc_berCnt(BANK115(i))	
         );		
         
      amcBank115_cdc: entity work.clk_domain_bridge			
         port map (
            WRCLK_I 								=> txOutClk_from_amc_buff(7),
            RDCLK_I 								=> ipb_clk_i,
            WDATA_I( 63 downto  0)			=> errCnt_from_amc_berCnt(BANK115(i)),
            WDATA_I(127 downto 64)			=> bitsCnt_from_amc_berCnt(BANK115(i)),
            RDATA_O( 63 downto  0) 			=> errCnt_from_amc_cdc(BANK115(i)),
            RDATA_O(127 downto 64) 			=> bitsCnt_from_amc_cdc(BANK115(i))					
         );
   
   end generate;      

   -- Bank 114 (osc_xpoint_b -> 125MHz):   -- Note!! GTX mapping: gtx_0 -> amc_8
                                           --                     gtx_1 -> amc_9
                                           --                     gtx_2 -> amc_10
                                           --                     gtx_3 -> amc_11
   
   amcBank114_txOutClkBufg: bufg
      port map (
         I                                => txOutClk_from_amc(8),
         O                                => txOutClk_from_amc_buff(8)
      );
   
   amcBank114_gtx: entity work.gtx_5gbps_125mhz_x4_init_mbm
      generic map (
         EXAMPLE_SIM_GTRESET_SPEEDUP      => "TRUE",     
         EXAMPLE_SIMULATION               => 0,          
         EXAMPLE_USE_CHIPSCOPE            => 0)
      port map (
         SYSCLK_IN                        => osc_xpoint_b_bufg,                         
         SOFT_RESET_IN                    => ipb_rst_i,              
         DONT_RESET_ON_DATA_ERROR_IN      => '1',             
         GT0_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(8),             
         GT0_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(8),             
         GT0_DATA_VALID_IN                => '1',                 
         GT1_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(9),                
         GT1_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(9),                
         GT1_DATA_VALID_IN                => '1',                             
         GT2_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(10),                
         GT2_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(10),                
         GT2_DATA_VALID_IN                => '1',                             
         GT3_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(11),                
         GT3_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(11),                
         GT3_DATA_VALID_IN                => '1',                             
         ---------------------------------
         GT0_CPLLFBCLKLOST_OUT            => open,   
         GT0_CPLLLOCK_OUT                 => open,   
         GT0_CPLLLOCKDETCLK_IN            => osc_xpoint_b_bufg,      
         GT0_CPLLPD_IN                    => amc_gtx_cpll_powerDown(8),
         GT0_CPLLRESET_IN                 => '0',   
         GT0_GTREFCLK0_IN                 => osc_xpoint_b,
         GT0_DRPADDR_IN                   => (others => '0'),                     
         GT0_DRPCLK_IN                    => '0',             
         GT0_DRPDI_IN                     => (others => '0'),               
         GT0_DRPDO_OUT                    => open,             
         GT0_DRPEN_IN                     => '0',              
         GT0_DRPRDY_OUT                   => open,             
         GT0_DRPWE_IN                     => '0',              
         GT0_LOOPBACK_IN                  => gtx_loopback,
         GT0_RXPD_IN                      => (amc_gtx_rx_powerDown(8),amc_gtx_rx_powerDown(8)),
         GT0_TXPD_IN                      => (amc_gtx_tx_powerDown(8),amc_gtx_tx_powerDown(8)),              
         GT0_RXUSERRDY_IN                 => '1',             
         GT0_EYESCANDATAERROR_OUT         => open,               
         GT0_RXCDRLOCK_OUT                => open,               
         GT0_RXUSRCLK_IN                  => txOutClk_from_amc_buff(8),                   
         GT0_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(8),                       
         GT0_RXDATA_OUT                   => open,                   
         GT0_RXPRBSERR_OUT                => rxPrbsErr_from_amc(8),                 
         GT0_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)                   
         GT0_RXPRBSCNTRESET_IN            => '0',                 
         GT0_GTXRXP_IN                    => k7_amc_rx_p(8),                 
         GT0_GTXRXN_IN                    => k7_amc_rx_n(8),                 
         GT0_GTRXRESET_IN                 => amc_gtx_rx_reset(8),                 
         GT0_RXPMARESET_IN                => '0',                 
         GT0_RXPOLARITY_IN                => '0',                 
         GT0_RXRESETDONE_OUT              => open,                   
         GT0_GTTXRESET_IN                 => amc_gtx_tx_reset(8),                 
         GT0_TXUSERRDY_IN                 => '1',              
         GT0_TXUSRCLK_IN                  => txOutClk_from_amc_buff(8),                    
         GT0_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(8),              
         GT0_TXPRBSFORCEERR_IN            => '0',              
         GT0_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)              
         GT0_TXDATA_IN                    => (others => '0'),                 
         GT0_GTXTXN_OUT                   => amc_tx_n(8),             
         GT0_GTXTXP_OUT                   => amc_tx_p(8),             
         GT0_TXOUTCLK_OUT                 => txOutClk_from_amc(8),              
         GT0_TXOUTCLKFABRIC_OUT           => open,                
         GT0_TXOUTCLKPCS_OUT              => open,                
         GT0_TXRESETDONE_OUT              => open,                
         GT0_TXPOLARITY_IN                => '0',              
         GT0_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)                     
         ---------------------------------
         GT1_CPLLFBCLKLOST_OUT            => open,   
         GT1_CPLLLOCK_OUT                 => open,   
         GT1_CPLLLOCKDETCLK_IN            => osc_xpoint_b_bufg,  
         GT1_CPLLPD_IN                    => amc_gtx_cpll_powerDown(9),         
         GT1_CPLLRESET_IN                 => '0',   
         GT1_GTREFCLK0_IN                 => osc_xpoint_b,
         GT1_DRPADDR_IN                   => (others => '0'),                     
         GT1_DRPCLK_IN                    => '0',             
         GT1_DRPDI_IN                     => (others => '0'),               
         GT1_DRPDO_OUT                    => open,             
         GT1_DRPEN_IN                     => '0',              
         GT1_DRPRDY_OUT                   => open,             
         GT1_DRPWE_IN                     => '0',              
         GT1_LOOPBACK_IN                  => gtx_loopback, 
         GT1_RXPD_IN                      => (amc_gtx_rx_powerDown(9),amc_gtx_rx_powerDown(9)),
         GT1_TXPD_IN                      => (amc_gtx_tx_powerDown(9),amc_gtx_tx_powerDown(9)),              
         GT1_RXUSERRDY_IN                 => '1',             
         GT1_EYESCANDATAERROR_OUT         => open,               
         GT1_RXCDRLOCK_OUT                => open,               
         GT1_RXUSRCLK_IN                  => txOutClk_from_amc_buff(8),             
         GT1_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(8),             
         GT1_RXDATA_OUT                   => open,                   
         GT1_RXPRBSERR_OUT                => rxPrbsErr_from_amc(9),               
         GT1_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT1_RXPRBSCNTRESET_IN            => '0',                 
         GT1_GTXRXP_IN                    => k7_amc_rx_p(9),                 
         GT1_GTXRXN_IN                    => k7_amc_rx_n(9),                 
         GT1_GTRXRESET_IN                 => amc_gtx_rx_reset(9),                 
         GT1_RXPMARESET_IN                => '0',                 
         GT1_RXPOLARITY_IN                => '0',                 
         GT1_RXRESETDONE_OUT              => open,                   
         GT1_GTTXRESET_IN                 => amc_gtx_tx_reset(9),                 
         GT1_TXUSERRDY_IN                 => '1',              
         GT1_TXUSRCLK_IN                  => txOutClk_from_amc_buff(8),             
         GT1_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(8),             
         GT1_TXPRBSFORCEERR_IN            => '0',              
         GT1_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT1_TXDATA_IN                    => (others => '0'),                 
         GT1_GTXTXN_OUT                   => amc_tx_n(9),             
         GT1_GTXTXP_OUT                   => amc_tx_p(9),             
         GT1_TXOUTCLK_OUT                 => open,              
         GT1_TXOUTCLKFABRIC_OUT           => open,                
         GT1_TXOUTCLKPCS_OUT              => open,                
         GT1_TXRESETDONE_OUT              => open,                
         GT1_TXPOLARITY_IN                => '0',              
         GT1_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT2_CPLLFBCLKLOST_OUT            => open,   
         GT2_CPLLLOCK_OUT                 => open,   
         GT2_CPLLLOCKDETCLK_IN            => osc_xpoint_b_bufg,
         GT2_CPLLPD_IN                    => amc_gtx_cpll_powerDown(10),                  
         GT2_CPLLRESET_IN                 => '0',   
         GT2_GTREFCLK0_IN                 => osc_xpoint_b,
         GT2_DRPADDR_IN                   => (others => '0'),                     
         GT2_DRPCLK_IN                    => '0',             
         GT2_DRPDI_IN                     => (others => '0'),               
         GT2_DRPDO_OUT                    => open,             
         GT2_DRPEN_IN                     => '0',              
         GT2_DRPRDY_OUT                   => open,             
         GT2_DRPWE_IN                     => '0',              
         GT2_LOOPBACK_IN                  => gtx_loopback,
         GT2_RXPD_IN                      => (amc_gtx_rx_powerDown(10),amc_gtx_rx_powerDown(10)),
         GT2_TXPD_IN                      => (amc_gtx_tx_powerDown(10),amc_gtx_tx_powerDown(10)),              
         GT2_RXUSERRDY_IN                 => '1',             
         GT2_EYESCANDATAERROR_OUT         => open,               
         GT2_RXCDRLOCK_OUT                => open,               
         GT2_RXUSRCLK_IN                  => txOutClk_from_amc_buff(8),             
         GT2_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(8),             
         GT2_RXDATA_OUT                   => open,                   
         GT2_RXPRBSERR_OUT                => rxPrbsErr_from_amc(10),               
         GT2_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT2_RXPRBSCNTRESET_IN            => '0',                 
         GT2_GTXRXP_IN                    => k7_amc_rx_p(10),                 
         GT2_GTXRXN_IN                    => k7_amc_rx_n(10),                 
         GT2_GTRXRESET_IN                 => amc_gtx_rx_reset(10),                 
         GT2_RXPMARESET_IN                => '0',                 
         GT2_RXPOLARITY_IN                => '0',                 
         GT2_RXRESETDONE_OUT              => open,                   
         GT2_GTTXRESET_IN                 => amc_gtx_tx_reset(10),                 
         GT2_TXUSERRDY_IN                 => '1',              
         GT2_TXUSRCLK_IN                  => txOutClk_from_amc_buff(8),             
         GT2_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(8),             
         GT2_TXPRBSFORCEERR_IN            => '0',              
         GT2_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT2_TXDATA_IN                    => (others => '0'),                 
         GT2_GTXTXN_OUT                   => amc_tx_n(10),             
         GT2_GTXTXP_OUT                   => amc_tx_p(10),             
         GT2_TXOUTCLK_OUT                 => open,              
         GT2_TXOUTCLKFABRIC_OUT           => open,                
         GT2_TXOUTCLKPCS_OUT              => open,                
         GT2_TXRESETDONE_OUT              => open,                
         GT2_TXPOLARITY_IN                => '0',              
         GT2_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT3_CPLLFBCLKLOST_OUT            => open,   
         GT3_CPLLLOCK_OUT                 => open,   
         GT3_CPLLLOCKDETCLK_IN            => osc_xpoint_b_bufg, 
         GT3_CPLLPD_IN                    => amc_gtx_cpll_powerDown(11),         
         GT3_CPLLRESET_IN                 => '0',   
         GT3_GTREFCLK0_IN                 => osc_xpoint_b,
         GT3_DRPADDR_IN                   => (others => '0'),                     
         GT3_DRPCLK_IN                    => '0',             
         GT3_DRPDI_IN                     => (others => '0'),               
         GT3_DRPDO_OUT                    => open,             
         GT3_DRPEN_IN                     => '0',              
         GT3_DRPRDY_OUT                   => open,             
         GT3_DRPWE_IN                     => '0',              
         GT3_LOOPBACK_IN                  => gtx_loopback,
         GT3_RXPD_IN                      => (amc_gtx_rx_powerDown(11),amc_gtx_rx_powerDown(11)),
         GT3_TXPD_IN                      => (amc_gtx_tx_powerDown(11),amc_gtx_tx_powerDown(11)),              
         GT3_RXUSERRDY_IN                 => '1',             
         GT3_EYESCANDATAERROR_OUT         => open,               
         GT3_RXCDRLOCK_OUT                => open,               
         GT3_RXUSRCLK_IN                  => txOutClk_from_amc_buff(8),             
         GT3_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(8),             
         GT3_RXDATA_OUT                   => open,                   
         GT3_RXPRBSERR_OUT                => rxPrbsErr_from_amc(11),               
         GT3_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT3_RXPRBSCNTRESET_IN            => '0',                 
         GT3_GTXRXP_IN                    => k7_amc_rx_p(11),                 
         GT3_GTXRXN_IN                    => k7_amc_rx_n(11),                 
         GT3_GTRXRESET_IN                 => amc_gtx_rx_reset(11),                 
         GT3_RXPMARESET_IN                => '0',                 
         GT3_RXPOLARITY_IN                => '0',                 
         GT3_RXRESETDONE_OUT              => open,                   
         GT3_GTTXRESET_IN                 => amc_gtx_tx_reset(11),                 
         GT3_TXUSERRDY_IN                 => '1',              
         GT3_TXUSRCLK_IN                  => txOutClk_from_amc_buff(8),             
         GT3_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(8),             
         GT3_TXPRBSFORCEERR_IN            => '0',              
         GT3_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT3_TXDATA_IN                    => (others => '0'),                 
         GT3_GTXTXN_OUT                   => amc_tx_n(11),             
         GT3_GTXTXP_OUT                   => amc_tx_p(11),             
         GT3_TXOUTCLK_OUT                 => open,              
         GT3_TXOUTCLKFABRIC_OUT           => open,                
         GT3_TXOUTCLKPCS_OUT              => open,                
         GT3_TXRESETDONE_OUT              => open,                
         GT3_TXPOLARITY_IN                => '0',              
         GT3_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT0_GTREFCLK0_COMMON_IN          => '0',              
         GT0_QPLLLOCK_OUT                 => open,              
         GT0_QPLLLOCKDETCLK_IN            => '0',              
         GT0_QPLLRESET_IN                 => '0'              
      );                               
                                       
   amcBank114_ber_gen: for i in 0 to 3 generate
      
      -- BANK114 = (8,9,10,11)
      
      amcBank114_berCnt: entity work.gtx_ber_cntr
         port map (
            RESET_I 								=> (not rxReady_from_amc(BANK114(i))) or gtx_bert_reset,
            CLK_I 								=> txOutClk_from_amc_buff(8),
            TRIGGER_I 							=> gtx_bert_trigger,
            ERROR_FLAG_I	 					=> rxPrbsErr_from_amc(BANK114(i)),
            ERROR_CNT_O 						=> errCnt_from_amc_berCnt(BANK114(i)),
            BITS_CNT_O 							=> bitsCnt_from_amc_berCnt(BANK114(i))	
         );		
         
      amcBank114_cdc: entity work.clk_domain_bridge			
         port map (
            WRCLK_I 								=> txOutClk_from_amc_buff(8),
            RDCLK_I 								=> ipb_clk_i,
            WDATA_I( 63 downto  0)			=> errCnt_from_amc_berCnt(BANK114(i)),
            WDATA_I(127 downto 64)			=> bitsCnt_from_amc_berCnt(BANK114(i)),
            RDATA_O( 63 downto  0) 			=> errCnt_from_amc_cdc(BANK114(i)),
            RDATA_O(127 downto 64) 			=> bitsCnt_from_amc_cdc(BANK114(i))					
         );
   
   end generate;

   -- Bank 113 (osc_xpoint_d -> 125MHz):   -- Note!! GTX mapping: gtx_0 -> amc_0 -- Note!! Used by system
                                           --                     gtx_1 -> amc_1
                                           --                     gtx_2 -> amc_2
                                           --                     gtx_3 -> amc_3
   
   amcBank113_txOutClkBufg: bufg
      port map (
         I                                => txOutClk_from_amc(1),
         O                                => txOutClk_from_amc_buff(1)
      );
   
   amcBank113_gtx: entity work.gtx_5gbps_125mhz_x3_init_mbm
      generic map (
         EXAMPLE_SIM_GTRESET_SPEEDUP      => "TRUE",     
         EXAMPLE_SIMULATION               => 0,          
         EXAMPLE_USE_CHIPSCOPE            => 0)
      port map (
         SYSCLK_IN                        => osc_xpoint_d_bufg,                         
         SOFT_RESET_IN                    => ipb_rst_i,              
         DONT_RESET_ON_DATA_ERROR_IN      => '1',             
         GT0_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(1),             
         GT0_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(1),             
         GT0_DATA_VALID_IN                => '1',                 
         GT1_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(2),                
         GT1_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(2),                
         GT1_DATA_VALID_IN                => '1',                             
         GT2_TX_FSM_RESET_DONE_OUT        => txReady_from_amc(3),                
         GT2_RX_FSM_RESET_DONE_OUT        => rxReady_from_amc(3),                
         GT2_DATA_VALID_IN                => '1',                             
         ---------------------------------
         GT0_CPLLFBCLKLOST_OUT            => open,   
         GT0_CPLLLOCK_OUT                 => open,   
         GT0_CPLLLOCKDETCLK_IN            => osc_xpoint_d_bufg,      
         GT0_CPLLPD_IN                    => amc_gtx_cpll_powerDown(1),
         GT0_CPLLRESET_IN                 => '0',   
         GT0_GTREFCLK0_IN                 => osc_xpoint_d,
         GT0_DRPADDR_IN                   => (others => '0'),                     
         GT0_DRPCLK_IN                    => '0',             
         GT0_DRPDI_IN                     => (others => '0'),               
         GT0_DRPDO_OUT                    => open,             
         GT0_DRPEN_IN                     => '0',              
         GT0_DRPRDY_OUT                   => open,             
         GT0_DRPWE_IN                     => '0',              
         GT0_LOOPBACK_IN                  => gtx_loopback,    
         GT0_RXPD_IN                      => (amc_gtx_rx_powerDown(1),amc_gtx_rx_powerDown(1)),
         GT0_TXPD_IN                      => (amc_gtx_tx_powerDown(1),amc_gtx_tx_powerDown(1)),      
         GT0_RXUSERRDY_IN                 => '1',             
         GT0_EYESCANDATAERROR_OUT         => open,               
         GT0_RXCDRLOCK_OUT                => open,               
         GT0_RXUSRCLK_IN                  => txOutClk_from_amc_buff(1),                   
         GT0_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(1),                       
         GT0_RXDATA_OUT                   => open,                   
         GT0_RXPRBSERR_OUT                => rxPrbsErr_from_amc(1),                 
         GT0_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)                   
         GT0_RXPRBSCNTRESET_IN            => '0',                 
         GT0_GTXRXP_IN                    => k7_amc_rx_p(1),                 
         GT0_GTXRXN_IN                    => k7_amc_rx_n(1),                 
         GT0_GTRXRESET_IN                 => amc_gtx_rx_reset(1),                 
         GT0_RXPMARESET_IN                => '0',                 
         GT0_RXPOLARITY_IN                => '0',                 
         GT0_RXRESETDONE_OUT              => open,                   
         GT0_GTTXRESET_IN                 => amc_gtx_tx_reset(1),                 
         GT0_TXUSERRDY_IN                 => '1',              
         GT0_TXUSRCLK_IN                  => txOutClk_from_amc_buff(1),                    
         GT0_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(1),              
         GT0_TXPRBSFORCEERR_IN            => '0',              
         GT0_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)              
         GT0_TXDATA_IN                    => (others => '0'),                 
         GT0_GTXTXN_OUT                   => amc_tx_n(1),             
         GT0_GTXTXP_OUT                   => amc_tx_p(1),             
         GT0_TXOUTCLK_OUT                 => txOutClk_from_amc(1),              
         GT0_TXOUTCLKFABRIC_OUT           => open,                
         GT0_TXOUTCLKPCS_OUT              => open,                
         GT0_TXRESETDONE_OUT              => open,                
         GT0_TXPOLARITY_IN                => '0',              
         GT0_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)                     
         ---------------------------------
         GT1_CPLLFBCLKLOST_OUT            => open,   
         GT1_CPLLLOCK_OUT                 => open,   
         GT1_CPLLLOCKDETCLK_IN            => osc_xpoint_d_bufg,      
         GT1_CPLLPD_IN                    => amc_gtx_cpll_powerDown(2),
         GT1_CPLLRESET_IN                 => '0',   
         GT1_GTREFCLK0_IN                 => osc_xpoint_d,
         GT1_DRPADDR_IN                   => (others => '0'),                     
         GT1_DRPCLK_IN                    => '0',             
         GT1_DRPDI_IN                     => (others => '0'),               
         GT1_DRPDO_OUT                    => open,             
         GT1_DRPEN_IN                     => '0',              
         GT1_DRPRDY_OUT                   => open,             
         GT1_DRPWE_IN                     => '0',              
         GT1_LOOPBACK_IN                  => gtx_loopback,
         GT1_RXPD_IN                      => (amc_gtx_rx_powerDown(2),amc_gtx_rx_powerDown(2)),
         GT1_TXPD_IN                      => (amc_gtx_tx_powerDown(2),amc_gtx_tx_powerDown(2)),               
         GT1_RXUSERRDY_IN                 => '1',             
         GT1_EYESCANDATAERROR_OUT         => open,               
         GT1_RXCDRLOCK_OUT                => open,               
         GT1_RXUSRCLK_IN                  => txOutClk_from_amc_buff(1),             
         GT1_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(1),             
         GT1_RXDATA_OUT                   => open,                   
         GT1_RXPRBSERR_OUT                => rxPrbsErr_from_amc(2),               
         GT1_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT1_RXPRBSCNTRESET_IN            => '0',                 
         GT1_GTXRXP_IN                    => k7_amc_rx_p(2),                 
         GT1_GTXRXN_IN                    => k7_amc_rx_n(2),                 
         GT1_GTRXRESET_IN                 => amc_gtx_rx_reset(2),                 
         GT1_RXPMARESET_IN                => '0',                 
         GT1_RXPOLARITY_IN                => '0',                 
         GT1_RXRESETDONE_OUT              => open,                   
         GT1_GTTXRESET_IN                 => amc_gtx_tx_reset(2),                 
         GT1_TXUSERRDY_IN                 => '1',              
         GT1_TXUSRCLK_IN                  => txOutClk_from_amc_buff(1),             
         GT1_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(1),             
         GT1_TXPRBSFORCEERR_IN            => '0',              
         GT1_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT1_TXDATA_IN                    => (others => '0'),                 
         GT1_GTXTXN_OUT                   => amc_tx_n(2),             
         GT1_GTXTXP_OUT                   => amc_tx_p(2),             
         GT1_TXOUTCLK_OUT                 => open,              
         GT1_TXOUTCLKFABRIC_OUT           => open,                
         GT1_TXOUTCLKPCS_OUT              => open,                
         GT1_TXRESETDONE_OUT              => open,                
         GT1_TXPOLARITY_IN                => '0',              
         GT1_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT2_CPLLFBCLKLOST_OUT            => open,   
         GT2_CPLLLOCK_OUT                 => open,   
         GT2_CPLLLOCKDETCLK_IN            => osc_xpoint_d_bufg,      
         GT2_CPLLPD_IN                    => amc_gtx_cpll_powerDown(3),
         GT2_CPLLRESET_IN                 => '0',   
         GT2_GTREFCLK0_IN                 => osc_xpoint_d,
         GT2_DRPADDR_IN                   => (others => '0'),                     
         GT2_DRPCLK_IN                    => '0',             
         GT2_DRPDI_IN                     => (others => '0'),               
         GT2_DRPDO_OUT                    => open,             
         GT2_DRPEN_IN                     => '0',              
         GT2_DRPRDY_OUT                   => open,             
         GT2_DRPWE_IN                     => '0',              
         GT2_LOOPBACK_IN                  => gtx_loopback,
         GT2_RXPD_IN                      => (amc_gtx_rx_powerDown(3),amc_gtx_rx_powerDown(3)),
         GT2_TXPD_IN                      => (amc_gtx_tx_powerDown(3),amc_gtx_tx_powerDown(3)),               
         GT2_RXUSERRDY_IN                 => '1',             
         GT2_EYESCANDATAERROR_OUT         => open,               
         GT2_RXCDRLOCK_OUT                => open,               
         GT2_RXUSRCLK_IN                  => txOutClk_from_amc_buff(1),             
         GT2_RXUSRCLK2_IN                 => txOutClk_from_amc_buff(1),             
         GT2_RXDATA_OUT                   => open,                   
         GT2_RXPRBSERR_OUT                => rxPrbsErr_from_amc(3),               
         GT2_RXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 137)       
         GT2_RXPRBSCNTRESET_IN            => '0',                 
         GT2_GTXRXP_IN                    => k7_amc_rx_p(3),                 
         GT2_GTXRXN_IN                    => k7_amc_rx_n(3),                 
         GT2_GTRXRESET_IN                 => amc_gtx_rx_reset(3),                 
         GT2_RXPMARESET_IN                => '0',                 
         GT2_RXPOLARITY_IN                => '0',                 
         GT2_RXRESETDONE_OUT              => open,                   
         GT2_GTTXRESET_IN                 => amc_gtx_tx_reset(3),                 
         GT2_TXUSERRDY_IN                 => '1',              
         GT2_TXUSRCLK_IN                  => txOutClk_from_amc_buff(1),             
         GT2_TXUSRCLK2_IN                 => txOutClk_from_amc_buff(1),             
         GT2_TXPRBSFORCEERR_IN            => '0',              
         GT2_TXDIFFCTRL_IN                => "1100",   -- 1,018 mVppd (Xilinx UG476 page 147)  
         GT2_TXDATA_IN                    => (others => '0'),                 
         GT2_GTXTXN_OUT                   => amc_tx_p(3),   -- Note!! Inverted             
         GT2_GTXTXP_OUT                   => amc_tx_n(3),   --          
         GT2_TXOUTCLK_OUT                 => open,              
         GT2_TXOUTCLKFABRIC_OUT           => open,                
         GT2_TXOUTCLKPCS_OUT              => open,                
         GT2_TXRESETDONE_OUT              => open,                
         GT2_TXPOLARITY_IN                => '1',              
         GT2_TXPRBSSEL_IN                 => "100",   -- PRBS-31 (Xilinx UG476 page 213)       
         ---------------------------------
         GT0_GTREFCLK0_COMMON_IN          => '0',              
         GT0_QPLLLOCK_OUT                 => open,              
         GT0_QPLLLOCKDETCLK_IN            => '0',              
         GT0_QPLLRESET_IN                 => '0'              
      );                               
                                       
   amcBank113_ber_gen: for i in 1 to 3 generate
      
      -- BANK113 = (1,2,3)
      
      amcBank113_berCnt: entity work.gtx_ber_cntr
         port map (
            RESET_I 								=> (not rxReady_from_amc(BANK113(i))) or gtx_bert_reset,
            CLK_I 								=> txOutClk_from_amc_buff(1),
            TRIGGER_I 							=> gtx_bert_trigger,
            ERROR_FLAG_I	 					=> rxPrbsErr_from_amc(BANK113(i)),
            ERROR_CNT_O 						=> errCnt_from_amc_berCnt(BANK113(i)),
            BITS_CNT_O 							=> bitsCnt_from_amc_berCnt(BANK113(i))	
         );		
         
      amcBank113_cdc: entity work.clk_domain_bridge			
         port map (
            WRCLK_I 								=> txOutClk_from_amc_buff(1),
            RDCLK_I 								=> ipb_clk_i,
            WDATA_I( 63 downto  0)			=> errCnt_from_amc_berCnt(BANK113(i)),
            WDATA_I(127 downto 64)			=> bitsCnt_from_amc_berCnt(BANK113(i)),
            RDATA_O( 63 downto  0) 			=> errCnt_from_amc_cdc(BANK113(i)),
            RDATA_O(127 downto 64) 			=> bitsCnt_from_amc_cdc(BANK113(i))					
         );
   
   end generate; 
   
   --===========================================--
   -- CERN Test - User I/O
   --===========================================--  
   
   -- FMC L8:
   ----------
   
   fmcL8_p_userIo_test: entity work.bscan_emu
      generic map (	
         BUS_WIDTH								=> 17,
         READ_DELAY								=> 10)
      port map (
         RESET_I 									=> ipb_rst_i or io_test_reset,
         CLK_I 									=> ipb_clk_i,
         TRIGGER_I 								=> io_test_trigger,
         DATA_O 									=> fmcL8_p_pattern_out,
         DATA_I 									=> fmcL8_p_pattern_in,
         BUSY_O 									=> open,
         BUS_ERRORS_O 							=> fmcL8_p_user_io_errors,
         BUS_CORRECT_O 							=> fmcL8_p_user_io_correct
      );	

   fmcL8_p_userIo_buffers_gen: for i in 0 to 16 generate	

		fmcL8_p_userIo_obuf: obuf
			generic map (
				DRIVE 								=> 24,
				IOSTANDARD 							=> "LVCMOS25",
				SLEW 									=> "SLOW")
			port map (
				O 										=> fmc_l8_la_p(i),   
				I 										=> fmcL8_p_pattern_out(i)    
			);
         
      fmcL8_p_userIo_obuf_pullDown: pulldown
			port map (
				O 										=> fmc_l8_la_p(i)   
			);		   
         
		fmcL8_p_userIo_ibuf: ibuf
			generic map (
				IBUF_LOW_PWR 						=> FALSE, 
				IOSTANDARD 							=> "LVCMOS25")
			port map (
				O 										=> fmcL8_p_pattern_in(i),   
				I 										=> fmc_l8_la_p(i+17)    
			);

	end generate;
     
   ---------------------------------------
	  
   fmcL8_n_userIo_test: entity work.bscan_emu
      generic map (	
         BUS_WIDTH								=> 17,
         READ_DELAY								=> 10)
      port map (
         RESET_I 									=> ipb_rst_i or io_test_reset,
         CLK_I 									=> ipb_clk_i,
         TRIGGER_I 								=> io_test_trigger,
         DATA_O 									=> fmcL8_n_pattern_out,
         DATA_I 									=> fmcL8_n_pattern_in,
         BUSY_O 									=> open,
         BUS_ERRORS_O 							=> fmcL8_n_user_io_errors,
         BUS_CORRECT_O 							=> fmcL8_n_user_io_correct
      );	

   fmcL8_n_userIo_buffers_gen: for i in 0 to 16 generate	

		fmcL8_n_userIo_obuf: obuf
			generic map (
				DRIVE 								=> 24,
				IOSTANDARD 							=> "LVCMOS25",
				SLEW 									=> "SLOW")
			port map (
				O 										=> fmc_l8_la_n(i),   
				I 										=> fmcL8_n_pattern_out(i)    
			);

      fmcL8_n_userIo_obuf_pullDown: pulldown
			port map (
				O 										=> fmc_l8_la_n(i)   
			);				
         
		fmcL8_n_userIo_ibuf: ibuf
			generic map (
				IBUF_LOW_PWR 						=> FALSE, 
				IOSTANDARD 							=> "LVCMOS25")
			port map (
				O 										=> fmcL8_n_pattern_in(i),   
				I 										=> fmc_l8_la_n(i+17)    
			);
		
	end generate;	

   -- FMC L12:
   -----------
   
   fmcL12_p_userIo_test: entity work.bscan_emu
      generic map (	
         BUS_WIDTH								=> 17,
         READ_DELAY								=> 10)
      port map (
         RESET_I 									=> ipb_rst_i or io_test_reset,
         CLK_I 									=> ipb_clk_i,
         TRIGGER_I 								=> io_test_trigger,
         DATA_O 									=> fmcL12_p_pattern_out,
         DATA_I 									=> fmcL12_p_pattern_in,
         BUSY_O 									=> open,
         BUS_ERRORS_O 							=> fmcL12_p_user_io_errors,
         BUS_CORRECT_O 							=> fmcL12_p_user_io_correct
      );	

   fmcL12_p_userIo_buffers_gen: for i in 0 to 16 generate	

		fmcL12_p_userIo_obuf: obuf
			generic map (
				DRIVE 								=> 24,
				IOSTANDARD 							=> "LVCMOS25",
				SLEW 									=> "SLOW")
			port map (
				O 										=> fmc_l12_la_p(i),   
				I 										=> fmcL12_p_pattern_out(i)    
			);

      fmcL12_p_userIo_obuf_pullDown: pulldown
			port map (
				O 										=> fmc_l12_la_p(i)   
			);		
         
		fmcL12_p_userIo_ibuf: ibuf
			generic map (
				IBUF_LOW_PWR 						=> FALSE, 
				IOSTANDARD 							=> "LVCMOS25")
			port map (
				O 										=> fmcL12_p_pattern_in(i),   
				I 										=> fmc_l12_la_p(i+17)    
			);

	end generate;
     
   ---------------------------------------
	  
   fmcL12_n_userIo_test: entity work.bscan_emu
      generic map (	
         BUS_WIDTH								=> 17,
         READ_DELAY								=> 10)
      port map (
         RESET_I 									=> ipb_rst_i or io_test_reset,
         CLK_I 									=> ipb_clk_i,
         TRIGGER_I 								=> io_test_trigger,
         DATA_O 									=> fmcL12_n_pattern_out,
         DATA_I 									=> fmcL12_n_pattern_in,
         BUSY_O 									=> open,
         BUS_ERRORS_O 							=> fmcL12_n_user_io_errors,
         BUS_CORRECT_O 							=> fmcL12_n_user_io_correct
      );	

   fmcL12_n_userIo_buffers_gen: for i in 0 to 16 generate	

		fmcL12_n_userIo_obuf: obuf
			generic map (
				DRIVE 								=> 24,
				IOSTANDARD 							=> "LVCMOS25",
				SLEW 									=> "SLOW")
			port map (
				O 										=> fmc_l12_la_n(i),   
				I 										=> fmcL12_n_pattern_out(i)    
			);	
         
      fmcL12_n_userIo_obuf_pullDown: pulldown
			port map (
				O 										=> fmc_l12_la_n(i)   
			);		
         
		fmcL12_n_userIo_ibuf: ibuf
			generic map (
				IBUF_LOW_PWR 						=> FALSE, 
				IOSTANDARD 							=> "LVCMOS25")
			port map (
				O 										=> fmcL12_n_pattern_in(i),   
				I 										=> fmc_l12_la_n(i+17)    
         );   
   
	end generate;	

   -- AMC:
   -------
	
   amc_p_userIo_test: entity work.bscan_emu
      generic map (	
         BUS_WIDTH								=> 4,
         READ_DELAY								=> 10)
      port map (
         RESET_I 									=> ipb_rst_i or io_test_reset,
         CLK_I 									=> ipb_clk_i,
         TRIGGER_I 								=> io_test_trigger,
         DATA_O 									=> amc_p_pattern_out,
         DATA_I 									=> amc_p_pattern_in,
         BUSY_O 									=> open,
         BUS_ERRORS_O 							=> amc_p_user_io_errors,
         BUS_CORRECT_O 							=> amc_p_user_io_correct
      );	

   amc_p_userIo_buffers_gen: for i in 12 to 15 generate	

		fmcL12_p_userIo_obuf: obuf
			generic map (
				DRIVE 								=> 24,
				IOSTANDARD 							=> "LVCMOS25",
				SLEW 									=> "SLOW")
			port map (
				O 										=> amc_tx_p(i),   
				I 										=> amc_p_pattern_out(i)    
			);	
         
		amc_p_userIo_obuf_pullDown: pulldown
			port map (
				O 										=> amc_tx_p(i)   
			);			
      
      amc_p_userIo_ibuf: ibuf
			generic map (
				IBUF_LOW_PWR 						=> FALSE, 
				IOSTANDARD 							=> "LVCMOS25")
			port map (
				O 										=> amc_p_pattern_in(i),   
				I 										=> k7_amc_rx_p(i)    
			);

	end generate;
     
   ---------------------------------------
	  
   amc_n_userIo_test: entity work.bscan_emu
      generic map (	
         BUS_WIDTH								=> 4,
         READ_DELAY								=> 10)
      port map (
         RESET_I 									=> ipb_rst_i or io_test_reset,
         CLK_I 									=> ipb_clk_i,
         TRIGGER_I 								=> io_test_trigger,
         DATA_O 									=> amc_n_pattern_out,
         DATA_I 									=> amc_n_pattern_in,
         BUSY_O 									=> open,
         BUS_ERRORS_O 							=> amc_n_user_io_errors,
         BUS_CORRECT_O 							=> amc_n_user_io_correct
      );	

   amc_n_userIo_buffers_gen: for i in 12 to 15 generate	

		amc_n_userIo_obuf: obuf
			generic map (
				DRIVE 								=> 24,
				IOSTANDARD 							=> "LVCMOS25",
				SLEW 									=> "SLOW")
			port map (
				O 										=> amc_tx_n(i),   
				I 										=> amc_n_pattern_out(i)    
			);	
      
      amc_n_userIo_obuf_pullDown: pulldown
			port map (
				O 										=> amc_tx_n(i)   
			);			
         
		amc_n_userIo_ibuf: ibuf
			generic map (
				IBUF_LOW_PWR 						=> FALSE, 
				IOSTANDARD 							=> "LVCMOS25")
			port map (
				O 										=> amc_n_pattern_in(i),   
				I 										=> k7_amc_rx_n(i)    
			);

	end generate;	
	

end structural;