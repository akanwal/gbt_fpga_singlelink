library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.ipbus.all;
use work.system_package.all;

--! user packages
use work.user_package.all;
use work.user_version_package.all;

library unisim;
use unisim.vcomponents.all;

entity user_logic is 
port
(
	--## pcie clk
	pcie_clk_p							: in		std_logic;
	pcie_clk_n							: in		std_logic;
	
	--## osc mgt clk
	osc125_b_p							: in		std_logic;
	osc125_b_n							: in		std_logic;
			
	osc_xpoint_a_p						: in		std_logic;
	osc_xpoint_a_n						: in		std_logic;
	osc_xpoint_b_p						: in		std_logic;
	osc_xpoint_b_n						: in		std_logic;
	osc_xpoint_c_p						: in		std_logic;
	osc_xpoint_c_n						: in		std_logic;
	osc_xpoint_d_p						: in		std_logic;
	osc_xpoint_d_n						: in		std_logic;
			
	--## cdce mgt clk		
	ttc_mgt_xpoint_a_p				: in		std_logic;
	ttc_mgt_xpoint_a_n				: in		std_logic;
	ttc_mgt_xpoint_b_p				: in		std_logic;
	ttc_mgt_xpoint_b_n				: in		std_logic;
	ttc_mgt_xpoint_c_p				: in		std_logic;
	ttc_mgt_xpoint_c_n				: in		std_logic;
			
	--# fmc mgt clk		
	fmc_l12_gbtclk0_a_p				: in		std_logic; 
	fmc_l12_gbtclk0_a_n				: in		std_logic; 
	fmc_l12_gbtclk1_a_p				: in		std_logic; 
	fmc_l12_gbtclk1_a_n				: in		std_logic; 
		
	fmc_l12_gbtclk0_b_p				: in		std_logic; 
	fmc_l12_gbtclk0_b_n				: in		std_logic; 
	fmc_l12_gbtclk1_b_p				: in		std_logic; 
	fmc_l12_gbtclk1_b_n				: in		std_logic; 
		
	fmc_l8_gbtclk0_p					: in		std_logic; 
	fmc_l8_gbtclk0_n					: in		std_logic; 
	fmc_l8_gbtclk1_p					: in		std_logic; 
	fmc_l8_gbtclk1_n					: in		std_logic; 

	--# gpio
	usrled1_r							: out		std_logic; -- fmc_l12_spare[8]
	usrled1_g							: out		std_logic; -- fmc_l12_spare[9]
	usrled1_b							: out		std_logic; -- fmc_l12_spare[10]
	usrled2_r							: out		std_logic; -- fmc_l12_spare[11]
	usrled2_g							: out		std_logic; -- fmc_l12_spare[12]
	usrled2_b							: out		std_logic; -- fmc_l12_spare[13]
	dipsw									: in		std_logic_vector( 7 downto 0); -- fmc_l12_spare[21:14]
--	fmc_l12_spare						: inout	std_logic_vector(21 downto 0);
	--
	fmc_l12_dp_c2m_p					: out		std_logic_vector(11 downto 0);
	fmc_l12_dp_c2m_n					: out		std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_p					: in		std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_n					: in		std_logic_vector(11 downto 0);
	--
	fmc_l8_dp_c2m_p					: out		std_logic_vector( 7 downto 0);
	fmc_l8_dp_c2m_n					: out		std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_p					: in		std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_n					: in		std_logic_vector( 7 downto 0);
	--		
	fmc_l8_la_p							: inout	std_logic_vector(33 downto 0);
	fmc_l8_la_n							: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_p						: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_n						: inout	std_logic_vector(33 downto 0);
	--		
	fmc_l8_clk0							: in		std_logic; 
	fmc_l8_clk1             		: in		std_logic;
	fmc_l12_clk0            		: in		std_logic;
	fmc_l12_clk1            		: in		std_logic;	
	--		
	k7_amc_rx_p							: in		std_logic_vector(15 downto 1);
	k7_amc_rx_n							: in		std_logic_vector(15 downto 1);
	amc_tx_p								: out		std_logic_vector(15 downto 1);
	amc_tx_n								: out		std_logic_vector(15 downto 1);
	--		
	k7_fabric_amc_rx_p03				: in		std_logic;
	k7_fabric_amc_rx_n03    		: in		std_logic;
	k7_fabric_amc_tx_p03    		: out		std_logic;
	k7_fabric_amc_tx_n03    		: out		std_logic;
	--		
	fpga_refclkout_p 					: out 	std_logic;
	fpga_refclkout_n 		   		: out 	std_logic;
	--
	ddr3_sys_clk_p 					: in		std_logic;
	ddr3_sys_clk_n 					: in		std_logic;
	ddr3_dq                 		: inout 	std_logic_vector( 31 downto 0);
	ddr3_dqs_p              		: inout 	std_logic_vector(  3 downto 0);
	ddr3_dqs_n              		: inout 	std_logic_vector(  3 downto 0);
	ddr3_addr               		: out   	std_logic_vector( 13 downto 0);
	ddr3_ba                 		: out   	std_logic_vector(  2 downto 0);
	ddr3_ras_n              		: out   	std_logic;
	ddr3_cas_n              		: out   	std_logic;
	ddr3_we_n               		: out   	std_logic;
	ddr3_reset_n            		: out   	std_logic;
	ddr3_ck_p               		: out   	std_logic_vector(  0 downto 0);
	ddr3_ck_n               		: out   	std_logic_vector(  0 downto 0);
	ddr3_cke                		: out   	std_logic_vector(  0 downto 0);
	ddr3_cs_n               		: out   	std_logic_vector(  0 downto 0);
	ddr3_dm                 		: out   	std_logic_vector(  3 downto 0);
	ddr3_odt                		: out   	std_logic_vector(  0 downto 0);

	--==========================--		
	-- system_core interface		
	--==========================--		
	ip_addr_o							: out		std_logic_vector(31 downto 0);
	mac_addr_o							: out		std_logic_vector(47 downto 0);
			
	fabric_coax_or_osc_bufg_i		: in		std_logic;
	fabric_clk_bufg_i					: in	 	std_logic;
	osc125_a_bufg_i					: in	 	std_logic;
	osc125_a_mgtrefclk_i				: in		std_logic;
	clk200_bufg_i						: in		std_logic;

	ipb_rst_i				         : in	  std_logic;
	ipb_clk_i				         : in	  std_logic;
	ipb_clk_o				         : out	  std_logic;
	ipb_miso_o			            : out	  ipb_rbus_array(0 to nbr_usr_slaves-1);
	ipb_mosi_i			            : in	  ipb_wbus_array(0 to nbr_usr_slaves-1)	
);
end user_logic;

architecture usr of user_logic is
   
 	attribute keep     							: string;  
   
	signal ipb_clk									: std_logic;
 	attribute keep of ipb_clk 	            : signal is "true";

	signal ctrl_reg		         			: array_32x32bit;
	signal stat_reg		         			: array_32x32bit;

	constant led_off								: std_logic:='1';
	constant led_on								: std_logic:='1';

   -- CERN test:
   
   signal cern_test_stat_reg		         : array_192x32bit;
   signal cern_test_ctrl_reg		         : array_32x32bit;
   
begin


	--===========================================--
	-- clk management
	--===========================================--
	ipb_clk 		<= ipb_clk_i; 				-- option A: use 31.25MHz as ipb_clk
--	ipb_clk 		<= fabric_clk_bufg_i; 	-- option B: use LHC clk  as ipb_clk
	ipb_clk_o 	<= ipb_clk;					-- always forward the selected ipb_clk to system core
	--===========================================--	



	--===========================================--
	-- ip & mac
	--===========================================--
	ip_addr_o 	<= x"c0_a8_00" 		& dipsw;			-- switch J7 (pins pulled-up inside the FPGA)
	mac_addr_o	<= x"aa_bb_cc_dd_ee" & dipsw;			-- switch J7 (pins pulled-up inside the FPGA)
	--===========================================--
	

	--===========================================--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--	
	--===========================================--



	--===========================================--
	stat_regs_inst: entity work.ipb_user_status_regs
	--===========================================--	
   port map
	(
		reset					=> ipb_rst_i,
		clk					=> ipb_clk,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_stat_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_stat_regs),
		regs_i				=> stat_reg
	);
	--===========================================--



	--===========================================--
	ctrl_regs_inst: entity work.ipb_user_control_regs
	--===========================================--
	port map
	(
		reset					=> ipb_rst_i,
		clk					=> ipb_clk,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_ctrl_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_ctrl_regs),
		regs_o				=> ctrl_reg
	);
	--===========================================--
		

	--===========================================--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--===========================================--
	


   --===========================================--
	-- register mapping
	--===========================================--
											stat_reg(0)					<= usr_id_0;
											stat_reg(2)					<= firmware_id;
	--===========================================--
	
	
	
   --===========================================--
	-- IO mapping
	--===========================================--


	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	--###########################################--
	
   
       --###################################--
     --#######################################--
   --###########################################--
   --################           ################--  
   --################ CERN Test ################--
   --################           ################--
   --###########################################--
     --#######################################--
       --###################################--

       
   --===========================================--
	-- CERN Test -  banks of registers
	--===========================================--
   
	--===========================================--
	cern_test_statRegs: entity work.ipb_status_regs_test_cern
	--===========================================--
	generic map
   (
      addr_width        => 7   -- Note!! Added to address more than 128 registers  
   )      
   port map
	(
		reset					=> ipb_rst_i,
		clk					=> ipb_clk,
		ipb_mosi_i			=> ipb_mosi_i(cern_test_ipb_stat_regs),
		ipb_miso_o			=> ipb_miso_o(cern_test_ipb_stat_regs),
		regs_i				=> cern_test_stat_reg
	);
	--===========================================--

	--===========================================--
	cern_test_ctrlRegs: entity work.ipb_control_regs_test_cern
	--===========================================--
	port map
	(
		reset					=> ipb_rst_i,
		clk					=> ipb_clk,
		ipb_mosi_i			=> ipb_mosi_i(cern_test_ipb_ctrl_regs),
		ipb_miso_o			=> ipb_miso_o(cern_test_ipb_ctrl_regs),
		regs_o				=> cern_test_ctrl_reg
	);
	--===========================================--   
   
   
   --===========================================--
   -- High-Speed & User I/O test
   --===========================================--
   
   --===========================================--
   cern_test_gtxAndIo: entity work.gtx_and_io_test_cern
   --===========================================--
   port map
   (
      osc125_b_p					=>	osc125_b_p,
      osc125_b_n					=>	osc125_b_n,
      osc_xpoint_a_p				=>	osc_xpoint_a_p,
      osc_xpoint_a_n				=>	osc_xpoint_a_n,
      osc_xpoint_b_p				=>	osc_xpoint_b_p,
      osc_xpoint_b_n				=>	osc_xpoint_b_n,
      osc_xpoint_c_p				=>	osc_xpoint_c_p,
      osc_xpoint_c_n				=>	osc_xpoint_c_n,
      osc_xpoint_d_p				=>	osc_xpoint_d_p,
      osc_xpoint_d_n				=>	osc_xpoint_d_n,
      --
      fmc_l12_dp_c2m_p			=>	fmc_l12_dp_c2m_p,	
      fmc_l12_dp_c2m_n			=>	fmc_l12_dp_c2m_n,
      fmc_l12_dp_m2c_p			=>	fmc_l12_dp_m2c_p,	
      fmc_l12_dp_m2c_n			=>	fmc_l12_dp_m2c_n,	
      fmc_l8_dp_c2m_p			=>	fmc_l8_dp_c2m_p,	
      fmc_l8_dp_c2m_n			=>	fmc_l8_dp_c2m_n,	
      fmc_l8_dp_m2c_p			=>	fmc_l8_dp_m2c_p,	
      fmc_l8_dp_m2c_n			=>	fmc_l8_dp_m2c_n,	
      k7_amc_rx_p					=>	k7_amc_rx_p,			
      k7_amc_rx_n					=>	k7_amc_rx_n,			
      amc_tx_p						=> amc_tx_p,				
      amc_tx_n						=> amc_tx_n,				
      --
      fmc_l8_la_p					=>	fmc_l8_la_p,	
      fmc_l8_la_n					=>	fmc_l8_la_n,
      fmc_l12_la_p				=>	fmc_l12_la_p,
      fmc_l12_la_n				=>	fmc_l12_la_n,
      --                   	
      ipb_rst_i				  	=> ipb_rst_i,			
      ipb_clk_i				  	=> ipb_clk_i,			
      ipb_stat_reg_o          => cern_test_stat_reg(0 to 191),
      ipb_ctrl_reg_i		      => cern_test_ctrl_reg(0 to  31)
   );
   
end usr;