library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
--! specific packages
library unisim;
use unisim.vcomponents.all;


entity clk_domain_bridge is
generic (n : integer range 0 to 128:=128);
port
(
	wrclk_i							: in		std_logic;
	rdclk_i							: in		std_logic;
	wdata_i							: in		std_logic_vector(n-1 downto 0);
	rdata_o							: out		std_logic_vector(n-1 downto 0)
);                    	
end clk_domain_bridge;

                    	
architecture clk_domain_bridge_arch of clk_domain_bridge is                    	


begin


	--================================--
	dpram: entity work.k7_dpram_bram
	--================================--
	port map
	(
		clka					  => wrclk_i,
		wea(0)				  => '1',
		addra(0)				  => '0',
		dina(n-1 downto 0)  => wdata_i,
		dina(127 downto n)  => (others=>'0'),
		---------------------
		clkb   				  => rdclk_i,
		addrb(0)				  => '0',
		doutb(n-1 downto 0) => rdata_o,
		doutb(127 downto n) => open
	);
end clk_domain_bridge_arch;