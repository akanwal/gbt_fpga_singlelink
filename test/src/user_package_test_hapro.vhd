library ieee;
use ieee.std_logic_1164.all;
 
package user_package is

	--=== ipb slaves =============--
	constant nbr_usr_slaves				: positive := 5 ;
   
	constant user_ipb_stat_regs		: integer  := 0 ;
	constant user_ipb_ctrl_regs		: integer  := 1 ;
   constant cern_test_ipb_stat_regs	: integer  := 2 ;
	constant cern_test_ipb_ctrl_regs	: integer  := 3 ;	
	constant user_ddr3					: integer  := 4 ;

	constant ddr3_enable					: std_logic:='1';
	
   type array_nx64bit               is array (integer range <>) of std_logic_vector(63 downto 0);
   type array_192x32bit             is array (0 to 191) of std_logic_vector(31 downto 0);
                                    
   type bank_mapping                is array (0 to 3) of integer;
   
   -- l8
   constant bank112                 : bank_mapping := (3,2,1,0);
   constant bank111                 : bank_mapping := (5,6,7,4);
   -- l12
   constant bank118                 : bank_mapping := (2,9,0,1);
   constant bank117                 : bank_mapping := (7,3,8,11);
   constant bank116                 : bank_mapping := (5,6,10,4);
   -- amc
   constant bank115                 : bank_mapping := (7,6,5,4);
   constant bank114                 : bank_mapping := (8,9,10,11);
   constant bank113                 : bank_mapping := (0,1,2,3);	
	
end user_package;
   
package body user_package is
end user_package;

	

