library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.scan_package.all;

entity scan_clockregion is 
generic (regs: pblock_regs_type);
port
(
    clk_i  : in  std_logic;
    rst_i  : in  std_logic;
    data_i : in  std_logic_vector(31 downto 0);
    data_o : out std_logic_vector(31 downto 0)
);
end scan_clockregion;

architecture chain of scan_clockregion is

begin

b: for x in 0 to 31 generate
    s: entity work.shreg 
    generic map(length => regs(x)) 
    port map (clk_i => clk_i, rst_i => rst_i, i => data_i(x), o => data_o(x));
end generate b;

end chain;