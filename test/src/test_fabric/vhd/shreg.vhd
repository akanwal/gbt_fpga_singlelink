library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity shreg is 
generic (length: integer:= 1250);
port
(
    clk_i : in  std_logic;
    rst_i : in  std_logic;
    i     : in  std_logic;
    o     : out std_logic
);
end shreg;

architecture shift_left of shreg is

signal reg                 : std_logic_vector(length-1 downto 0);
attribute keep             : string;
attribute keep of reg      : signal is "true";
attribute srl_style        : string;
attribute srl_style of reg : signal is "register";

begin

process(clk_i, rst_i)
begin
if rst_i = '1' then
    reg <= (others => '0');    
elsif rising_edge(clk_i) then
    reg <= reg(length-2 downto 0) & i;
end if;
end process;

o <= reg(length-1);	
	
end shift_left;