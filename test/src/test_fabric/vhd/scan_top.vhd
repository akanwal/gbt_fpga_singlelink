library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

use work.scan_package.all;

library unisim;
use unisim.vcomponents.all;

entity scan_top is 
port
(
	fabric_coax_or_osc_p 			: in 	std_logic;
	fabric_coax_or_osc_n 			: in 	std_logic;
	
	osc_coax_sel                    : out   std_logic;
	
	sysled1_r                       : out   std_logic;
	sysled1_g                       : out   std_logic;
	sysled1_b                       : out   std_logic;
	sysled2_r                       : out   std_logic;
	sysled2_g                       : out   std_logic;
	sysled2_b                       : out   std_logic;

	usrled1_r                       : out   std_logic;
	usrled1_g                       : out   std_logic;
	usrled1_b                       : out   std_logic;
	usrled2_r                       : out   std_logic;
	usrled2_g                       : out   std_logic;
	usrled2_b                       : out   std_logic
	
);
end scan_top;

architecture chipscope of scan_top is


signal clk_pre_buf   : std_logic;
signal clk           : std_logic;   

signal vio_rst       : std_logic;   
signal vio_ctrl      : std_logic_vector(31 downto 0);   
signal vio_stat      : std_logic_vector(31 downto 0);   
signal vio_sel       : std_logic_vector( 3 downto 0);   


type array_16x32bit is array (0 to 15) of std_logic_vector(31 downto 0);
signal ctrl	         : array_16x32bit;
signal stat          : array_16x32bit;

attribute mark_debug : string;

attribute mark_debug of vio_rst : signal is "true";
attribute mark_debug of vio_ctrl: signal is "true";
attribute mark_debug of vio_sel : signal is "true";
attribute mark_debug of vio_stat: signal is "true";


begin

	osc_ibuf: ibufgds port map (i => fabric_coax_or_osc_p, ib => fabric_coax_or_osc_n, 	o => clk_pre_buf);
	osc_bufg: bufg 	port map (i => clk_pre_buf, 								        o => clk);


    osc_coax_sel <= '1';

    hb_inst: entity work.hb port map(rst => '0', i => clk, o => sysled1_r);

-- 	sysled1_r <= '1';
	sysled1_g <= '1';
	sysled1_b <= '1';
	sysled2_r <= '1';
	sysled2_g <= '1';
	sysled2_b <= '1';   

	usrled1_r <= '1';
	usrled1_g <= '1';
	usrled1_b <= '1';
	usrled2_r <= '1';
	usrled2_g <= '1';
	usrled2_b <= '1'; 


    --===========================================--
	-- flip-flop scanning
	--===========================================--
    x: for x_coord in 0 to 1 generate
       y: for y_coord in 0 to 7 generate
          --    
          clkregion: entity work.scan_clockregion 
          generic map 
          (
             regs => array_pblock_regs(8*x_coord + y_coord)
          )
          port map
          (   
             clk_i       => clk,
             rst_i       => vio_rst,
             data_i      => ctrl(8*x_coord + y_coord),
             data_o      => stat(8*x_coord + y_coord)
          );
          --
        end generate y;
    end generate x;
    --===========================================--
 

    --===========================================--
    mx: for b in 0 to 31 generate
    --===========================================--
        mux: entity work.mux16 
        port map
        (   
            data_in(15) => stat(15)(b),
            data_in(14) => stat(14)(b),
            data_in(13) => stat(13)(b),
            data_in(12) => stat(12)(b),
            data_in(11) => stat(11)(b),
            data_in(10) => stat(10)(b),
            data_in( 9) => stat( 9)(b),
            data_in( 8) => stat( 8)(b),
            data_in( 7) => stat( 7)(b),
            data_in( 6) => stat( 6)(b),
            data_in( 5) => stat( 5)(b),
            data_in( 4) => stat( 4)(b),
            data_in( 3) => stat( 3)(b),
            data_in( 2) => stat( 2)(b),
            data_in( 1) => stat( 1)(b),
            data_in( 0) => stat( 0)(b),
            sel         => vio_sel,
            data_out    => vio_stat(b)
        );
    end generate mx;
    --===========================================--



    --===========================================--
    c: for z in 0 to 15 generate
        ctrl(z) <= vio_ctrl;
    end generate c;
    --===========================================--



    --###########################################--
    --###########################################--
    --###########################################--
    --###########################################--
    --###########################################--
    --###########################################--

    --===========================================--
    vio: entity work.vio_0
    --===========================================--
    port map 
    (
        clk      => clk,
        probe_in0   => vio_stat,
        probe_out0(31 downto  0) => vio_ctrl,
        probe_out0(35 downto 32) => vio_sel,
        probe_out0(36)           => vio_rst
                
    );
    --===========================================--

        
end chipscope;
