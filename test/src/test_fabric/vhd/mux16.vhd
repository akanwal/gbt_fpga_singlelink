library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library unisim;
use unisim.vcomponents.all;

entity mux16 is
  Port (  data_in : in std_logic_vector(15 downto 0);
              sel : in std_logic_vector(3 downto 0);
         data_out : out std_logic);
  end mux16;

architecture low_level of mux16 is

signal data_selection : std_logic_vector(3 downto 0);
signal       combiner : std_logic_vector(1 downto 0);

begin

  selection0_lut: LUT6
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data_in(0),
            I1 => data_in(1),
            I2 => data_in(2),
            I3 => data_in(3),
            I4 => sel(0),
            I5 => sel(1),
             O => data_selection(0)); 


  selection1_lut: LUT6
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data_in(4),
            I1 => data_in(5),
            I2 => data_in(6),
            I3 => data_in(7),
            I4 => sel(0),
            I5 => sel(1),
             O => data_selection(1)); 


  combiner0_muxf7: MUXF7
  port map( I0 => data_selection(0),
            I1 => data_selection(1),
             S => sel(2),
             O => combiner(0));


  selection2_lut: LUT6
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data_in(8),
            I1 => data_in(9),
            I2 => data_in(10),
            I3 => data_in(11),
            I4 => sel(0),
            I5 => sel(1),
             O => data_selection(2)); 


  selection3_lut: LUT6
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data_in(12),
            I1 => data_in(13),
            I2 => data_in(14),
            I3 => data_in(15),
            I4 => sel(0),
            I5 => sel(1),
             O => data_selection(3)); 


  combiner1_muxf7: MUXF7
  port map( I0 => data_selection(2),
            I1 => data_selection(3),
             S => sel(2),
             O => combiner(1));


  combiner_muxf8: MUXF8
  port map( I0 => combiner(0),
            I1 => combiner(1),
             S => sel(3),
             O => data_out);


end low_level;