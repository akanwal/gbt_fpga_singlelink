# config vio
set_property OUTPUT_VALUE_RADIX UNSIGNED [get_hw_probes vio_sel    -of_objects [get_hw_vios hw_vio_1]]
set_property OUTPUT_VALUE_RADIX HEX [get_hw_probes vio_ctrl   -of_objects [get_hw_vios hw_vio_1]]
set_property INPUT_VALUE_RADIX HEX  [get_hw_probes vio_stat   -of_objects [get_hw_vios hw_vio_1]]
# reset
set_property OUTPUT_VALUE 1         [get_hw_probes  vio_rst   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_rst}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
set_property OUTPUT_VALUE 0         [get_hw_probes  vio_rst   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_rst}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
# 00000000
set_property OUTPUT_VALUE 00000000  [get_hw_probes  vio_ctrl  -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_ctrl} -of_objects [get_hw_vios hw_vio_1]]
#x0y0
set_property OUTPUT_VALUE 0         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y1
set_property OUTPUT_VALUE 1         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y2
set_property OUTPUT_VALUE 2         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y3
set_property OUTPUT_VALUE 3         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y4
set_property OUTPUT_VALUE 4         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y5
set_property OUTPUT_VALUE 5         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y6
set_property OUTPUT_VALUE 6         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y7
set_property OUTPUT_VALUE 7         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y0
set_property OUTPUT_VALUE 8         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y1
set_property OUTPUT_VALUE 9         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y2
set_property OUTPUT_VALUE 10        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y3
set_property OUTPUT_VALUE 11        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y4
set_property OUTPUT_VALUE 12        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y5
set_property OUTPUT_VALUE 13        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y6
set_property OUTPUT_VALUE 14        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y7
set_property OUTPUT_VALUE 15        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
# reset
set_property OUTPUT_VALUE 1         [get_hw_probes  vio_rst   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_rst}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
set_property OUTPUT_VALUE 0         [get_hw_probes  vio_rst   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_rst}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
# ffffffff
set_property OUTPUT_VALUE ffffffff  [get_hw_probes  vio_ctrl  -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_ctrl} -of_objects [get_hw_vios hw_vio_1]]
#x0y0
set_property OUTPUT_VALUE 0         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y1
set_property OUTPUT_VALUE 1         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y2
set_property OUTPUT_VALUE 2         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y3
set_property OUTPUT_VALUE 3         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y4
set_property OUTPUT_VALUE 4         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y5
set_property OUTPUT_VALUE 5         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y6
set_property OUTPUT_VALUE 6         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x0y7
set_property OUTPUT_VALUE 7         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y0
set_property OUTPUT_VALUE 8         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y1
set_property OUTPUT_VALUE 9         [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y2
set_property OUTPUT_VALUE 10        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y3
set_property OUTPUT_VALUE 11        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y4
set_property OUTPUT_VALUE 12        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y5
set_property OUTPUT_VALUE 13        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y6
set_property OUTPUT_VALUE 14        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#x1y7
set_property OUTPUT_VALUE 15        [get_hw_probes  vio_sel   -of_objects [get_hw_vios hw_vio_1]]
commit_hw_vio                       [get_hw_probes {vio_sel}  -of_objects [get_hw_vios hw_vio_1]]
after 1000 
get_property INPUT_VALUE            [get_hw_probes  vio_stat  -of_objects [get_hw_vios hw_vio_1]]
#close
close_hw
