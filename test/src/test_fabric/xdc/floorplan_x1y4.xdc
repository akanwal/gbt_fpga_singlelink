##############################
# fine scan
##############################

##====== x1y4 =======##

create_pblock                    pblock_x1y4_31
create_pblock                    pblock_x1y4_30
create_pblock                    pblock_x1y4_29
create_pblock                    pblock_x1y4_28
create_pblock                    pblock_x1y4_27
create_pblock                    pblock_x1y4_26
create_pblock                    pblock_x1y4_25
create_pblock                    pblock_x1y4_24
create_pblock                    pblock_x1y4_23
create_pblock                    pblock_x1y4_22
create_pblock                    pblock_x1y4_21
create_pblock                    pblock_x1y4_20
create_pblock                    pblock_x1y4_19
create_pblock                    pblock_x1y4_18
create_pblock                    pblock_x1y4_17
create_pblock                    pblock_x1y4_16
create_pblock                    pblock_x1y4_15
create_pblock                    pblock_x1y4_14
create_pblock                    pblock_x1y4_13
create_pblock                    pblock_x1y4_12
create_pblock                    pblock_x1y4_11
create_pblock                    pblock_x1y4_10
create_pblock                    pblock_x1y4_9
create_pblock                    pblock_x1y4_8
create_pblock                    pblock_x1y4_7
create_pblock                    pblock_x1y4_6
create_pblock                    pblock_x1y4_5
create_pblock                    pblock_x1y4_4
create_pblock                    pblock_x1y4_3
create_pblock                    pblock_x1y4_2
create_pblock                    pblock_x1y4_1
create_pblock                    pblock_x1y4_0

add_cells_to_pblock [get_pblocks pblock_x1y4_31] [get_cells x[1].y[4].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_30] [get_cells x[1].y[4].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_29] [get_cells x[1].y[4].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_28] [get_cells x[1].y[4].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_27] [get_cells x[1].y[4].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_26] [get_cells x[1].y[4].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_25] [get_cells x[1].y[4].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_24] [get_cells x[1].y[4].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_23] [get_cells x[1].y[4].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_22] [get_cells x[1].y[4].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_21] [get_cells x[1].y[4].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_20] [get_cells x[1].y[4].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_19] [get_cells x[1].y[4].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_18] [get_cells x[1].y[4].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_17] [get_cells x[1].y[4].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_16] [get_cells x[1].y[4].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_15] [get_cells x[1].y[4].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_14] [get_cells x[1].y[4].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_13] [get_cells x[1].y[4].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_12] [get_cells x[1].y[4].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_11] [get_cells x[1].y[4].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_10] [get_cells x[1].y[4].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_9]  [get_cells x[1].y[4].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_8]  [get_cells x[1].y[4].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_7]  [get_cells x[1].y[4].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_6]  [get_cells x[1].y[4].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_5]  [get_cells x[1].y[4].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_4]  [get_cells x[1].y[4].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_3]  [get_cells x[1].y[4].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_2]  [get_cells x[1].y[4].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_1]  [get_cells x[1].y[4].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x1y4_0]  [get_cells x[1].y[4].clkregion/b[0].s]

resize_pblock       [get_pblocks pblock_x1y4_31] -add {SLICE_X182Y238:SLICE_X189Y249} 
resize_pblock       [get_pblocks pblock_x1y4_30] -add {SLICE_X182Y225:SLICE_X189Y237} 
resize_pblock       [get_pblocks pblock_x1y4_29] -add {SLICE_X182Y213:SLICE_X189Y224} 
resize_pblock       [get_pblocks pblock_x1y4_28] -add {SLICE_X182Y200:SLICE_X189Y212}

resize_pblock       [get_pblocks pblock_x1y4_27] -add {SLICE_X162Y238:SLICE_X181Y249} 
resize_pblock       [get_pblocks pblock_x1y4_26] -add {SLICE_X162Y225:SLICE_X181Y237} 
resize_pblock       [get_pblocks pblock_x1y4_25] -add {SLICE_X162Y213:SLICE_X181Y224} 
resize_pblock       [get_pblocks pblock_x1y4_24] -add {SLICE_X162Y200:SLICE_X181Y212}

resize_pblock       [get_pblocks pblock_x1y4_23] -add {SLICE_X152Y238:SLICE_X161Y249} 
resize_pblock       [get_pblocks pblock_x1y4_22] -add {SLICE_X152Y225:SLICE_X161Y237} 
resize_pblock       [get_pblocks pblock_x1y4_21] -add {SLICE_X152Y213:SLICE_X161Y224} 
resize_pblock       [get_pblocks pblock_x1y4_20] -add {SLICE_X152Y200:SLICE_X161Y212}

resize_pblock       [get_pblocks pblock_x1y4_19] -add {SLICE_X142Y238:SLICE_X151Y249} 
resize_pblock       [get_pblocks pblock_x1y4_18] -add {SLICE_X142Y225:SLICE_X151Y237} 
resize_pblock       [get_pblocks pblock_x1y4_17] -add {SLICE_X142Y213:SLICE_X151Y224} 
resize_pblock       [get_pblocks pblock_x1y4_16] -add {SLICE_X142Y200:SLICE_X151Y212}

resize_pblock       [get_pblocks pblock_x1y4_15] -add {SLICE_X132Y238:SLICE_X141Y249} 
resize_pblock       [get_pblocks pblock_x1y4_14] -add {SLICE_X132Y225:SLICE_X141Y237} 
resize_pblock       [get_pblocks pblock_x1y4_13] -add {SLICE_X132Y213:SLICE_X141Y224} 
resize_pblock       [get_pblocks pblock_x1y4_12] -add {SLICE_X132Y200:SLICE_X141Y212}

resize_pblock       [get_pblocks pblock_x1y4_11] -add {SLICE_X122Y238:SLICE_X131Y249} 
resize_pblock       [get_pblocks pblock_x1y4_10] -add {SLICE_X122Y225:SLICE_X131Y237} 
resize_pblock       [get_pblocks pblock_x1y4_9]  -add {SLICE_X122Y213:SLICE_X131Y224} 
resize_pblock       [get_pblocks pblock_x1y4_8]  -add {SLICE_X122Y200:SLICE_X131Y212}

resize_pblock       [get_pblocks pblock_x1y4_7]  -add {SLICE_X102Y238:SLICE_X121Y249} 
resize_pblock       [get_pblocks pblock_x1y4_6]  -add {SLICE_X102Y225:SLICE_X121Y237} 
resize_pblock       [get_pblocks pblock_x1y4_5]  -add {SLICE_X102Y213:SLICE_X121Y224} 
resize_pblock       [get_pblocks pblock_x1y4_4]  -add {SLICE_X102Y200:SLICE_X121Y212}

resize_pblock       [get_pblocks pblock_x1y4_3]  -add {SLICE_X92Y238:SLICE_X101Y249} 
resize_pblock       [get_pblocks pblock_x1y4_2]  -add {SLICE_X92Y225:SLICE_X101Y237} 
resize_pblock       [get_pblocks pblock_x1y4_1]  -add {SLICE_X92Y213:SLICE_X101Y224} 
resize_pblock       [get_pblocks pblock_x1y4_0]  -add {SLICE_X92Y200:SLICE_X101Y212}