##############################
# fine scan
##############################

##====== x1y6 =======##

create_pblock                    pblock_x1y6_31
create_pblock                    pblock_x1y6_30
create_pblock                    pblock_x1y6_29
create_pblock                    pblock_x1y6_28
create_pblock                    pblock_x1y6_27
create_pblock                    pblock_x1y6_26
create_pblock                    pblock_x1y6_25
create_pblock                    pblock_x1y6_24
create_pblock                    pblock_x1y6_23
create_pblock                    pblock_x1y6_22
create_pblock                    pblock_x1y6_21
create_pblock                    pblock_x1y6_20
create_pblock                    pblock_x1y6_19
create_pblock                    pblock_x1y6_18
create_pblock                    pblock_x1y6_17
create_pblock                    pblock_x1y6_16
create_pblock                    pblock_x1y6_15
create_pblock                    pblock_x1y6_14
create_pblock                    pblock_x1y6_13
create_pblock                    pblock_x1y6_12
create_pblock                    pblock_x1y6_11
create_pblock                    pblock_x1y6_10
create_pblock                    pblock_x1y6_9
create_pblock                    pblock_x1y6_8
create_pblock                    pblock_x1y6_7
create_pblock                    pblock_x1y6_6
create_pblock                    pblock_x1y6_5
create_pblock                    pblock_x1y6_4
create_pblock                    pblock_x1y6_3
create_pblock                    pblock_x1y6_2
create_pblock                    pblock_x1y6_1
create_pblock                    pblock_x1y6_0

add_cells_to_pblock [get_pblocks pblock_x1y6_31] [get_cells x[1].y[6].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_30] [get_cells x[1].y[6].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_29] [get_cells x[1].y[6].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_28] [get_cells x[1].y[6].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_27] [get_cells x[1].y[6].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_26] [get_cells x[1].y[6].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_25] [get_cells x[1].y[6].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_24] [get_cells x[1].y[6].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_23] [get_cells x[1].y[6].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_22] [get_cells x[1].y[6].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_21] [get_cells x[1].y[6].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_20] [get_cells x[1].y[6].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_19] [get_cells x[1].y[6].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_18] [get_cells x[1].y[6].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_17] [get_cells x[1].y[6].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_16] [get_cells x[1].y[6].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_15] [get_cells x[1].y[6].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_14] [get_cells x[1].y[6].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_13] [get_cells x[1].y[6].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_12] [get_cells x[1].y[6].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_11] [get_cells x[1].y[6].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_10] [get_cells x[1].y[6].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_9]  [get_cells x[1].y[6].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_8]  [get_cells x[1].y[6].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_7]  [get_cells x[1].y[6].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_6]  [get_cells x[1].y[6].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_5]  [get_cells x[1].y[6].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_4]  [get_cells x[1].y[6].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_3]  [get_cells x[1].y[6].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_2]  [get_cells x[1].y[6].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_1]  [get_cells x[1].y[6].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x1y6_0]  [get_cells x[1].y[6].clkregion/b[0].s]

resize_pblock       [get_pblocks pblock_x1y6_31] -add {SLICE_X182Y338:SLICE_X189Y349} 
resize_pblock       [get_pblocks pblock_x1y6_30] -add {SLICE_X182Y325:SLICE_X189Y337} 
resize_pblock       [get_pblocks pblock_x1y6_29] -add {SLICE_X182Y313:SLICE_X189Y324} 
resize_pblock       [get_pblocks pblock_x1y6_28] -add {SLICE_X182Y300:SLICE_X189Y312}

resize_pblock       [get_pblocks pblock_x1y6_27] -add {SLICE_X162Y338:SLICE_X181Y349} 
resize_pblock       [get_pblocks pblock_x1y6_26] -add {SLICE_X162Y325:SLICE_X181Y337} 
resize_pblock       [get_pblocks pblock_x1y6_25] -add {SLICE_X162Y313:SLICE_X181Y324} 
resize_pblock       [get_pblocks pblock_x1y6_24] -add {SLICE_X162Y300:SLICE_X181Y312}

resize_pblock       [get_pblocks pblock_x1y6_23] -add {SLICE_X152Y338:SLICE_X161Y349} 
resize_pblock       [get_pblocks pblock_x1y6_22] -add {SLICE_X152Y325:SLICE_X161Y337} 
resize_pblock       [get_pblocks pblock_x1y6_21] -add {SLICE_X152Y313:SLICE_X161Y324} 
resize_pblock       [get_pblocks pblock_x1y6_20] -add {SLICE_X152Y300:SLICE_X161Y312}

resize_pblock       [get_pblocks pblock_x1y6_19] -add {SLICE_X142Y338:SLICE_X151Y349} 
resize_pblock       [get_pblocks pblock_x1y6_18] -add {SLICE_X142Y325:SLICE_X151Y337} 
resize_pblock       [get_pblocks pblock_x1y6_17] -add {SLICE_X142Y313:SLICE_X151Y324} 
resize_pblock       [get_pblocks pblock_x1y6_16] -add {SLICE_X142Y300:SLICE_X151Y312}

resize_pblock       [get_pblocks pblock_x1y6_15] -add {SLICE_X132Y338:SLICE_X141Y349} 
resize_pblock       [get_pblocks pblock_x1y6_14] -add {SLICE_X132Y325:SLICE_X141Y337} 
resize_pblock       [get_pblocks pblock_x1y6_13] -add {SLICE_X132Y313:SLICE_X141Y324} 
resize_pblock       [get_pblocks pblock_x1y6_12] -add {SLICE_X132Y300:SLICE_X141Y312}

resize_pblock       [get_pblocks pblock_x1y6_11] -add {SLICE_X122Y338:SLICE_X131Y349} 
resize_pblock       [get_pblocks pblock_x1y6_10] -add {SLICE_X122Y325:SLICE_X131Y337} 
resize_pblock       [get_pblocks pblock_x1y6_9]  -add {SLICE_X122Y313:SLICE_X131Y324} 
resize_pblock       [get_pblocks pblock_x1y6_8]  -add {SLICE_X122Y300:SLICE_X131Y312}

resize_pblock       [get_pblocks pblock_x1y6_7]  -add {SLICE_X102Y338:SLICE_X121Y349} 
resize_pblock       [get_pblocks pblock_x1y6_6]  -add {SLICE_X102Y325:SLICE_X121Y337} 
resize_pblock       [get_pblocks pblock_x1y6_5]  -add {SLICE_X102Y313:SLICE_X121Y324} 
resize_pblock       [get_pblocks pblock_x1y6_4]  -add {SLICE_X102Y300:SLICE_X121Y312}

resize_pblock       [get_pblocks pblock_x1y6_3]  -add {SLICE_X92Y338:SLICE_X101Y349} 
resize_pblock       [get_pblocks pblock_x1y6_2]  -add {SLICE_X92Y325:SLICE_X101Y337} 
resize_pblock       [get_pblocks pblock_x1y6_1]  -add {SLICE_X92Y313:SLICE_X101Y324} 
resize_pblock       [get_pblocks pblock_x1y6_0]  -add {SLICE_X92Y300:SLICE_X101Y312}