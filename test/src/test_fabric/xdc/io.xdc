create_clock -name fabric_coax_or_osc_p -period 25 [get_ports fabric_coax_or_osc_p]
set_property PACKAGE_PIN AL18                      [get_ports fabric_coax_or_osc_p]
set_property IOSTANDARD LVDS_25                    [get_ports fabric_coax_or_osc_p]
set_property DIFF_TERM true                        [get_ports fabric_coax_or_osc_p]
set_property PACKAGE_PIN AL19                      [get_ports fabric_coax_or_osc_n]
set_property IOSTANDARD LVDS_25                    [get_ports fabric_coax_or_osc_n]
set_property DIFF_TERM true                        [get_ports fabric_coax_or_osc_n]

set_property PACKAGE_PIN AA24                      [get_ports sysled1_r]
set_property IOSTANDARD LVCMOS33                   [get_ports sysled1_r]

set_property PACKAGE_PIN V27                       [get_ports sysled1_g]
set_property IOSTANDARD LVCMOS33                   [get_ports sysled1_g]

set_property PACKAGE_PIN AA31                      [get_ports sysled1_b]
set_property IOSTANDARD LVCMOS33                   [get_ports sysled1_b]

set_property PACKAGE_PIN AA29                      [get_ports sysled2_r]
set_property IOSTANDARD LVCMOS33                   [get_ports sysled2_r]

set_property PACKAGE_PIN Y28                       [get_ports sysled2_g]
set_property IOSTANDARD LVCMOS33                   [get_ports sysled2_g]

set_property PACKAGE_PIN Y29                       [get_ports sysled2_b]
set_property IOSTANDARD LVCMOS33                   [get_ports sysled2_b]

set_property PACKAGE_PIN AH29                      [get_ports osc_coax_sel]
set_property IOSTANDARD LVCMOS18                   [get_ports osc_coax_sel]

set_property PACKAGE_PIN P25                       [get_ports usrled1_r]
set_property IOSTANDARD LVCMOS18                   [get_ports usrled1_r]

set_property PACKAGE_PIN P26                       [get_ports usrled1_g]
set_property IOSTANDARD LVCMOS18                   [get_ports usrled1_g]

set_property PACKAGE_PIN R24                       [get_ports usrled1_b]
set_property IOSTANDARD LVCMOS18                   [get_ports usrled1_b]

set_property PACKAGE_PIN P24                       [get_ports usrled2_r]
set_property IOSTANDARD LVCMOS18                   [get_ports usrled2_r]

set_property PACKAGE_PIN R26                       [get_ports usrled2_g]
set_property IOSTANDARD LVCMOS18                   [get_ports usrled2_g]

set_property PACKAGE_PIN R27                       [get_ports usrled2_b]
set_property IOSTANDARD LVCMOS18                   [get_ports usrled2_b]