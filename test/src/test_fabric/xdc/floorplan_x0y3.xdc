##############################
# fine scan
##############################

##====== x0y3 =======##

create_pblock                    pblock_x0y3_31
create_pblock                    pblock_x0y3_30
create_pblock                    pblock_x0y3_29
create_pblock                    pblock_x0y3_28
create_pblock                    pblock_x0y3_27
create_pblock                    pblock_x0y3_26
create_pblock                    pblock_x0y3_25
create_pblock                    pblock_x0y3_24
create_pblock                    pblock_x0y3_23
create_pblock                    pblock_x0y3_22
create_pblock                    pblock_x0y3_21
create_pblock                    pblock_x0y3_20
create_pblock                    pblock_x0y3_19
create_pblock                    pblock_x0y3_18
create_pblock                    pblock_x0y3_17
create_pblock                    pblock_x0y3_16
create_pblock                    pblock_x0y3_15
create_pblock                    pblock_x0y3_14
create_pblock                    pblock_x0y3_13
create_pblock                    pblock_x0y3_12
create_pblock                    pblock_x0y3_11
create_pblock                    pblock_x0y3_10
create_pblock                    pblock_x0y3_9
create_pblock                    pblock_x0y3_8
create_pblock                    pblock_x0y3_7
create_pblock                    pblock_x0y3_6
create_pblock                    pblock_x0y3_5
create_pblock                    pblock_x0y3_4
create_pblock                    pblock_x0y3_3
create_pblock                    pblock_x0y3_2
create_pblock                    pblock_x0y3_1
create_pblock                    pblock_x0y3_0

add_cells_to_pblock [get_pblocks pblock_x0y3_31] [get_cells x[0].y[3].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_30] [get_cells x[0].y[3].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_29] [get_cells x[0].y[3].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_28] [get_cells x[0].y[3].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_27] [get_cells x[0].y[3].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_26] [get_cells x[0].y[3].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_25] [get_cells x[0].y[3].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_24] [get_cells x[0].y[3].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_23] [get_cells x[0].y[3].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_22] [get_cells x[0].y[3].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_21] [get_cells x[0].y[3].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_20] [get_cells x[0].y[3].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_19] [get_cells x[0].y[3].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_18] [get_cells x[0].y[3].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_17] [get_cells x[0].y[3].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_16] [get_cells x[0].y[3].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_15] [get_cells x[0].y[3].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_14] [get_cells x[0].y[3].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_13] [get_cells x[0].y[3].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_12] [get_cells x[0].y[3].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_11] [get_cells x[0].y[3].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_10] [get_cells x[0].y[3].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_9]  [get_cells x[0].y[3].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_8]  [get_cells x[0].y[3].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_7]  [get_cells x[0].y[3].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_6]  [get_cells x[0].y[3].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_5]  [get_cells x[0].y[3].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_4]  [get_cells x[0].y[3].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_3]  [get_cells x[0].y[3].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_2]  [get_cells x[0].y[3].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_1]  [get_cells x[0].y[3].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x0y3_0]  [get_cells x[0].y[3].clkregion/b[0].s]

resize_pblock       [get_pblocks pblock_x0y3_31] -add {SLICE_X82Y188:SLICE_X91Y199} 
resize_pblock       [get_pblocks pblock_x0y3_30] -add {SLICE_X82Y175:SLICE_X91Y187} 
resize_pblock       [get_pblocks pblock_x0y3_29] -add {SLICE_X82Y163:SLICE_X91Y174} 
resize_pblock       [get_pblocks pblock_x0y3_28] -add {SLICE_X82Y150:SLICE_X91Y162}

resize_pblock       [get_pblocks pblock_x0y3_27] -add {SLICE_X72Y188:SLICE_X81Y199} 
resize_pblock       [get_pblocks pblock_x0y3_26] -add {SLICE_X72Y175:SLICE_X81Y187} 
resize_pblock       [get_pblocks pblock_x0y3_25] -add {SLICE_X72Y163:SLICE_X81Y174} 
resize_pblock       [get_pblocks pblock_x0y3_24] -add {SLICE_X72Y150:SLICE_X81Y162}

resize_pblock       [get_pblocks pblock_x0y3_23] -add {SLICE_X62Y188:SLICE_X71Y199} 
resize_pblock       [get_pblocks pblock_x0y3_22] -add {SLICE_X62Y175:SLICE_X71Y187} 
resize_pblock       [get_pblocks pblock_x0y3_21] -add {SLICE_X62Y163:SLICE_X71Y174} 
resize_pblock       [get_pblocks pblock_x0y3_20] -add {SLICE_X62Y150:SLICE_X71Y162}

resize_pblock       [get_pblocks pblock_x0y3_19] -add {SLICE_X52Y188:SLICE_X61Y199} 
resize_pblock       [get_pblocks pblock_x0y3_18] -add {SLICE_X52Y175:SLICE_X61Y187} 
resize_pblock       [get_pblocks pblock_x0y3_17] -add {SLICE_X52Y163:SLICE_X61Y174} 
resize_pblock       [get_pblocks pblock_x0y3_16] -add {SLICE_X52Y150:SLICE_X61Y162}

resize_pblock       [get_pblocks pblock_x0y3_15] -add {SLICE_X42Y188:SLICE_X51Y199} 
resize_pblock       [get_pblocks pblock_x0y3_14] -add {SLICE_X42Y175:SLICE_X51Y187} 
resize_pblock       [get_pblocks pblock_x0y3_13] -add {SLICE_X42Y163:SLICE_X51Y174} 
resize_pblock       [get_pblocks pblock_x0y3_12] -add {SLICE_X42Y150:SLICE_X51Y162}

resize_pblock       [get_pblocks pblock_x0y3_11] -add {SLICE_X20Y188:SLICE_X41Y199} 
resize_pblock       [get_pblocks pblock_x0y3_10] -add {SLICE_X20Y175:SLICE_X41Y187} 
resize_pblock       [get_pblocks pblock_x0y3_9]  -add {SLICE_X20Y163:SLICE_X41Y174} 
resize_pblock       [get_pblocks pblock_x0y3_8]  -add {SLICE_X20Y150:SLICE_X41Y162}

resize_pblock       [get_pblocks pblock_x0y3_7]  -add {SLICE_X10Y188:SLICE_X19Y199} 
resize_pblock       [get_pblocks pblock_x0y3_6]  -add {SLICE_X10Y175:SLICE_X19Y187} 
resize_pblock       [get_pblocks pblock_x0y3_5]  -add {SLICE_X10Y163:SLICE_X19Y174} 
resize_pblock       [get_pblocks pblock_x0y3_4]  -add {SLICE_X10Y150:SLICE_X19Y162}

resize_pblock       [get_pblocks pblock_x0y3_3]  -add {SLICE_X0Y188:SLICE_X9Y199} 
resize_pblock       [get_pblocks pblock_x0y3_2]  -add {SLICE_X0Y175:SLICE_X9Y187} 
resize_pblock       [get_pblocks pblock_x0y3_1]  -add {SLICE_X0Y163:SLICE_X9Y174} 
resize_pblock       [get_pblocks pblock_x0y3_0]  -add {SLICE_X0Y150:SLICE_X9Y162}