##############################
# fine scan
##############################

##====== x1y0 =======##

create_pblock                    pblock_x1y0_31
create_pblock                    pblock_x1y0_30
create_pblock                    pblock_x1y0_29
create_pblock                    pblock_x1y0_28
create_pblock                    pblock_x1y0_27
create_pblock                    pblock_x1y0_26
create_pblock                    pblock_x1y0_25
create_pblock                    pblock_x1y0_24
create_pblock                    pblock_x1y0_23
create_pblock                    pblock_x1y0_22
create_pblock                    pblock_x1y0_21
create_pblock                    pblock_x1y0_20
create_pblock                    pblock_x1y0_19
create_pblock                    pblock_x1y0_18
create_pblock                    pblock_x1y0_17
create_pblock                    pblock_x1y0_16
create_pblock                    pblock_x1y0_15
create_pblock                    pblock_x1y0_14
create_pblock                    pblock_x1y0_13
create_pblock                    pblock_x1y0_12
create_pblock                    pblock_x1y0_11
create_pblock                    pblock_x1y0_10
create_pblock                    pblock_x1y0_9
create_pblock                    pblock_x1y0_8
create_pblock                    pblock_x1y0_7
create_pblock                    pblock_x1y0_6
create_pblock                    pblock_x1y0_5
create_pblock                    pblock_x1y0_4
create_pblock                    pblock_x1y0_3
create_pblock                    pblock_x1y0_2
create_pblock                    pblock_x1y0_1
create_pblock                    pblock_x1y0_0

add_cells_to_pblock [get_pblocks pblock_x1y0_31] [get_cells x[1].y[0].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_30] [get_cells x[1].y[0].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_29] [get_cells x[1].y[0].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_28] [get_cells x[1].y[0].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_27] [get_cells x[1].y[0].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_26] [get_cells x[1].y[0].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_25] [get_cells x[1].y[0].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_24] [get_cells x[1].y[0].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_23] [get_cells x[1].y[0].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_22] [get_cells x[1].y[0].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_21] [get_cells x[1].y[0].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_20] [get_cells x[1].y[0].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_19] [get_cells x[1].y[0].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_18] [get_cells x[1].y[0].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_17] [get_cells x[1].y[0].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_16] [get_cells x[1].y[0].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_15] [get_cells x[1].y[0].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_14] [get_cells x[1].y[0].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_13] [get_cells x[1].y[0].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_12] [get_cells x[1].y[0].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_11] [get_cells x[1].y[0].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_10] [get_cells x[1].y[0].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_9]  [get_cells x[1].y[0].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_8]  [get_cells x[1].y[0].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_7]  [get_cells x[1].y[0].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_6]  [get_cells x[1].y[0].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_5]  [get_cells x[1].y[0].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_4]  [get_cells x[1].y[0].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_3]  [get_cells x[1].y[0].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_2]  [get_cells x[1].y[0].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_1]  [get_cells x[1].y[0].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x1y0_0]  [get_cells x[1].y[0].clkregion/b[0].s]

resize_pblock       [get_pblocks pblock_x1y0_31] -add {SLICE_X182Y38:SLICE_X189Y49} 
resize_pblock       [get_pblocks pblock_x1y0_30] -add {SLICE_X182Y25:SLICE_X189Y37} 
resize_pblock       [get_pblocks pblock_x1y0_29] -add {SLICE_X182Y13:SLICE_X189Y24} 
resize_pblock       [get_pblocks pblock_x1y0_28] -add {SLICE_X182Y0:SLICE_X189Y12}

resize_pblock       [get_pblocks pblock_x1y0_27] -add {SLICE_X162Y38:SLICE_X181Y49} 
resize_pblock       [get_pblocks pblock_x1y0_26] -add {SLICE_X162Y25:SLICE_X181Y37} 
resize_pblock       [get_pblocks pblock_x1y0_25] -add {SLICE_X162Y13:SLICE_X181Y24} 
resize_pblock       [get_pblocks pblock_x1y0_24] -add {SLICE_X162Y0:SLICE_X181Y12}

resize_pblock       [get_pblocks pblock_x1y0_23] -add {SLICE_X152Y38:SLICE_X161Y49} 
resize_pblock       [get_pblocks pblock_x1y0_22] -add {SLICE_X152Y25:SLICE_X161Y37} 
resize_pblock       [get_pblocks pblock_x1y0_21] -add {SLICE_X152Y13:SLICE_X161Y24} 
resize_pblock       [get_pblocks pblock_x1y0_20] -add {SLICE_X152Y0:SLICE_X161Y12}

resize_pblock       [get_pblocks pblock_x1y0_19] -add {SLICE_X142Y38:SLICE_X151Y49} 
resize_pblock       [get_pblocks pblock_x1y0_18] -add {SLICE_X142Y25:SLICE_X151Y37} 
resize_pblock       [get_pblocks pblock_x1y0_17] -add {SLICE_X142Y13:SLICE_X151Y24} 
resize_pblock       [get_pblocks pblock_x1y0_16] -add {SLICE_X142Y0:SLICE_X151Y12}

resize_pblock       [get_pblocks pblock_x1y0_15] -add {SLICE_X132Y38:SLICE_X141Y49} 
resize_pblock       [get_pblocks pblock_x1y0_14] -add {SLICE_X132Y25:SLICE_X141Y37} 
resize_pblock       [get_pblocks pblock_x1y0_13] -add {SLICE_X132Y13:SLICE_X141Y24} 
resize_pblock       [get_pblocks pblock_x1y0_12] -add {SLICE_X132Y0:SLICE_X141Y12}

resize_pblock       [get_pblocks pblock_x1y0_11] -add {SLICE_X122Y38:SLICE_X131Y49} 
resize_pblock       [get_pblocks pblock_x1y0_10] -add {SLICE_X122Y25:SLICE_X131Y37} 
resize_pblock       [get_pblocks pblock_x1y0_9]  -add {SLICE_X122Y13:SLICE_X131Y24} 
resize_pblock       [get_pblocks pblock_x1y0_8]  -add {SLICE_X122Y0:SLICE_X131Y12}

resize_pblock       [get_pblocks pblock_x1y0_7]  -add {SLICE_X102Y38:SLICE_X121Y49} 
resize_pblock       [get_pblocks pblock_x1y0_6]  -add {SLICE_X102Y25:SLICE_X121Y37} 
resize_pblock       [get_pblocks pblock_x1y0_5]  -add {SLICE_X102Y13:SLICE_X121Y24} 
resize_pblock       [get_pblocks pblock_x1y0_4]  -add {SLICE_X102Y0:SLICE_X121Y12}

resize_pblock       [get_pblocks pblock_x1y0_3]  -add {SLICE_X92Y38:SLICE_X101Y49} 
resize_pblock       [get_pblocks pblock_x1y0_2]  -add {SLICE_X92Y25:SLICE_X101Y37} 
resize_pblock       [get_pblocks pblock_x1y0_1]  -add {SLICE_X92Y13:SLICE_X101Y24} 
resize_pblock       [get_pblocks pblock_x1y0_0]  -add {SLICE_X92Y0:SLICE_X101Y12}