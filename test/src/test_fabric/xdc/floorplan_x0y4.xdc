##############################
# fine scan
##############################

##====== x0y4 =======##

create_pblock                    pblock_x0y4_31
create_pblock                    pblock_x0y4_30
create_pblock                    pblock_x0y4_29
create_pblock                    pblock_x0y4_28
create_pblock                    pblock_x0y4_27
create_pblock                    pblock_x0y4_26
create_pblock                    pblock_x0y4_25
create_pblock                    pblock_x0y4_24
create_pblock                    pblock_x0y4_23
create_pblock                    pblock_x0y4_22
create_pblock                    pblock_x0y4_21
create_pblock                    pblock_x0y4_20
create_pblock                    pblock_x0y4_19
create_pblock                    pblock_x0y4_18
create_pblock                    pblock_x0y4_17
create_pblock                    pblock_x0y4_16
create_pblock                    pblock_x0y4_15
create_pblock                    pblock_x0y4_14
create_pblock                    pblock_x0y4_13
create_pblock                    pblock_x0y4_12
create_pblock                    pblock_x0y4_11
create_pblock                    pblock_x0y4_10
create_pblock                    pblock_x0y4_9
create_pblock                    pblock_x0y4_8
create_pblock                    pblock_x0y4_7
create_pblock                    pblock_x0y4_6
create_pblock                    pblock_x0y4_5
create_pblock                    pblock_x0y4_4
create_pblock                    pblock_x0y4_3
create_pblock                    pblock_x0y4_2
create_pblock                    pblock_x0y4_1
create_pblock                    pblock_x0y4_0

add_cells_to_pblock [get_pblocks pblock_x0y4_31] [get_cells x[0].y[4].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_30] [get_cells x[0].y[4].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_29] [get_cells x[0].y[4].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_28] [get_cells x[0].y[4].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_27] [get_cells x[0].y[4].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_26] [get_cells x[0].y[4].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_25] [get_cells x[0].y[4].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_24] [get_cells x[0].y[4].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_23] [get_cells x[0].y[4].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_22] [get_cells x[0].y[4].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_21] [get_cells x[0].y[4].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_20] [get_cells x[0].y[4].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_19] [get_cells x[0].y[4].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_18] [get_cells x[0].y[4].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_17] [get_cells x[0].y[4].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_16] [get_cells x[0].y[4].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_15] [get_cells x[0].y[4].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_14] [get_cells x[0].y[4].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_13] [get_cells x[0].y[4].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_12] [get_cells x[0].y[4].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_11] [get_cells x[0].y[4].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_10] [get_cells x[0].y[4].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_9]  [get_cells x[0].y[4].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_8]  [get_cells x[0].y[4].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_7]  [get_cells x[0].y[4].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_6]  [get_cells x[0].y[4].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_5]  [get_cells x[0].y[4].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_4]  [get_cells x[0].y[4].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_3]  [get_cells x[0].y[4].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_2]  [get_cells x[0].y[4].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_1]  [get_cells x[0].y[4].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x0y4_0]  [get_cells x[0].y[4].clkregion/b[0].s]

resize_pblock       [get_pblocks pblock_x0y4_31] -add {SLICE_X82Y238:SLICE_X91Y249} 
resize_pblock       [get_pblocks pblock_x0y4_30] -add {SLICE_X82Y225:SLICE_X91Y237} 
resize_pblock       [get_pblocks pblock_x0y4_29] -add {SLICE_X82Y213:SLICE_X91Y224} 
resize_pblock       [get_pblocks pblock_x0y4_28] -add {SLICE_X82Y200:SLICE_X91Y212}

resize_pblock       [get_pblocks pblock_x0y4_27] -add {SLICE_X72Y238:SLICE_X81Y249} 
resize_pblock       [get_pblocks pblock_x0y4_26] -add {SLICE_X72Y225:SLICE_X81Y237} 
resize_pblock       [get_pblocks pblock_x0y4_25] -add {SLICE_X72Y213:SLICE_X81Y224} 
resize_pblock       [get_pblocks pblock_x0y4_24] -add {SLICE_X72Y200:SLICE_X81Y212}

resize_pblock       [get_pblocks pblock_x0y4_23] -add {SLICE_X62Y238:SLICE_X71Y249} 
resize_pblock       [get_pblocks pblock_x0y4_22] -add {SLICE_X62Y225:SLICE_X71Y237} 
resize_pblock       [get_pblocks pblock_x0y4_21] -add {SLICE_X62Y213:SLICE_X71Y224} 
resize_pblock       [get_pblocks pblock_x0y4_20] -add {SLICE_X62Y200:SLICE_X71Y212}

resize_pblock       [get_pblocks pblock_x0y4_19] -add {SLICE_X52Y238:SLICE_X61Y249} 
resize_pblock       [get_pblocks pblock_x0y4_18] -add {SLICE_X52Y225:SLICE_X61Y237} 
resize_pblock       [get_pblocks pblock_x0y4_17] -add {SLICE_X52Y213:SLICE_X61Y224} 
resize_pblock       [get_pblocks pblock_x0y4_16] -add {SLICE_X52Y200:SLICE_X61Y212}

resize_pblock       [get_pblocks pblock_x0y4_15] -add {SLICE_X42Y238:SLICE_X51Y249} 
resize_pblock       [get_pblocks pblock_x0y4_14] -add {SLICE_X42Y225:SLICE_X51Y237} 
resize_pblock       [get_pblocks pblock_x0y4_13] -add {SLICE_X42Y213:SLICE_X51Y224} 
resize_pblock       [get_pblocks pblock_x0y4_12] -add {SLICE_X42Y200:SLICE_X51Y212}

resize_pblock       [get_pblocks pblock_x0y4_11] -add {SLICE_X20Y238:SLICE_X41Y249} 
resize_pblock       [get_pblocks pblock_x0y4_10] -add {SLICE_X20Y225:SLICE_X41Y237} 
resize_pblock       [get_pblocks pblock_x0y4_9]  -add {SLICE_X20Y213:SLICE_X41Y224} 
resize_pblock       [get_pblocks pblock_x0y4_8]  -add {SLICE_X20Y200:SLICE_X41Y212}

resize_pblock       [get_pblocks pblock_x0y4_7]  -add {SLICE_X10Y238:SLICE_X19Y249} 
resize_pblock       [get_pblocks pblock_x0y4_6]  -add {SLICE_X10Y225:SLICE_X19Y237} 
resize_pblock       [get_pblocks pblock_x0y4_5]  -add {SLICE_X10Y213:SLICE_X19Y224} 
resize_pblock       [get_pblocks pblock_x0y4_4]  -add {SLICE_X10Y200:SLICE_X19Y212}

resize_pblock       [get_pblocks pblock_x0y4_3]  -add {SLICE_X0Y238:SLICE_X9Y249} 
resize_pblock       [get_pblocks pblock_x0y4_2]  -add {SLICE_X0Y225:SLICE_X9Y237} 
resize_pblock       [get_pblocks pblock_x0y4_1]  -add {SLICE_X0Y213:SLICE_X9Y224} 
resize_pblock       [get_pblocks pblock_x0y4_0]  -add {SLICE_X0Y200:SLICE_X9Y212}