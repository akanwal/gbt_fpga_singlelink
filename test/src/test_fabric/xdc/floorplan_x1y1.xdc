##############################
# fine scan
##############################

##====== x1y1 =======##

create_pblock                    pblock_x1y1_31
create_pblock                    pblock_x1y1_30
create_pblock                    pblock_x1y1_29
create_pblock                    pblock_x1y1_28
create_pblock                    pblock_x1y1_27
create_pblock                    pblock_x1y1_26
create_pblock                    pblock_x1y1_25
create_pblock                    pblock_x1y1_24
create_pblock                    pblock_x1y1_23
create_pblock                    pblock_x1y1_22
create_pblock                    pblock_x1y1_21
create_pblock                    pblock_x1y1_20
create_pblock                    pblock_x1y1_19
create_pblock                    pblock_x1y1_18
create_pblock                    pblock_x1y1_17
create_pblock                    pblock_x1y1_16
create_pblock                    pblock_x1y1_15
create_pblock                    pblock_x1y1_14
create_pblock                    pblock_x1y1_13
create_pblock                    pblock_x1y1_12
create_pblock                    pblock_x1y1_11
create_pblock                    pblock_x1y1_10
create_pblock                    pblock_x1y1_9
create_pblock                    pblock_x1y1_8
create_pblock                    pblock_x1y1_7
create_pblock                    pblock_x1y1_6
create_pblock                    pblock_x1y1_5
create_pblock                    pblock_x1y1_4
create_pblock                    pblock_x1y1_3
create_pblock                    pblock_x1y1_2
create_pblock                    pblock_x1y1_1
create_pblock                    pblock_x1y1_0

add_cells_to_pblock [get_pblocks pblock_x1y1_31] [get_cells x[1].y[1].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_30] [get_cells x[1].y[1].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_29] [get_cells x[1].y[1].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_28] [get_cells x[1].y[1].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_27] [get_cells x[1].y[1].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_26] [get_cells x[1].y[1].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_25] [get_cells x[1].y[1].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_24] [get_cells x[1].y[1].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_23] [get_cells x[1].y[1].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_22] [get_cells x[1].y[1].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_21] [get_cells x[1].y[1].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_20] [get_cells x[1].y[1].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_19] [get_cells x[1].y[1].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_18] [get_cells x[1].y[1].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_17] [get_cells x[1].y[1].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_16] [get_cells x[1].y[1].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_15] [get_cells x[1].y[1].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_14] [get_cells x[1].y[1].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_13] [get_cells x[1].y[1].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_12] [get_cells x[1].y[1].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_11] [get_cells x[1].y[1].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_10] [get_cells x[1].y[1].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_9]  [get_cells x[1].y[1].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_8]  [get_cells x[1].y[1].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_7]  [get_cells x[1].y[1].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_6]  [get_cells x[1].y[1].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_5]  [get_cells x[1].y[1].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_4]  [get_cells x[1].y[1].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_3]  [get_cells x[1].y[1].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_2]  [get_cells x[1].y[1].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_1]  [get_cells x[1].y[1].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x1y1_0]  [get_cells x[1].y[1].clkregion/b[0].s]

resize_pblock       [get_pblocks pblock_x1y1_31] -add {SLICE_X182Y88:SLICE_X189Y99} 
resize_pblock       [get_pblocks pblock_x1y1_30] -add {SLICE_X182Y75:SLICE_X189Y87} 
resize_pblock       [get_pblocks pblock_x1y1_29] -add {SLICE_X182Y63:SLICE_X189Y74} 
resize_pblock       [get_pblocks pblock_x1y1_28] -add {SLICE_X182Y50:SLICE_X189Y62}

resize_pblock       [get_pblocks pblock_x1y1_27] -add {SLICE_X162Y88:SLICE_X181Y99} 
resize_pblock       [get_pblocks pblock_x1y1_26] -add {SLICE_X162Y75:SLICE_X181Y87} 
resize_pblock       [get_pblocks pblock_x1y1_25] -add {SLICE_X162Y63:SLICE_X181Y74} 
resize_pblock       [get_pblocks pblock_x1y1_24] -add {SLICE_X162Y50:SLICE_X181Y62}

resize_pblock       [get_pblocks pblock_x1y1_23] -add {SLICE_X152Y88:SLICE_X161Y99} 
resize_pblock       [get_pblocks pblock_x1y1_22] -add {SLICE_X152Y75:SLICE_X161Y87} 
resize_pblock       [get_pblocks pblock_x1y1_21] -add {SLICE_X152Y63:SLICE_X161Y74} 
resize_pblock       [get_pblocks pblock_x1y1_20] -add {SLICE_X152Y50:SLICE_X161Y62}

resize_pblock       [get_pblocks pblock_x1y1_19] -add {SLICE_X142Y88:SLICE_X151Y99} 
resize_pblock       [get_pblocks pblock_x1y1_18] -add {SLICE_X142Y75:SLICE_X151Y87} 
resize_pblock       [get_pblocks pblock_x1y1_17] -add {SLICE_X142Y63:SLICE_X151Y74} 
resize_pblock       [get_pblocks pblock_x1y1_16] -add {SLICE_X142Y50:SLICE_X151Y62}

resize_pblock       [get_pblocks pblock_x1y1_15] -add {SLICE_X132Y88:SLICE_X141Y99} 
resize_pblock       [get_pblocks pblock_x1y1_14] -add {SLICE_X132Y75:SLICE_X141Y87} 
resize_pblock       [get_pblocks pblock_x1y1_13] -add {SLICE_X132Y63:SLICE_X141Y74} 
resize_pblock       [get_pblocks pblock_x1y1_12] -add {SLICE_X132Y50:SLICE_X141Y62}

resize_pblock       [get_pblocks pblock_x1y1_11] -add {SLICE_X122Y88:SLICE_X131Y99} 
resize_pblock       [get_pblocks pblock_x1y1_10] -add {SLICE_X122Y75:SLICE_X131Y87} 
resize_pblock       [get_pblocks pblock_x1y1_9]  -add {SLICE_X122Y63:SLICE_X131Y74} 
resize_pblock       [get_pblocks pblock_x1y1_8]  -add {SLICE_X122Y50:SLICE_X131Y62}

resize_pblock       [get_pblocks pblock_x1y1_7]  -add {SLICE_X102Y88:SLICE_X121Y99} 
resize_pblock       [get_pblocks pblock_x1y1_6]  -add {SLICE_X102Y75:SLICE_X121Y87} 
resize_pblock       [get_pblocks pblock_x1y1_5]  -add {SLICE_X102Y63:SLICE_X121Y74} 
resize_pblock       [get_pblocks pblock_x1y1_4]  -add {SLICE_X102Y50:SLICE_X121Y62}

resize_pblock       [get_pblocks pblock_x1y1_3]  -add {SLICE_X92Y88:SLICE_X101Y99} 
resize_pblock       [get_pblocks pblock_x1y1_2]  -add {SLICE_X92Y75:SLICE_X101Y87} 
resize_pblock       [get_pblocks pblock_x1y1_1]  -add {SLICE_X92Y63:SLICE_X101Y74} 
resize_pblock       [get_pblocks pblock_x1y1_0]  -add {SLICE_X92Y50:SLICE_X101Y62}