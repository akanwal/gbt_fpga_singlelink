##############################
# fine scan
##############################

##====== x1y7 =======##

create_pblock                    pblock_x1y7_31
create_pblock                    pblock_x1y7_30
create_pblock                    pblock_x1y7_29
create_pblock                    pblock_x1y7_28
create_pblock                    pblock_x1y7_27
create_pblock                    pblock_x1y7_26
create_pblock                    pblock_x1y7_25
create_pblock                    pblock_x1y7_24
create_pblock                    pblock_x1y7_23
create_pblock                    pblock_x1y7_22
create_pblock                    pblock_x1y7_21
create_pblock                    pblock_x1y7_20
create_pblock                    pblock_x1y7_19
create_pblock                    pblock_x1y7_18
create_pblock                    pblock_x1y7_17
create_pblock                    pblock_x1y7_16
create_pblock                    pblock_x1y7_15
create_pblock                    pblock_x1y7_14
create_pblock                    pblock_x1y7_13
create_pblock                    pblock_x1y7_12
create_pblock                    pblock_x1y7_11
create_pblock                    pblock_x1y7_10
create_pblock                    pblock_x1y7_9
create_pblock                    pblock_x1y7_8
create_pblock                    pblock_x1y7_7
create_pblock                    pblock_x1y7_6
create_pblock                    pblock_x1y7_5
create_pblock                    pblock_x1y7_4
create_pblock                    pblock_x1y7_3
create_pblock                    pblock_x1y7_2
create_pblock                    pblock_x1y7_1
create_pblock                    pblock_x1y7_0

add_cells_to_pblock [get_pblocks pblock_x1y7_31] [get_cells x[1].y[7].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_30] [get_cells x[1].y[7].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_29] [get_cells x[1].y[7].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_28] [get_cells x[1].y[7].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_27] [get_cells x[1].y[7].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_26] [get_cells x[1].y[7].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_25] [get_cells x[1].y[7].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_24] [get_cells x[1].y[7].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_23] [get_cells x[1].y[7].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_22] [get_cells x[1].y[7].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_21] [get_cells x[1].y[7].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_20] [get_cells x[1].y[7].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_19] [get_cells x[1].y[7].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_18] [get_cells x[1].y[7].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_17] [get_cells x[1].y[7].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_16] [get_cells x[1].y[7].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_15] [get_cells x[1].y[7].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_14] [get_cells x[1].y[7].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_13] [get_cells x[1].y[7].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_12] [get_cells x[1].y[7].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_11] [get_cells x[1].y[7].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_10] [get_cells x[1].y[7].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_9]  [get_cells x[1].y[7].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_8]  [get_cells x[1].y[7].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_7]  [get_cells x[1].y[7].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_6]  [get_cells x[1].y[7].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_5]  [get_cells x[1].y[7].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_4]  [get_cells x[1].y[7].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_3]  [get_cells x[1].y[7].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_2]  [get_cells x[1].y[7].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_1]  [get_cells x[1].y[7].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x1y7_0]  [get_cells x[1].y[7].clkregion/b[0].s]

resize_pblock       [get_pblocks pblock_x1y7_31] -add {SLICE_X182Y388:SLICE_X189Y399} 
resize_pblock       [get_pblocks pblock_x1y7_30] -add {SLICE_X182Y375:SLICE_X189Y387} 
resize_pblock       [get_pblocks pblock_x1y7_29] -add {SLICE_X182Y363:SLICE_X189Y374} 
resize_pblock       [get_pblocks pblock_x1y7_28] -add {SLICE_X182Y350:SLICE_X189Y362}

resize_pblock       [get_pblocks pblock_x1y7_27] -add {SLICE_X162Y388:SLICE_X181Y399} 
resize_pblock       [get_pblocks pblock_x1y7_26] -add {SLICE_X162Y375:SLICE_X181Y387} 
resize_pblock       [get_pblocks pblock_x1y7_25] -add {SLICE_X162Y363:SLICE_X181Y374} 
resize_pblock       [get_pblocks pblock_x1y7_24] -add {SLICE_X162Y350:SLICE_X181Y362}

resize_pblock       [get_pblocks pblock_x1y7_23] -add {SLICE_X152Y388:SLICE_X161Y399} 
resize_pblock       [get_pblocks pblock_x1y7_22] -add {SLICE_X152Y375:SLICE_X161Y387} 
resize_pblock       [get_pblocks pblock_x1y7_21] -add {SLICE_X152Y363:SLICE_X161Y374} 
resize_pblock       [get_pblocks pblock_x1y7_20] -add {SLICE_X152Y350:SLICE_X161Y362}

resize_pblock       [get_pblocks pblock_x1y7_19] -add {SLICE_X142Y388:SLICE_X151Y399} 
resize_pblock       [get_pblocks pblock_x1y7_18] -add {SLICE_X142Y375:SLICE_X151Y387} 
resize_pblock       [get_pblocks pblock_x1y7_17] -add {SLICE_X142Y363:SLICE_X151Y374} 
resize_pblock       [get_pblocks pblock_x1y7_16] -add {SLICE_X142Y350:SLICE_X151Y362}

resize_pblock       [get_pblocks pblock_x1y7_15] -add {SLICE_X132Y388:SLICE_X141Y399} 
resize_pblock       [get_pblocks pblock_x1y7_14] -add {SLICE_X132Y375:SLICE_X141Y387} 
resize_pblock       [get_pblocks pblock_x1y7_13] -add {SLICE_X132Y363:SLICE_X141Y374} 
resize_pblock       [get_pblocks pblock_x1y7_12] -add {SLICE_X132Y350:SLICE_X141Y362}

resize_pblock       [get_pblocks pblock_x1y7_11] -add {SLICE_X122Y388:SLICE_X131Y399} 
resize_pblock       [get_pblocks pblock_x1y7_10] -add {SLICE_X122Y375:SLICE_X131Y387} 
resize_pblock       [get_pblocks pblock_x1y7_9]  -add {SLICE_X122Y363:SLICE_X131Y374} 
resize_pblock       [get_pblocks pblock_x1y7_8]  -add {SLICE_X122Y350:SLICE_X131Y362}

resize_pblock       [get_pblocks pblock_x1y7_7]  -add {SLICE_X102Y388:SLICE_X121Y399} 
resize_pblock       [get_pblocks pblock_x1y7_6]  -add {SLICE_X102Y375:SLICE_X121Y387} 
resize_pblock       [get_pblocks pblock_x1y7_5]  -add {SLICE_X102Y363:SLICE_X121Y374} 
resize_pblock       [get_pblocks pblock_x1y7_4]  -add {SLICE_X102Y350:SLICE_X121Y362}

resize_pblock       [get_pblocks pblock_x1y7_3]  -add {SLICE_X92Y388:SLICE_X101Y399} 
resize_pblock       [get_pblocks pblock_x1y7_2]  -add {SLICE_X92Y375:SLICE_X101Y387} 
resize_pblock       [get_pblocks pblock_x1y7_1]  -add {SLICE_X92Y363:SLICE_X101Y374} 
resize_pblock       [get_pblocks pblock_x1y7_0]  -add {SLICE_X92Y350:SLICE_X101Y362}