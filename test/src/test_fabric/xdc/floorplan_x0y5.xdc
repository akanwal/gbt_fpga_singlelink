##############################
# fine scan
##############################

##====== x0y5 =======##

create_pblock                    pblock_x0y5_31
create_pblock                    pblock_x0y5_30
create_pblock                    pblock_x0y5_29
create_pblock                    pblock_x0y5_28
create_pblock                    pblock_x0y5_27
create_pblock                    pblock_x0y5_26
create_pblock                    pblock_x0y5_25
create_pblock                    pblock_x0y5_24
create_pblock                    pblock_x0y5_23
create_pblock                    pblock_x0y5_22
create_pblock                    pblock_x0y5_21
create_pblock                    pblock_x0y5_20
create_pblock                    pblock_x0y5_19
create_pblock                    pblock_x0y5_18
create_pblock                    pblock_x0y5_17
create_pblock                    pblock_x0y5_16
create_pblock                    pblock_x0y5_15
create_pblock                    pblock_x0y5_14
create_pblock                    pblock_x0y5_13
create_pblock                    pblock_x0y5_12
create_pblock                    pblock_x0y5_11
create_pblock                    pblock_x0y5_10
create_pblock                    pblock_x0y5_9
create_pblock                    pblock_x0y5_8
create_pblock                    pblock_x0y5_7
create_pblock                    pblock_x0y5_6
create_pblock                    pblock_x0y5_5
create_pblock                    pblock_x0y5_4
create_pblock                    pblock_x0y5_3
create_pblock                    pblock_x0y5_2
create_pblock                    pblock_x0y5_1
create_pblock                    pblock_x0y5_0

add_cells_to_pblock [get_pblocks pblock_x0y5_31] [get_cells x[0].y[5].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_30] [get_cells x[0].y[5].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_29] [get_cells x[0].y[5].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_28] [get_cells x[0].y[5].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_27] [get_cells x[0].y[5].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_26] [get_cells x[0].y[5].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_25] [get_cells x[0].y[5].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_24] [get_cells x[0].y[5].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_23] [get_cells x[0].y[5].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_22] [get_cells x[0].y[5].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_21] [get_cells x[0].y[5].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_20] [get_cells x[0].y[5].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_19] [get_cells x[0].y[5].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_18] [get_cells x[0].y[5].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_17] [get_cells x[0].y[5].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_16] [get_cells x[0].y[5].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_15] [get_cells x[0].y[5].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_14] [get_cells x[0].y[5].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_13] [get_cells x[0].y[5].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_12] [get_cells x[0].y[5].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_11] [get_cells x[0].y[5].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_10] [get_cells x[0].y[5].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_9]  [get_cells x[0].y[5].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_8]  [get_cells x[0].y[5].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_7]  [get_cells x[0].y[5].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_6]  [get_cells x[0].y[5].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_5]  [get_cells x[0].y[5].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_4]  [get_cells x[0].y[5].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_3]  [get_cells x[0].y[5].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_2]  [get_cells x[0].y[5].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_1]  [get_cells x[0].y[5].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x0y5_0]  [get_cells x[0].y[5].clkregion/b[0].s]


resize_pblock       [get_pblocks pblock_x0y5_31] -add {SLICE_X82Y288:SLICE_X91Y299} 
resize_pblock       [get_pblocks pblock_x0y5_30] -add {SLICE_X82Y275:SLICE_X91Y287} 
resize_pblock       [get_pblocks pblock_x0y5_29] -add {SLICE_X82Y263:SLICE_X91Y274} 
resize_pblock       [get_pblocks pblock_x0y5_28] -add {SLICE_X82Y250:SLICE_X91Y262}

resize_pblock       [get_pblocks pblock_x0y5_27] -add {SLICE_X72Y288:SLICE_X81Y299} 
resize_pblock       [get_pblocks pblock_x0y5_26] -add {SLICE_X72Y275:SLICE_X81Y287} 
resize_pblock       [get_pblocks pblock_x0y5_25] -add {SLICE_X72Y263:SLICE_X81Y274} 
resize_pblock       [get_pblocks pblock_x0y5_24] -add {SLICE_X72Y250:SLICE_X81Y262}

resize_pblock       [get_pblocks pblock_x0y5_23] -add {SLICE_X62Y288:SLICE_X71Y299} 
resize_pblock       [get_pblocks pblock_x0y5_22] -add {SLICE_X62Y275:SLICE_X71Y287} 
resize_pblock       [get_pblocks pblock_x0y5_21] -add {SLICE_X62Y263:SLICE_X71Y274} 
resize_pblock       [get_pblocks pblock_x0y5_20] -add {SLICE_X62Y250:SLICE_X71Y262}

resize_pblock       [get_pblocks pblock_x0y5_19] -add {SLICE_X52Y288:SLICE_X61Y299} 
resize_pblock       [get_pblocks pblock_x0y5_18] -add {SLICE_X52Y275:SLICE_X61Y287} 
resize_pblock       [get_pblocks pblock_x0y5_17] -add {SLICE_X52Y263:SLICE_X61Y274} 
resize_pblock       [get_pblocks pblock_x0y5_16] -add {SLICE_X52Y250:SLICE_X61Y262}

resize_pblock       [get_pblocks pblock_x0y5_15] -add {SLICE_X42Y288:SLICE_X51Y299} 
resize_pblock       [get_pblocks pblock_x0y5_14] -add {SLICE_X42Y275:SLICE_X51Y287} 
resize_pblock       [get_pblocks pblock_x0y5_13] -add {SLICE_X42Y263:SLICE_X51Y274} 
resize_pblock       [get_pblocks pblock_x0y5_12] -add {SLICE_X42Y250:SLICE_X51Y262}

resize_pblock       [get_pblocks pblock_x0y5_11] -add {SLICE_X20Y288:SLICE_X41Y299}
resize_pblock       [get_pblocks pblock_x0y5_10] -add {SLICE_X20Y275:SLICE_X41Y287}
resize_pblock       [get_pblocks pblock_x0y5_9]  -add {SLICE_X20Y263:SLICE_X41Y274} 
resize_pblock       [get_pblocks pblock_x0y5_8]  -add {SLICE_X20Y250:SLICE_X41Y262}

resize_pblock       [get_pblocks pblock_x0y5_7]  -add {SLICE_X10Y288:SLICE_X19Y299} 
resize_pblock       [get_pblocks pblock_x0y5_6]  -add {SLICE_X10Y275:SLICE_X19Y287} 
resize_pblock       [get_pblocks pblock_x0y5_5]  -add {SLICE_X10Y263:SLICE_X19Y274}
resize_pblock       [get_pblocks pblock_x0y5_4]  -add {SLICE_X10Y250:SLICE_X19Y262}

resize_pblock       [get_pblocks pblock_x0y5_3]  -add {SLICE_X0Y288:SLICE_X9Y299}
resize_pblock       [get_pblocks pblock_x0y5_2]  -add {SLICE_X0Y275:SLICE_X9Y287}
resize_pblock       [get_pblocks pblock_x0y5_1]  -add {SLICE_X0Y263:SLICE_X9Y274} 
resize_pblock       [get_pblocks pblock_x0y5_0]  -add {SLICE_X0Y250:SLICE_X9Y262}