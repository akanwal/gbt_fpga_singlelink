##############################
# fine scan
##############################

##====== x0y7 =======##

create_pblock                    pblock_x0y7_31
create_pblock                    pblock_x0y7_30
create_pblock                    pblock_x0y7_29
create_pblock                    pblock_x0y7_28
create_pblock                    pblock_x0y7_27
create_pblock                    pblock_x0y7_26
create_pblock                    pblock_x0y7_25
create_pblock                    pblock_x0y7_24
create_pblock                    pblock_x0y7_23
create_pblock                    pblock_x0y7_22
create_pblock                    pblock_x0y7_21
create_pblock                    pblock_x0y7_20
create_pblock                    pblock_x0y7_19
create_pblock                    pblock_x0y7_18
create_pblock                    pblock_x0y7_17
create_pblock                    pblock_x0y7_16
create_pblock                    pblock_x0y7_15
create_pblock                    pblock_x0y7_14
create_pblock                    pblock_x0y7_13
create_pblock                    pblock_x0y7_12
create_pblock                    pblock_x0y7_11
create_pblock                    pblock_x0y7_10
create_pblock                    pblock_x0y7_9
create_pblock                    pblock_x0y7_8
create_pblock                    pblock_x0y7_7
create_pblock                    pblock_x0y7_6
create_pblock                    pblock_x0y7_5
create_pblock                    pblock_x0y7_4
create_pblock                    pblock_x0y7_3
create_pblock                    pblock_x0y7_2
create_pblock                    pblock_x0y7_1
create_pblock                    pblock_x0y7_0

add_cells_to_pblock [get_pblocks pblock_x0y7_31] [get_cells x[0].y[7].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_30] [get_cells x[0].y[7].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_29] [get_cells x[0].y[7].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_28] [get_cells x[0].y[7].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_27] [get_cells x[0].y[7].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_26] [get_cells x[0].y[7].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_25] [get_cells x[0].y[7].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_24] [get_cells x[0].y[7].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_23] [get_cells x[0].y[7].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_22] [get_cells x[0].y[7].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_21] [get_cells x[0].y[7].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_20] [get_cells x[0].y[7].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_19] [get_cells x[0].y[7].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_18] [get_cells x[0].y[7].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_17] [get_cells x[0].y[7].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_16] [get_cells x[0].y[7].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_15] [get_cells x[0].y[7].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_14] [get_cells x[0].y[7].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_13] [get_cells x[0].y[7].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_12] [get_cells x[0].y[7].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_11] [get_cells x[0].y[7].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_10] [get_cells x[0].y[7].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_9]  [get_cells x[0].y[7].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_8]  [get_cells x[0].y[7].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_7]  [get_cells x[0].y[7].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_6]  [get_cells x[0].y[7].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_5]  [get_cells x[0].y[7].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_4]  [get_cells x[0].y[7].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_3]  [get_cells x[0].y[7].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_2]  [get_cells x[0].y[7].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_1]  [get_cells x[0].y[7].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x0y7_0]  [get_cells x[0].y[7].clkregion/b[0].s]


resize_pblock       [get_pblocks pblock_x0y7_31] -add {SLICE_X82Y388:SLICE_X91Y399} 
resize_pblock       [get_pblocks pblock_x0y7_30] -add {SLICE_X82Y375:SLICE_X91Y387} 
resize_pblock       [get_pblocks pblock_x0y7_29] -add {SLICE_X82Y363:SLICE_X91Y374} 
resize_pblock       [get_pblocks pblock_x0y7_28] -add {SLICE_X82Y350:SLICE_X91Y362}

resize_pblock       [get_pblocks pblock_x0y7_27] -add {SLICE_X72Y388:SLICE_X81Y399} 
resize_pblock       [get_pblocks pblock_x0y7_26] -add {SLICE_X72Y375:SLICE_X81Y387} 
resize_pblock       [get_pblocks pblock_x0y7_25] -add {SLICE_X72Y363:SLICE_X81Y374} 
resize_pblock       [get_pblocks pblock_x0y7_24] -add {SLICE_X72Y350:SLICE_X81Y362}

resize_pblock       [get_pblocks pblock_x0y7_23] -add {SLICE_X62Y388:SLICE_X71Y399} 
resize_pblock       [get_pblocks pblock_x0y7_22] -add {SLICE_X62Y375:SLICE_X71Y387} 
resize_pblock       [get_pblocks pblock_x0y7_21] -add {SLICE_X62Y363:SLICE_X71Y374} 
resize_pblock       [get_pblocks pblock_x0y7_20] -add {SLICE_X62Y350:SLICE_X71Y362}

resize_pblock       [get_pblocks pblock_x0y7_19] -add {SLICE_X52Y388:SLICE_X61Y399} 
resize_pblock       [get_pblocks pblock_x0y7_18] -add {SLICE_X52Y375:SLICE_X61Y387} 
resize_pblock       [get_pblocks pblock_x0y7_17] -add {SLICE_X52Y363:SLICE_X61Y374} 
resize_pblock       [get_pblocks pblock_x0y7_16] -add {SLICE_X52Y350:SLICE_X61Y362}

resize_pblock       [get_pblocks pblock_x0y7_15] -add {SLICE_X42Y388:SLICE_X51Y399} 
resize_pblock       [get_pblocks pblock_x0y7_14] -add {SLICE_X42Y375:SLICE_X51Y387} 
resize_pblock       [get_pblocks pblock_x0y7_13] -add {SLICE_X42Y363:SLICE_X51Y374} 
resize_pblock       [get_pblocks pblock_x0y7_12] -add {SLICE_X42Y350:SLICE_X51Y362}

resize_pblock       [get_pblocks pblock_x0y7_11] -add {SLICE_X20Y388:SLICE_X41Y399}
resize_pblock       [get_pblocks pblock_x0y7_10] -add {SLICE_X20Y375:SLICE_X41Y387}
resize_pblock       [get_pblocks pblock_x0y7_9]  -add {SLICE_X20Y363:SLICE_X41Y374} 
resize_pblock       [get_pblocks pblock_x0y7_8]  -add {SLICE_X20Y350:SLICE_X41Y362}

resize_pblock       [get_pblocks pblock_x0y7_7]  -add {SLICE_X10Y388:SLICE_X19Y399} 
resize_pblock       [get_pblocks pblock_x0y7_6]  -add {SLICE_X10Y375:SLICE_X19Y387} 
resize_pblock       [get_pblocks pblock_x0y7_5]  -add {SLICE_X10Y363:SLICE_X19Y374}
resize_pblock       [get_pblocks pblock_x0y7_4]  -add {SLICE_X10Y350:SLICE_X19Y362}

resize_pblock       [get_pblocks pblock_x0y7_3]  -add {SLICE_X0Y388:SLICE_X9Y399}
resize_pblock       [get_pblocks pblock_x0y7_2]  -add {SLICE_X0Y375:SLICE_X9Y387}
resize_pblock       [get_pblocks pblock_x0y7_1]  -add {SLICE_X0Y363:SLICE_X9Y374} 
resize_pblock       [get_pblocks pblock_x0y7_0]  -add {SLICE_X0Y350:SLICE_X9Y362}