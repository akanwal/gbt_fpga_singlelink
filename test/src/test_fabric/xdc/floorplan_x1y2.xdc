##############################
# fine scan
##############################

##====== x1y2 =======##

create_pblock                    pblock_x1y2_31
create_pblock                    pblock_x1y2_30
create_pblock                    pblock_x1y2_29
create_pblock                    pblock_x1y2_28
create_pblock                    pblock_x1y2_27
create_pblock                    pblock_x1y2_26
create_pblock                    pblock_x1y2_25
create_pblock                    pblock_x1y2_24
create_pblock                    pblock_x1y2_23
create_pblock                    pblock_x1y2_22
create_pblock                    pblock_x1y2_21
create_pblock                    pblock_x1y2_20
create_pblock                    pblock_x1y2_19
create_pblock                    pblock_x1y2_18
create_pblock                    pblock_x1y2_17
create_pblock                    pblock_x1y2_16
create_pblock                    pblock_x1y2_15
create_pblock                    pblock_x1y2_14
create_pblock                    pblock_x1y2_13
create_pblock                    pblock_x1y2_12
create_pblock                    pblock_x1y2_11
create_pblock                    pblock_x1y2_10
create_pblock                    pblock_x1y2_9
create_pblock                    pblock_x1y2_8
create_pblock                    pblock_x1y2_7
create_pblock                    pblock_x1y2_6
create_pblock                    pblock_x1y2_5
create_pblock                    pblock_x1y2_4
create_pblock                    pblock_x1y2_3
create_pblock                    pblock_x1y2_2
create_pblock                    pblock_x1y2_1
create_pblock                    pblock_x1y2_0

add_cells_to_pblock [get_pblocks pblock_x1y2_31] [get_cells x[1].y[2].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_30] [get_cells x[1].y[2].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_29] [get_cells x[1].y[2].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_28] [get_cells x[1].y[2].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_27] [get_cells x[1].y[2].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_26] [get_cells x[1].y[2].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_25] [get_cells x[1].y[2].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_24] [get_cells x[1].y[2].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_23] [get_cells x[1].y[2].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_22] [get_cells x[1].y[2].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_21] [get_cells x[1].y[2].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_20] [get_cells x[1].y[2].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_19] [get_cells x[1].y[2].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_18] [get_cells x[1].y[2].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_17] [get_cells x[1].y[2].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_16] [get_cells x[1].y[2].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_15] [get_cells x[1].y[2].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_14] [get_cells x[1].y[2].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_13] [get_cells x[1].y[2].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_12] [get_cells x[1].y[2].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_11] [get_cells x[1].y[2].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_10] [get_cells x[1].y[2].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_9]  [get_cells x[1].y[2].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_8]  [get_cells x[1].y[2].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_7]  [get_cells x[1].y[2].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_6]  [get_cells x[1].y[2].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_5]  [get_cells x[1].y[2].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_4]  [get_cells x[1].y[2].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_3]  [get_cells x[1].y[2].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_2]  [get_cells x[1].y[2].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_1]  [get_cells x[1].y[2].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x1y2_0]  [get_cells x[1].y[2].clkregion/b[0].s]

resize_pblock       [get_pblocks pblock_x1y2_31] -add {SLICE_X182Y138:SLICE_X189Y149} 
resize_pblock       [get_pblocks pblock_x1y2_30] -add {SLICE_X182Y125:SLICE_X189Y137} 
resize_pblock       [get_pblocks pblock_x1y2_29] -add {SLICE_X182Y113:SLICE_X189Y124} 
resize_pblock       [get_pblocks pblock_x1y2_28] -add {SLICE_X182Y100:SLICE_X189Y112}

resize_pblock       [get_pblocks pblock_x1y2_27] -add {SLICE_X162Y138:SLICE_X181Y149} 
resize_pblock       [get_pblocks pblock_x1y2_26] -add {SLICE_X162Y125:SLICE_X181Y137} 
resize_pblock       [get_pblocks pblock_x1y2_25] -add {SLICE_X162Y113:SLICE_X181Y124} 
resize_pblock       [get_pblocks pblock_x1y2_24] -add {SLICE_X162Y100:SLICE_X181Y112}

resize_pblock       [get_pblocks pblock_x1y2_23] -add {SLICE_X152Y138:SLICE_X161Y149} 
resize_pblock       [get_pblocks pblock_x1y2_22] -add {SLICE_X152Y125:SLICE_X161Y137} 
resize_pblock       [get_pblocks pblock_x1y2_21] -add {SLICE_X152Y113:SLICE_X161Y124} 
resize_pblock       [get_pblocks pblock_x1y2_20] -add {SLICE_X152Y100:SLICE_X161Y112}

resize_pblock       [get_pblocks pblock_x1y2_19] -add {SLICE_X142Y138:SLICE_X151Y149} 
resize_pblock       [get_pblocks pblock_x1y2_18] -add {SLICE_X142Y125:SLICE_X151Y137} 
resize_pblock       [get_pblocks pblock_x1y2_17] -add {SLICE_X142Y113:SLICE_X151Y124} 
resize_pblock       [get_pblocks pblock_x1y2_16] -add {SLICE_X142Y100:SLICE_X151Y112}

resize_pblock       [get_pblocks pblock_x1y2_15] -add {SLICE_X132Y138:SLICE_X141Y149} 
resize_pblock       [get_pblocks pblock_x1y2_14] -add {SLICE_X132Y125:SLICE_X141Y137} 
resize_pblock       [get_pblocks pblock_x1y2_13] -add {SLICE_X132Y113:SLICE_X141Y124} 
resize_pblock       [get_pblocks pblock_x1y2_12] -add {SLICE_X132Y100:SLICE_X141Y112}

resize_pblock       [get_pblocks pblock_x1y2_11] -add {SLICE_X122Y138:SLICE_X131Y149} 
resize_pblock       [get_pblocks pblock_x1y2_10] -add {SLICE_X122Y125:SLICE_X131Y137} 
resize_pblock       [get_pblocks pblock_x1y2_9]  -add {SLICE_X122Y113:SLICE_X131Y124} 
resize_pblock       [get_pblocks pblock_x1y2_8]  -add {SLICE_X122Y100:SLICE_X131Y112}

resize_pblock       [get_pblocks pblock_x1y2_7]  -add {SLICE_X102Y138:SLICE_X121Y149} 
resize_pblock       [get_pblocks pblock_x1y2_6]  -add {SLICE_X102Y125:SLICE_X121Y137} 
resize_pblock       [get_pblocks pblock_x1y2_5]  -add {SLICE_X102Y113:SLICE_X121Y124} 
resize_pblock       [get_pblocks pblock_x1y2_4]  -add {SLICE_X102Y100:SLICE_X121Y112}

resize_pblock       [get_pblocks pblock_x1y2_3]  -add {SLICE_X92Y138:SLICE_X101Y149} 
resize_pblock       [get_pblocks pblock_x1y2_2]  -add {SLICE_X92Y125:SLICE_X101Y137} 
resize_pblock       [get_pblocks pblock_x1y2_1]  -add {SLICE_X92Y113:SLICE_X101Y124} 
resize_pblock       [get_pblocks pblock_x1y2_0]  -add {SLICE_X92Y100:SLICE_X101Y112}