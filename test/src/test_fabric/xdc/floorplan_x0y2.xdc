##############################
# fine scan
##############################

##====== x0y2 =======##

create_pblock                    pblock_x0y2_31
create_pblock                    pblock_x0y2_30
create_pblock                    pblock_x0y2_29
create_pblock                    pblock_x0y2_28
create_pblock                    pblock_x0y2_27
create_pblock                    pblock_x0y2_26
create_pblock                    pblock_x0y2_25
create_pblock                    pblock_x0y2_24
create_pblock                    pblock_x0y2_23
create_pblock                    pblock_x0y2_22
create_pblock                    pblock_x0y2_21
create_pblock                    pblock_x0y2_20
create_pblock                    pblock_x0y2_19
create_pblock                    pblock_x0y2_18
create_pblock                    pblock_x0y2_17
create_pblock                    pblock_x0y2_16
create_pblock                    pblock_x0y2_15
create_pblock                    pblock_x0y2_14
create_pblock                    pblock_x0y2_13
create_pblock                    pblock_x0y2_12
create_pblock                    pblock_x0y2_11
create_pblock                    pblock_x0y2_10
create_pblock                    pblock_x0y2_9
create_pblock                    pblock_x0y2_8
create_pblock                    pblock_x0y2_7
create_pblock                    pblock_x0y2_6
create_pblock                    pblock_x0y2_5
create_pblock                    pblock_x0y2_4
create_pblock                    pblock_x0y2_3
create_pblock                    pblock_x0y2_2
create_pblock                    pblock_x0y2_1
create_pblock                    pblock_x0y2_0

add_cells_to_pblock [get_pblocks pblock_x0y2_31] [get_cells x[0].y[2].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_30] [get_cells x[0].y[2].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_29] [get_cells x[0].y[2].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_28] [get_cells x[0].y[2].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_27] [get_cells x[0].y[2].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_26] [get_cells x[0].y[2].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_25] [get_cells x[0].y[2].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_24] [get_cells x[0].y[2].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_23] [get_cells x[0].y[2].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_22] [get_cells x[0].y[2].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_21] [get_cells x[0].y[2].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_20] [get_cells x[0].y[2].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_19] [get_cells x[0].y[2].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_18] [get_cells x[0].y[2].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_17] [get_cells x[0].y[2].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_16] [get_cells x[0].y[2].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_15] [get_cells x[0].y[2].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_14] [get_cells x[0].y[2].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_13] [get_cells x[0].y[2].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_12] [get_cells x[0].y[2].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_11] [get_cells x[0].y[2].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_10] [get_cells x[0].y[2].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_9]  [get_cells x[0].y[2].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_8]  [get_cells x[0].y[2].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_7]  [get_cells x[0].y[2].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_6]  [get_cells x[0].y[2].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_5]  [get_cells x[0].y[2].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_4]  [get_cells x[0].y[2].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_3]  [get_cells x[0].y[2].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_2]  [get_cells x[0].y[2].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_1]  [get_cells x[0].y[2].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x0y2_0]  [get_cells x[0].y[2].clkregion/b[0].s]


resize_pblock       [get_pblocks pblock_x0y2_31] -add {SLICE_X82Y138:SLICE_X91Y149} 
resize_pblock       [get_pblocks pblock_x0y2_30] -add {SLICE_X82Y125:SLICE_X91Y137} 
resize_pblock       [get_pblocks pblock_x0y2_29] -add {SLICE_X82Y113:SLICE_X91Y124} 
resize_pblock       [get_pblocks pblock_x0y2_28] -add {SLICE_X82Y100:SLICE_X91Y112}

resize_pblock       [get_pblocks pblock_x0y2_27] -add {SLICE_X72Y138:SLICE_X81Y149} 
resize_pblock       [get_pblocks pblock_x0y2_26] -add {SLICE_X72Y125:SLICE_X81Y137} 
resize_pblock       [get_pblocks pblock_x0y2_25] -add {SLICE_X72Y113:SLICE_X81Y124} 
resize_pblock       [get_pblocks pblock_x0y2_24] -add {SLICE_X72Y100:SLICE_X81Y112}

resize_pblock       [get_pblocks pblock_x0y2_23] -add {SLICE_X62Y138:SLICE_X71Y149} 
resize_pblock       [get_pblocks pblock_x0y2_22] -add {SLICE_X62Y125:SLICE_X71Y137} 
resize_pblock       [get_pblocks pblock_x0y2_21] -add {SLICE_X62Y113:SLICE_X71Y124} 
resize_pblock       [get_pblocks pblock_x0y2_20] -add {SLICE_X62Y100:SLICE_X71Y112}

resize_pblock       [get_pblocks pblock_x0y2_19] -add {SLICE_X52Y138:SLICE_X61Y149} 
resize_pblock       [get_pblocks pblock_x0y2_18] -add {SLICE_X52Y125:SLICE_X61Y137} 
resize_pblock       [get_pblocks pblock_x0y2_17] -add {SLICE_X52Y113:SLICE_X61Y124} 
resize_pblock       [get_pblocks pblock_x0y2_16] -add {SLICE_X52Y100:SLICE_X61Y112}

resize_pblock       [get_pblocks pblock_x0y2_15] -add {SLICE_X42Y138:SLICE_X51Y149} 
resize_pblock       [get_pblocks pblock_x0y2_14] -add {SLICE_X42Y125:SLICE_X51Y137} 
resize_pblock       [get_pblocks pblock_x0y2_13] -add {SLICE_X42Y113:SLICE_X51Y124} 
resize_pblock       [get_pblocks pblock_x0y2_12] -add {SLICE_X42Y100:SLICE_X51Y112}

resize_pblock       [get_pblocks pblock_x0y2_11] -add {SLICE_X20Y138:SLICE_X41Y149}
resize_pblock       [get_pblocks pblock_x0y2_10] -add {SLICE_X20Y125:SLICE_X41Y137}
resize_pblock       [get_pblocks pblock_x0y2_9]  -add {SLICE_X20Y113:SLICE_X41Y124} 
resize_pblock       [get_pblocks pblock_x0y2_8]  -add {SLICE_X20Y100:SLICE_X41Y112}

resize_pblock       [get_pblocks pblock_x0y2_7]  -add {SLICE_X10Y138:SLICE_X19Y149} 
resize_pblock       [get_pblocks pblock_x0y2_6]  -add {SLICE_X10Y125:SLICE_X19Y137} 
resize_pblock       [get_pblocks pblock_x0y2_5]  -add {SLICE_X10Y113:SLICE_X19Y124}
resize_pblock       [get_pblocks pblock_x0y2_4]  -add {SLICE_X10Y100:SLICE_X19Y112}

resize_pblock       [get_pblocks pblock_x0y2_3]  -add {SLICE_X0Y138:SLICE_X9Y149}
resize_pblock       [get_pblocks pblock_x0y2_2]  -add {SLICE_X0Y125:SLICE_X9Y137}
resize_pblock       [get_pblocks pblock_x0y2_1]  -add {SLICE_X0Y113:SLICE_X9Y124} 
resize_pblock       [get_pblocks pblock_x0y2_0]  -add {SLICE_X0Y100:SLICE_X9Y112}