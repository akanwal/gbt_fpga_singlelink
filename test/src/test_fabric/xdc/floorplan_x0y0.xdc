##############################
# fine scan
##############################

##====== x0y0 =======##

create_pblock                    pblock_x0y0_31
create_pblock                    pblock_x0y0_30
create_pblock                    pblock_x0y0_29
create_pblock                    pblock_x0y0_28
create_pblock                    pblock_x0y0_27
create_pblock                    pblock_x0y0_26
create_pblock                    pblock_x0y0_25
create_pblock                    pblock_x0y0_24
create_pblock                    pblock_x0y0_23
create_pblock                    pblock_x0y0_22
create_pblock                    pblock_x0y0_21
create_pblock                    pblock_x0y0_20
create_pblock                    pblock_x0y0_19
create_pblock                    pblock_x0y0_18
create_pblock                    pblock_x0y0_17
create_pblock                    pblock_x0y0_16
create_pblock                    pblock_x0y0_15
create_pblock                    pblock_x0y0_14
create_pblock                    pblock_x0y0_13
create_pblock                    pblock_x0y0_12
create_pblock                    pblock_x0y0_11
create_pblock                    pblock_x0y0_10
create_pblock                    pblock_x0y0_9
create_pblock                    pblock_x0y0_8
create_pblock                    pblock_x0y0_7
create_pblock                    pblock_x0y0_6
create_pblock                    pblock_x0y0_5
create_pblock                    pblock_x0y0_4
create_pblock                    pblock_x0y0_3
create_pblock                    pblock_x0y0_2
create_pblock                    pblock_x0y0_1
create_pblock                    pblock_x0y0_0

add_cells_to_pblock [get_pblocks pblock_x0y0_31] [get_cells x[0].y[0].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_30] [get_cells x[0].y[0].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_29] [get_cells x[0].y[0].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_28] [get_cells x[0].y[0].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_27] [get_cells x[0].y[0].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_26] [get_cells x[0].y[0].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_25] [get_cells x[0].y[0].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_24] [get_cells x[0].y[0].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_23] [get_cells x[0].y[0].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_22] [get_cells x[0].y[0].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_21] [get_cells x[0].y[0].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_20] [get_cells x[0].y[0].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_19] [get_cells x[0].y[0].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_18] [get_cells x[0].y[0].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_17] [get_cells x[0].y[0].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_16] [get_cells x[0].y[0].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_15] [get_cells x[0].y[0].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_14] [get_cells x[0].y[0].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_13] [get_cells x[0].y[0].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_12] [get_cells x[0].y[0].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_11] [get_cells x[0].y[0].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_10] [get_cells x[0].y[0].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_9]  [get_cells x[0].y[0].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_8]  [get_cells x[0].y[0].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_7]  [get_cells x[0].y[0].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_6]  [get_cells x[0].y[0].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_5]  [get_cells x[0].y[0].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_4]  [get_cells x[0].y[0].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_3]  [get_cells x[0].y[0].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_2]  [get_cells x[0].y[0].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_1]  [get_cells x[0].y[0].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x0y0_0]  [get_cells x[0].y[0].clkregion/b[0].s]


resize_pblock       [get_pblocks pblock_x0y0_31] -add {SLICE_X82Y38:SLICE_X91Y49} 
resize_pblock       [get_pblocks pblock_x0y0_30] -add {SLICE_X82Y25:SLICE_X91Y37} 
resize_pblock       [get_pblocks pblock_x0y0_29] -add {SLICE_X82Y13:SLICE_X91Y24} 
resize_pblock       [get_pblocks pblock_x0y0_28] -add {SLICE_X82Y0:SLICE_X91Y12}

resize_pblock       [get_pblocks pblock_x0y0_27] -add {SLICE_X72Y38:SLICE_X81Y49} 
resize_pblock       [get_pblocks pblock_x0y0_26] -add {SLICE_X72Y25:SLICE_X81Y37} 
resize_pblock       [get_pblocks pblock_x0y0_25] -add {SLICE_X72Y13:SLICE_X81Y24} 
resize_pblock       [get_pblocks pblock_x0y0_24] -add {SLICE_X72Y0:SLICE_X81Y12}

resize_pblock       [get_pblocks pblock_x0y0_23] -add {SLICE_X62Y38:SLICE_X71Y49} 
resize_pblock       [get_pblocks pblock_x0y0_22] -add {SLICE_X62Y25:SLICE_X71Y37} 
resize_pblock       [get_pblocks pblock_x0y0_21] -add {SLICE_X62Y13:SLICE_X71Y24} 
resize_pblock       [get_pblocks pblock_x0y0_20] -add {SLICE_X62Y0:SLICE_X71Y12}

resize_pblock       [get_pblocks pblock_x0y0_19] -add {SLICE_X52Y38:SLICE_X61Y49} 
resize_pblock       [get_pblocks pblock_x0y0_18] -add {SLICE_X52Y25:SLICE_X61Y37} 
resize_pblock       [get_pblocks pblock_x0y0_17] -add {SLICE_X52Y13:SLICE_X61Y24} 
resize_pblock       [get_pblocks pblock_x0y0_16] -add {SLICE_X52Y0:SLICE_X61Y12}

resize_pblock       [get_pblocks pblock_x0y0_15] -add {SLICE_X42Y38:SLICE_X51Y49} 
resize_pblock       [get_pblocks pblock_x0y0_14] -add {SLICE_X42Y25:SLICE_X51Y37} 
resize_pblock       [get_pblocks pblock_x0y0_13] -add {SLICE_X42Y13:SLICE_X51Y24} 
resize_pblock       [get_pblocks pblock_x0y0_12] -add {SLICE_X42Y0:SLICE_X51Y12}

resize_pblock       [get_pblocks pblock_x0y0_11] -add {SLICE_X20Y38:SLICE_X41Y49}
resize_pblock       [get_pblocks pblock_x0y0_10] -add {SLICE_X20Y25:SLICE_X41Y37}
resize_pblock       [get_pblocks pblock_x0y0_9]  -add {SLICE_X20Y13:SLICE_X41Y24} 
resize_pblock       [get_pblocks pblock_x0y0_8]  -add {SLICE_X20Y0:SLICE_X41Y12}

resize_pblock       [get_pblocks pblock_x0y0_7]  -add {SLICE_X10Y38:SLICE_X19Y49} 
resize_pblock       [get_pblocks pblock_x0y0_6]  -add {SLICE_X10Y25:SLICE_X19Y37} 
resize_pblock       [get_pblocks pblock_x0y0_5]  -add {SLICE_X10Y13:SLICE_X19Y24}
resize_pblock       [get_pblocks pblock_x0y0_4]  -add {SLICE_X10Y0:SLICE_X19Y12}

resize_pblock       [get_pblocks pblock_x0y0_3]  -add {SLICE_X0Y38:SLICE_X9Y49}
resize_pblock       [get_pblocks pblock_x0y0_2]  -add {SLICE_X0Y25:SLICE_X9Y37}
resize_pblock       [get_pblocks pblock_x0y0_1]  -add {SLICE_X0Y13:SLICE_X9Y24} 
resize_pblock       [get_pblocks pblock_x0y0_0]  -add {SLICE_X0Y0:SLICE_X9Y12}