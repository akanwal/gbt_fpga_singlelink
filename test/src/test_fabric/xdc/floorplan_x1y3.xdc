##############################
# fine scan
##############################

##====== x1y3 =======##

create_pblock                    pblock_x1y3_31
create_pblock                    pblock_x1y3_30
create_pblock                    pblock_x1y3_29
create_pblock                    pblock_x1y3_28
create_pblock                    pblock_x1y3_27
create_pblock                    pblock_x1y3_26
create_pblock                    pblock_x1y3_25
create_pblock                    pblock_x1y3_24
create_pblock                    pblock_x1y3_23
create_pblock                    pblock_x1y3_22
create_pblock                    pblock_x1y3_21
create_pblock                    pblock_x1y3_20
create_pblock                    pblock_x1y3_19
create_pblock                    pblock_x1y3_18
create_pblock                    pblock_x1y3_17
create_pblock                    pblock_x1y3_16
create_pblock                    pblock_x1y3_15
create_pblock                    pblock_x1y3_14
create_pblock                    pblock_x1y3_13
create_pblock                    pblock_x1y3_12
create_pblock                    pblock_x1y3_11
create_pblock                    pblock_x1y3_10
create_pblock                    pblock_x1y3_9
create_pblock                    pblock_x1y3_8
create_pblock                    pblock_x1y3_7
create_pblock                    pblock_x1y3_6
create_pblock                    pblock_x1y3_5
create_pblock                    pblock_x1y3_4
create_pblock                    pblock_x1y3_3
create_pblock                    pblock_x1y3_2
create_pblock                    pblock_x1y3_1
create_pblock                    pblock_x1y3_0

add_cells_to_pblock [get_pblocks pblock_x1y3_31] [get_cells x[1].y[3].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_30] [get_cells x[1].y[3].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_29] [get_cells x[1].y[3].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_28] [get_cells x[1].y[3].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_27] [get_cells x[1].y[3].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_26] [get_cells x[1].y[3].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_25] [get_cells x[1].y[3].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_24] [get_cells x[1].y[3].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_23] [get_cells x[1].y[3].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_22] [get_cells x[1].y[3].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_21] [get_cells x[1].y[3].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_20] [get_cells x[1].y[3].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_19] [get_cells x[1].y[3].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_18] [get_cells x[1].y[3].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_17] [get_cells x[1].y[3].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_16] [get_cells x[1].y[3].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_15] [get_cells x[1].y[3].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_14] [get_cells x[1].y[3].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_13] [get_cells x[1].y[3].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_12] [get_cells x[1].y[3].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_11] [get_cells x[1].y[3].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_10] [get_cells x[1].y[3].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_9]  [get_cells x[1].y[3].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_8]  [get_cells x[1].y[3].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_7]  [get_cells x[1].y[3].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_6]  [get_cells x[1].y[3].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_5]  [get_cells x[1].y[3].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_4]  [get_cells x[1].y[3].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_3]  [get_cells x[1].y[3].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_2]  [get_cells x[1].y[3].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_1]  [get_cells x[1].y[3].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x1y3_0]  [get_cells x[1].y[3].clkregion/b[0].s]

resize_pblock       [get_pblocks pblock_x1y3_31] -add {SLICE_X182Y188:SLICE_X189Y199} 
resize_pblock       [get_pblocks pblock_x1y3_30] -add {SLICE_X182Y175:SLICE_X189Y187} 
resize_pblock       [get_pblocks pblock_x1y3_29] -add {SLICE_X182Y163:SLICE_X189Y174} 
resize_pblock       [get_pblocks pblock_x1y3_28] -add {SLICE_X182Y150:SLICE_X189Y162}

resize_pblock       [get_pblocks pblock_x1y3_27] -add {SLICE_X162Y188:SLICE_X181Y199} 
resize_pblock       [get_pblocks pblock_x1y3_26] -add {SLICE_X162Y175:SLICE_X181Y187} 
resize_pblock       [get_pblocks pblock_x1y3_25] -add {SLICE_X162Y163:SLICE_X181Y174} 
resize_pblock       [get_pblocks pblock_x1y3_24] -add {SLICE_X162Y150:SLICE_X181Y162}

resize_pblock       [get_pblocks pblock_x1y3_23] -add {SLICE_X152Y188:SLICE_X161Y199} 
resize_pblock       [get_pblocks pblock_x1y3_22] -add {SLICE_X152Y175:SLICE_X161Y187} 
resize_pblock       [get_pblocks pblock_x1y3_21] -add {SLICE_X152Y163:SLICE_X161Y174} 
resize_pblock       [get_pblocks pblock_x1y3_20] -add {SLICE_X152Y150:SLICE_X161Y162}

resize_pblock       [get_pblocks pblock_x1y3_19] -add {SLICE_X142Y188:SLICE_X151Y199} 
resize_pblock       [get_pblocks pblock_x1y3_18] -add {SLICE_X142Y175:SLICE_X151Y187} 
resize_pblock       [get_pblocks pblock_x1y3_17] -add {SLICE_X142Y163:SLICE_X151Y174} 
resize_pblock       [get_pblocks pblock_x1y3_16] -add {SLICE_X142Y150:SLICE_X151Y162}

resize_pblock       [get_pblocks pblock_x1y3_15] -add {SLICE_X132Y188:SLICE_X141Y199} 
resize_pblock       [get_pblocks pblock_x1y3_14] -add {SLICE_X132Y175:SLICE_X141Y187} 
resize_pblock       [get_pblocks pblock_x1y3_13] -add {SLICE_X132Y163:SLICE_X141Y174} 
resize_pblock       [get_pblocks pblock_x1y3_12] -add {SLICE_X132Y150:SLICE_X141Y162}

resize_pblock       [get_pblocks pblock_x1y3_11] -add {SLICE_X122Y188:SLICE_X131Y199} 
resize_pblock       [get_pblocks pblock_x1y3_10] -add {SLICE_X122Y175:SLICE_X131Y187} 
resize_pblock       [get_pblocks pblock_x1y3_9]  -add {SLICE_X122Y163:SLICE_X131Y174} 
resize_pblock       [get_pblocks pblock_x1y3_8]  -add {SLICE_X122Y150:SLICE_X131Y162}

resize_pblock       [get_pblocks pblock_x1y3_7]  -add {SLICE_X102Y188:SLICE_X121Y199} 
resize_pblock       [get_pblocks pblock_x1y3_6]  -add {SLICE_X102Y175:SLICE_X121Y187} 
resize_pblock       [get_pblocks pblock_x1y3_5]  -add {SLICE_X102Y163:SLICE_X121Y174} 
resize_pblock       [get_pblocks pblock_x1y3_4]  -add {SLICE_X102Y150:SLICE_X121Y162}

resize_pblock       [get_pblocks pblock_x1y3_3]  -add {SLICE_X92Y188:SLICE_X101Y199} 
resize_pblock       [get_pblocks pblock_x1y3_2]  -add {SLICE_X92Y175:SLICE_X101Y187} 
resize_pblock       [get_pblocks pblock_x1y3_1]  -add {SLICE_X92Y163:SLICE_X101Y174} 
resize_pblock       [get_pblocks pblock_x1y3_0]  -add {SLICE_X92Y150:SLICE_X101Y162}