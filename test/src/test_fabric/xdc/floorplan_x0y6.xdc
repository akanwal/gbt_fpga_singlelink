##############################
# fine scan
##############################

##====== x0y6 =======##

create_pblock                    pblock_x0y6_31
create_pblock                    pblock_x0y6_30
create_pblock                    pblock_x0y6_29
create_pblock                    pblock_x0y6_28
create_pblock                    pblock_x0y6_27
create_pblock                    pblock_x0y6_26
create_pblock                    pblock_x0y6_25
create_pblock                    pblock_x0y6_24
create_pblock                    pblock_x0y6_23
create_pblock                    pblock_x0y6_22
create_pblock                    pblock_x0y6_21
create_pblock                    pblock_x0y6_20
create_pblock                    pblock_x0y6_19
create_pblock                    pblock_x0y6_18
create_pblock                    pblock_x0y6_17
create_pblock                    pblock_x0y6_16
create_pblock                    pblock_x0y6_15
create_pblock                    pblock_x0y6_14
create_pblock                    pblock_x0y6_13
create_pblock                    pblock_x0y6_12
create_pblock                    pblock_x0y6_11
create_pblock                    pblock_x0y6_10
create_pblock                    pblock_x0y6_9
create_pblock                    pblock_x0y6_8
create_pblock                    pblock_x0y6_7
create_pblock                    pblock_x0y6_6
create_pblock                    pblock_x0y6_5
create_pblock                    pblock_x0y6_4
create_pblock                    pblock_x0y6_3
create_pblock                    pblock_x0y6_2
create_pblock                    pblock_x0y6_1
create_pblock                    pblock_x0y6_0

add_cells_to_pblock [get_pblocks pblock_x0y6_31] [get_cells x[0].y[6].clkregion/b[31].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_30] [get_cells x[0].y[6].clkregion/b[30].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_29] [get_cells x[0].y[6].clkregion/b[29].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_28] [get_cells x[0].y[6].clkregion/b[28].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_27] [get_cells x[0].y[6].clkregion/b[27].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_26] [get_cells x[0].y[6].clkregion/b[26].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_25] [get_cells x[0].y[6].clkregion/b[25].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_24] [get_cells x[0].y[6].clkregion/b[24].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_23] [get_cells x[0].y[6].clkregion/b[23].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_22] [get_cells x[0].y[6].clkregion/b[22].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_21] [get_cells x[0].y[6].clkregion/b[21].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_20] [get_cells x[0].y[6].clkregion/b[20].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_19] [get_cells x[0].y[6].clkregion/b[19].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_18] [get_cells x[0].y[6].clkregion/b[18].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_17] [get_cells x[0].y[6].clkregion/b[17].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_16] [get_cells x[0].y[6].clkregion/b[16].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_15] [get_cells x[0].y[6].clkregion/b[15].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_14] [get_cells x[0].y[6].clkregion/b[14].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_13] [get_cells x[0].y[6].clkregion/b[13].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_12] [get_cells x[0].y[6].clkregion/b[12].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_11] [get_cells x[0].y[6].clkregion/b[11].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_10] [get_cells x[0].y[6].clkregion/b[10].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_9]  [get_cells x[0].y[6].clkregion/b[9].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_8]  [get_cells x[0].y[6].clkregion/b[8].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_7]  [get_cells x[0].y[6].clkregion/b[7].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_6]  [get_cells x[0].y[6].clkregion/b[6].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_5]  [get_cells x[0].y[6].clkregion/b[5].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_4]  [get_cells x[0].y[6].clkregion/b[4].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_3]  [get_cells x[0].y[6].clkregion/b[3].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_2]  [get_cells x[0].y[6].clkregion/b[2].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_1]  [get_cells x[0].y[6].clkregion/b[1].s]
add_cells_to_pblock [get_pblocks pblock_x0y6_0]  [get_cells x[0].y[6].clkregion/b[0].s]


resize_pblock       [get_pblocks pblock_x0y6_31] -add {SLICE_X82Y338:SLICE_X91Y349} 
resize_pblock       [get_pblocks pblock_x0y6_30] -add {SLICE_X82Y325:SLICE_X91Y337} 
resize_pblock       [get_pblocks pblock_x0y6_29] -add {SLICE_X82Y313:SLICE_X91Y324} 
resize_pblock       [get_pblocks pblock_x0y6_28] -add {SLICE_X82Y300:SLICE_X91Y312}

resize_pblock       [get_pblocks pblock_x0y6_27] -add {SLICE_X72Y338:SLICE_X81Y349} 
resize_pblock       [get_pblocks pblock_x0y6_26] -add {SLICE_X72Y325:SLICE_X81Y337} 
resize_pblock       [get_pblocks pblock_x0y6_25] -add {SLICE_X72Y313:SLICE_X81Y324} 
resize_pblock       [get_pblocks pblock_x0y6_24] -add {SLICE_X72Y300:SLICE_X81Y312}

resize_pblock       [get_pblocks pblock_x0y6_23] -add {SLICE_X62Y338:SLICE_X71Y349} 
resize_pblock       [get_pblocks pblock_x0y6_22] -add {SLICE_X62Y325:SLICE_X71Y337} 
resize_pblock       [get_pblocks pblock_x0y6_21] -add {SLICE_X62Y313:SLICE_X71Y324} 
resize_pblock       [get_pblocks pblock_x0y6_20] -add {SLICE_X62Y300:SLICE_X71Y312}

resize_pblock       [get_pblocks pblock_x0y6_19] -add {SLICE_X52Y338:SLICE_X61Y349} 
resize_pblock       [get_pblocks pblock_x0y6_18] -add {SLICE_X52Y325:SLICE_X61Y337} 
resize_pblock       [get_pblocks pblock_x0y6_17] -add {SLICE_X52Y313:SLICE_X61Y324} 
resize_pblock       [get_pblocks pblock_x0y6_16] -add {SLICE_X52Y300:SLICE_X61Y312}

resize_pblock       [get_pblocks pblock_x0y6_15] -add {SLICE_X42Y338:SLICE_X51Y349} 
resize_pblock       [get_pblocks pblock_x0y6_14] -add {SLICE_X42Y325:SLICE_X51Y337} 
resize_pblock       [get_pblocks pblock_x0y6_13] -add {SLICE_X42Y313:SLICE_X51Y324} 
resize_pblock       [get_pblocks pblock_x0y6_12] -add {SLICE_X42Y300:SLICE_X51Y312}

resize_pblock       [get_pblocks pblock_x0y6_11] -add {SLICE_X20Y338:SLICE_X41Y349}
resize_pblock       [get_pblocks pblock_x0y6_10] -add {SLICE_X20Y325:SLICE_X41Y337}
resize_pblock       [get_pblocks pblock_x0y6_9]  -add {SLICE_X20Y313:SLICE_X41Y324} 
resize_pblock       [get_pblocks pblock_x0y6_8]  -add {SLICE_X20Y300:SLICE_X41Y312}

resize_pblock       [get_pblocks pblock_x0y6_7]  -add {SLICE_X10Y338:SLICE_X19Y349} 
resize_pblock       [get_pblocks pblock_x0y6_6]  -add {SLICE_X10Y325:SLICE_X19Y337} 
resize_pblock       [get_pblocks pblock_x0y6_5]  -add {SLICE_X10Y313:SLICE_X19Y324}
resize_pblock       [get_pblocks pblock_x0y6_4]  -add {SLICE_X10Y300:SLICE_X19Y312}

resize_pblock       [get_pblocks pblock_x0y6_3]  -add {SLICE_X0Y338:SLICE_X9Y349}
resize_pblock       [get_pblocks pblock_x0y6_2]  -add {SLICE_X0Y325:SLICE_X9Y337}
resize_pblock       [get_pblocks pblock_x0y6_1]  -add {SLICE_X0Y313:SLICE_X9Y324} 
resize_pblock       [get_pblocks pblock_x0y6_0]  -add {SLICE_X0Y300:SLICE_X9Y312}