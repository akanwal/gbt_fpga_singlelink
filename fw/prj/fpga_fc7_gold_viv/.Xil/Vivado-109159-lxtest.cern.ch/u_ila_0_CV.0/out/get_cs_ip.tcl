#
#Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
#
set_param chipscope.flow 0
set part xc7k420tffg1156-2
set ip_vlnv xilinx.com:ip:ila:6.2
set ip_module_name u_ila_0_CV
set params {{{PARAM_VALUE.ALL_PROBE_SAME_MU} {true} {PARAM_VALUE.ALL_PROBE_SAME_MU_CNT} {1} {PARAM_VALUE.C_ADV_TRIGGER} {false} {PARAM_VALUE.C_DATA_DEPTH} {1024} {PARAM_VALUE.C_EN_STRG_QUAL} {false} {PARAM_VALUE.C_INPUT_PIPE_STAGES} {0} {PARAM_VALUE.C_NUM_OF_PROBES} {6} {PARAM_VALUE.C_PROBE0_TYPE} {0} {PARAM_VALUE.C_PROBE0_WIDTH} {6} {PARAM_VALUE.C_PROBE1_TYPE} {0} {PARAM_VALUE.C_PROBE1_WIDTH} {8} {PARAM_VALUE.C_PROBE2_TYPE} {0} {PARAM_VALUE.C_PROBE2_WIDTH} {1} {PARAM_VALUE.C_PROBE3_TYPE} {0} {PARAM_VALUE.C_PROBE3_WIDTH} {1} {PARAM_VALUE.C_PROBE4_TYPE} {0} {PARAM_VALUE.C_PROBE4_WIDTH} {1} {PARAM_VALUE.C_PROBE5_TYPE} {0} {PARAM_VALUE.C_PROBE5_WIDTH} {1} {PARAM_VALUE.C_TRIGIN_EN} {0} {PARAM_VALUE.C_TRIGOUT_EN} {0}}}
set output_xci /home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7/fw/prj/fpga_fc7_gold_viv/.Xil/Vivado-109159-lxtest.cern.ch/u_ila_0_CV.0/out/result.xci
set output_dcp /home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7/fw/prj/fpga_fc7_gold_viv/.Xil/Vivado-109159-lxtest.cern.ch/u_ila_0_CV.0/out/result.dcp
set output_dir /home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7/fw/prj/fpga_fc7_gold_viv/.Xil/Vivado-109159-lxtest.cern.ch/u_ila_0_CV.0/out
set ip_repo_paths {}
set ip_output_repo {}
set ip_cache_permissions disable

set oopbus_ip_repo_paths [get_param chipscope.oopbus_ip_repo_paths]

set synth_opts {}
set xdc_files {}
source {/opt/Xilinx/Vivado/2016.4/scripts/ip/ipxchipscope.tcl}

set failed [catch {ipx::chipscope::gen_and_synth_ip $part $ip_vlnv $ip_module_name $params $output_xci $output_dcp $output_dir $ip_repo_paths $ip_output_repo $ip_cache_permissions $oopbus_ip_repo_paths $synth_opts $xdc_files} errMessage]

if { $failed } {
  puts "Caught exception:"
  puts "$errMessage"
  exit 1
}
