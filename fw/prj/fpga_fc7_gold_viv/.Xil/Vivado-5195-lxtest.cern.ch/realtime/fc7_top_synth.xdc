set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/prj/fpga_fc7_gold_viv/.Xil/Vivado-5195-lxtest.cern.ch/dcp_3/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_in_context.xdc rfile:../dcp_3/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_in_context.xdc id:1 order:EARLY scoped_inst:{usr/gbt_wrapper_0/gbtBank_Clk_gen[1].gbtBank_rxFrmClkPhAlgnr/latOpt_phalgnr_gen.mmcm_inst/pll}} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/prj/fpga_fc7_gold_viv/.Xil/Vivado-5195-lxtest.cern.ch/dcp_4/xlx_k7v7_vio_in_context.xdc rfile:../dcp_4/xlx_k7v7_vio_in_context.xdc id:2 order:EARLY scoped_inst:usr/vio} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/prj/fpga_fc7_gold_viv/.Xil/Vivado-5195-lxtest.cern.ch/dcp_5/xlx_k7v7_vivado_debug_in_context.xdc rfile:../dcp_5/xlx_k7v7_vivado_debug_in_context.xdc id:3 order:EARLY scoped_inst:usr/txILa} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/prj/fpga_fc7_gold_viv/.Xil/Vivado-5195-lxtest.cern.ch/dcp_5/xlx_k7v7_vivado_debug_in_context.xdc rfile:../dcp_5/xlx_k7v7_vivado_debug_in_context.xdc id:4 order:EARLY scoped_inst:usr/rxIla} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/prj/fpga_fc7_gold_viv/.Xil/Vivado-5195-lxtest.cern.ch/dcp_6/xlx_k7v7_tx_pll_in_context.xdc rfile:../dcp_6/xlx_k7v7_tx_pll_in_context.xdc id:5 order:EARLY scoped_inst:usr/txPll} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/src/usr/ucf/usr_io_fmc.xdc rfile:../../../../../src/usr/ucf/usr_io_fmc.xdc id:6} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/src/usr/ucf/usr_opt.xdc rfile:../../../../../src/usr/ucf/usr_opt.xdc id:7} [current_design]
set_property src_info {type:SCOPED_XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property CLOCK_PERIOD_OOC_TARGET 8.332 [get_pins {usr/gbt_wrapper_0/gbtBank_Clk_gen[1].gbtBank_rxFrmClkPhAlgnr/latOpt_phalgnr_gen.mmcm_inst/pll/clk_in1}]
set_property src_info {type:SCOPED_XDC file:1 line:3 export:INPUT save:INPUT read:READ} [current_design]
set_property IS_IP_OOC_CELL true [get_cells {usr/gbt_wrapper_0/gbtBank_Clk_gen[1].gbtBank_rxFrmClkPhAlgnr/latOpt_phalgnr_gen.mmcm_inst/pll}]
set_property src_info {type:SCOPED_XDC file:2 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property CLOCK_PERIOD_OOC_TARGET 10 [get_pins usr/vio/clk]
set_property src_info {type:SCOPED_XDC file:2 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property IS_IP_OOC_CELL true [get_cells usr/vio]
set_property src_info {type:SCOPED_XDC file:3 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property CLOCK_PERIOD_OOC_TARGET 10 [get_pins usr/txILa/clk]
set_property src_info {type:SCOPED_XDC file:3 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property IS_IP_OOC_CELL true [get_cells usr/txILa]
set_property src_info {type:SCOPED_XDC file:4 line:1 export:INPUT save:INPUT read:READ} [current_design]
set_property CLOCK_PERIOD_OOC_TARGET 10 [get_pins usr/rxIla/clk]
set_property src_info {type:SCOPED_XDC file:4 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property IS_IP_OOC_CELL true [get_cells usr/rxIla]
set_property src_info {type:SCOPED_XDC file:5 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property CLOCK_PERIOD_OOC_TARGET 8.333 [get_pins usr/txPll/clk_in1]
set_property src_info {type:SCOPED_XDC file:5 line:3 export:INPUT save:INPUT read:READ} [current_design]
set_property IS_IP_OOC_CELL true [get_cells usr/txPll]
set_property src_info {type:XDC file:6 line:13 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[33]}]
set_property src_info {type:XDC file:6 line:15 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[32]}]
set_property src_info {type:XDC file:6 line:17 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[31]}]
set_property src_info {type:XDC file:6 line:19 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[30]}]
set_property src_info {type:XDC file:6 line:21 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[29]}]
set_property src_info {type:XDC file:6 line:23 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[28]}]
set_property src_info {type:XDC file:6 line:25 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[27]}]
set_property src_info {type:XDC file:6 line:27 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[26]}]
set_property src_info {type:XDC file:6 line:29 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[25]}]
set_property src_info {type:XDC file:6 line:31 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[24]}]
set_property src_info {type:XDC file:6 line:33 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[23]}]
set_property src_info {type:XDC file:6 line:35 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[22]}]
set_property src_info {type:XDC file:6 line:37 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[21]}]
set_property src_info {type:XDC file:6 line:39 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[20]}]
set_property src_info {type:XDC file:6 line:41 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[19]}]
set_property src_info {type:XDC file:6 line:43 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[18]}]
set_property src_info {type:XDC file:6 line:45 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[17]}]
set_property src_info {type:XDC file:6 line:47 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[16]}]
set_property src_info {type:XDC file:6 line:49 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[15]}]
set_property src_info {type:XDC file:6 line:51 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[14]}]
set_property src_info {type:XDC file:6 line:53 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[13]}]
set_property src_info {type:XDC file:6 line:55 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[12]}]
set_property src_info {type:XDC file:6 line:57 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[11]}]
set_property src_info {type:XDC file:6 line:59 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[10]}]
set_property src_info {type:XDC file:6 line:61 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[9]}]
set_property src_info {type:XDC file:6 line:63 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[8]}]
set_property src_info {type:XDC file:6 line:65 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[7]}]
set_property src_info {type:XDC file:6 line:67 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[6]}]
set_property src_info {type:XDC file:6 line:69 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[5]}]
set_property src_info {type:XDC file:6 line:71 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[4]}]
set_property src_info {type:XDC file:6 line:73 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[3]}]
set_property src_info {type:XDC file:6 line:75 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[2]}]
set_property src_info {type:XDC file:6 line:77 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[1]}]
set_property src_info {type:XDC file:6 line:79 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_p[0]}]
set_property src_info {type:XDC file:6 line:82 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[33]}]
set_property src_info {type:XDC file:6 line:84 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[32]}]
set_property src_info {type:XDC file:6 line:86 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[31]}]
set_property src_info {type:XDC file:6 line:88 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[30]}]
set_property src_info {type:XDC file:6 line:90 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[29]}]
set_property src_info {type:XDC file:6 line:92 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[28]}]
set_property src_info {type:XDC file:6 line:94 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[27]}]
set_property src_info {type:XDC file:6 line:96 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[26]}]
set_property src_info {type:XDC file:6 line:98 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[25]}]
set_property src_info {type:XDC file:6 line:100 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[24]}]
set_property src_info {type:XDC file:6 line:102 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[23]}]
set_property src_info {type:XDC file:6 line:104 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[22]}]
set_property src_info {type:XDC file:6 line:106 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[21]}]
set_property src_info {type:XDC file:6 line:108 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[20]}]
set_property src_info {type:XDC file:6 line:110 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[19]}]
set_property src_info {type:XDC file:6 line:112 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[18]}]
set_property src_info {type:XDC file:6 line:114 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[17]}]
set_property src_info {type:XDC file:6 line:116 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[16]}]
set_property src_info {type:XDC file:6 line:118 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[15]}]
set_property src_info {type:XDC file:6 line:120 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[14]}]
set_property src_info {type:XDC file:6 line:122 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[13]}]
set_property src_info {type:XDC file:6 line:124 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[12]}]
set_property src_info {type:XDC file:6 line:126 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[11]}]
set_property src_info {type:XDC file:6 line:128 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[10]}]
set_property src_info {type:XDC file:6 line:130 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[9]}]
set_property src_info {type:XDC file:6 line:132 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[8]}]
set_property src_info {type:XDC file:6 line:134 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[7]}]
set_property src_info {type:XDC file:6 line:136 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[6]}]
set_property src_info {type:XDC file:6 line:138 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[5]}]
set_property src_info {type:XDC file:6 line:140 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[4]}]
set_property src_info {type:XDC file:6 line:142 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[3]}]
set_property src_info {type:XDC file:6 line:144 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[2]}]
set_property src_info {type:XDC file:6 line:146 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[1]}]
set_property src_info {type:XDC file:6 line:148 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_la_n[0]}]
set_property src_info {type:XDC file:6 line:154 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[33]}]
set_property src_info {type:XDC file:6 line:156 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[32]}]
set_property src_info {type:XDC file:6 line:158 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[31]}]
set_property src_info {type:XDC file:6 line:160 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[30]}]
set_property src_info {type:XDC file:6 line:162 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[29]}]
set_property src_info {type:XDC file:6 line:164 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[28]}]
set_property src_info {type:XDC file:6 line:166 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[27]}]
set_property src_info {type:XDC file:6 line:168 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[26]}]
set_property src_info {type:XDC file:6 line:170 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[25]}]
set_property src_info {type:XDC file:6 line:172 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[24]}]
set_property src_info {type:XDC file:6 line:174 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[23]}]
set_property src_info {type:XDC file:6 line:176 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[22]}]
set_property src_info {type:XDC file:6 line:178 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[21]}]
set_property src_info {type:XDC file:6 line:180 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[20]}]
set_property src_info {type:XDC file:6 line:182 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[19]}]
set_property src_info {type:XDC file:6 line:184 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[18]}]
set_property src_info {type:XDC file:6 line:186 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[17]}]
set_property src_info {type:XDC file:6 line:188 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[16]}]
set_property src_info {type:XDC file:6 line:190 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[15]}]
set_property src_info {type:XDC file:6 line:192 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[14]}]
set_property src_info {type:XDC file:6 line:194 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[13]}]
set_property src_info {type:XDC file:6 line:196 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[12]}]
set_property src_info {type:XDC file:6 line:198 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[11]}]
set_property src_info {type:XDC file:6 line:200 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[10]}]
set_property src_info {type:XDC file:6 line:202 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[9]}]
set_property src_info {type:XDC file:6 line:204 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[8]}]
set_property src_info {type:XDC file:6 line:206 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[7]}]
set_property src_info {type:XDC file:6 line:208 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[6]}]
set_property src_info {type:XDC file:6 line:210 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[5]}]
set_property src_info {type:XDC file:6 line:212 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[4]}]
set_property src_info {type:XDC file:6 line:214 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[3]}]
set_property src_info {type:XDC file:6 line:216 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[2]}]
set_property src_info {type:XDC file:6 line:218 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[1]}]
set_property src_info {type:XDC file:6 line:220 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_p[0]}]
set_property src_info {type:XDC file:6 line:223 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[33]}]
set_property src_info {type:XDC file:6 line:225 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[32]}]
set_property src_info {type:XDC file:6 line:227 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[31]}]
set_property src_info {type:XDC file:6 line:229 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[30]}]
set_property src_info {type:XDC file:6 line:231 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[29]}]
set_property src_info {type:XDC file:6 line:233 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[28]}]
set_property src_info {type:XDC file:6 line:235 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[27]}]
set_property src_info {type:XDC file:6 line:237 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[26]}]
set_property src_info {type:XDC file:6 line:239 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[25]}]
set_property src_info {type:XDC file:6 line:241 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[24]}]
set_property src_info {type:XDC file:6 line:243 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[23]}]
set_property src_info {type:XDC file:6 line:245 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[22]}]
set_property src_info {type:XDC file:6 line:247 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[21]}]
set_property src_info {type:XDC file:6 line:249 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[20]}]
set_property src_info {type:XDC file:6 line:251 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[19]}]
set_property src_info {type:XDC file:6 line:253 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[18]}]
set_property src_info {type:XDC file:6 line:255 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[17]}]
set_property src_info {type:XDC file:6 line:257 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[16]}]
set_property src_info {type:XDC file:6 line:259 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[15]}]
set_property src_info {type:XDC file:6 line:261 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[14]}]
set_property src_info {type:XDC file:6 line:263 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[13]}]
set_property src_info {type:XDC file:6 line:265 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[12]}]
set_property src_info {type:XDC file:6 line:267 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[11]}]
set_property src_info {type:XDC file:6 line:269 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[10]}]
set_property src_info {type:XDC file:6 line:271 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[9]}]
set_property src_info {type:XDC file:6 line:273 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[8]}]
set_property src_info {type:XDC file:6 line:275 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[7]}]
set_property src_info {type:XDC file:6 line:277 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[6]}]
set_property src_info {type:XDC file:6 line:279 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[5]}]
set_property src_info {type:XDC file:6 line:281 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[4]}]
set_property src_info {type:XDC file:6 line:283 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[3]}]
set_property src_info {type:XDC file:6 line:285 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[2]}]
set_property src_info {type:XDC file:6 line:287 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[1]}]
set_property src_info {type:XDC file:6 line:289 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l12_la_n[0]}]
set_property src_info {type:SCOPED_XDC file:1 line:1 export:INPUT save:INPUT read:READ} [current_design]
create_generated_clock -source [get_ports {fmc_l8_la_p[6]}] -edges {1 2 3} -edge_shift {0.000 8.332 16.664} [get_pins {usr/gbt_wrapper_0/gbtBank_Clk_gen[1].gbtBank_rxFrmClkPhAlgnr/latOpt_phalgnr_gen.mmcm_inst/pll/clk_out1}]
set_property src_info {type:SCOPED_XDC file:5 line:1 export:INPUT save:INPUT read:READ} [current_design]
create_generated_clock -source [get_pins usr/txPll/clk_in1] -edges {1 2 3} -edge_shift {0.000 8.333 16.666} [get_pins usr/txPll/clk_out1]
set_property src_info {type:XDC file:7 line:11 export:INPUT save:INPUT read:READ} [current_design]
create_clock -period 8.333 -name MGT_REFCLK [get_ports ttc_mgt_xpoint_c_p]
