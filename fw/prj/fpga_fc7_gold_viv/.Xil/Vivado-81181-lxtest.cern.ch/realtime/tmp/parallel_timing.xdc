#
# Created by 
#   realTimeFpga.exe  on Tue Jun  6 16:57:28 2017
# (c) Xilinx, Inc.
#
# define clock osc125_a_mgtrefclk_o_IBUFDS_GTE2_GENERATED
create_generated_clock -source [get_pins sys\/osc125a_gtebuf/I] -multiply_by 1 -name osc125_a_mgtrefclk_o_IBUFDS_GTE2_GENERATED [get_pins sys\/osc125a_gtebuf/O]
# define clock osc125_b_mgtrefclk_o_IBUFDS_GTE2_GENERATED
create_generated_clock -source [get_pins sys\/osc125b_gtebuf/I] -multiply_by 1 -name osc125_b_mgtrefclk_o_IBUFDS_GTE2_GENERATED [get_pins sys\/osc125b_gtebuf/O]
# define clock ttcMgtXpoint_from_ibufdsGtxe1_IBUFDS_GTE2_GENERATED
create_generated_clock -source [get_pins usr\/cdceOut0IbufdsGtxe2/I] -multiply_by 1 -name ttcMgtXpoint_from_ibufdsGtxe1_IBUFDS_GTE2_GENERATED [get_pins usr\/cdceOut0IbufdsGtxe2/O]
# define clock usri_1/usr/txPll/bbstub_clk_out1/O
create_generated_clock -source [get_pins {usr\/gbt_wrapper_1/gbtBank_rst_gen[1].gbtBank_gbtBankRst\/gbtResetRx_from_rxRstFsm_reg/C}] -edges {1 2 3} -edge_shift {0.000000 8.333000 16.666000} -name usri_1\/usr\/txPll\/bbstub_clk_out1\/O [get_pins usr\/txPll/bbstub_clk_out1/O]
# define clock constraints osc125_a_mgtrefclk_o_IBUFDS_GTE2_GENERATED
# define clock constraints osc125_b_mgtrefclk_o_IBUFDS_GTE2_GENERATED
# define clock constraints ttcMgtXpoint_from_ibufdsGtxe1_IBUFDS_GTE2_GENERATED
# define clock constraints usri_1/usr/txPll/bbstub_clk_out1/O
