set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/src/sys/ucf/sys.xdc rfile:../../../src/sys/ucf/sys.xdc id:1} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/src/sys/ucf/sys_floorplanning.xdc rfile:../../../src/sys/ucf/sys_floorplanning.xdc id:2} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/src/usr/ucf/usr_clk.xdc rfile:../../../src/usr/ucf/usr_clk.xdc id:3} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/src/usr/ucf/usr_mgt_fmc_l12.xdc rfile:../../../src/usr/ucf/usr_mgt_fmc_l12.xdc id:4} [current_design]
set_property SRC_FILE_INFO {cfile:/home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7_singlelink/fw/src/usr/ucf/gbt_timingclosure.xdc rfile:../../../src/usr/ucf/gbt_timingclosure.xdc id:5} [current_design]
set_property src_info {type:XDC file:1 line:389 export:INPUT save:INPUT read:READ} [current_design]
set_operating_conditions -board_layers 16+
set_property src_info {type:XDC file:1 line:390 export:INPUT save:INPUT read:READ} [current_design]
set_operating_conditions -board custom
set_property src_info {type:XDC file:1 line:396 export:INPUT save:INPUT read:READ} [current_design]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property src_info {type:XDC file:1 line:397 export:INPUT save:INPUT read:READ} [current_design]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property src_info {type:XDC file:1 line:398 export:INPUT save:INPUT read:READ} [current_design]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
set_property src_info {type:XDC file:1 line:399 export:INPUT save:INPUT read:READ} [current_design]
connect_debug_port dbg_hub/clk [get_nets clk]
set_property src_info {type:XDC file:2 line:5 export:INPUT save:INPUT read:READ} [current_design]
add_cells_to_pblock [get_pblocks pblock_gold] -top
set_property src_info {type:XDC file:2 line:7 export:INPUT save:INPUT read:READ} [current_design]
resize_pblock       [get_pblocks pblock_gold] -add  {CLOCKREGION_X0Y2:CLOCKREGION_X1Y3}
set_property src_info {type:XDC file:2 line:8 export:INPUT save:INPUT read:READ} [current_design]
resize_pblock       [get_pblocks pblock_gold] -add  {MMCME2_ADV_X0Y2:MMCME2_ADV_X0Y2}
set_property src_info {type:XDC file:2 line:9 export:INPUT save:INPUT read:READ} [current_design]
resize_pblock       [get_pblocks pblock_gold] -add  {PLLE2_ADV_X0Y2:PLLE2_ADV_X0Y3}
set_property src_info {type:XDC file:3 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_operating_conditions -airflow 0
set_property src_info {type:XDC file:3 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_operating_conditions -heatsink low
set_property src_info {type:XDC file:4 line:31 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN b10 [get_ports fmc_l12_dp_m2c_p[0]]
set_property src_info {type:XDC file:5 line:50 export:INPUT save:INPUT read:READ} [current_design]
set_max_delay 16 -from [get_pins -hier -filter {NAME =~ */*/*/scrambler/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/txGearbox/*/D}] -datapath_only
set_property src_info {type:XDC file:5 line:56 export:INPUT save:INPUT read:READ} [current_design]
set_max_delay 20 -from [get_pins -hier -filter {NAME =~ */*/*/rxGearbox/*/C}] -to [get_pins -hier -filter {NAME =~ */*/*/descrambler/*/D}] -datapath_only
