
 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {110 110} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} }

 add_fsm_encoding \
       {gtwizard_v2_3_gbe_RECCLK_MONITOR.HW_circuitry.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {transactor_if.state} \
       { }  \
       {{000 0000001} {001 0000010} {010 0000100} {011 0010000} {100 0100000} {101 0001000} {110 1000000} }

 add_fsm_encoding \
       {transactor_sm.state} \
       { }  \
       {{000 000001} {001 000010} {010 000100} {011 001000} {100 010000} {101 100000} }

 add_fsm_encoding \
       {icap_interface.w_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }

 add_fsm_encoding \
       {icap_interface_fsm.state} \
       { }  \
       {{000 00001} {001 00010} {010 00100} {011 01000} {100 10000} }

 add_fsm_encoding \
       {i2c_bitwise.datafsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 101} {101 100} {110 110} {111 111} }

 add_fsm_encoding \
       {i2c_ctrl.chopexecfsm} \
       { }  \
       {{00 00} {01 01} {10 10} {11 11} }

 add_fsm_encoding \
       {i2c_ctrl.ctrlfsm} \
       { }  \
       {{00000 00000} {00001 00001} {00010 00010} {00011 00011} {00100 00100} {00101 00101} {00110 00110} {00111 01010} {01000 01011} {01010 00111} {01011 01000} {01100 01001} {01101 01100} {01110 01101} {01111 01110} {10000 01111} {10001 10000} }

 add_fsm_encoding \
       {i2c_eep_autoread.fsm} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_STARTUP_FSM.tx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_RX_STARTUP_FSM.rx_state} \
       { }  \
       {{0000 0000} {0001 0001} {0010 0010} {0011 0011} {0100 0100} {0101 0101} {0110 0110} {0111 0111} {1000 1000} {1001 1001} {1010 1010} }

 add_fsm_encoding \
       {xlx_k7v7_mgt_ip_TX_MANUAL_PHASE_ALIGN.tx_phalign_manual_state} \
       { }  \
       {{0000 000001} {0001 000010} {0010 000100} {0011 001000} {0100 010000} {1000 100000} }

 add_fsm_encoding \
       {mgt_latopt_bitslipctrl.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} {111 110} }

 add_fsm_encoding \
       {gbt_rx_framealigner_pattsearch.state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} }

 add_fsm_encoding \
       {gbt_bank_reset.general_state} \
       { }  \
       {{000 000} {001 001} {010 010} {011 011} {100 100} {101 101} }
