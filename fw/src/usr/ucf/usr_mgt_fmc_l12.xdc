
# Created by me as an adaptation of the original usr_mgt_fmc_l12.ucf
# Uncommented signals come from fc7_gbt_exampldsgn_io.xdc


##############################
# fmc l12 tx
##############################
#set_property PACKAGE_PIN b6 [get_ports fmc_l12_dp_c2m_p[11]]

#set_property PACKAGE_PIN h2 [get_ports fmc_l12_dp_m2c_p[10]]

#set_property PACKAGE_PIN a8 [get_ports fmc_l12_dp_m2c_p[9]]

#set_property PACKAGE_PIN a4 [get_ports fmc_l12_dp_m2c_p[8]]

#set_property PACKAGE_PIN d2 [get_ports fmc_l12_dp_m2c_p[7]]

#set_property PACKAGE_PIN k2 [get_ports fmc_l12_dp_m2c_p[6]]

#set_property PACKAGE_PIN m2 [get_ports fmc_l12_dp_m2c_p[5]]

#set_property PACKAGE_PIN f2 [get_ports fmc_l12_dp_m2c_p[4]]

#set_property PACKAGE_PIN b2 [get_ports fmc_l12_dp_m2c_p[3]]

#set_property PACKAGE_PIN c8 [get_ports fmc_l12_dp_m2c_p[2]]

#set_property PACKAGE_PIN a12 [get_ports fmc_l12_dp_m2c_p[1]]

set_property PACKAGE_PIN b10 [get_ports fmc_l12_dp_m2c_p[0]]


##

#set_property PACKAGE_PIN b5 [get_ports fmc_l12_dp_c2m_n[11]]

#set_property PACKAGE_PIN h1 [get_ports fmc_l12_dp_c2m_n[10]]

#set_property PACKAGE_PIN a7 [get_ports fmc_l12_dp_c2m_n[9]]

#set_property PACKAGE_PIN a3 [get_ports fmc_l12_dp_c2m_n[8]]

#set_property PACKAGE_PIN d1 [get_ports fmc_l12_dp_c2m_n[7]]

#set_property PACKAGE_PIN k1 [get_ports fmc_l12_dp_c2m_n[6]]

#set_property PACKAGE_PIN m1 [get_ports fmc_l12_dp_c2m_n[5]]

#set_property PACKAGE_PIN f1 [get_ports fmc_l12_dp_c2m_n[4]]

#set_property PACKAGE_PIN b1 [get_ports fmc_l12_dp_c2m_n[3]]

#set_property PACKAGE_PIN c7 [get_ports fmc_l12_dp_c2m_n[2]]

#set_property PACKAGE_PIN a11 [get_ports fmc_l12_dp_c2m_n[1]]

set_property PACKAGE_PIN b9 [get_ports fmc_l12_dp_c2m_n[0]]



###############################
## fmc l12 rx
###############################

#set_property PACKAGE_PIN c4 [get_ports fmc_l12_dp_m2c_p[11]]

#set_property PACKAGE_PIN j4 [get_ports fmc_l12_dp_m2c_p[10]]

#set_property PACKAGE_PIN e12 [get_ports fmc_l12_dp_m2c_p[9]]

#set_property PACKAGE_PIN d6 [get_ports fmc_l12_dp_m2c_p[8]]

#set_property PACKAGE_PIN g4 [get_ports fmc_l12_dp_m2c_p[7]]

#set_property PACKAGE_PIN k6 [get_ports fmc_l12_dp_m2c_p[6]]

#set_property PACKAGE_PIN l4 [get_ports fmc_l12_dp_m2c_p[5]]

#set_property PACKAGE_PIN h6 [get_ports fmc_l12_dp_m2c_p[4]]

#set_property PACKAGE_PIN e4 [get_ports fmc_l12_dp_m2c_p[3]]

#set_property PACKAGE_PIN f10 [get_ports fmc_l12_dp_m2c_p[2]]

#set_property PACKAGE_PIN c12 [get_ports fmc_l12_dp_m2c_p[1]]

set_property PACKAGE_PIN d10 [get_ports fmc_l12_dp_m2c_p[0]]

##

#set_property PACKAGE_PIN c3 [get_ports fmc_l12_dp_m2c_n[11]]

#set_property PACKAGE_PIN j3 [get_ports fmc_l12_dp_m2c_n[10]]

#set_property PACKAGE_PIN e11 [get_ports fmc_l12_dp_m2c_n[9]]

#set_property PACKAGE_PIN d5 [get_ports fmc_l12_dp_m2c_n[8]]

#set_property PACKAGE_PIN g3 [get_ports fmc_l12_dp_m2c_n[7]]

#set_property PACKAGE_PIN k5 [get_ports fmc_l12_dp_m2c_n[6]]

#set_property PACKAGE_PIN l3 [get_ports fmc_l12_dp_m2c_n[5]]

#set_property PACKAGE_PIN h5 [get_ports fmc_l12_dp_m2c_n[4]]

#set_property PACKAGE_PIN e3 [get_ports fmc_l12_dp_m2c_n[3]]

#set_property PACKAGE_PIN f9 [get_ports fmc_l12_dp_m2c_n[2]]

#set_property PACKAGE_PIN c11 [get_ports fmc_l12_dp_m2c_n[1]]

set_property PACKAGE_PIN d9 [get_ports fmc_l12_dp_m2c_n[0]]

