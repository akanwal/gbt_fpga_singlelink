
# Created by me as an adaptation of the original usr_mgt_fmc_l8.ucf
# All signals in this file were origianally all commented out


###############################
## fmc l8 tx
###############################
#set_property PACKAGE_PIN an7    [get_ports {fmc_l8_dp_c2m_p[7]]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_p[7]]

#set_property PACKAGE_PIN ap9    [get_ports {fmc_l8_dp_c2m_p[6]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_p[6]}]

#set_property PACKAGE_PIN an11    [get_ports {fmc_l8_dp_c2m_p[5]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_p[5]}]

#set_property PACKAGE_PIN al7    [get_ports {fmc_l8_dp_c2m_p[4]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_p[4]}]

#set_property PACKAGE_PIN ap5    [get_ports {fmc_l8_dp_c2m_p[3]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_p[3]}]

#set_property PACKAGE_PIN an3    [get_ports {fmc_l8_dp_c2m_p[2]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_p[2]}]

#set_property PACKAGE_PIN ap1    [get_ports {fmc_l8_dp_c2m_p[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_p[1]}]

#set_property PACKAGE_PIN am1    [get_ports {fmc_l8_dp_c2m_p[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_p[0]}]


##


#set_property PACKAGE_PIN an8    [get_ports {fmc_l8_dp_c2m_n[7]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_n[7]}]

#set_property PACKAGE_PIN ap10    [get_ports {fmc_l8_dp_c2m_n[6]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_n[6]}]

#set_property PACKAGE_PIN an12    [get_ports {fmc_l8_dp_c2m_n[5]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_n[5]}]

#set_property PACKAGE_PIN al8    [get_ports {fmc_l8_dp_c2m_n[4]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_n[4]}]

#set_property PACKAGE_PIN ap6    [get_ports {fmc_l8_dp_c2m_n[3]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_n[3]}]

#set_property PACKAGE_PIN an4    [get_ports {fmc_l8_dp_c2m_n[2]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_n[2]}]

#set_property PACKAGE_PIN ap2    [get_ports {fmc_l8_dp_c2m_n[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_n[1]}]

#set_property PACKAGE_PIN am2    [get_ports {fmc_l8_dp_c2m_n[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_c2m_n[0]}]



###############################
## fmc l8 rx
###############################

#set_property PACKAGE_PIN aj11    [get_ports {fmc_l8_dp_m2c_p[7]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_p[7]}]

#set_property PACKAGE_PIN al11    [get_ports {fmc_l8_dp_m2c_p[6]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_p[6]}]

#set_property PACKAGE_PIN am9    [get_ports {fmc_l8_dp_m2c_p[5]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_p[5]}]

#set_property PACKAGE_PIN ak9    [get_ports {fmc_l8_dp_m2c_p[4]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_p[4]}]

#set_property PACKAGE_PIN am5    [get_ports {fmc_l8_dp_m2c_p[3]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_p[3]}]

#set_property PACKAGE_PIN al3    [get_ports {fmc_l8_dp_m2c_p[2]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_p[2]}]

#set_property PACKAGE_PIN ak5    [get_ports {fmc_l8_dp_m2c_p[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_p[1]}]

#set_property PACKAGE_PIN aj3    [get_ports {fmc_l8_dp_m2c_p[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_p[0]}]


##

#set_property PACKAGE_PIN aj12    [get_ports {fmc_l8_dp_m2c_n[7]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_n[7]}]

#set_property PACKAGE_PIN ajal123    [get_ports {fmc_l8_dp_m2c_n[6]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_n[6]}]

#set_property PACKAGE_PIN am10    [get_ports {fmc_l8_dp_m2c_n[5]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_n[5]}]

#set_property PACKAGE_PIN ak10    [get_ports {fmc_l8_dp_m2c_n[4]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_n[4]}]

#set_property PACKAGE_PIN am6    [get_ports {fmc_l8_dp_m2c_n[3]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_n[3]}]

#set_property PACKAGE_PIN al4    [get_ports {fmc_l8_dp_m2c_n[2]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_n[2]}]

#set_property PACKAGE_PIN ak6    [get_ports {fmc_l8_dp_m2c_n[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_n[1]}]

#set_property PACKAGE_PIN aj4    [get_ports {fmc_l8_dp_m2c_n[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {fmc_l8_dp_m2c_n[0]}]




