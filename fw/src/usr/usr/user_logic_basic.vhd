library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


use work.ipbus.all;
use work.system_package.all;
--! user packages
use work.user_package.all;
use work.user_version_package.all;

-- GBT packages
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;


library unisim;
use unisim.vcomponents.all;

entity user_logic is 
	generic (
		GBT_BANK_ID                     : integer := 0;
		NUM_LINKS			: integer := 1;
		TX_OPTIMIZATION			: integer range 0 to 1 := STANDARD;
		RX_OPTIMIZATION			: integer range 0 to 1 := STANDARD;
		TX_ENCODING			: integer range 0 to 1 := GBT_FRAME;
		RX_ENCODING			: integer range 0 to 1 := GBT_FRAME
);
port
(

	--# led
	usrled1_r				: out	std_logic; -- fmc_l12_spare[8]
	usrled1_g				: out	std_logic; -- fmc_l12_spare[9]
	usrled1_b				: out	std_logic; -- fmc_l12_spare[10]
	usrled2_r				: out	std_logic; -- fmc_l12_spare[11]
	usrled2_g				: out	std_logic; -- fmc_l12_spare[12]
	usrled2_b				: out	std_logic; -- fmc_l12_spare[13]

	--# on-board fabric clk
    fabric_clk_p                                : in    std_logic; -- new port [PV 2015.08.19]
    fabric_clk_n                                : in    std_logic; -- new port [PV 2015.08.19]
    fabric_coax_or_osc_p 			: in 	std_logic;
	fabric_coax_or_osc_n 			: in 	std_logic;

	--# on-board mgt clk
	pcie_clk_p				: in	std_logic;
    pcie_clk_n                                  : in    std_logic;
	osc_xpoint_a_p				: in	std_logic;
	osc_xpoint_a_n				: in	std_logic;
	osc_xpoint_b_p				: in	std_logic;
	osc_xpoint_b_n				: in	std_logic;
	osc_xpoint_c_p				: in	std_logic;
	osc_xpoint_c_n				: in	std_logic;
	osc_xpoint_d_p				: in	std_logic;
	osc_xpoint_d_n				: in	std_logic;
	ttc_mgt_xpoint_a_p			: in	std_logic;
	ttc_mgt_xpoint_a_n			: in	std_logic;
	ttc_mgt_xpoint_b_p			: in	std_logic;
	ttc_mgt_xpoint_b_n			: in	std_logic;
	ttc_mgt_xpoint_c_p			: in	std_logic;
	ttc_mgt_xpoint_c_n			: in	std_logic;
			
	--# fmc mgt clk		
	fmc_l12_gbtclk0_a_p			: in	std_logic; 
	fmc_l12_gbtclk0_a_n			: in	std_logic; 
	fmc_l12_gbtclk1_a_p			: in	std_logic; 
	fmc_l12_gbtclk1_a_n			: in	std_logic; 
	fmc_l12_gbtclk0_b_p			: in	std_logic; 
	fmc_l12_gbtclk0_b_n			: in	std_logic; 
	fmc_l12_gbtclk1_b_p			: in	std_logic; 
	fmc_l12_gbtclk1_b_n			: in	std_logic; 
	fmc_l8_gbtclk0_p			: in	std_logic; 
	fmc_l8_gbtclk0_n			: in	std_logic; 
	fmc_l8_gbtclk1_p			: in	std_logic; 
	fmc_l8_gbtclk1_n			: in	std_logic; 
 
--    Following Mykyta's example
--	# fmc mgt
	fmc_l12_dp_c2m_p			: out	std_logic_vector(11 downto 0);
	fmc_l12_dp_c2m_n			: out	std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_p			: in	std_logic_vector(11 downto 0);
	fmc_l12_dp_m2c_n			: in	std_logic_vector(11 downto 0);
	fmc_l8_dp_c2m_p				: out	std_logic_vector( 7 downto 0);
	fmc_l8_dp_c2m_n				: out	std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_p				: in	std_logic_vector( 7 downto 0);
	fmc_l8_dp_m2c_n				: in	std_logic_vector( 7 downto 0);


	--# fmc fabric clk	
    fmc_l8_clk0                                 : in    std_logic; 
    fmc_l8_clk1                                 : in    std_logic;
    fmc_l12_clk0                                : in    std_logic;
    fmc_l12_clk1                                : in    std_logic;    

	--# fmc gpio		
	fmc_l8_la_p				: inout	std_logic_vector(33 downto 0);
	fmc_l8_la_n				: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_p				: inout	std_logic_vector(33 downto 0);
	fmc_l12_la_n				: inout	std_logic_vector(33 downto 0);
	
	--# amc mgt		
	k7_amc_rx_p				: inout	std_logic_vector(15 downto 1);
	k7_amc_rx_n				: inout	std_logic_vector(15 downto 1);
	amc_tx_p				: inout	std_logic_vector(15 downto 1);
	amc_tx_n				: inout	std_logic_vector(15 downto 1);
	
	--# amc fabric
	k7_fabric_amc_rx_p03			: inout	std_logic;
	k7_fabric_amc_rx_n03    		: inout	std_logic;
	k7_fabric_amc_tx_p03    		: inout	std_logic;
	k7_fabric_amc_tx_n03    		: inout	std_logic;

	--# ddr3
	ddr3_sys_clk_p 				: in	std_logic;
	ddr3_sys_clk_n 				: in	std_logic;
	ddr3_dq                 		: inout std_logic_vector( 31 downto 0);
	ddr3_dqs_p              		: inout std_logic_vector(  3 downto 0);
	ddr3_dqs_n              		: inout std_logic_vector(  3 downto 0);
	ddr3_addr               		: out   std_logic_vector( 13 downto 0);
	ddr3_ba                 		: out   std_logic_vector(  2 downto 0);
	ddr3_ras_n              		: out   std_logic;
	ddr3_cas_n              		: out   std_logic;
	ddr3_we_n               		: out   std_logic;
	ddr3_reset_n            		: out   std_logic;
	ddr3_ck_p               		: out   std_logic_vector(  0 downto 0);
	ddr3_ck_n               		: out   std_logic_vector(  0 downto 0);
	ddr3_cke                		: out   std_logic_vector(  0 downto 0);
	ddr3_cs_n               		: out   std_logic_vector(  0 downto 0);
	ddr3_dm                 		: out   std_logic_vector(  3 downto 0);
	ddr3_odt                		: out   std_logic_vector(  0 downto 0);

    --# cdce
	cdce_pll_lock_i                         : in    std_logic; -- new port [PV 2015.08.19]  
    cdce_pri_clk_bufg_o 		        : out 	std_logic; -- new port [PV 2015.08.19] 
    cdce_ref_sel_o                              : out   std_logic; -- new port [PV 2015.08.19]   
    cdce_pwrdown_o                              : out   std_logic; -- new port [PV 2015.08.19]  
    cdce_sync_o                                 : out   std_logic; -- new port [PV 2015.08.19]  
    cdce_sync_clk_o                             : out   std_logic; -- new port [PV 2015.08.19]  

	--# system clk		
	osc125_a_bufg_i				: in	std_logic;
	osc125_a_mgtrefclk_i			: in	std_logic;
	osc125_b_bufg_i				: in 	std_logic;
	osc125_b_mgtrefclk_i			: in	std_logic;
    clk_31_250_bufg_i		                : in	std_logic; -- new port [PV 2015.08.19]
    
    --# ipbus comm    
	ipb_clk_o				: out	std_logic;
	ipb_rst_i				: in	std_logic;  -- this is only a SIGNAL in gbt example design while it is a module input here: ERROR
	ipb_miso_o			        : out	ipb_rbus_array(0 to nbr_usr_slaves-1);
	ipb_mosi_i			        : in	ipb_wbus_array(0 to nbr_usr_slaves-1);

    --# ipbus conf
	ip_addr_o				: out	std_logic_vector(31 downto 0);
    mac_addr_o                                  : out   std_logic_vector(47 downto 0);
    rarp_en_o                                   : out   std_logic;
    use_i2c_eeprom_o                            : out   std_logic;
    
    --# GBT signals
    -- General reset
    sw3                                            : in    std_logic;
    
    -- Crosspoint Switch U42 & CDCE62005:         
    fmc_l8_spare                                   : inout std_logic_vector(0 to 19);
    
    -- Commented by me to avoid fmc_l12 conflict with golden image
    -- User LEDs:
--    sysled1_r                                      : out   std_logic; -- fmc_l12_spare[8]    -- D22
--    sysled1_g                                      : out   std_logic; -- fmc_l12_spare[9]
--    sysled1_b                                      : out   std_logic; -- fmc_l12_spare[10]   
--    sysled2_r                                      : out   std_logic; -- fmc_l12_spare[11]   -- D23
--    sysled2_g                                      : out   std_logic; -- fmc_l12_spare[12]
--    sysled2_b                                      : out   std_logic; -- fmc_l12_spare[13] 
     
    -- Crosspoint Switches U8 & U7:      
    k7_master_xpoint_ctrl                          : out   std_logic_vector(0 to 9);   
      
    -- FMC power:      
    fmc_l8_pwr_en                                  : out   std_logic;   
    fmc_l12_pwr_en                                 : out   std_logic;
     
    -- Signals forwarding --
    -- Header P4 pins 9 & 10 (schematics page 6 "K7_GEN_IO"):      
    fmc_l12_spare                                  : inout std_logic_vector(7 downto 6)      -- Comment: * TTC_MGT_XPOINT_C or FABRIC_CLK -> pin  9
                                                                                             --          * TX_WORDCLK or TX_FRAMECLK      -> pin 10
     
     
);

end user_logic;

architecture usr of user_logic is

	signal ipb_clk				: std_logic;
    
	signal fabric_clk_pre_buf                   : std_logic;                
	signal fabric_clk                           : std_logic;
    
	signal ctrl_reg		                : array_32x32bit;
	signal stat_reg		                : array_32x32bit;

	signal cdce_pri_clk_bufg                    : std_logic;

	signal cdce_sync_from_ipb                   : std_logic;     
	signal cdce_sel_from_ipb                    : std_logic;                    
	signal cdce_pwrdown_from_ipb                : std_logic;
	signal cdce_ctrl_from_ipb                   : std_logic;  
--  signal cdce_sel_from_fabric                 : std_logic;                    
--  signal cdce_pwrdown_from_fabric             : std_logic;
--  signal cdce_ctrl_from_fabric                : std_logic; 






	-- GBT bank
    -----------
    -- FC7 clocks scheme                    
    signal txFrameClk_from_txPll                      : std_logic;

    -- General reset    
    signal reset_powerup_b                            : std_logic;    
    signal reset_powerup                              : std_logic;    
           
    signal fabric_clk_bufg_i                          : std_logic;      
    signal user_cdce_locked_i                         : std_logic;      
    signal header                                     : std_logic_vector(9 to 10);

    -- User
    ---- General reset
       
    signal generalReset_from_generaReset              : std_logic;         
                        
    -- FC7 clocks scheme --                     
    -- MGT(GTX) reference clock:           
    signal ttcMgtXpoint_from_ibufdsGtxe1              : std_logic;
    signal ttcMgtXpoint_from_bufg                     : std_logic;
       
       
       
       
       
       
       
       
    -- GBT wrapper 
    -- SINGLE-LINK GBT SIGNALS
    --------------------------                       
    -- GBT wrapper 
    -- SINGLE-LINK GBT SIGNALS
    --------------------------
    ----GBT Bank 1      
    ---- Control:      
    signal generalReset_from_user                     : std_logic;      
    signal manualResetTx_from_user                    : std_logic; 
    signal manualResetRx_from_user                    : std_logic; 
    signal clkMuxSel_from_user                        : std_logic;       
    signal testPatterSel_from_gbtBank_1_user                    : std_logic_vector(1 downto 0); 
    signal loopBack_from_user                         : std_logic_vector(2 downto 0); 
    signal resetDataErrorSeenFlag_from_gbtBank_1_user           : std_logic; 
    signal resetGbtRxReadyLostFlag_from_gbtBank_1_user          : std_logic; 
    signal txIsDataSel_from_gbtBank_1_user                      : std_logic;   
    --------------------------------------------------       
    signal latOptGbtBankTx_from_gbtBank_1          : std_logic;
    signal latOptGbtBankRx_from_gbtBank_1          : std_logic;
    signal txFrameClkPllLocked_from_gbtBank_1      : std_logic;
    signal mgtReady_from_gbtBank_1                 : std_logic; 
    signal rxBitSlipNbr_from_gbtBank_1             : std_logic_vector(GBTRX_BITSLIP_NBR_MSB downto 0);
    signal rxWordClkReady_from_gbtBank_1           : std_logic; 
    signal rxFrameClkReady_from_gbtBank_1          : std_logic; 
    signal gbtRxReady_from_gbtBank_1               : std_logic;    
    signal rxIsData_from_gbtBank_1                 : std_logic;        
    signal gbtRxReadyLostFlag_from_gbtBank_1       : std_logic; 
    signal rxDataErrorSeen_from_gbtBank_1          : std_logic; 
    signal rxExtrDataWidebusErSeen_from_gbtBank_1  : std_logic; 
       
    -- Data:
    signal txData_from_gbtBank_1                   : std_logic_vector(83 downto 0);
    signal rxData_from_gbtBank_1                   : std_logic_vector(83 downto 0);
   
    --------------------------------------------------       
    signal txExtraDataWidebus_from_gbtBank_1       : std_logic_vector(31 downto 0);
    signal rxExtraDataWidebus_from_gbtBank_1       : std_logic_vector(31 downto 0);
    -------------------------------------------------- 
    
    signal txMatchFlag_from_gbtBank_1              : std_logic;
    signal rxMatchFlag_from_gbtBank_1              : std_logic;
    --------------------------------------------------
    -- Chipscope --g
           
    signal vioControl_from_icon                       : std_logic_vector(35 downto 0); 
    signal txIlaControl_from_icon                     : std_logic_vector(35 downto 0); 
    signal rxIlaControl_from_icon                     : std_logic_vector(35 downto 0); 
    -------------------------------------------------- 
    signal sync_from_vio                              : std_logic_vector(11 downto 0);
    signal async_to_vio                               : std_logic_vector(17 downto 0);
       
    -- Latency measurement --       
    signal txFrameClk                                 : std_logic;
    signal txWordClk                                : std_logic;
    signal rxFrameClk                  : std_logic;
    signal rxWordClk                   : std_logic;
     
          
    signal sysclk:                    std_logic;
              
    -- ILA component  --
     --================--
     -- Vivado synthesis tool does not support mixed-language
     -- Solution: http://www.xilinx.com/support/answers/47454.html
         COMPONENT xlx_k7v7_vivado_debug PORT(
            CLK: in std_logic;
            PROBE0: in std_logic_vector(83 downto 0);
            PROBE1: in std_logic_vector(31 downto 0);
            PROBE2: in std_logic_vector(3 downto 0);
            PROBE3: in std_logic_vector(0 downto 0)
         );
         END COMPONENT;
         
         COMPONENT xlx_k7v7_vio
           PORT (
             clk : IN STD_LOGIC;
             probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in5 : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
             probe_in6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_in12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_out1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_out2 : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
             probe_out3 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
             probe_out4 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_out5 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_out6 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_out7 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
             probe_out8 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
           );
         END COMPONENT;
 
     attribute mark_debug : string;
     
     attribute mark_debug of rxIsData_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of txFrameClkPllLocked_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of latOptGbtBankTx_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of mgtReady_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of rxWordClkReady_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of rxBitSlipNbr_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of rxFrameClkReady_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of gbtRxReady_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of gbtRxReadyLostFlag_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of rxDataErrorSeen_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of rxExtrDataWidebusErSeen_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of latOptGbtBankRx_from_gbtBank_1: signal is "TRUE";
     attribute mark_debug of generalReset_from_user: signal is "TRUE";
     attribute mark_debug of clkMuxSel_from_user: signal is "TRUE";
     attribute mark_debug of testPatterSel_from_gbtBank_1_user: signal is "TRUE";
     attribute mark_debug of loopBack_from_user: signal is "TRUE";
     attribute mark_debug of resetDataErrorSeenFlag_from_gbtBank_1_user: signal is "TRUE";
     attribute mark_debug of resetGbtRxReadyLostFlag_from_gbtBank_1_user: signal is "TRUE";
     attribute mark_debug of txIsDataSel_from_gbtBank_1_user: signal is "TRUE";
     attribute mark_debug of manualResetTx_from_user: signal is "TRUE";
     attribute mark_debug of manualResetRx_from_user: signal is "TRUE";
--=====================================================================================--

    --================--
    -- Clock component--
    --================--
    -- Vivado synthesis tool does not support mixed-language
    -- Solution: http://www.xilinx.com/support/answers/47454.html
    COMPONENT xlx_k7v7_tx_pll PORT(
     clk_in1: in std_logic;
     RESET: in std_logic;
     CLK_OUT1: out std_logic;
     LOCKED: out std_logic
    );
    END COMPONENT;
          
                     

begin

	--===========================================--
	-- ipbus management
	--===========================================--
	ipb_clk 		               <= clk_31_250_bufg_i; 	-- select the frequency of the ipbus clock 
	ipb_clk_o 	                       <= ipb_clk;				-- always forward the selected ipb_clk to system core
    --
	ip_addr_o 	                       <= x"c0_a8_00_50";
	mac_addr_o	                       <= x"aa_bb_cc_dd_ee_50";
	rarp_en_o 		                   <= '0';
	use_i2c_eeprom_o                   <= '1';
	--===========================================--


	--===========================================--
	-- other clocks 
	--===========================================--
	fclk_ibuf:      ibufgds     port map (i => fabric_clk_p, ib => fabric_clk_n, o => fabric_clk_pre_buf);
    	fclk_bufg:      bufg        port map (i => fabric_clk_pre_buf,               o => fabric_clk);
	--===========================================--
        


    --===================================--
    cdce_synch: entity work.cdce_synchronizer
    --===================================--
    generic map
    (    
        pwrdown_delay     => 1000,
        sync_delay        => 1000000    
    )
    port map
    (    
        reset_i           => ipb_rst_i,
        ------------------
        ipbus_ctrl_i      => not cdce_ctrl_from_ipb,          -- default: 1 (ipb controlled) 
        ipbus_sel_i       =>     cdce_sel_from_ipb,           -- default: 0 (select secondary clock)
        ipbus_pwrdown_i   => not cdce_pwrdown_from_ipb,       -- default: 1 (powered up)
        ipbus_sync_i      => not cdce_sync_from_ipb,          -- default: 1 (disable sync mode), rising edge needed to resync
        ------------------                                                                       
        user_sel_i        => '1', -- cdce_sel_from_fabric     -- effective only when ipbus_ctrl_i = '0'            
        user_pwrdown_i    => '1', -- cdce_pwrdown_from_fabric -- effective only when ipbus_ctrl_i = '0'
        user_sync_i       => '1', -- cdce_sync_from_fabric    -- effective only when ipbus_ctrl_i = '0'  
        ------------------
        pri_clk_i                               => cdce_pri_clk_bufg,
        sec_clk_i         => fabric_clk,         -- copy of what is actually send to the secondary clock input
        pwrdown_o         => cdce_pwrdown_o,
        sync_o            => cdce_sync_o,
        ref_sel_o         => cdce_ref_sel_o,    
        sync_clk_o        => cdce_sync_clk_o
    );
        
        cdce_pri_clk_bufg_o                    <= cdce_pri_clk_bufg; cdce_pri_clk_bufg   <= '0'; -- [note: in this example, the cdce_pri_clk_bufg is not used]
    --===================================--

	
	
	--===========================================--
	stat_regs_inst: entity work.ipb_user_status_regs
	--===========================================--
	port map
	(
		clk				=> ipb_clk,
		reset				=> ipb_rst_i,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_stat_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_stat_regs),
		regs_i				=> stat_reg
	);
	--===========================================--



	--===========================================--
	ctrl_regs_inst: entity work.ipb_user_control_regs
	--===========================================--
	port map
	(
		clk				=> ipb_clk,
		reset				=> ipb_rst_i,
		ipb_mosi_i			=> ipb_mosi_i(user_ipb_ctrl_regs),
		ipb_miso_o			=> ipb_miso_o(user_ipb_ctrl_regs),
		regs_o				=> ctrl_reg
	);
	--===========================================--
		
		

   --===========================================--
	-- example register mapping
	--===========================================--
									stat_reg(0)					<= usr_id_0;
									stat_reg(1)					<= x"0000_0000";
									stat_reg(2)					<= firmware_id;
									
	usrled1_r				<= not ctrl_reg(0)(0);
	usrled1_g				<= not ctrl_reg(0)(1);
	usrled1_b				<= not ctrl_reg(0)(2);
	
	usrled2_r				<= not ctrl_reg(0)(4);
	usrled2_g				<= not ctrl_reg(0)(5);
	usrled2_b				<= not ctrl_reg(0)(6);
	
	

	 --===============--
      -- General reset -- 
      --===============-- 
      
      reset_gen:srl16 port map (clk=>fabric_clk, q=>reset_powerup_b, a0=>'1',a1=>'1',a2 =>'1',a3 => '1',d=>'1');
      -- reset_powerup                                     <= (not reset_powerup_b) or (not sw3); -- commented by me: investigate later
      
      -- Commented by me to remove conflict with module input signal of the same name
      -- ipb_rst_i                                         <= reset_powerup; 
      
      --===============--
      -- Clocks scheme -- 
      --===============--   
   
      -- Commented by me to avoid conflict with clokc buffers on same signal in golden firmware
      -- Fabric clock (40MHz):
      ------------------------       
      
--      fabric_clk_ibufgds: IBUFGDS
--         generic map (
--            IBUF_LOW_PWR                                => FALSE,
--            IOSTANDARD                                  => "LVDS_25")
--         port map (                 
--            O                                           => fabric_clk_pre_buf,
--            I                                           => fabric_clk_p,
--            IB                                          => fabric_clk_n
--         );
         
--      fclk_bufg: BUFG 
--         port map (
--            I                                           => fabric_clk_pre_buf,
--            O                                           => fabric_clk
--         );
                  
       debugclk_bufg: BUFG 
          port map (
             I                                           => fabric_clk_pre_buf,
             O                                           => sysclk
          );
         
      fabric_clk_bufg_i                                 <=  fabric_clk;  
   
      --=============--
      -- FC7 control --
      --=============--
      
      -- Crosspoint Switch U42:
      -------------------------
      
      -- Comment: * The clock signal from the output U0 of the clock synthesizer CDCE62005 (U38) is forwarded
      --            to the FPGA through the output TTC_MGT_XPOINT_C of the Crosspoint Switch U42 to be used as
      --            reference clock by the MGT of the GBT Bank.
      --
      --          * Note!! The output U0 of the clock synthesizer CDCE62005 (U38) MUST be set to 120MHz.
      
      -- fmc_l8_spare(8)                                   <= '0'; -- commented by me: investigate later
      
      -- Crosspoint Switches U8 & U7:
      -------------------------------
      
      -- Comment: The clock signal from the 40MHz crystal oscillator Y5 is forwarded to the FPGA through
      --          the output 1 of the Crosspoint Switch U8 (CKX) and the output 3 of the Crosspoint Switch U7
      --          (FABRIC_CLK) to be used as fabric clock by the "fc7_gbt_example_design". 
      
      k7_master_xpoint_ctrl(8)                          <= '1'; -- Comment: Crosspoint Switch U8 output 1 (CKX) is driven by 
      k7_master_xpoint_ctrl(9)                          <= '1'; --          input 4 (OSC).  
      
      k7_master_xpoint_ctrl(4)                          <= '1'; -- Comment: Crosspoint Switch U7 output 3 (FABRIC_CLK) is 
      k7_master_xpoint_ctrl(5)                          <= '1'; --          driven by input 4 (CKX).   
     
      -- CDCE62005:
      -------------
   
      -- Comment: The clock synthesizer CDCE62005 is reset and synchronized with "fabric_clk" after power up.
    
      -- Commented by me: a cdce synchronizer already exists in folden image
--      cdceSync: entity work.cdce_synchronizer
--         generic map (  
--            PWRDOWN_DELAY                               => 1000,
--            SYNC_DELAY                                  => 1000000)
--         port map (
--            RESET_I                                     => reset_powerup, 
--            --------------------------------------------                     
--            IPBUS_CTRL_I                                => '0',              -- Comment: * Control by USER
--            IPBUS_SEL_I                                 => '1',              --          * CDCE62005 PRI_REF *NOT USED*
--            IPBUS_PWRDOWN_I                             => '1',              --          * Active low        *NOT USED*
--            IPBUS_SYNC_I                                => '1',              --          * Active low        *NOT USED*
--            --------------------------------------------                     --       
--            USER_SEL_I                                  => '0',              --          * CDCE62005 SEC_REF         
--            USER_PWRDOWN_I                              => '1',              --          * Active low    
--            USER_SYNC_I                                 => '1',              --          * Active low    
--            --------------------------------------------                     --          
--            PRI_CLK_I                                   => '0',              --          * *NOT USED*
--            SEC_CLK_I                                   => fabric_clk,       --          * 40MHz         
--            PWRDOWN_O                                   => fmc_l8_spare(5),
--            SYNC_O                                      => fmc_l8_spare(4),
--            REF_SEL_O                                   => fmc_l8_spare(0),         
--            --------------------------------------------               
--            SYNC_CMD_O                                  => open,
--            SYNC_CLK_O                                  => open,
--            SYNC_BUSY_O                                 => open,         
--            SYNC_DONE_O                                 => open
--         );
         
--      fmc_l8_spare(1)                                   <= '1';   -- Comment: - "cdce_spi_le"
--      fmc_l8_spare(2)                                   <= '0';   --          - "cdce_spi_clk"
--      fmc_l8_spare(3)                                   <= '0';   --          - "cdce_spi_mosi"
      
      --===========--
      -- FMC power --
      --===========--
      
      fmc_l8_pwr_en                                     <= '1';
      fmc_l12_pwr_en                                    <= '1'; 
      
      --================--
      -- User interface --
      --================--
      
      user_cdce_locked_i                                <= fmc_l8_spare(10);
      
      -- Commented by me to avoid conflict with existing defition from golden image
--      sysled1_r                                         <= usrled1_r;
--      sysled1_g                                         <= usrled1_g; 
--      sysled1_b                                         <= usrled1_b;
      
--      sysled2_r                                         <= usrled2_r;
--      sysled2_g                                         <= usrled2_g; 
--      sysled2_b                                         <= usrled2_b;
      
      fmc_l12_spare(6)                                  <= header( 9); 
      fmc_l12_spare(7)                                  <= header(10);
      
      
      --=====================================================================================--
      -----------------------------------------------------------------------------------------
      ------------------------------------------ User -----------------------------------------
      -----------------------------------------------------------------------------------------
      --=====================================================================================--
   
     
      --===============--
      -- General reset -- 
      --===============--
      
      generalReset: entity work.xlx_k7v7_reset
         port map (
            CLK_I                                       => fabric_clk_bufg_i,
            RESET1_B_I                                  => not ipb_rst_i,
            RESET2_B_I                                  => not generalReset_from_user,
            RESET_O                                     => generalReset_from_generaReset
         );
         
      --===============--
      -- Clocks scheme -- 
      --===============--   
      
      -- MGT(GTX) reference clock:
      ----------------------------
      
      -- Comment: * The clock signal from the output U0 of the clock synthesizer CDCE62005 (U38) is forwarded
      --            to the FPGA through the output TTC_MGT_XPOINT_C of the Crosspoint Switch U42 to be used as
      --            reference clock by the MGT of the GBT Bank.
      --
      --          * Note!! The output U0 of the clock synthesizer CDCE62005 (U38) MUST be set to 120MHz.   
      
      cdceOut0IbufdsGtxe2: ibufds_gte2
         port map (
            O                                           => ttcMgtXpoint_from_ibufdsGtxe1,
            ODIV2                                       => open,
            CEB                                         => '0',
            I                                           => ttc_mgt_xpoint_c_p,
            IB                                          => ttc_mgt_xpoint_c_n
         );
      
      cdceOut0Bufg: bufg
         port map (
            O                                           => ttcMgtXpoint_from_bufg, 
            I                                           => ttcMgtXpoint_from_ibufdsGtxe1
         );
            
        -- Frame clock
        txPll: xlx_k7v7_tx_pll
          port map (
             clk_in1                                  => ttcMgtXpoint_from_ibufdsGtxe1,
             CLK_OUT1                                 => txFrameClk_from_txPll,
             -----------------------------------------  
             RESET                                    => '0',
             LOCKED                                   => txFrameClkPllLocked_from_gbtBank_1
          );
     
            
        --=========================--
        -- GBT Bank example design --
        --=========================--  
        -- GBT bank 1  
       gbt_wrapper_0: entity work.gbt_fpga_wrapper
              generic map(
                  GBT_BANK_ID                                            => 1,
                  NUM_LINKS                                              => GBT_BANKS_USER_SETUP(1).NUM_LINKS,
                  TX_OPTIMIZATION                                        => GBT_BANKS_USER_SETUP(1).TX_OPTIMIZATION,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                  RX_OPTIMIZATION                                        => GBT_BANKS_USER_SETUP(1).RX_OPTIMIZATION,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                  TX_ENCODING                                            => GBT_BANKS_USER_SETUP(1).TX_ENCODING,
                  RX_ENCODING                                            => GBT_BANKS_USER_SETUP(1).RX_ENCODING,
                  
                  -- Extended configuration --
                  DATA_GENERATOR_ENABLE                                  => 1,
                  DATA_CHECKER_ENABLE                                    => 0,
                  MATCH_FLAG_ENABLE                                      => 1
              )
            port map (                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        
              --==============--
              -- Clocks       --
              --==============--
              FRAMECLK_40MHZ                                             => txFrameClk_from_txPll,
              XCVRCLK                                                    => ttcMgtXpoint_from_ibufdsGtxe1,
              
              TX_FRAMECLK_O(1)                                           => txFrameClk,  
        
              TX_WORDCLK_O(1)                                            => txWordClk, 
             
              RX_FRAMECLK_O(1)                                           => rxFrameClk,  
        
              RX_WORDCLK_O(1)                                            => rxWordClk,
        
              --==============--
              -- Reset        --
              --==============--
              GBTBANK_GENERAL_RESET_I                                    => generalReset_from_generaReset,
              GBTBANK_MANUAL_RESET_TX_I                                  => manualResetTx_from_user,
              GBTBANK_MANUAL_RESET_RX_I                                  => manualResetRx_from_user,
              
              --==============--
              -- Serial lanes --
              --==============--
              GBTBANK_MGT_RX_P(1)                                        => fmc_l12_dp_m2c_p(0),
        
              GBTBANK_MGT_RX_N(1)                                        => fmc_l12_dp_m2c_n(0),
              
              GBTBANK_MGT_TX_P(1)                                        => fmc_l12_dp_c2m_p(0),
              
              GBTBANK_MGT_TX_N(1)                                        => fmc_l12_dp_c2m_n(0),
              
              --==============--
              -- Data             --
              --==============--        
              -- GBTBANK_GBT_DATA_I(1)                                      => (others => '0'),
              -- GBTBANK_GBT_DATA_I(1)                                      => x"3_000_4_00_5_000",   
              -- GBTBANK_GBT_DATA_I(1)                                      => x"3333_ffff_ff_3_333_4_33_5_333",   
              -- GBTBANK_GBT_DATA_I(1)                                      => test_ipb_reg_i,
              GBTBANK_GBT_DATA_I(1)                                      => x"3333_ffff_ff_3_333_4_33_5_333",
              GBTBANK_WB_DATA_I(1)                                       => (others => '0'),
              
              TX_DATA_O(1)                                               => txData_from_gbtBank_1,   
              
              WB_DATA_O(1)                                               => open, --txExtraDataWidebus_from_gbtExmplDsgn,
              
              
              GBTBANK_GBT_DATA_O(1)                                      => rxData_from_gbtBank_1,
              
              GBTBANK_WB_DATA_O(1)                                       => open, --rxExtraDataWidebus_from_gbtExmplDsgn,
              
              --==============--
              -- Reconf.         --
              --==============--
              GBTBANK_MGT_DRP_RST                                        => '0',
              GBTBANK_MGT_DRP_CLK                                        => fabric_clk, --
              
              --==============--
              -- TX ctrl        --
              --==============--
              GBTBANK_TX_ISDATA_SEL_I(1)                                => txIsDataSel_from_gbtBank_1_user, --
              
              GBTBANK_TEST_PATTERN_SEL_I                                => testPatterSel_from_gbtBank_1_user, --
              
              --==============--
              -- RX ctrl      --
              --==============--
              GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I(1)                   => resetGbtRxReadyLostFlag_from_gbtBank_1_user, --
              
              GBTBANK_RESET_DATA_ERRORSEEN_FLAG_I(1)                    => resetDataErrorSeenFlag_from_gbtBank_1_user, --
              
              --==============--
              -- TX Status    --
              --==============--
              GBTBANK_LINK_READY_O(1)                                   => mgtReady_from_gbtBank_1, --
              
              GBTBANK_TX_MATCHFLAG_O                                    => txMatchFlag_from_gbtBank_1,--
              
              --==============--
              -- RX Status    --
              --==============--
              GBTBANK_GBTRX_READY_O(1)                                  => gbtRxReady_from_gbtBank_1, --
              
              GBTBANK_LINK1_BITSLIP_O                                   => rxBitSlipNbr_from_gbtBank_1, --
              
              GBTBANK_GBTRXREADY_LOST_FLAG_O(1)                         => gbtRxReadyLostFlag_from_gbtBank_1, --
              
              GBTBANK_RXDATA_ERRORSEEN_FLAG_O(1)                        => rxDataErrorSeen_from_gbtBank_1, --
              
              GBTBANK_RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O(1)           => rxExtrDataWidebusErSeen_from_gbtBank_1, --
              
              GBTBANK_RX_MATCHFLAG_O(1)                                 => rxMatchFlag_from_gbtBank_1, --
              
              GBTBANK_RX_ISDATA_SEL_O(1)                                => rxIsData_from_gbtBank_1, --
                              
              
              --==============--
              -- XCVR ctrl    --
              --==============--
              GBTBANK_LOOPBACK_I                                        => loopBack_from_user, --
              
              GBTBANK_TX_POL(1)                                         => '0', --
              
              GBTBANK_RX_POL(1)                                         => '0' --
         ); 
         
         
         
            -- Logic analyzer
            -----------------
            
            latOptGbtBankTx_from_gbtBank_1                      <= '1' when GBT_BANKS_USER_SETUP(1).TX_OPTIMIZATION = LATENCY_OPTIMIZED else
                                                                                          '0';
            latOptGbtBankRx_from_gbtBank_1                       <= '1' when GBT_BANKS_USER_SETUP(1).RX_OPTIMIZATION = LATENCY_OPTIMIZED else
                                                                                          '0';
                                                                                          
               vio : xlx_k7v7_vio
                     PORT MAP (
                       clk => sysclk,
                       -- in
                       probe_in0(0) => rxIsData_from_gbtBank_1,
                       probe_in1(0) => txFrameClkPllLocked_from_gbtBank_1,
                       probe_in2(0) => latOptGbtBankTx_from_gbtBank_1,
                       probe_in3(0) => mgtReady_from_gbtBank_1,
                       probe_in4(0) => rxWordClkReady_from_gbtBank_1,
                       probe_in5    => rxBitSlipNbr_from_gbtBank_1,
                       probe_in6(0) => rxFrameClkReady_from_gbtBank_1,
                       probe_in7(0) => gbtRxReady_from_gbtBank_1,
                       probe_in8(0) => gbtRxReadyLostFlag_from_gbtBank_1,
                       probe_in9(0) => rxDataErrorSeen_from_gbtBank_1,
                       probe_in10(0) => rxExtrDataWidebusErSeen_from_gbtBank_1,
                       probe_in11(0) => '0', --8b10b support removed  
                       -- probe_in12(0) => latOptGbtBankRx_from_gbtBank_1,
                       probe_in12(0) => generalReset_from_user,
                       -- out
                       probe_out0(0) => generalReset_from_user,
                       probe_out1(0) => clkMuxSel_from_user,
                       probe_out2 => testPatterSel_from_gbtBank_1_user,
                       probe_out3 => loopBack_from_user,
                       probe_out4(0) => resetDataErrorSeenFlag_from_gbtBank_1_user,
                       probe_out5(0) => resetGbtRxReadyLostFlag_from_gbtBank_1_user,
                       probe_out6(0) => txIsDataSel_from_gbtBank_1_user,
                       probe_out7(0) => manualResetTx_from_user,
                       probe_out8(0) => manualResetRx_from_user
        --               probe_out3 => loopBack_from_user,
        --               probe_out4(0) => resetDataErrorSeenFlag_from_gbtBank_1_user,
        --               probe_out5(0) => resetGbtRxReadyLostFlag_from_gbtBank_1_user,
        --               probe_out7(0) => manualResetTx_from_user,
        --               probe_out8(0) => manualResetRx_from_user
                     );
                     
               -- hard code signals the dirty way
        --       generalReset_from_user               <= '0';
        --       clkMuxSel_from_user                  <= '1';
        --       testPatterSel_from_gbtBank_1_user    <= "10";
        --       txIsDataSel_from_gbtBank_1_user      <= '1';
                       
               txILa: xlx_k7v7_vivado_debug
                   port map (
                      CLK => sysclk,
                      PROBE0 => txData_from_gbtBank_1,
                      PROBE1 => txExtraDataWidebus_from_gbtBank_1,
                      -- PROBE2 => (others => '0'), -- 8b10b support removed
                      PROBE2(0) => generalReset_from_user,
                      PROBE2(1) => manualResetTx_from_user,
                      PROBE2(2) => manualResetRx_from_user,
                      PROBE2(3) => '0',
                      PROBE3(0) => txIsDataSel_from_gbtBank_1_user);  
             
               rxIla: xlx_k7v7_vivado_debug
                   port map (
                      CLK => sysclk,
                      PROBE0 => rxData_from_gbtBank_1,
                      PROBE1 => rxExtraDataWidebus_from_gbtBank_1,
                      PROBE2 => (others => '0'), --Removed 8b10b support
                      -- PROBE3(0) => rxIsData_from_gbtBank_1); 
                      PROBE3(0) => sysclk);   
      -- On-board LEDs:             
      -----------------
      
      -- Commented by me to avoid conflict with golden firmware signals of the same name
--      usrled1_r                                         <= '1';
--      usrled1_g                                         <= '1';                                    
--      usrled1_b                                         <= not (user_cdce_locked_i and txFrameClkPllLocked_from_gbtExmplDsgn);
      
--      usrled2_r                                         <= '1';
--      usrled2_g                                         <= '1';  
--      usrled2_b                                         <= not mgtReady_from_gbtExmplDsgn;
      
      --=====================--
      -- Latency measurement --
      --=====================--
      
      -- Clock forwarding:
      --------------------
      
      -- Comment: Header P4 pins 9 & 10 (schematics page 6 "K7_GEN_IO").
      
      header( 9)                                        <= ttcMgtXpoint_from_bufg      when clkMuxSel_from_user = '1' else 
                                                           fabric_clk_bufg_i;          
   
      header(10)                                        <= txWordClk when clkMuxSel_from_user = '1' else 
                                                           txFrameClk;
   
      -- Comment: * The forwarding of the clocks allows to check the phase alignment of the different
      --            clocks using an oscilloscope.
      --
      --          * Note!! If RX DATA comes from another board with a different reference clock, 
      --                   then the TX_FRAMECLK/TX_WORDCLK domains are asynchronous with respect to the
      --                   TX_FRAMECLK/TX_WORDCLK domains. 
      
      fmc_l8_la_p( 0)                                   <= txFrameClk;
      fmc_l8_la_p( 2)                                   <= txWordClk;
      --------------------------------------------------   
      fmc_l8_la_p( 4)                                   <= rxFrameClk;  
      fmc_l8_la_p( 6)                                   <= rxWordClk;  
   
      -- Pattern match flags:
      -----------------------
      
      -- Comment: * The latency of the link can be measured using an oscilloscope by comparing 
      --            the TX FLAG with the RX FLAG.
      --
      --          * The COUNTER TEST PATTERN must be used for this test.  
      
      fmc_l8_la_p( 8)                                   <= txMatchFlag_from_gbtBank_1;
      fmc_l8_la_p(10)                                   <= rxMatchFlag_from_gbtBank_1;
	
	

end usr;
