
module alt_a10_lpm_shiftreg (
	clock,
	shiftin,
	shiftout);	

	input		clock;
	input		shiftin;
	output		shiftout;
endmodule
