vlib work
vlib msim

vlib msim/xil_defaultlib
vlib msim/xpm

vmap xil_defaultlib msim/xil_defaultlib
vmap xpm msim/xpm

vlog -work xil_defaultlib -64 -sv "+incdir+../../../../../../core_sources/chipscope_ila/vivado/hdl/verilog" "+incdir+../../../../../../core_sources/chipscope_ila/vivado/hdl/verilog" \
"/opt/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/opt/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 "+incdir+../../../../../../core_sources/chipscope_ila/vivado/hdl/verilog" "+incdir+../../../../../../core_sources/chipscope_ila/vivado/hdl/verilog" \
"../../../../../../core_sources/chipscope_ila/vivado/sim/xlx_k7v7_vivado_debug.v" \

vlog -work xil_defaultlib "glbl.v"

