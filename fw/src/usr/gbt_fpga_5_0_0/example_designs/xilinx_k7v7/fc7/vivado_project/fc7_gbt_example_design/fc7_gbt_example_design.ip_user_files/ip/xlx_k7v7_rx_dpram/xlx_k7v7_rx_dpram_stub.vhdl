-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.2 (lin64) Build 1577090 Thu Jun  2 16:32:35 MDT 2016
-- Date        : Tue Mar 14 18:02:51 2017
-- Host        : lxtest.cern.ch running 64-bit Scientific Linux CERN SLC release 6.8 (Carbon)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/akanwal/fc7/vivado/gbt_fpga_5_0_0/gbt_bank/xilinx_k7v7/gbt_rx/rx_dpram_vivado/xlx_k7v7_rx_dpram_stub.vhdl
-- Design      : xlx_k7v7_rx_dpram
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k420tffg1156-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity xlx_k7v7_rx_dpram is
  Port ( 
    clka : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 4 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 39 downto 0 );
    clkb : in STD_LOGIC;
    addrb : in STD_LOGIC_VECTOR ( 2 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 159 downto 0 )
  );

end xlx_k7v7_rx_dpram;

architecture stub of xlx_k7v7_rx_dpram is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clka,wea[0:0],addra[4:0],dina[39:0],clkb,addrb[2:0],doutb[159:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "blk_mem_gen_v8_3_3,Vivado 2016.2";
begin
end;
