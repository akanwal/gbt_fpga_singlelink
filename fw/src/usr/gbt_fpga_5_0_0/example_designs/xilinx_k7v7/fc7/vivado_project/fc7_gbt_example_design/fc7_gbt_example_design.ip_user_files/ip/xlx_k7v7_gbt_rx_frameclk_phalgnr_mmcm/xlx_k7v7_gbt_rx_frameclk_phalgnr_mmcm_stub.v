// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.2 (lin64) Build 1577090 Thu Jun  2 16:32:35 MDT 2016
// Date        : Tue Mar 14 18:02:15 2017
// Host        : lxtest.cern.ch running 64-bit Scientific Linux CERN SLC release 6.8 (Carbon)
// Command     : write_verilog -force -mode synth_stub
//               /home/akanwal/fc7/vivado/gbt_fpga_5_0_0/example_designs/xilinx_k7v7/core_sources/gbt_rx_frameclk_phalgnr/vivado/xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm_stub.v
// Design      : xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k420tffg1156-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm(clk_in1, clk_out1, psclk, psen, psincdec, psdone, reset, locked)
/* synthesis syn_black_box black_box_pad_pin="clk_in1,clk_out1,psclk,psen,psincdec,psdone,reset,locked" */;
  input clk_in1;
  output clk_out1;
  input psclk;
  input psen;
  input psincdec;
  output psdone;
  input reset;
  output locked;
endmodule
