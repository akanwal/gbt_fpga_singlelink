// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ila,Vivado 2016.4" *)
module xlx_k7v7_vivado_debug(clk, probe0, probe1, probe2, probe3);
  input clk;
  input [83:0]probe0;
  input [31:0]probe1;
  input [3:0]probe2;
  input [0:0]probe3;
endmodule
