// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module xlx_k7v7_gbt_rx_frameclk_phalgnr_mmcm(clk_out1, psclk, psen, psincdec, psdone, reset, 
  locked, clk_in1);
  output clk_out1;
  input psclk;
  input psen;
  input psincdec;
  output psdone;
  input reset;
  output locked;
  input clk_in1;
endmodule
