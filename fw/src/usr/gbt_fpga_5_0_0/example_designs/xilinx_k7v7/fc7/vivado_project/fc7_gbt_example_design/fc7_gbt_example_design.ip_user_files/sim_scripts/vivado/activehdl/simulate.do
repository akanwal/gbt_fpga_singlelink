onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+xlx_k7v7_vio -L unisims_ver -L unimacro_ver -L secureip -L xil_defaultlib -L xpm -O5 xil_defaultlib.xlx_k7v7_vio xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {xlx_k7v7_vio.udo}

run -all

endsim

quit -force
