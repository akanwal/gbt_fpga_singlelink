
module alt_sv_issp_gbtBank2 (
	source,
	source_ena,
	probe,
	source_clk);	

	output	[8:0]	source;
	input		source_ena;
	input	[19:0]	probe;
	input		source_clk;
endmodule
