// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
// Date        : Fri Sep 22 11:03:30 2017
// Host        : lxtest.cern.ch running 64-bit Scientific Linux CERN SLC release 6.9 (Carbon)
// Command     : write_verilog -force -mode synth_stub
//               /home/akanwal/fc7/vivado/alishba/gbt_fpga_fc7/fw/src/usr/gbt_fpga_5_0_0/gbt_bank/xilinx_k7v7/gbt_tx/tx_dpram/xlx_k7v7_tx_dpram_stub.v
// Design      : xlx_k7v7_tx_dpram
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k420tffg1156-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_3_5,Vivado 2016.4" *)
module xlx_k7v7_tx_dpram(clka, wea, addra, dina, clkb, addrb, doutb)
/* synthesis syn_black_box black_box_pad_pin="clka,wea[0:0],addra[2:0],dina[159:0],clkb,addrb[4:0],doutb[39:0]" */;
  input clka;
  input [0:0]wea;
  input [2:0]addra;
  input [159:0]dina;
  input clkb;
  input [4:0]addrb;
  output [39:0]doutb;
endmodule
