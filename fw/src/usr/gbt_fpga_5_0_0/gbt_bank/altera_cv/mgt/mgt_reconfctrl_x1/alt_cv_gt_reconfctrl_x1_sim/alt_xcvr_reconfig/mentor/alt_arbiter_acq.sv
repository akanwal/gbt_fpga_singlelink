// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:50 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
cwnq7LuTaAff7LsFROakhBKn/ctBqr0PV4SpThha9H7Yi+BQLOTAZMIbT5Mzuvzo
g/XqtLOo//H7gdJvMtGIWs6P/JVIeQ24XNzHNa9lH2zN1iHmcaeWuwE6IvMgREka
liHZyR+xFD3wj0FZawcsWV9f1mwhSQ1eEXMfZ5YvnFQ=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3264)
H1IuEyAyRqrRIbwQ2zvKwVvGc4Jex9ptwpXb+ZuJTmpnH7E6ZKDtiZm2TxBh++NN
/jLLGM/BEr/0ykWdRect2wG1H/X+/o8rGPPKeOILA+3gkwgd7O/LodL8HT+hAxgg
rN/U4Q0c3+BtN+Y6n2kQ7xv4YTB+P6KZ9Br8GYWeyTUU7VMPOFZsd4WtotfjwTBR
6omxm5EWH7fuDhIbuky8MrCj/A68/Xn4ZbiDthidqx1yfZr2ip59a3wgqdJyUSiF
aWHJkv1Qy4FCRx1MJgXop4boQjxnRJcWivZRsN8vl2dctsA9EFj7piiT1sLqZE5h
wPydfN7CYc892m3AZ/2F7R0HeQNKn+k3RsZwFBS1HSu/3Ix2Ms+5AELVPPOqsi8w
YK7tyIrLJbK4yFhZmv9rolNiK7hZBc0hqT012fUVSKUyaSyb1WP0Gx8pZ1iw6fXp
suKmo5dHQ+OnV4ws91jet42Kvixi/oKwb8JbV0LkO+zirADbIx9vQuqJ0v4SYN0R
UBWnSq68u6a3qJ/PcenJEvDvkMWapasrkMIa9NXNhR5zkkZzPI1ciczwuIeE/jDI
bYXnmm2gHpSyx96IK/ymHCsy+CvkbbgPiDqQcjxNoASDwvvs1BMbzdaVhoU2pKeJ
K1FCt2u2IzKkZ1NP6d+3RujAyngromN1K/dqfcjRIXB+z1ImyeSIqCVgF1omM8x2
3AAt4tqd1dXB06d75pRf0cNyebiJ435EkHty0BWAtnfuiU8MUtTr01G16FkbuHQN
/FRPfiKvRAuqfSBp+XpzOMolcXFvsl1OuDBthhU5292OPfSnJJP/gvG3XH/C+oi0
HKbJa3fCge8a9E9Et2ibuM2n1xxxUkzgcV+jHCZhMQEg2UNpfYUJWxEDwbD8f24J
upENseWS5mVROImCS7O01f5/5LrTq/rda3wPYBAaohh2lkrjNUcCrWFvt7jFW/8z
vC64EQH6thHsygjYJxyk+15ITIKpue1SH42lPqVlefGNyKDIHT1N1nJsP4rKe5IT
WghNV1rPslJkz8HFLqbKsmtPwG9pUD2h3Eu/XfsKjTRgwjeMQmG61W7EMhuhPdBF
ChoH40QYWeAXYnRR9p4GXhY7wFkt9YEwhwAJQm1bEwNXev76n9/R48opiioh37cx
ASvbiZokuitg9t2v37ei1XepB9pcb0drOgZE+T+VfS9r/lzbcvfLm99snyczvv+y
ffFhymu61qpX3VNBOnRBUx1j0jUTiVqivdYklYBIFut6t+913Tu9kn36rUf/ArGd
0AfpUsdvlCIIBATty3Ob9tQq+cdaDZPBS5ap+/1qzOJXxFUcC2sK4TLYxyuQIYnk
576pDcy/bEVr16PKHZW6iei3bbyGmcOr8z/IOBUpHRo87fZ0NGowiDEl0ao/Ufwr
COGTyD5grHMEqMA3GDWWKLCq5XmmAlfGwNQ56ofPfK4+/fe8NO0RkliuHzLWEAPz
96z8igJbQ9A6QZ190kto2kpGZZ2EBVmhiZ/Q7OrUTSwleMIDPZfy6dRj/BRWxxmv
RGm7m4qURRhwr2f5+/JtGTwfrWk4UovWz/fkTUgOr4xNYKNfrwwit7UdTlviwccd
fri+epo0O3OUhp/v7Gk34QkAlTTEKEPVQv/EHCETA1GfhjktxfWvSJBvaqGPta/B
B7qI47bIIdsE1ymj5LcOJNrKPy/luyvCJIOB0yYlocdgeCnSxIXcMErDyvrnjLjv
NKmK7wMCAdPnCy7Az0u94nvYKORtQnV5EeM47P6jbPlb8mI7RX+NVmy1cd6siJFb
o3sO42cujwCUhXmy5kbhiJ6PYjScfUHiVyFVUd2v42gIurb8x9oIewupZERBOnhf
q0CiQp3y1tY5dsG2t27CJ1TX2mBMyIm+zLaJoNut/eYTU6LkbTLRUEUq1VaXDUq2
IlVDSs3VKex+folCTvlyiZ8rvWiyXgoSV8XRhMPHLkWMrDiCNZjYthZ62vt7mi4p
C8k80v9mMTEyDzoZhfxMwvK7PW6lkEcEohAyQ4TtzcjVZaarJermkgnCyif9rrIi
NdKKGglYIWChLRBmRuiT1i/YnpkFX+KMZCMosq9KTNl1hoNCtkXKL29x6I45ucwr
X0UY/hj9c5B9mczziTvgYltsKwaMgez8T9gElJWGpx16cSnrGz/HHhTUQvBAbo+s
acsQhpQA3nsjqTyyhpL2+5nizmisIalty1rGACU9xEtTrqtTawVyRgq2GjMFazb6
twGvbL1WmbkeDlZspntqDiJXVELioBwBCh0AXmmWzstTOzVm2M2FZRWnyVn2VE8W
cqzPkdk4neiqabfywWW85FRXW/pWWi0ZZ2211/wyCEGHm4kK0f8Kad7wV5m2DUVC
0sxQBQ5gKZjut5DzwxofANpFk2xWijY0w+n2YiMzLS2goh+1g7PP8G3qBBfxK1Gi
uBWPFVQpvAGM/YT6+5QSkC1q/kRouLq8xlBW5b4FU9wPRAyqDGav0Zzt9gUV2mH+
TFlJbQsHbxUH3Li/91S0z/ybbw6D7sB1eo1ivH5SQmSJRU2XtxSRLcqgBVDVYBo6
f6vt28UKO2SWe/dBgScEo0azeJbcX10DuluI9R+thoa1gvNPRpnczOBeWHCfm11r
tfWa90YG0SBgWbiJrAdVKf2CJA7bMaxL9bbMxHOSwXwzPwC7SukFY0Nb7Dkh/0N3
Ieva6cGbZdg1NqBPICP/WimCUvawSGMlj4WULGafSah7rINK6+9ll+s42JDUxodj
U5+IZ78P4r56k9Sr4jYf0s2MkEp65DtIFey54jJg+XSuheTPNRXDprb363uckLB9
5oUqCYigHXTZ8KDvynO/X7/tw8yu7PjeU2vYKwSNVKola60y3rv0HUBVG6EVXYeS
DdJ5h9farJ4FJWsJLuhYkJBMiae36oDHD56wWXEeADXSyhfPXvI/mrkyqiicfoyP
ND9X55vCnAnCJELzwHN6YhE0yTKNiZ+E0VN0ejmlAmUpqKQlp1LNU8lTgyq9Kj3H
0GAgV3oljZ9J1zBlTPpkcaC4VEij56kWKIPTmwH9VKtAoeV9YJU/aJEtTZE/cr12
KEdJeKfxptaEA4jrW72DDiYYtaP7qvJgsGq1tGPQiPyr1z2PAmZjBQjBIY7lJn1Y
PHjnBA+6Ikl3Ytk/BA3hG8VUNcnYkm9iifM6YQ8e4Ebt3Zm0xa0SfPo6f73Q9Nd7
JbgAHLYnkaLJhvz+bgzoeUmZ8josy1KFOFm9osJ7m8yqMngToT4T5V1LSbl36lV0
adef1mWQhkVJKPdBgWvt10Y5sZhp9jTo/cY+KXWS0AS9riZAzFl9Ur0OpIMv73QF
zb+aojyN8qECtqrQd4CJHyngnVp3RwCamkoh30YZhDtw0ge/mUH3IWqv9cNyRpFZ
d+dh+MTEXMQV1RSnllJ7a9e7GcGifyKEwxvSDvmvg11oyeLhWOU4oX6E+fOClDsV
7mmmHvJxRkhb3XPq3Cj30WNL44bSR3tgIN2lz6IiDoFt5daRQk/xHi7e0Gp9G7hQ
Mm0U1Ns5SHMm7Jl1CAP+NVhx2RfB6ybI4e8yQoDxjDR2C7sQNLCJik18e3fQ4DGV
1hG44Vl3i4yylUYMCM9mS04xjTLZAQ+DuJPCG+nP+wnty8+cM21W+YGn+eO1k1v/
94ZTGwLfOHG+FihiMlO+z/d+xIhxSc05xnYcUvR2I8i3BPhZCs3L7H3z0bXw8WFs
2jR+2LKinRTBGpTsm3f944pPsK+We4MkYKeyz/LXpc1M0zqd7QddegIY6YRjvVuy
SE6uJ/k+X5wsY5dop4WzVVTYEi7MKGMyH/D64sMWp79T7aBJFaRaBJnQ1QkAmc5u
dH3sAv0p1iwJG4HiqK1K267EmQMoSy2EqjnwqdbxoY1ABbch+TaJGY/iJ1+UZ5yJ
vOIAq50I7+nGLNUX3witRzPWHUP1+7QrMZyZh6JRbSnbt56m7djcYhfmuMjY0YhE
FjEhuy41Fj/rYFIZS96DRkLVF9gVM9y54ZA2iF5jkWABodDUZFuGEtBSQ0tS/oqt
jMjvH6/Fzxs3uSeYazmZgmEQCLPi7zxa4nmRcfLOgyuJ9R1g9G1uFa9OfC7Giqfc
QTuHPX66Fur8aTpNS31dJ75TfBdbSjgT6LfX3EeBdbYTp8Z/mtND5IOktbNDX3VX
MZYEZjaz0SMdfmBg02k0qnST9bEc7v1rYWvxAGGc4mFkC7POFzbKJtNHh5urwEUb
YxFL26fjCUsNtoM0SCiLmAHw42GhL8qY8P1ak5n8mmzQ81rDyrkP27RMF3bqfF7m
RIzTww1Jbv7gwOHA49HKcJYomLKIG3UkKVhWdmvo0UCjYmxUCCH76Y/pTOPLRWP9
`pragma protect end_protected
