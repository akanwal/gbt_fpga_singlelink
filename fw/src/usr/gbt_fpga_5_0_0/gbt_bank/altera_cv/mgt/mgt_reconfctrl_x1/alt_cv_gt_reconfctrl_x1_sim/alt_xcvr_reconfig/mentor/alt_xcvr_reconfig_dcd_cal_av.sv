// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:52 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
lPjFVJzqqhv/qcswCsYYNADmBR9OrMXyTgE+L+KgqyYcvgRfMK8kHYsZrMKlXJYr
WZ461+mBNOkY12LhCOpBiWPh5jOe7FybQO9b2OBShReer9IsN26i7YSlfYJtG6VN
iA0OWtMx6T7eowFYb2EjZkVz3tE727z0pEjGbmHaCiI=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3600)
CNXW0Eh/DW2edLPa61MGq+GM6c+XyxFcOrkW1PsiP6O/RsBJYYyHoA3YBjIlS2gx
XL+pXEfR2WkP/CUjDXb9X2ePP0zP2U0ynPIS4R3ogbRNp48Gq33EgpL9e+0OVYV7
5D7qpOrxK9K3Nu/MBCXjTKUIK2wz23NkqiDFrIzQ/bxO1iEiCBStWK5Oj+KF1Wsc
lKLlJbrUojkLf1GmyCmaBB9Ej9ehVe5vt+nNhmvYBMwxfds4Q2eJ1Iw0/ieywxhk
KGZhrOX7EDn3StT24uBZ01eqtbE2ECb/uL4iAhmeUjapbscXSUpdZAgoVblF0pHf
wZU5OhJn2DsNDFD1uTfdRObMuQ0FR43YmLcQoqq23AqCNPX8CxdxdEMIHdK0s6El
zxxUpp5Xng0MxHgY6EyPJK9UmWFYx33V3W2YBbyGxiWZWYeFUsFXpObYR7nedQJp
F38ykQyWUdhgjYhNU2DnCl38Ku6wcm+FGLOUK4ol8hOxuENRGRVZOVth2hih9odc
VUX94dvOCuMLSSMtrMeeX5rfu53lxmjgys2WrU4liUoPJ3pENyMduI75sUQae3eC
Rw1phPgvAGNXBhADwcqvwz7O5rROXoP265Giw4EozIXskofbJ17af3UYDmW2zNqN
Ctv5IyZafYllkjKBSZimQvBQeMLSJkAvGAJFgihWw+sLm+oR/B7O2pxDPG70/1fB
VbNl/tebdcLMVvAh/e5S2E5RZZCzNE7vkFmUkuNnHftjupex1F1TMTFODZB4ZA9S
L6NSyRrJLuS4g1pjHZRwh7+CScXGuiVtgx23bWhRATDujvOTiUzQwPycb9CInjic
yNkEBrP6brbU/yko3xVVRi31OmcJ2rcft4vpIiDc+CtAG/ZrNkahTrqFnuW77O1g
LO3VrGsrlZjUHIlhaiFpUND2DEyFNgnLUwAPS6uI11PWjDPCCCwGOI71yx4uXFC1
ATQDdvzsp6nWGiNezJ+8u6hVvSQJn/naV2CPBsPQZnrsVmghXRUL9Ji+JrWJAdnk
7FNtKiPi++ejQCpiCWR934U8QheqkpHZ/6/VeEbyxW7wT3oaiM+tPbdF3FAyIz/B
X/STtuOU2tjnv/OatiLU1Tk3jlXylVYhI95vamKB8wSWFcm/HoBC823mT9jyqD/X
s/KcZ6pSey3FxmZ17bQkjSUItAjj/H5xRksuPizpd9tSjPp5JYKqU21+x3m6DyrZ
xuP6SbQ+xZaVSXZD2+RwLRoxvtIoMAAiFf9Wn37mrLfeg6qNUNoBih49TJLaak1p
OY2CFAgMezezp+tq2pdScBfRn0/2S3Z8jvyyqa8Fwbgy2ezxwdZ5MOeRdvNEMBJA
CRQRB1yxQ547cqZh7RYKZoz3uCmDACDX/1bsFrwnu+A2moWH6/m0GG2xig/CGnAZ
1kUOK1fZhVHY2padRUtSWwx5q8Baxb4mqsFljUkFH4m/FWA1Tlr9575EXYDsuZSH
3hPzHiElEa7u2AkX6wykFvIy9CZ2SZmXSvAL45iS0jo/qFUumUjGDxZPsgz3EhDo
OuEDS91+Ra9hg8oNcZSONAR1byZYiUWIYzJJXksMnJEmV/ifie/l/WM9+2hUYqMl
wStL+LoDlDEax64oxoewa1i2oenNFsmWRjtjhXG8PIYz3ZqWvjSxFPFwXaI/lDNP
uZ0gTChhw7sKZ4L4uztQE38eK3svuDbitHrRkcbIk6K86x2bTro4zk9r2YsbrqXc
nzS+Pdw8jrp+WC9CV8dnooGR3S0KijZY6EpaUgYVenJzJgtooGuraNKOXaBnflhN
KJdcnefrRXwxx2BbNUHf5pxxjkapAMI6aKpKoHUL9IMRf/CAxpuJ3WBpSQmAjysl
yp+MwfJ6pkm9p7YMfOuZ4L5KB4Ie8LIGwyrsnWQ94uFbez5ZeZHaqno9r4VSOLTB
UYneitpXVKcMozoDWElCCWy6pZrtjqEi7Xz8E54OnnjI94LbKVuPjuj+329vdmsl
ggPstySBMx3Jep6UUtiBKIgWGHbIm4fbu0Z5AyvU4D6bgMJ5wQYA8S6P5b6wfXvV
LYkYwi6CI8jyMBLClB8FF5TwNStQT5lgkn5mH3KVKJvLHtNxy3M6oZkOOPcl/+6G
3lUANGIZs4V5Inw+DZxgia6fjJl/XSthwLsXJywi5rM5AR/rzVUEC246M4WZbpOy
Z4F0USQxjjsqBdbH5ys8vMT47OHBnzci4+oeDCkzYZerI7jaR+Tz4Yy6vRyDio3k
6VF3KWMfBpcqjTdsXKTaGuBN1r6x5JLpBllpxZ9wa6toetvqO0vYbcrYe6wKa2Xx
483ABZ6g+4J+i1kRg2fr+mSLioUgSDeDMvowC76zRNmra07n6SipvOxbO2jdCvFv
RHiEKyz6HtO+RltcHtZtTDryUD0xtLNPTQGs8ctvNF8aeaRxEzhaHGAqhWc2zmuU
WnZtyDRMvhJmhdlavEF7Tuko6PvLJRbgTsNdDL7YX5nzJDLljBJ82F3ixQ5LKC0C
xwhNuO2ysdEokT+7dij7KeBe+lJyzX9o9DAAd/q3zRE4jx5T7pkUULoP7/FO4en7
rZZdWHAJCUyNzGoZFPJltcyH01ybzyIfvL++jLeuyKcwLFHHc30Dd2ihsMW2H6zp
f3+c9hEaehw3JFMZwNMOqvXzcNyaUv6/ZokvfS/jjiC0oROqCDoAgmXDuzviMscD
c1mGni7lnAhbDwHmGjEBGtuZgpHYzvrm7TOm6v3s6ppx05d9nZZfwdM8fAY934g9
6DSQn6Zprkdd7bV2fFicYnO5Ckgw2kEdSTqozlfJCdUOpzCDG8fmH67BGCr2JVlf
sAwhqiUTmPSnkx+wJSbEAqB14mB2iqT9nTJ8hlni/ll/aOFleoKTvH1wBWU0OR6X
Nond4ROXwU0OfODWjV/0GluTr8C3u1gSfMCJJlw5JugGsxZ8RpmFhyKziTsVcmaX
4nqcjQpIiuPgTeLQNt7QCZnnJCBffb1EXvBPf5Et09FL72s3DNl6riHxbZEuEKfW
l7/k5TlAajJQqxVBDWZwZgLUtP/qS00oei1zoiNBNW/UKDxP9x9Kfg2PZ3AJrnj4
bzu/fF8tA5to5gL2XvHSpmz044cQDDkqdQDcWyjlZb+CxbvQW8q5he0eTiMVw5pb
xRCx8S0UfyUCPw4jrJto64pXXp58GXd+uzeB1c4RTBJ1ewDjVRHhpnQQIpnRnVP3
P+Ve64hoxpmdiAZv/43IogOh0f6sncWUE4f7l3rKt0nBx4/R58GcvYJvAjxVvfVO
TwE6Cp0O5c0HDCsOuXNbMKl35Z7Rq6h2uHVR+LOmAdULpAlFJZTehn2JIJKiQZup
XfqkCU/ZFQagAfYKoF87hRH/Gm7KAq2Wx5X5i7p//ZuJp735WXM+GCNw5PyeG5J/
ur2sfVmnj3iBzoOkPcCKm7sywEedZUaFHGPecY3IMzP7K8A57FyNNv21A9AYUibQ
xtVAaQBipupxE4kGzB3v2Uh9UZemPxTrt7gaRUi7CuQ8lkEejrkPtW/twCVzxXOF
WoXUePQiDr9hLid9e3Ip4fDIsPsI0fyzS4M5xX0nJMP3i6fmD1RpMH2GWl0pKhaW
QvOMuPsvx5GkX3YJQI0NtWVLeHfMUcbfXAWwKlDBPxT2FYblEhDqTc99AmSZdq45
CTCSob+0FqnHGKZJ7n8vTn+Bsi2NbDvQpWFhuhbwTNGB1o4MAY6vPucEIxf+eX7z
Uc4TzcCrUNKIg0NOUow5mwOvwXsTefsnWJYtHBmsfhFqv9hw2lT0SrRdzeKrZGAr
p7E/T0AwLxs/McjYMvCKmjBTbbxBClIJTuETKRpbB4fjDRx47Ja30ik47SD1e6Tl
95CV9bhu0FLz8GvepLpkBEyn6ysVcO+1kaRRrh1XgHBvI0KowqMQdk8qMoUsWuZj
elydPYe1MkkwYD28QXSBhymWuAhZNCpNOeUGKptrniQPEHAswlcgYmanq+0rG3sO
9oiIPkxDBaWrJfft+xUiwjggALcQ/8UJrdfLfwv3VzV/K2H0ZkehIirJxF1Vgrgc
KltlV8LmyV78ZdcZmutrZJ6z9zC4dhr4KRX42UYhLuA0jRhDxrw43vvFhnsW7J0D
mJ2iqw7v2nlaIZiwGB8PaiFBW/68YQVBMUUy4M6GNKQv5wo/2kvseCipBIs8wZ8V
x+ie2JJCJysAV9vc5r9GmfYsJUVo93nSFTBTU3Sz74oDqwxXxqljiGEB2Ad22+pL
Me7h7i6C7w5CBoQIXDWL8RoCYus0wFMdxr9H70HUbMZiLpqgoxidak3hWwOxx/PA
ZyHmH55c944uSpq4kitVLWX9oZ81oknwaJfIvxivC1NdT3S6mvonFotT60SC7CLy
cSoIJUuEJK0/rJ0U2+AhIA05oA+JpCcHuVU3/Ao7RFomnRVzQwezVSDVKWroxAnr
hoZSwgqEWqeRJIHN7rCvuwjwA1Vj3iDyFKbBsB4PW8VFiQ41bh9KacIlDpnm524q
vdCZJ1p3YaKC7bHYsysTbuNQdIVlgotyMmUsPqsWX2/nj6hS/qnvOCuv8UqsYGIS
sg5gYBEBps8smI32WI6BKu/675jzMnZax58bTLhSgdRUDabsBhHq+mZtfBzO7LQv
vEf2tu8VlBEky3R53/FjrT6tbXT0J95tfnfg27F/S2NlHJ13LN8ubRCX2rOBaDrT
s+gT+hu1wMm9tmfnkBSkQ+Fx0vBVYfZxtbX53TK/iRS74buOuqkDdf4RsstmZlYG
dHRQEJsnzoUXaROESMNJzU0gOlVwcB4VpvEzMkEuaF2L7aNuMVWgCt2EoRTlmEAg
`pragma protect end_protected
