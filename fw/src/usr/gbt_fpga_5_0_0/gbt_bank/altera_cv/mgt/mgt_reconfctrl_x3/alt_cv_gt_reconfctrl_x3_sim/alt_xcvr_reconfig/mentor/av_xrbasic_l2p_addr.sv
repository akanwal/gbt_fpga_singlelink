// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:50 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
em76sfhjMiSUHlM3Wn3lFJPyfZ0AVsWSzkA24yQOw/kTVUoHOPYzgP+DGsFflwrb
pHZxpQLbt0qifYU7PWFgl3+Wax7mvAqbN53b9msZ/3PqXZvbGxWjf61we1GREUOI
0CKzDn5X/UJer98bcChSShxu1ZPw0z+6gcWyCDaFEiY=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2224)
dzI2J8Ctgw7+iga2/EGd6fUKLVkRj2Zie6FWetKWYb2vTQInMJM52lFznDtAL2u/
Q5+Vy1M1Njl21iY/kgMUjRg/Tp2HTSkn2hPgP9FCAqOaGa04BEqQvCrwlNtuMxpu
UEDeVETQfZeFSErGZ9apCFJlrfYUD6w87uXFbfQJB3J+rn1CxYFyCmQnTa+PmRhx
sgPgQhvn8iArF3DvBYSIovza/21KbfQrrf3qBZcH2tCR1vRIu+TS59OJriGqV6pj
gihuix3JgNgkMiUiToOnKpgIns7lUwZ7htR/h5RPNVfGqrn0aynlZX/mdWLl6WkV
7IGYlfNL1U8AMkvzRyf2IKrfiUvXtW1fKspUqLPJnUKzGr6IXXE4oXsAz2lROiIm
o8pyaVyKF1LwTu9bPNaotW4/0kmD8SjUk1Rwr/99YkRtKKOAnD4l/hgIdXj7EqJn
LbdupQlr5edgAZDmqJk/7qHq1k1fwN9f25E+3eVYCJoHsh0CIx7eJW4qaoEgdfM9
Ihhj+fcWbT1Od3qCrtla37uqIwqH2yOuJq/JbjyDY4nMg7vcxYYCv7Qj5e+ADWiI
qsHbf5e1vpOrTx3EZ1QIr95PLGOWbA4fupw/7yPLpSP1aBsBXHC/uSuvK4T6nxf0
1wunRxeDkifMr8fAjn5UkEbGl6bkWo2UXC3WKYwPbdymCZuw3SOSzGdOGkoct5s6
dlcNvLOtPVDsJF2YWEVKVHmCNDpjvFvlVkAF1jFDLalGOm+dOrS/F8vWbzojB9LM
6XNRNA9vdW+T12ZIOcDMomoKJcPhNG7GOKUoW/GsSW47Sphg1XwbQ34Y3f+sD9zp
7UCpFDDqoB+/UzLNzOKT7m/ZY2+G+ZVFPOegxjiN4ZRGUOUxkkMA9CoSnhDPYEp3
wMM8/bgaKfuHnnvDKSA0fgbATGfyXevB1NOTr9ovXmU02koXNi3h0rhUKrL6XznM
L0oShrIT9dBzCXg2GYwQm9pFFfb3nopvqw1j1oRQihCtvNtqw2jNpjBdMlv4U8R1
uFiqtoQ+SXGFlD09LqDnZzxzjWNigNshuJASsWhyBxu46aFaKOkGp7NhUt9ChqYa
D65C48ALr/snPkvEFwr5M1+x3Py7eVm2dv8OFTY26JGP98J/G3EmYOT3TUfC/ElS
8XxrzCZp2/hccarRT/Af4FI+MmzV0NFuAqAQQAZan5m5/AJdtj2YBbVj2paGB8my
cGFeshtGYiT0pPlV6qk4nDdB/WGz++PklkQVGgtWkgtqKUdRzlhyMAk7Gr4OIux4
sE7FEXeH5LclFEUWNB1gdpGueBSzadvxxHVVYnuYswhLBFg3ln1PNgc+Wfo9TwH9
TpUrMwymhMlqw/tMlCejxlkCm+46CK4EOBtKPjyPbvzzB4jHeZP7fXdUEhKsEBEf
oyCITXsMzn/YQ0ANIsRC1aOM+/P4E0DBKr0t1PwUDev4eMzkySS8HJyUiHQt1VdV
klaN0VrHhSUReLajLpqJfXBKM8IJ7Sv8pXfq4rTS8FNb3NBFgNL4RYdCoP94Frzi
qUzMGeq94BRAWji5Bckgy3v30ifcjrOq3/Ic5e4BGwZwg82L1KPRhtpMiCZ4uGX2
1Yart2djzOH1v8rUGURs9cJ0shFCcEXHgnsoJ4pDEIHmji4j5YglAYjjz/YRDRyb
DxfwVspAuIhwKqytKni2cz2LyfyInE73Srh4kTz7i766fbNvh3veTTweevwlBH9C
U3eiEzUEkKuRLI//SwbBMCkq2oM7t79BXNmmX75UrfXBgZ5J/ZFQtjqRcMq/JFrR
ww2y4Z5B/tfSHf73BUbBQqce2Y0hQCf43kHj1PVMfWC7ldBXrvZj/avQuL70QxtF
Gj2r5Yz/GN4BdRxAfdvFQXUVqSZGiR9D+VHFID7itrk4My5FASvrVPnPWZelPAYq
9rAb3jAFpCSXxav0v9bN+2qX5T4vbwpIbIOASaHCwedjnacINsooo1pCbX3J3dYh
HfMNxwzoGO8o27baGDBPjAdnUg6v6cVo+WoH+496WTtrxB/w7J1v+kul3dKYX+rB
lDd3v1yLYTjtw6oAU7Iv2xyOq76a73xjzeLnmsptin/HW1vP11THdwXQ8NUL0nDf
MXAT9Y6Jo6U0H/i7q7a6bLsCju3NmG7Pgg/fBQdOr1ZGBKx5u9kanmBYO7OtFtjb
koL33Sny1nNvkJjPOJNutW7g6eplo+Ik2Fft/zoE9n2dGL+cxz+ZcCeAX1KupmUo
yC+DBOiOYoT7kUyFhXX8GMAthc5mcbx4dcGc05OCkk2/phA4gl3bckVdBMc07pEl
NMevtaCJqxtEGkG1oruwEZZ7vpMRlFRB3Il8XoEc64W3Z3yPL81AiD/oPpt6AsjN
uu6+iImVbG9bwvgOCdQgHEf925JoL80ezqw0CZwuFe7T3o4+MnVunkElGhzJvIZH
++TAZ7uA++mJF2k9RgCH1gYXcerdYGZIra7cTsDxL+XlKYLzMLHY5WhiEPdCfwzp
GT6QsbTtNY6ch93xpDQzW/r/8OU3BQMRqV0fJJIO1mMV9DBEtctkksZHhhY5ylu6
D3VudEASEdeoOFnSWJaT1bLzpVcXbcPtlLR9epmobiH0gFxd/ezqj2+FLbUiLkIc
Z9IDX8uRGZZ7f8bYr7h73keGWTBAtCr6Dm4EoCwwiG0SZCDsqHIe0/A5SzV0eMQr
CgUgOLtLSCls1Bcj4gv4/nK/2lbuvpQFNWoqbcqUpZHkCp9AkjWzlRvMKuVeD8ZO
UEoh49jgbtqcKgvSS9ATg0lt3nyU3g1hdGBBwBONgT76jIwcvRx8/g+HElcQMe8N
eh/YUoHymKwMy2Ob5ihge3gr6POmsQNEiiwBWUW+lTdQobt0/BjI+6EpOx6ffbGM
1A2lUg+txGCfox/txoTLaqFVrDZWCohwnU/OlxL8F5cdFN5ZpCBGgj2/scZ8RxzB
uF9c3DE9ci+4zgpPTS86cA==
`pragma protect end_protected
