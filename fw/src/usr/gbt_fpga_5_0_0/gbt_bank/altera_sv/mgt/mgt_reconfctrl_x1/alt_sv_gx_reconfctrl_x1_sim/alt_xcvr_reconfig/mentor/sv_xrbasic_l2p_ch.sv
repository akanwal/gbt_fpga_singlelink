// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:51 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
toxNZ9zcy4wgDPPKS6revWUmcX4HkS+RqyITThDYgpBu9lzdBc4mk7pn7IXCi0Kb
qqxQys91X4P3TS74pH5VYWjOGpDDY+6fjQu8lRUd4gP9Ee0JryzCD8vyno383TEZ
s+szYdiNGuHgnRt4jaII4uIiL2V2SGuQGKF+Jd8Nogc=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5088)
ALkdfd0piC+FOImtmAG9XDvvaAiMYNFk2gSYAGehedeVj9trj2KC7MBPZExTYhY6
hM+ALD5Hn4qktaUMM5Q7m8dzdqSxcrlypsEuKJYUhOlgHbR1I886EOCd6KFL3QWn
x+YKv9XkFGJT/7W8bYCSOxvgFdAB4o6rUOHQ91vNZ90XbVnIJklDGCDHmEIINcZP
HVqW17PV19UZRcKwk6rDLseiZsswo23KfCPZvWdA9d0p+KZfTLhxlOkkykes+bqx
3ETQF9/yWiMc514SP+o9Zja5VyW4O3T54Wb3Knndcf2FL6ykmZt6axRPdPBfN/y9
tE1kdP/ABwwdncvCzg6zqHZGaxOLlt6CbtkXC9M3CkdNK37uYIO38MeRaitKvCrC
9zI+/7xfzg2ZixFBquGodfCu0P4kwycuxhAZJpF7b3tA9XZefGH6hO51ex5dzjvO
5KZ0WcZmNQJMaBtwf5FFgXvxUwK7vG3iRyAC84IZljWUjCXvl4rMFeVXjgCo3tdj
vlvTSp4JMloQwhp/mNML8eQC6FoHnwrYgEEu3JllFlZQJ9b/lNaLGec2v1SqEkvL
rV01recYh0aS2j0OEL/hMhHjtfN0MgO6Tz7eKoHPByItmUDJB7f6IBWFlL1HAmQc
Uqe45aX7YS37lHbWutLaxAgVNHnjx6P+IKMZT2Q2LaP5pyRfEslTtU91nE0BL1sc
ex+DCWUrcDXUvr8AR4PfnmDnHV0DCuMAmyn1lWkKZsaajTCn+GIFQo+a/fJm+kdd
PzManhijPsAgqN64iINx7/ZTZ5sKSZG0BtZiEHMhXYpT4vCz6Ci0Tue0yxp3DySg
RXCl5d78GA5D7rgmsTDK+Z4CWO0aatqsO3jFfBHo5Yjt0LiSD9HIQQGF0d4iftag
mrrDm2tTNBOaQJNJ5eakZO8IFRwFE6RqRokNL9snYMwQJQf5Zf6bAAP0GNIP56sE
zbk0YcIJtjwLenuAbvhS0m4b+VHKXi3D3zZyiRTp76iO2yqcf2y2H/mriMSgBbtU
K1x8oEOvpaj6QgewOw7CIixD7xji1ZevaOB1rRWIhxvq4mum2WgrPF3vS/AFyWsX
JosC8XQAcesQL8s+Pqthmha8Dy3+rHSQLtF/dd/FD6WwsGgJHWf46MGPtoO+QY1m
j7X5bz9k0yuDZqKVPNK5LnYvEB5vHFRxw+0UeISvj2ExeNQnn2Gjp5xmLNsYV53S
6ixYi4vVfBxEjvcr9FexF6iP3HLFbzEW+rAmq9UbY1RBgFtPW1osbZfboKXkbOsd
OaYamKzy+1lQC9Uo7tXRfPuoe3pCx0OnPNVJ9VAG6XHLX4qqlTVoHhOFvgfKIS57
iQvxDqPL8UwW6jLFN/rb26yoRZaX5P1qz49dx5HN7/QUcv6H4pk7/xiw9nlZ5YJY
gI7OU/cMxK+aoOEh6b9a48tcADa6vFkJjtn/FOhUZEMVtSK92QSzvussvgJewlcH
6Iafgb63cPjrzNT0LnKV7vv9kjmjOxXYJ6AtQ1G1Z2IEQovHXvBbaKCe1gMNrouk
q+GWGhpyLfbKcuca8wbigbdYezIH/tT0rw0vI0cWnVruPcwJzgVMUuHPDMoMOjJG
TJI2cvdcqcnkHdoNDIcqr1k3yZYg83gzYdFqpYESNH3kJYZj7zQ7qdfZzxUFdWYI
A6JEjrYA0hgGzsj2J0JHURkcfX0MjK65QXw8voKLwv06hEgjbbfpkTvzx1eO5B4y
axB9USmS//MrIsuAPnu8hIfq0wPXTKsl7bK3vHFu4oGVytoIVdox0qJRhKPlQGrO
So40TFA+kB1UzL4TbtOaWDpdafB1CWs6gMPt5DNghroUTOAu1TzqQthmL7ijgq8s
RaPGq3YrNysaLtYvBmwrgBgIi77vqmiQTLDuONGTcOsDyhQiMkzbuEKdxrL0ox+D
yL4jFhRHhDaeo20nuSglBZvYVttJkYF3NfKjd3qC/j7SLLo6OHUUX1dkdxbFW1j3
L+hy4PY2nPOFa8CWaoohc5cQLvhIMOKpBls24hX/yJ2EI5ScAfZI6Hty3eHCDdkB
zjm16JpuJZZHuJ1k5W6nRAsdc0WPXm2heqPKR6dGL/U59yijy6L/eWidhbwO1dnz
GQ6ot4QNRHCLzvtEpDFSd9tbbx866h4sZozV3J7+M+BUKvaBf0flH2csQizmUgC2
hRGTaA60bWpXwJwFOcygW03OrgBtFL402BNv/DUGvSsNMGsVDsXdIT9Bh8O8w6Ia
oFI/Ut0lnXmUghlXuJ2Jx+dJ7BXRBvVc8QTnsB3MNNqmkB+WeF/1yLgtDeYyuZDY
eMhLu7Uh8/8Nkyj0Ath2CaiBQZ7Nl7+drzDXQylK7EYa7HIaoPaRfO8AtyhMAlci
TUX6tbGDE8KIhhHhtXVw9qr0W8tMtD9RTHIASKt9Hr7+lDzbxzZMNpMZyUv+KCkA
6a+cIS3zAOoJMTl1bW9/f0NztMKh4R8t5fW1aMBgZUTuecUZtOwxQ7dLhNDxrv2h
QqosBBaVzCgpOX9bC5UDuQ/hWnTYlz9vvYkklI/QDzrXQueEuPDNUqGDlrv4Q+Cu
EK2zzRJaVOwofWvHLGZ4PgmbTDOqoEM51PFGsB/zKxU6JPn9k1cCbMPy97csPqVM
4s7fT6NzFcgdyomJlsCQw/VAjY/N2Brh5KIUo2vTjlYapLvmmB/jNFLSrpcSY84h
/tCHmQAPZNx9IGXL08rYQQMNqZak5fHv5h4mgZY62FCtGKeRoX16t5aOTakmhKec
lBWL/63lIbkMYCEjmAAcmuOqaiZvlyjSyeq93+xesqtKeLoH1t4ILuX3K+/YvpbV
flpiR1C26pqk7WWDQTuKS4Bl78wkn2FpXl5/VNnOddvwL4Ilvbt++0Wn/N8Fc0nh
7vSQYVfFFmaPKBrpg3jJGBmN3ZuoXYxMqtfOOfy11mxhX5nL5JeYRljHQI8ZjfIC
a8GFrRscPyBlThu/nPuQz0QghN1Yno1eUnnOfsjoiXblADs/b15RAQz6iZucu69e
AofPKG0f1aTSPUnwuTQiSIbhVu48E8e6cekwNZF5TFfwNl/QZ3HI+o/DiPSRpsUf
il14HJXZXmIvD/pcuKg12YrNDB866VB6r4ZltEAXfkgqQ+/oaLdwVyKg7X1dzU8d
9+XzB82V4OxbT+U0m0UNjAl6PhTslbWqPFjGgh7AoxTKfauxF0AJQJK3d2u+cWhb
40/jayDznTaZHel+Q28uT0WO3PyGEvNE5EC7XMdiFLbUU0avqX59fbqv/SPIaD+f
Wdpq5SsWGqIzSrXQ06ClH8XszhdLd82B8dL5N3tYiZrwnuvEdCLlxaa998/sXfXC
gHIt7ZErHniqmlZMKlLOoUAVu8L9JlZyfwB48ZoZvsw/pSnTQMY2I9eUfF8Ynz9/
TVXbL1ktNtIMXOqKcf64QCL/bgHsgvgcHjCrYpW4REqRX2MuO8/yc+fIGRnKjFYg
c3SnmwBdQGr1p0hK21JDGtnxaiw6DYGGKyFEtfCEKtPjb/RRlSN76eCigRtMTlmq
e/6huapeUFI3Lr5dHl/v/7i0CeHdGWYhpERXGZLpHRyDPf3Z/SyLwGHxxlFzCK54
GW88YjaxXsanO+zaMHWkBPsSZSFy/WnYQQOWJ62DTIYpDtzwESGSJYIM9KNiDyIu
UXJmxHP+UF7xXAQFDBoSxGOctG7MDeDdwXgxArCzbci9G0Tf+ieQOYLr1FKrsh3o
ew0yFu83mvy0pPdgaGotPFYi8whPhdd9mBsdOcl/au6Qf8DxeaO3yACx05Ch/08+
0HEQH3NX5uQGL1wc5B2s45yHfzPd/uXGTzkffVYqPsiS8By06r6HvgYsJ9kmsHUZ
JYVSoJWCUJCP0jhJEAaP+vCKQuxkbvkllSIupkngdJbd5BLdBPcRGXBzq+ctz+m8
KdSSHxHxpLtVKj25lnaoO9HEfmBKoc2lJH3wvwTYO/OV3jnlUDBK99PhT/nV4T8p
fTpO5q1R5G8mUj7XVZscJT8VLJPkWeWFaHMJARv3eWneuGFoiysuJOiNyZmKNYQY
02XvdBIr0X5OZBITHsJBcF1/+e1I7PSshkQlJ/X8Xxwlc/1K4dcLhowaVQNnPW7t
NEHSOdqUSXufTt5zf2Qd6baifqcxP2q2nfUHHwaFQiSO5sJ7XpqVFFTzqgxPYs3E
X0qb9o6h5Sru6/6oGqYoPzN4tkXTB+92njg/PO0p+L2OB3F7jO2hlea/97XbZljh
DW/NCYwZO98FZvILvmVW8ZWE+tSRMtCUPAescHSyk86HAsJxHDTIGkflJL/Fn0jT
pPlSvaBSlWTrS9VhdBN3Ldt44bAT+sO/jhcKjifyn8i9j6LZLXk7+crOttI8qBb8
46j6TBIFhkN+HruU8PYeYuespFRyFP8YByPT12KuB1kAODW4UoU0QMK7h83fBsGN
dVDjN9YTDRQoW5RyjHXH19kdmUnifHZNW7PA7P/FewHXSkc+xi3lx94SSmxkGiZP
IIVSBwZx8GFEZR1V5Vdi7kK6RIo5MWGQoVTiXHDRsI/uPaIbYQByyR1OnX+u2tve
ziTRHyLGxG/JTViSRYcSM4abMWUyz7/qydxgvXBtGQ682ypZFLn0mbUWtS3jTi+T
vt7jUYA63cSx9oJDSWDe1G1yRTx90UPcZ2rwAYASYkidtB/zaK35dWI11pR25DQ5
yXd2Yd5HfL3o0vD3e5h78I0dB/MCy3QQpXh7NGX/onRRgyww22g9Rnn1C0hNxKnG
ochgXLaZpe8Tu/pZaMdohYTL3dmOBQ1gkC4QNCosHOIRw/RuOwb1Tcx+jcFlh9nl
/UShYpBnUKMhMuDE2YswT+vzZyM+0dDWWqHHXYl3dKddrBhE7ypx4Bp0GuftYdWT
r9WyWd2Zbf1hBkxcEPFY4wqqbRP7lYZj23giNAoJw5DttW2AawZTzTXqnm+gQ34p
74jsG7G0L9hgVW0DtwdJTLXeoSi15LfJVSlQKVpguCf9U/n1AUB+TvsZibQr4bQ8
A80opXPgzw2IGeq+Q/E+QpfF9aG5yHOh49+YKPbH5ThsSmj8aA9dK54NDmE2+28r
X+shKpEX+s7f8VOz98NpFupQiufLlIoFcY91vJ3lvNXf+7HaDy3isJ2nfEjWsXQZ
zINthRbImZDjucW+R7vcXLFr6lBdm9ju2QtepN/7kWEMSyv5F29qjxcDA0VY/trb
x0hpgWc2MGcF7MUjXuOEXVtoQXu4+S4gLD7lWKj/oXEpalRKYTv1L8huHdakZaK8
2P/hkrAkRcTCFmHo+NKxsBcVt7Q5FVsk+MfF/tn9hI9xTIlaJApBopEaspn6Xk3G
zzGh8h7SXyGh8mdJpO+KPm09m/+poDrwPYfryz92VChQPtaGdSPCGIpecosckIkX
9SByz0J+5wukG8ypDLqLsAvVdpFqLCCKKtByAvVkxM47gAm0jm4tELQ4wgXJ7IkH
KSpXBTTmqrF8/RoYqTSTMbMxRVTUoa4/1Ot4cl6VgJYtKm0GNFlD78blAyUNU7Q0
Q5oSXov2r4VyEcG4A+1GRdwZalF/LU7imIgNUZifdkJjuIJHXZFQ70P1ptv1EmqF
14z213qY8Ajk8WDnNyTbJQQv7QEYb6XQp3lpv+25CcyFLFYuFe7GnSFqjOgQx+zu
3xV2vNZrvQW/HMb/Y7BPo2FttMZotesLo4SHFgsk30A/qpWxAMT252ncFcYIbdf6
SW32Wekwf6zl0tdxQz4c4/I0yylNDkv3+xNidEAUDErE69m17p46TJyROuNbXZVp
dQW9UpXPlGC8U8W3VR2PijpN/dEDO0q+dAP5JzBrgP/hp58xk8UA0At8W6pX7Ljm
zNVV1qyUWsmqAI5BEYvjWnUve0f1lG6lX6Gn7hfUbKrS0+xhGLmUo1nnu2l+/bOn
ZWcmwmDgGgIO8xKS349+Ot8JC+snvKOYin5/kX9o48Ys5M9OkO85sCs0tphjpdCt
BpalhahkP9SstWzf775cEEsqNJH2a2mVgkei8PhX7DU6ilzoufp3pJOkRlkZ9YPy
D8J3MN0i4DwH41XJafsIh2LdE0/6sE3KY13RHUX3rWR/s6AcS3aqE9wz6zvYgZrn
IoQLhxGahF1vRajJKuul1uTq0pwVZnoCBqXXGFphdl6m8oav6AyI4hGkfjXAPo2u
UkRh+m1Faue7sYpTeRaSnh/emB62hBVNdY6uiNvNIvCTJnMnZjLIwiQl9t1nnUXq
gnDz/n8AI8l4TDVu28JZBnn/iEMrl3w2SSpM2G0XvkgDCYQFAv2WNqNqnsGheLwo
HzV2IQ7mDT+tVHge+MqAYg/tzQmcAYmbcL5tAU81J042L6nefluO6YNtjNeAmsvs
EAkKxYfxchUe0GJWQE/iwJ9CitT0MLSgtCEYlUVSC7lShveRM8kOt9ex20MhllW+
sMFL168Pc1sOOVv7zBNR3VFlVFHZJq+U10a/XQcCveGLqS7dI2rN9W/+tgQetl77
fq6+So8pwZ3K/0k6CsNhjtDf7JmmYZNMT/AbLM9fP44UVLqbeahOdgF2I26gSju/
HhyrmPdDizRu2eH2Wnuj7/6FcTI8kA6Pp5RjuJ7XnJD4aMhSLWbUAU1X8KC+UTeW
JKkbJ3D3NSpOLE/AIoXzK8j0nhaT+NoJc5AxuZGixSG92/erPr9tWG3MdC56hnJ0
RGlo7LIV/qDgsZWyq12zNpV3hPE+/BvtVevuHGaSf/VQsHqhPniy0VfPBZaGm5mw
z7Yjmwn4LiKxrIpAfZozOZZo3oxGVIgDQJl9oq6vhQ8Vq35jIPJL0McJXDvHd519
`pragma protect end_protected
