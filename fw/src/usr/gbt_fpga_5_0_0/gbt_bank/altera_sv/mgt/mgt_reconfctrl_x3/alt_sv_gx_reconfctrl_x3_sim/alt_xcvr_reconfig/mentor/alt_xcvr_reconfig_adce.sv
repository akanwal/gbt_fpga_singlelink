// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:48 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
UgXIAEyP+jVgBMUHHZUq4X8JdwDbJrScdaXCD+G4A4j2BnI5uumRl+N8VqCSsLzu
rvQGAUG6ILedGbMzBwyLvKAYZi92G2N9T67k4S6CFRFBxIB3er6tw8Bj8DkM3j3L
UW5UTCNV5li7uLF/RJT3HBjZelrxtjqd2gDBhh08BhU=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 4416)
iZfeboHL4D21XbE5yaDzEA151sDbvLpdCtHjsZpdZF2KrTPMw7iqWXVsT2MmOtVE
QuhOhuvHjGNVHKdeiReJZxi4zyPw7DE7PE0oEKR2Cgx1alP/m/U7cPU879wjdrIu
uXFLnWWjrESKLiPosGjGI9/Dqalebla5ReW0UvkURo0yYs8W2lokdftkDVPeqxrR
o9O36bk5NmGI5lTOBx8vrX5z9t/dKVfBu52mwl+UnLUwYD/7jQFnaUjcmA1S84iP
e77YoVK9bog5M+rymgExpTF6Xyt96aepVOPyKuzjEf8FeBk7PSoE1bqQfO6jFOHD
qopEh4pAKtNzKAAuzNHoW38riYTTQDTqEM77ft71KgO8Fekffmk/wSexw7Fuj+3B
xxcBEywIN56AplBAtglROkXTwCDxanKRuIPnt5n9GAYkRXJ36ECWFio9fLQBq0os
SWMyGmdpzTSntWmFWCVUOZXFxBtKA9KlCsqezBFUkHTx6s3lhAZDrlCqYB26PhH4
OngJXIs2cBdIc5FYCD5+cBxr0tyO20AUJq/eOsBO/EtgCe1HQ1F1lWg3tSQZPwfq
mM+e3eJe6x7cnpuClK+GRRiRQVoFjLA10ZjtdCQTk3/6w+131GFoKiyPOVkb8XC8
mIqv3KO55pP71M5QMKVTRXbipfWZeldqDm3tKP/2e3qkE8rQRLOD5CyagMWWXdec
nb/X0ZAS8zDITqYDvLx2kU3YPw1yRDaClukC03JsuenA+DPTvEDzOiVzoQdE7vYp
wwhOCRZ5Z5qtP4dApD1OQzSah9VoqafP2oPalb4hlUULUECuD8iTYfFfmBPZI/Og
qEbNIX2tqc7e+IRypFkF/3ufehsG52JVGh7SqrRuSSEWwIat6kjd8IC8szPaUSNG
LuFye2ifC0G1zpGFithPFgq/Q2uFKnn3CuvBUX3Nn/7nRa4wxS5RZeeP56Eso3bB
4AHEDgQO3JIWX8qu4oobUkgc1rdD1ZIIIGFovDg1o4LNP4/iQkanATv7y6+qbJaK
zff82sS2G42oLptBGL9DL0GbGPqYZaws1lVXHL2rINigTgYnLJlW9ixhOq7Oj/nf
TlnzNBBN/B4Z24WTFR7lUbl8IHy5ifWqy17tuBHwOgAH32Q1cfGtCL69QDsJ3VEv
deMXU6PTnpbjGe+qkSj8D61aDZeKGMyTNvun5yPufJExhdzTTNXwfM1AJxRySZRj
JdfLRiLExqY6K/31/LYdWTZF5xkkV4Wy4cijAS276tFPEeIaO++wgYigbeYQgGyI
jnS0cpAvHrp8eBTP4RAVKyKpUhu2lqatfZXm0K/NbdCveAfAoF818jRWQzihAhwg
iMohKMLQFXNt9OWEAbItm4H2bAwroUaUSIP6PU9YURJsDiHlt76vQxMH1a3HQ4Xb
dNwycZQBcXtQOoMmE0H8lsGy9lkqZmwCPAVnxHyH8xnH61vEvyR2Ka7OWjBg/Bl+
9BRXF4FDeq7MAR59Rn0nsjgu5N7b7Zzfzk2VjSP/1Em2L8hWGqyzpt7ZN94JvVnl
FM3K2t0uKhU1wvwPEqbAzR0f3hYbP/M/+TOAukPhMg/sl0e9e/0IvKPlkfRrl6Wa
nf/9RbPVy6FNxBJb7hCaJQkeiUqVWvKy/JI3OCmyTVpuq849TLAzji0iyKbWtP0y
8C+NWjjAbrtVXYiPbQeaqHxlWpD6QFnlucZwvRgP/loePrnbnBrvTKxnLEwmI8Za
ndqlp4r6+w3MMB3EyMtlH5a0EC7Dm/mpvhLJo3TQDiMyKpTlXlthLZsAwM5nxPBj
JVBgGmsLHqxo9fEWe75jd4+lkDmIpUMBTdhlStZTpwuw29qZoi19gbpBEeDnoqg1
vkIr0fDYicp2shmRePJbrAFE/8oZd4iZWS2K9+ml+2gEOvr2RrW9S/EDjWyF4Y6/
BS0ttLYzaseto8ZHQ4zp0HStvVgIEGYFLovL34z+iR3F0uQIgyMfd6oVVCCeLBqw
8+THyNdaznns+CXMrGcxrhD84Ec6I7unaKbpWgpBGBmykHesNWfrA9jXGFn3HOE2
dTtx5bC9UqPVZLpSqaeu+V17X36Oow5vaIAuBKb2ockts24q430MODFSGkYd4DUr
TxTsJ83zKDe22cOs08TlkySqN8VwTOKzvpH2aAcIZaNVqEoDn0g2eDDKBKK4FDae
BEMQgV5HioVBvkk8c3g0GHvgxnuwH7kIAKIczkh14BfWwMtF7LpseDyXiwqsNeGY
zPJqG9bLbh/Hp3RW5v4ofakhKdPVwg5efLB5nlfTZoesCjarvv3WwdygWtlRvS1f
BhmfVhWRVos1fcGQJ0qTZpiphiDdRUmu3ekvalDrSFIDrveHvshpMSi/FKmCayUO
lUJsNaUMQSt3CHnlkGslO041cxMWIrAL2mka2cDPe4JZ2rOhqWqqT9+eexcZwfjO
97+u36myc1wEuTfOCJLoyOoU2PU8ex5Ec7kIO42XlQLAs7kI8ZkfOz42hK2gpfv4
tMnBVtS8+2JQ3DBbDAUnOxMMKSckdBOVxXxqpSFboxGRqCQQ8vNaU4u3YaGiEvOB
Tv6EgsJHp6OJe6tJ7URdKSgdpoS075QK8lVnc/1SuWl/DfpWPU5nKHtYnXai7Dio
t8G6IU/q6+o0nlhAs4qM3mO8HSmdefcqjCsFDwBxDQWyMr3aQs0qQvU9lEkerLAX
yD3KH2ijBi6IDYJYGLJcb0Q7g5+OfjieJYyA9bLqJ5Q/3n9NnNU2BjI4lnppOd96
nsy29g8QjbpgaWuJ48h8vmvdYKMFK0lhXJ4SZ5+3YoGdkts7ruqs3AAqx/nnh4Zl
9opp8qaj+CV+ADfCe2FTqQHO/9Vy43k/hoLTVOg5un73Q1V1Z5fONfS4FSH1qZ2P
GTfEbRA4CnbNwPVzxijvqT4fb7Dja2lgGAbQCjWyKAkg4Wn1GOkAHF11ipdATI8U
jWYZNf/A2JYhovLRIA9XaWXJnd2PwQvk/B9Amswf+qBLfZ6KNmCW2+JZi3+Bx7XA
AL2tuu+tgsXQlKK1tUemqJAJDw+n+5YHtqE5hrjKEYJ5XmvBWK91KQ05Twx3anF9
P0hlmnlQ+ypNTrukDSypwnCTl0NOvvshd5YrDurTPUE4j4uiNlNnd7PWlrXPEs9G
jfqLpgY2wbI2H00rClGI09VbRnAdL1X5pPBS1CfWDZe9Y7LtFpGKhLG+zQARGd45
pyb3W08RftkVxA97jyEpEDZuqrLkdaOUBki7s9/J+boaXhDh2Ok/iP1tAHc3zTAn
zyGlpElNj7X9haTS7+RUFH5Vceb48XbF5LOOoaHWwVzRrEISJfwneHV/ty2dhRHx
GDW/ZaErEHzSMn7ohyPYLmlts3GvnxtulRWQhRy2z+QZS4ZFECPyBWPCWZlJGi2j
WHhPHS/GzxK+lFKONIW9C4CGgEmbuDhtskxstsKzteIXX+dpgTZg2adCWl8OvPoM
pl3ET/ugZBAClRHLHkv7XTO4GttoPBiQXqZ8skWUndao4h5eXg46A4nYx79jIW+d
ApyRbZpyEmHYBAOqdshRF3+35XeJaldgjBbwsB0IRRPSM4Boj/eEsL5gdt0Q1VWR
UARAOe87LmL9HU6IDNvel75cKlw9bfNKdGaFqCtnl0iQPIl8esIPBMU9wU6cpYBv
YPSONnExwT30rdxVFCY1r93QZNdlPlobe77Yy8DcVeGCSszTcjrmU8DDisl5sqQE
MY/KedZyLyub+2BzdjD6ZoCEIwQAs5sqRg+XGQIcQ3M0l0vYCr+zkIk0GDe6KkWq
n3//QxGVu6JTQ3domZ/XwXKJL4+6Q08j4d75ROakLKSVEsg/s6Bo3hfQFhHVQm4u
P0OJSWzv9RxPfUHUBxUYwrheBbKOw+2EIcmh3ALBrCuuwNlHYzWyTmqBGkfx7bVl
D/LpV+UqqRDtC1S51q8/UJ+2+A6wQ3hZ+gBpYzVUggPyfLVB3vFtWo8XocMw8Ses
MxetaQV7d0f9D4X0qf5R+u+wxi8gpLesY6toeE07fm5A/WycGZu0BnqWPImBB/fH
aNe47JA4BxCBbZqtESHYlXfD9TZ63adJqYP2PlTTF6nycUaPGpGrBTRrzgpoMW9n
8OiDYoDoa/pWdRQnd+Rcb3SwlK5CFFe+yl5VD7JkGlx5ZSv3tJJ4O2y5Z0AN1Y/d
3UIlBt8aNzVqNL/be9Uc7vP/hnwNlSJFlku+B/H6xRYHg8EIdO3u6RQzcaru8Cep
9n7zq4i+PXID7ts9hHnX69E7vQlR/GAOip5haGLtNYJDK6Aw/WgtjERR55UGC6hk
neQIaznCnZ0dYQ9N0mHYsfjzcY/vub0as0Vy5z1VDqKnjCzyrBDykZCIAgbCW7FD
WnxGDT/pFt/CI9WtLrmPekl5ip/xG+HmDaR48OP9lDC2QzddrYiqFTMmM7AzzsB8
Dvy9yDsUudGy40QFb7LGJ9b8Pf/HkkVEtcZj4XHDsGORJEc8D+Y/9x3+QNq1SiFO
Wkf2OHl8ssV9ynZ+T7KD7+jMNRAC2V9kQNH3O+e4XzpXsrjunVaPcyBDF3ZpIXk5
kXnVbuTbDCNDnLYQ5h+p9Nf++ey+Him+4/vFjnhPnH1vTkHP6j46MjHRJ9LnjNz/
+dSyjqhiuHTgMNsZ+e/ZMmWV9W1PQu2dKNSFvHRmsOxgsKPLQMyrDJ3zeOj7u47z
+uR9IxGlCE7OeS6Ji5wZywXOvsmLp07e8+1lDVyMKm2WS1sW0+FFz7WfXW2OybkO
oHWP1txp5c+RsEVYa/IQ9OfmHKVp0G5NvziN1U7VanWuYQahU2Y076eeYM+Duyu6
rPU8CDMXXyAu2jbilAUYi89ZDb+3x9QME21YzigJteXW3wqvc9Uz1oMrst2Tvfb6
boXuHIbyTpgJJtWTmqRVwKdVEnUWFE2l1qq2fIKUmhKgB713v5DnxEUMz0lzgE+M
eRnioMJqXktlYUl6NliSgHulCPmSaVWUCtXhF+dCw3QIN07ZpDVB6VI1lyS5WeRh
ukzzoxwmW291ZBQlldCCDpjIl+u/upnfvjrSGVkPYWRkm/Er55pXpRgJQlSHPrlN
QTA38G3VILc59a/MomvvHT/b8RvXdwNOzJWibXxrTKhAEYbmDMFb+OSBxYWGPVW/
Pe9nL4wa1X6rPY3RQcBR4iJQFF49lTiikTJ9RkZED8Y5xWR7dci6a6X4BtBR6vCB
QTmKm0ChYpHpNn3kwZtJmGdAMx4iAknS9z6layzQ4EiSkZqFK3wrZ2NUEWAISYeX
njVAzAjLo6FQIOgaEzYFJEcv9jQnQCPJf26dGVzIMNoWvlXMRx5prvgOntutQOqK
kCdX+rEVbwRRx06/F4RwL6K2wZbtzZctUc1b3Y7VDUeTVmv6c7exCWDiuQtpCwf9
kwfq9h9rmw2gL+eeOB5gcrGVPW90gwjifEC3TfnlhR3ihcu0NpXPTolsxbnh9Stl
knyKpW/IMUGjAfDDxekKBRt/Q9mxA7mwuEEm/D4hTekUP0sHZk7lARPip7m1h3to
MT/zmWkEBD16eALJyj9V77yoEWDUQmc3S9nBknznMNKfBOgW93zWBZNNu7c6nlYO
UUpju5J3X+Jyqw6wU/ClW9Q0VWeRalvyVXJb9EBNgKUGuGzcIAuqul3O3IE74fDP
Z0qLSD8Xf7U44BX+v6rG2LHxQuF090YVcR6czIhqwGgM8BRkn3KPLuXlqBP4l9Xs
M9ah9YkB7gL4oS7REptzJ7y4jLDpSSWl5q10iydfK2F87kyLPZdeUK2x0U7DJvbq
0cHefSJM5dS9SyNIVb/gvvdtYxD3U2QPinvyJ1h1IbnfTDVGxYWbolog2nX9JSxE
ZrA2PxaAZCxL7D/nNl0ZExge/UlYKn47ojQu6YLiDTNGGgS2UFWxR7/nDGMU41z/
`pragma protect end_protected
