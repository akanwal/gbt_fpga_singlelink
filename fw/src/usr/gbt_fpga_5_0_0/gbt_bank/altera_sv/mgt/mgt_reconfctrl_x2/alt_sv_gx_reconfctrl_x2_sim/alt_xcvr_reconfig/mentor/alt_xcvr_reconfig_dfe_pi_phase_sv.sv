// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:54 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
MjkPHgt/5GnmyIKQE/oHDdVVyVV+9UpkAOUH44YCLCXIpF+5X4kpTkvaW4JmYVMd
tGVhsed5oJl2n868KEkFYZqzsheU3AnWjZnfLEVBPfmdGQlnMPHxyGwYJyzk5oEJ
886i0+A+Zc305YsEi2Kg40CMLFRYES0GzxK0DF03HWY=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 4016)
1iAU6mBT/ZTqsig6HdOtR+64Yp2yD4FBztAKfk4CXTIT5KAVA4ocp63lbOGhZMyn
/WTv5banE7ASnrBX1T1AjgCXjPQYMGB7pFA64vhco4lb34fNeKE9xiUarFBtMQjL
54jpp6vcmVmUeyN+Rv9eRPF811kU8YYrCDVac0bW5UyypOpzjQNYHcyI6G+u8w7F
H9ybjunn7XO1c1SvqGilMBW2W2vTL65vcNtm2mXw9uC5vF0tFZA5G1h6pKHYXCx0
Q/QTtPmYe1Lmw8h0SGArQKX7CGSgFio1J98OOPXVv7dXxpW878MXbe9Ni0DBtevT
w77sMVuWpek7y8DlU/fuRrf42nLKUZsabHqqdEIx2xelldFPJ/GUBLJ0lIRf6WQ+
PhqQHT1eY5AoOGLOsJDiSNXuKGbQyE0J77eF6ZccKpNPTA9dosb1lcW5bX3bZ19V
4ZC/XpaSPYqxcMJoUJT7Tii1DMSB4pSynldAnRlf/juXJAMy6Uli1sd0b/nrhBw6
74ERHmMYYtEtX87vle9K9CQImpzJYHN02kzPujG7OHt1GQ66AJBaYvXv7v8ZJ1oN
0Qtgobg5XBF5htRqC8ljgY5ryYA0f85YjWDecJ9UJzrIReCAnC2ccKEQBqWyG8/R
lAosmTX2/jXQFnQIelto/c+7Xs5Nkr9cih+crEOkdK4xe6+4zV2slRVFK120UiwL
GeljXpDPYvrspHHs4psUtSUkn0e6PNFXLt6RbDC5jFhMJI5H4H1JWRGa9do2hIRe
1l0nXalV9xRytq4PBIKYyXr0SHgfu6qgYL2dKXrUkV+T3JCTAgJu1oArVpss4cgD
9eYi3jWX4JQDUehN+9O57r3IBx70XOeUbK8T6HukRbZK7u+LqGfO8XfYzUDcUqZH
wgasqrJliUQOvRLR/wr7aH1ulic+6Be0gtpdN8rpjggJhtHJgIXQAcpLCVNH2EXN
9aMAbCtXsLCr5awNGs8HWH1ukmFQnyTPDxnf8GirkRjho877m4V8rewKo5PKf8eN
qCwL1HTzaqZztYfBTrOfxzQNaRFgUgNqI2IECKJmTIB/YLqd9Uf0f3m7YWit3U/i
WpY/frneLGBwKxs2Ua1T0xa4ALBPcTfDUpzrGPXby59VaRFoKjMNLskeaEUe3CpS
BuyRSKJKrOcMUI3dPUSfmOkeqqAgQouu0PAR27aPgahSvrMAml4hNG7Xwzr2gool
uZXVrcK+oFNWByEk2Z+CSlTJ4jurDE63B0ZHI2yvDoN7TfeS1VUPSNt0Qie/yEuk
86hFIaSuXJS0U6p2IZUwOjxIjCKH8YpbWoUfJgzg/VIeAEjjNIQdzdrXg7IvB6qJ
XZ3CUCBSE9NbSvwQXoKPbBxf83u4oQmzOS4fQc9x/pQ6m5bCwvnRqA39lSLtRz1P
vvsLTRnIivRL6avZh3bxozEkcgCsPbamLHTJghOAzK1DD7YJ/7f68COi6IUFpQA+
6zMhe1/I/ezV6lGsepcHRz69FFRzFSRZt11JciMcHv9+Et3RLblzUgetwOGeHOm6
cdSUHbTGlMUsladNAG6IRPHlxbPXmxgHiXZSdcBuCZWCI6y6bVrVO7zTJRTvXmZp
1D2OzRWtJ4VMti+7PXn1AsAzB3cu2xdJIc6GsIrvT4L7/UgN6VnxcYK8bhlkapoW
Ev4D2zC8vQ6tB0D/iUyP7r63r0yOTX+DqpqyqbxOFxJRMeQlUN3f4H//xRXOSg2d
YkfX8i8uwFtO96lOofotz3QgiKi9DfY9KWDlQQjTtFVoPlJDnmzB5z8ApuImM+c4
xoAbX/HOc+O9Wot2CmXKxXZSDjTcx2niaQftdgo2W454XbbWZlVdw/8WyLFvnTbt
iS+JOTpa9khO9ZsFy0bZcFja85xKWLLqvz5din1u8KwiRkKXiujJOUNe0mqGkM7v
4kUIAssbRUtl8idCsXAkEo0JHhYbWQ4M/zHpK5IOIwj/LXtbWB2fRZBviY6K110Y
3UC3MRM+/5+nI+AwatC/qj9hJ+GOyNeDv6RB3XO6VHOvoZ2RHst0NAb7OeLWKeNs
xJzh4pY21eVHFgBOkUDTXsFncKHe8/KE69ge9FExFqo+ud/Uk5z8qd0hLTqexwnH
ubL70OrDeNdO8/1GCiSaqqAAhBhpm4SL4jEXiRQ1QjiSEbpG8aXXuMPeL97Rsfjo
L7pOsZx9EBz2pwmt0uHHtXofi1qx9ldNr1TTQI/dyT1ggd4T/SFxQZG2kX2MqTDS
1BAXMkqWMp+F1kYLh6eT5L+8gi8SHNcj9K8Kaez74S2VJBabX/l/RpYjAPtnJT/X
xcOjO3IxLrwe0gMCVCLHTExkCiTC7IDhdaUKzere3MkCHwelvcYe5T6Ceuaym7oV
BJTzvDcdKuX8xvVU9D7taUq93VWqoHm3rj61nL7kemUrutGoZVg6wEjKaC3wxhfr
7JTzix5vpkLfJUxRaMMHwILGdepcLC04m9xnkrXi689aIODkcI4tIayX/VWON2ve
/NEe0gm7l0QgBU+YY0ivi0Okn0R7q85AV/HPh/aqKcYb0/bTcBvvaKVszHLzb89B
5QLxmQ/wInPZM+U4uVu0AVScp7YT16PB8+pzzrJUitsYZN0z6ZdWYnA6KKDP6+NP
QDocVCa1+lRwYAxiR25MoK4MnwV9wdk48mdHpPi1W/5Am5PF5UTMn0DJxZqu33Om
pb29EEsu/L9XTwstm/xiGFQinHSp4tFMoWRIkwDl4HrpD01sfF/cwuVQfqQocPZ3
9fE6ABdqWqsoFBc5GZ+jqAuP9aMsWuCNLRnsgpptQTwRWov2mmgJj3SgUZxn68ca
KNhVJGcLG6cAgLnbUtYcE4WcfYSak1P47VBKm7xXHnhX7ItkWr5rWNDxko/e4myP
3fK7GHnAoToLn/KBYMBmfcp6mZN16CzoR38TmElJuqttWzXAm3ucWD9vOR0VYmA2
AWDBtrDyDlC3ESh1CYdcM4D6aqXCaX+/bn43t0CyVo8Jyq8OHYjT5SealVd4Ydl6
BFpBE1W8Cqwu8BRLl4WCl4UDjkoB9YXDVaj+sEwRmBD86flan1sWBH5UTCyPFLvp
g6jFTm4Qjidx1ae0ZyDkQCISqZnIbVye95N4KTlykBsRUfB80X8tVNQ8BBYzPCMA
xaXzSf3g3NELvWkI/8BI6yPoxH33SVZ27ZFqNKNnMzztI//4pugJeKVHocvBJZB+
9z6FLu1hhIfKPrySxzQoLcBpXEJgwAH6HvqcCKpeZN//KbpVTwmC9w+bCMeznNHl
fkDU6lRaHvmG/hSmHEgWB8gW8hO7G5RWWlWZkSdheG4S90pymviiyFUYh/VVg0Z1
Gin4LhrN1FXo5DgZcmQHyBldoaAGxJucJ4mZKV5JXmZNwW5S0xIjntTK2wBVL/WP
6JATAo3n3ykcLarO24eH8B9Wauvcl0x3N1Ms9wclmGd+AOpm927sF8mn6BSn0whC
cKa51mxF1DRdrbnPxWP9+Nw+akMH/uswO+EPXzFEmgjidtijcYP2Fk0GZeRyR6E1
rAaQD918pC+6xeY8DW9Z2dHRK9NAyHxiJ955FCy4/fL4vBEemmT807OMnQIj2k4u
zMnJSMEBOzCfd+Elftfkg2dA+fomj2MfYNurYtY94M+ghiPmIP6z0JxCevuuUoT0
Q3VsIkhXZBZKSUmXCuiBYZbJNiYRegzUmgZ/OyRtK05ZBxN0L9mohhRwl3ZHwkCa
nXHMXgz4pwSx9L7xikHIvdz223HBbJDN8dNZR5FKlhnPTW0mYPgkpd+KrbXJokR8
9Cj0n0rGrfBViV9waORzfwCCinSPhtT+/lmajuE3YrqfOIfGDYQ4KV5vqbhqMGp+
R+u80nGqGlbk1i7wbx6UZjjxQFCkUMIi6e6+W32Ly/xDrErenUjd/nHLaU1S0uhV
8YQ5kYWfn7J7lWUW4V6Eb1wuAsV/9z68FRro6mWQMaHPJbgDtEna5gHLHSouVGs4
//OOYeDCNFILczlxBauz6kQ7PMCZwUbMjOCugHwGlMQKKrOFKcVwuE1tvIq4laAP
7E7AXxodi005UO72u86qHuZup+Fyv//Z67BC4VJQd3kvCMzMNPr4Slvde+ShPK/E
whRWqA3RhKYNXvlsdOtfTy2+jeePnvX8fw6oNgETA5nQOD2M3a/svRTlQOv2z5ye
+Pc5/Soc5e9D1rsmhhPcPynWEXTRNMkpNsWEXhn/YKFn6xCq+/froRxrLvx019r8
bggEvPF2O7JrkyyRtTlTLGnPfwUrmTCpTa70idu7HZHUSyPBWy+F05FPNEZ2xGt9
kkzQlFvAdWOx15XHwyNhi1lFZhUVdiOUOCug2EXzD8gYEg2PN/Au1hy2t4h4e34/
eR3JJMke04KaNP9C3Y882JPlCrBYL2bBIASsIuQ71Q0rYn+bBIb7R7r0Z1ZhL1aX
QT0rT16/zzSB5DyoKOP+fxTk0lHDEp+HC7wvjOS/GkxGF6TgBM04yKu40L9W+aOH
Hb0Bm1GEBU3HRnmd1ePrltCVovchGrAZ4iUC6S3TmkV6MZ18gzglvuN5ddLZ3Uue
IVrJhxTzDMtP39TY3v3qnFytcr/Jm86orKZ69xT3QBolgJjqjmAzXN7v2Ntnlelw
om7/1vTKyj99VWWHeIpB5oVqcSyzn7EW1tiCj6FJtwZG6GSm6kE7c0zjGS5EUI4F
S9cWT4PLwItXhaG/QKrlsyVmzQElrgvewiRzsTKSMiwboTQChcJdmru/aXzMvBsd
kJ/6GYxsRRLHSd6QWnfOvgNg+Z9A0svzA87COkg3bM0mdzDC7Gx8Dq4GmfnZVCrz
iv8jCfuIV0BZwCsL81lrc0zrPmY1H7ekyfK270VJYu1bDC84MS6sNWBywyRJz6Tv
C/vIvaNYlm62m+r+tt3JRMZpo73wtRcvS9dqo9FPGZNkf3n0KBSI5X5PjFzVnkUt
V/BeNlfgLaGPDnFZyXN6Z9TL54JrjTANzSAbiFn7REEca3epNE/PPK5WNNCbDT0k
8CmwbmktVKEqzeEVS3/8w5tauL5XsZPGu02hurzE3fObFrYvRy7xhNRsisxUbpqg
J5CRTcHV1/gcox4cbWAn1JnVNSxaOErhi+Iz6uHtrLHVCCJjGidjbUq4X8aS+VZa
jCip7QgZWX/31CmkDZvOHJwHYChRGMHB0ikuqaEnu+h6r2+A5TEo9yZcJHGQCES9
Ax0VsB2JQUjx9EBCE8wKDoUYnhXwJlZUvv7IQIP2bK69A3cAwkn1OldGviD7bjCv
FB3h41k0h48rdWLGIUT5m/ZIVGwOWg5Trzd4tmAx+/A2GoCk7zGvwVGAxZY6QBVY
Dj0f0rT3A9CIQHDRiJpKhCj3t4U+J7d5vdVW7snE8zY=
`pragma protect end_protected
