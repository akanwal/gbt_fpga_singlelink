// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:54 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
AoYsrGfM0E06MvCvapqqLkcYhFTB+5E5kz30xLjbg2vTpBAISUjckuQW2JdjNlC4
2VqmBwi74FmnfNWwoG1rvJ7Fde9vCSXdLg/2xTl9E4XXH0fbkk1laUkYaDkSufSK
HOR7yLIm02TOg3Gic7RVmIFx6wUWHVv15kFFKfIyeGU=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3888)
Af2O8LNEZy6/eeHasAtWPGg07Hb6b31lh7ySMIv9AC29T5Ppe0295A/ewrY9f0oG
2+T/MXAVHFLNQXFomjub5rrj2mYfRCuRn7FFKcUhw1dTDpTYbIBAvqvT/73eOOLY
9jZxyzLy14/1hVziUanQWQVDRUKEWzfjjc+I7ZtpVJ9kn4W8EW4t10q/xAx1E8Oh
AroaQO/sIRVah+vQe9W13apv6PKEfpxYILYM3frtySw7fQ8Mv1KccKG0Rv1GstUY
X2ykATLHq+EXyp25I/V7HTTJGzgKneLOHmz5y/9Z7/TG8/0vjtbyLTnr3aJ8Zfy8
VpsnWfi7olb4Tnkvd9pUIPWXcKdT5QvtpQTzgDyC0/DGzqYkbFj4tH/7wqyHWopy
9yEF93Jkaaq1cpKm5hE/ov/fKHUOXHbDOo7S9eEo3ZDgeqU+FMi2LjqPqxw8z0Dv
03qfPbAHeR8wgTNZ5BK1jYpz93NOZQ6oEe30OFL8bE/8wdc+WuHZgihqQyulUijt
Bzeiv+dlcX810UCiAW3sZeYnOuEv6KipkaqzQrl328ibSaAL0FYHumIC/70WruOU
YONOBTsx95uhRdsqw3jErencQXJh0K5wCu2RWjKgedj3IjFN1S5QaN9mWBWuknyF
Cw1epkzgWxsIkZctjBw8EkgTwsr1VAXiGOQH3qeSkMbtsydCUoiVrq2jRcntKuBp
q6Gkwvxa6fTc0ACNRkVKhbDGcuZVSZ6bvUT4vvapJ5LgP9uoqLiW8pgW3WDLe8RZ
Dk7nBrHkSQX17CcR/xuy+aY0h5dRTHtangYoQ8kEZOdvaUI4YihiZ86Vp5DqQtzb
Hi6SwFDM8lqHrhBjRf6RUZCzsO4mVdVB/AlHOR+lGcfAzo9QJZIMHPfPIqH2MZng
S4D81zcZTlRI0NkREsU1ZobFr2paJ1O1disiwsHt+7TzZ5kY+DFHMmVHHMWfB2G+
OtABFcNRiWpe1AaNX/bdk9LOLiamafyXzO0h9KS585pUHlOGWhgOLe9PzZBAkrmF
T/3TcHZQ5hr0lP3JCaCyhk8X0u/YRvO7sBmsrl98itfpfCSInJeqqlde3c22EX68
waHaa9v721ytnKb3irs9hhPOZeuZFkWidR71SvdpDw5QG0bHD5AX4GmMmqrORZzK
WdcaKjKPjRCzi/1ZZ7i7lGRQ3mUQyTngl6eRj9cG0U/oG0KYhG2Kb/yJhESTm7Ha
I7rt3SsJNryQg1xvfp84wVR0TeCv6xSqmN4qBewQPopQlaLqyRYmcfICVutOk9OS
/iU6P8AiQq2miMCH/9wKDZTkgo1WLyA8NvDkDZaQidGyF/quzidIfLrxN/fHoZFd
m5d/c5jCoHY/hWEcz/slgtLo5dV3d/yzcHMVZpvYa7wWC08s6OLLKriC3Dx7I+Yi
pk38RsY2HWXC+sPrTkaMAJYpdud1MZixrmLLh2xlsFj6F08PX/GDx+PzPImwDB8c
LE8mzgz+apXdh4Y602EOuUoZ7tQHNcuw6cBcokyX2s5PWQq8tSDDzme0sV9NJ38m
xpJ6KcPMtT4KpOp/Rw+G7/u2SjcL17cJwnihwBpJRAbMNLYjl7cC4i2GkUFdy3gJ
cZjAVW5RznvTUNR5Ajk9NBf8CrwQgH/GH0QgxlOrbqRjhHqH8M/fP1ymcGviVoSP
Cfeb7S2VmLqMOOdGVFfEYx7YV/cGJDI5fk04Ks7XuyvlzZW9wRD7tdheFeH7Gokb
+EijsQv4qBfPEWQu+vOQ1Jm693nQSeTfyNHzuBJIKsSwp+sQhqs/PFI/Hw90wZMM
pX+kp9pMr40vzhM+LBVcohATg7y+J8rpWyGA/FP+HvmiwFFDJvKlbt+AMi/Vpu0s
k6P5NO3d9wQHDyIFRkNhsv2K06JMqYNpBYn5fRWYTImI0Jn/SwsD7fdQeB4oWrBX
UF8/iTyqNgA1YkIe5Q/2p10aIWCOFlQdpbYSlRov0h4/6LuBjt7FWQvHbBlnqjJp
hpcj7SeDRYzU9vsln1sVhqrXxGBY7paehPCm+A1GIWO4Ifyo3u192x7wSC3oys3R
M6jTS32ygcKC342qKR07ikOSkQHg27ygrg0QerClG4a67yh3XZ5Hr4KuPdmMObNt
ZZ+881QJE4ciTlujMoJz9UqasUjcD3I1PzbcV2zmgRKcJTKCYMbAu3A04vThhGP5
35IDWzeZ4M3Bfl2yoh6dWah7vE9tBGJF2ZaudpqoPO2ZPp8ohB/q3LlLkDJfHNjJ
kyN9WMfv1EaAnoRqggl48mU+cruE7CUjsPgEqI9jzD6hEcIJsOIon0kNT2VaLqU+
Y/5FH8Xrdz03Lt4k7vrk5JHkubmjLZP9Y44SQvDk9YBVCBn1n4qLTPpZL/leace6
zXrfDDvYhP0Itlo7GFYLBKlPYGcSNoVj+vMlFR5gIS3y/OEAdFAz8jpsg1Vy/zG/
xFx+Z51K5hZzTchE1/YiR62pdrEd/aA87WcJeB7tP5DYCGLcuDKoo4IWqnS8BEb4
V5jJFstE2J572w+LoArycNJQHR2kzBM5458N10D9x5wrjtgqFvvxKRmrBPOSz4Dc
m4Zu8oHTzhPQ4bdfmUJbufvVQueQyxNrenbmVtuDJWaYUIgVk/gwcYS4FwfTd3ux
CM8ctMhM93wDbcrD41hIEveL/kf8j4PLF5RpQQL+JzwHJhQ9yQ5z+z5WXDQUw/YE
A2bgVRYDmQGVY+gg1smEzPdaLDAXNW/TeWueNKlZ+cKcO2sxwlVqFX+FxklWGogy
jCau7Q8uMveemsUzT6DFpAFZuaE5vWnE8y04q0fRPK+m/MKIBxo151mDo5AnMzeN
MTCUELZJQRNHVdMWEjO7yI7ulB6mfyq+iRv/HixEMGxdmB57aCuJIfCroA8yjkJ7
JSySN20U+Ht3PK/Viyi5HcfmT/QWkpBzKGNzsv2EFlx3slsYnf6qseWNvmmnL8uy
v91ULXgbEngpKO+VwoX50CyIFRse1Yt0QNhKqAMpbI1rbcUptpX30r/0Qt7gQ26j
nauIuJhLTEt3+bLMu1BFJnrnqolj+fSUgqfANkOFnsBDd5B2Q0tROFg0UJQ66BXR
v8y7G8uU4gw8U2CMWPKtnRS/Pk8clwXmU+imQ/D2TWiJNGGpYxaNU6Y5/R7Pq3WK
6uHwj0LP3iHyP1Iw76N53leqwDFMN+YJLulQAN7k+fMYuTdJBH5/yoQiSO8oAno1
7S5jnm9EfyQR9OZg7jwjQRYvERbdhsEhDAUuJeFMMMpQ6VqHDurKa80MpGH7u2QY
hrHuawL0+4nUQZ7Pa6Q64nrU6syUyTmgzpyuqOsamnLbLIlvyVGOY5OhdUgDdJ/2
s9AEPV3uZ5Ws+aDwchxDhu19mOn783RU2oOVo1dQOu1fmSj2ErBCwKPdZxGL+ki7
y5iimR2Guru3y3WhGgEbROBeuwq4zz3oPdihO0CIT6epQ1kEixGizWaFqvjfwrW7
xV6Vgus+SpsDPfWTAhRRopOECBZekP+vJVi1plLGCGtsApDS9vSmT7moIiPITD+p
kv5qPaHOvMLdPy0diRdTI3lbfmnZy+ctWSDeqBR10VDY2t73DhIFENZ+joTejtIB
Yj7+3sNd5QkhCO7Spw/npcinvtVP71HmU8tRARljohIsb5Gw3O0IfNTvmkNm/ZKz
hRCeQlzVEv74BpMNtLa9aiP59APGgIIhkJtkuQgpu2mRp/nXFYyEqTfOncOt6RWc
vmbROV0PevKf9BBig5vu4EZxG47xdW2d6WTkJ8vMapNXrO2qj24JxUXUbBBh3eE/
z1lwRK4/PHALv3FxQ58yglm2QwYy+5VZdynvdiYe++1OkpJKirxisF4TDr0hwcA7
JhPnwD5PlD3rmR4HWs6IcUOHy8QuBke2kOzrw+gA1zfyP+Nq9ghCMcXvsPGff8Oi
H9fF26jIGA764mQEkyXgfvT92bNgAn5V2OFBW+trCu7qX0Rynx0tBbdYHW33Cuco
cy4KQLtWt9w6gx3nrKwv3XkPijPXOupl0+z2L0AWPZlhx5z2C5m6JkA63B8zwGf6
osFzJRt69DgB44A7i6Tkg974xNxtnF27DPkz0Hyzv5l9FMaKnUI7qI2gz02iA/uh
1QxSC48W5ND2OQXy9DJwAM1gUvxN+Tyb8KarH5RSp+fr8ksFpiJL58ebXcYoiZQ1
Q4Wv2v84AGdvmobTGljTyVXjIOIpH87gntjkgXNGowN5BedTau/xMDwFoH1hlyt+
ck29EsP8wGt/AAN7rIUwS3AMx1DuJdaDyltEGtJ/y6NnpfFhIB5QNc5+Fdq1/bwF
IMz1jIewTs4FV6qsNe4TJFERkcoTPy1NcZRWXjuDEQTwhW2W14PemBMmHGVkhcZI
sGxSsWlEAndfUTpGBNjYncRtp4IA3w/VwcVtxbzQFHQPnPKpO1apWTkgi8eR3vQH
/yL3LVFgUwBLzK34SBh5iN6hdOxfofHHUxc/0bPVQbUHTEACtU4GhmsWxUnwWi0r
bi4IDnsfpKcw9YB2F0NubanJULdRsqBGTsPRcNd7aKjzveqT1YNGJ+ZE03CYIQDR
bvbdE7F/eDDrMD223xo8jERJhfgv3OXQtW2o02ESdyZaDDpsm4rraLI3Wq6/0e4L
uyuP2rFydYinMS1XMtGhFa+Fc4UMKexSgcFqXH0Q+8yccz2HcBRx9BTDxPw2wRj5
EYY6xPuqEdn9c8zttNViRWw7GNf4nM+zMAaJHNiJWtAuTS9Q9NfoE7dU3Av6QGK/
B28MbP7U56D334pPPENHA14xlp58lBwlPZSjxrY7cobxMWjt2QQ5BGnwjRl8jTje
jnYlnG81uodLJbzXC83KLViWMWyZ3K8LJcUucVMdB/JME8iieT57f7xKo/tFAjzX
J7KLd6d4wNUhO6ahxErcfQDwE01SsfRuGSBfTa/HJCFlzgFEttXH71BMmsQbqcS+
T88nT0U22/aNWLdgReJC3gJhighRkldreSkWxEX/mzOfVHUQKCDSnWx9xCQysf7C
aoTeKhmW2ROEkIfvWacerB6FpTkVGw+4ajEe27NUbVux/yeBG0AxkGRN7VcdQqIr
C9A6PhA4M2L92UJB0KNmhT5rsb9EorinKoeNJ9FxXxWiMXx7ir67bOF4Cej562c6
k+ko10UgWr5F9A4DPpXeJo36Xw3WlOjJyvtcIAMQzKPzpfu70E046UEnTDk6ulIN
`pragma protect end_protected
