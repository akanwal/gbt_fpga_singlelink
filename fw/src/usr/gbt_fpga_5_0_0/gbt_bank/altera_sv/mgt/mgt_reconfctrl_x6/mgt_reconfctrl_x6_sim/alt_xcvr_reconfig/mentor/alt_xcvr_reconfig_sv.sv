// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:47 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
sWG6kcvkXkCyV9B5smdlpnzLWK6JLE5prJoNol9O7Qlo97NdwYD5KVVOMRWqfWKB
UZvGFyoDMgGom1grKbPoq2YZ5IBzRuWYPY9s6udBTFrcMWZWKNIlb2lAhhAA4n8f
4yXMx+iQNJr5ncsRWdfZfFOoARW5J12+zIZxpRjmKr8=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3168)
F2dis2dtHTJyXtw0+7AGSE4LG0C8AkvH/H0yXvueT6mNY2yatfWQAFfh0q4oIYl+
05b51U/Ij2aZwCXSWNhrRyaQuW90FaiFxFEkjriHeMkXyobhoXdtyvAYM69PuFKF
DsTUlVDsIKk2/y/o7bKErz1LiLdrdHcZBvRhjdr12H5mf/VsVjz1V165LObJYXpx
WjB3qneG5q5LNFguvUGC3qioQ0OQo4vTPKNMvfXWRtfx9iWL/RvLG3ICsEQwaEkU
wil3wvfW6pXuswR0lDyRVuQN7xoMIsHlGBFleVYCThf4MFxuQzA82TOTXPLjrjcT
5zzzLBFd68k7Z67nlN9P4Jnx0bmr5agl0C1XUgpmJQ4kI/GVpYJgB4CgUvmA9eTl
kokPc1mfcl2tMHvN6/mVZoWk8UGWb4JqF4UU564U5P6Seqkgk9v4c13bs5wfMjRM
91dv2BQzhcaAiIfhRuJQjQK0rA4aBq7TviXeu5K8+iKe0N1nsAucvy+xKP0llx0Y
dGz2fdesxXAS4KvFnfnmi949bFMYHiBc923NtdBN1vOtewU3hG8wPFzmrb0mUKbV
Ou4N5c2oNeTOCCEVoPSZyG1gC5/i+uUFSOaCQ6zybkbq8jx1F+BLFff+WQ4NAMFt
DetjhfOog/U1ddnmzDI0TbvmD3IH/1BmA6RhUyEDbIQ6m7KkfkoR2SfItxXYAuB9
X5OFAAgA+P+KKXCIIq5EId9YAqrk3LLCWtdSMaVP3T+SSkv3vpVyswknUqYu0XYl
yzLn76q8o4oawi05ZLZWG/xqhHaG43TGc46CPP1SWPxMzJ5nWtE0vML80DUMO3Gc
S30cZLj+4fppsC21WxWrUNyj8XN4YgirPRjCOOLI/wAkSrl+WFWJHe7UXlEYJefn
5vOPfhXuo3eXVquo+XOPA+J7IpS3CLoztez1ZEsPF7b6wv326y4L2eYGuW2OKOHl
PKcBTyyHKUWjhtjaiBmV0ivYaZOVPGsYNc1TJscTX6Xj6j5wDwx+kG1zQ7Bql+fh
a4ES1YC20r2USgIVOA6Bnvhhym7+hP97KQnsMAKR9/NJfD5wL1219LpVQJ1gZzyP
XK4OWZML0HHW3Q9kSFzw92vPUEkxOt7Ey1qXBh/qbmWjNKI2IxebUC3Ub1iY/ufC
LLcbc+owb59a6UzFDGOnCFsQN+0uXOIsRegGgj+q9iPRxHy1Zus2Dv6E2xq5akHr
DAfcJaCoC54ciBQIyuyFhR7jtfeD/ELR0+YRT9hw5ZcPmXw6fhZXv0tQK2c6rPYV
d7+hro0uE6VhlugKxMclCb1UAYTsW4C0L7BTejGo2+/y4f8NUKdxzpheqhnsM1mp
LpBwm/pUJoZOY4+l9B2319BdWL/Yv4Wgkrz4tNhLa6cYDBMMGFkbGaevwDoGY7LB
FGai0IcYgmPUAYSTHs4wjQOG09YmjBF2eag4lKqD9IOVA77SiNJ4c2ukpM4ndGXf
acqDvBQHzc9jnHM8oDL6+kiA+xTXtQfbuRKMi3vdnF24U3t1rPaJP7gmF1L6de4R
HMAG2oWkPGNYE7RbGFGLAeY2xqZRR7ZKgyGciNx9DYzY8b8enPGttRU9y2GLUN3f
bvZxnFxTsmpofb5RV0Eenr1FT/Jdd6+MOABOZ1M0d79t57zoUrObSJK5B3OZYPX+
oMewtcAwophQ8FqO4VwagyoA8zNU29ddgDA2B9kerVa5XrYCSGWfEr4AGiNQgLjV
GOWr+de/BlHlGrEArK8ZX7jz3wGjvFa9sLh+gUS8I+1+pEwwNcitMsbm/nnubIql
UvUQD4kfUd4w2Cr5IvA+ONQhC5PWVCYUM0af9z5Qp9urKU+vdH0p+XeuDOyk5Ctu
gaQaUjvwZiuZUOQwdeRjWXG///n0MrVcqKQmB57ISnqzhKSSr7wnNPJWXt1AN0J/
2O6JA2b+rETHuptcdLg+qXmone7s6WThDNtC9M6GbmFoAjOvGGcosMZ5oemc8Eha
PTv6eeY9OfAlqM60RZtUFrKulJo8flYVpXHbJDYzWDJNvY8TMz4k5BVp/B4UX9Mn
nUh4xOPfiBS1u/yqpGwqGCukGX5LuEiPJtKF1O5atASn3OtVc/FDTxwwdbvJcXIt
0RfDqe2QQgfiQAubTGzWrryioyDpZv7KVzeFq6cgRsYMsEzvJi3bjJN9gp1gPPY7
CaHGbJV0NkZlH605es8p9bnZrtiLE4wUXxi8X3wINXf056239HUv6OKNhK5EWjjt
eG9eIuHXSQ+zHVrb5q4eQP+vifi4xuworzgcLDayzeJgHUnnDyYsEaaZ+RzXA3eY
8S6dPJ7Bb8uYrpQSgacX4G7M2KlPOyOlNZXkzi0XOS1r+Q6WvLwV5RWv/O4cxZ3J
DkzuTMBTyQo6UVCkZs5TWKHjbaVuVzlKUFqaRSDVHwALS4jrEZAe8TY82dJYxucO
GGILLDXfHiW11mkFVjx62mp4/sSyGwuZNjoXCjm3RjDcJ9OYbws6zwHuvXJggOCU
GWDHTygQvLNvlQy3n/b4h72sfuvxtJNjUvvEHP/21iIE0b/uUjK7yypIbzz2XMLO
mCF/AXLjVN29opQHXxYS1KHV+SQPU5Y/EaCQ4io0eae0gcnj8rjTK0y/fEW43P3i
J4UQFl4jAPWInKBmz5TBuF2PxQN+9hStOZgkodbForQxHK0Htv6+FyFTZlsqOMQQ
q0PBRXRvr5BmwK6/MKbJqfx1irEZ+3e9CFxEGePuMKiDws9P2Ts4fImYqGtc7tnk
Zyos+4XVSwXdJCN+54tJHBzEDuzi+Tu+Isx0+sUFkwLfCz0T3Exjc0Bbv6NZD0Uz
poBvCna7/imiJVscyynRZfvaUDKaGnNkUcx9el+4IGHNKnNZiUVPheQ1/cA2Gwcs
3tRIDzfjOcSnHXs5VK+9SE8OeX41u6OG1RYpCHeCe1gEAKuKN1xD55e2kKtd9rRj
ZnwGEe58B+A5Fb5ihWG/MV+dy3aNTR/OyKnUr2qKTPVxHAgpyAxh93Qv9OtUnnvM
xBvkLIsB42f9ON3E9bbiZx022Ieprf7rfgo2j+UQa9gshzDVU7ngDItBdl3Lgyp7
FkQRobmsYSEsroqneqZT8oGlY9FJMwHa72JUMaXmfIKt5h2TuPvbPzOKeU0or3Wf
NlOPVSyYkA8Lk6gcp8DwIN4vdPXeNlzJ56YQIzzGvPzZbUOCRHTLxMG8r8f5v2BM
bsewC3RcwOTXRu7//DOstMxzswfNFKUiTgbmToWjHVuU9n6LttIL2xcE23fLJgkf
WHYtvHD0j3DaWpNNcmBlkh+NTVZY2f4p2lKyOsMsYL/kM1WCRTEilSltN+xtZGcM
wFando/Mn6i4aqCbaEQUBhIZW8iLUcPnjHT2Z662JCy8Nut+uFU1bBSrjp3keJoJ
AoYsUK+g8TlE1ltSCrEFoQz1n4s7uePLJavz7x52TjSRQjtg643lWuzzwHDye+LU
OleicJYFOBsnGNQ5772pt2goakDryKbi847QqhFaDrvd2F3MXay7Az3P4SrG8W5o
MuLWxbPEXYV+0XzRo0ag07MsjtpGuM8lb1UW2lC7nuL/Xj26w2jVXh7C5SiArl+6
tzH1KQMKCbjKe10tl9uEc5HopiasndspGRh3D7Zua00T0H6LDqfiQA7O/Ofs8l1w
BL19Il5vWcGJVsX34+bW00DN2CG5hJ/HRjcz1F1AFtdUcwd7DcLiezAHLict+WUt
jqKVdbCPCI/9+gxIkWnqrNiUXNtqH+SZB0y2KRS0kPOCPaN6s61FM1CPwwiXda0p
1v1k58Xcwx+jJaliyOVaZFjFZTTBAQtaCvjKFdd/uNav7tuUMi4KCOjd35rLp6ZB
FXQGy+y509MLW9kZFCRjC1yLay+kdh0xlNEIL4AxiUz+u7W1khh40f2RbIZCDnLu
pJPZ44TrlXIT3rRd0u8rwrRYREkDynIpQq18nqE2e6dyUN8Ty9Do5hK97GEvYodG
jfYlaU5FPuxsbYppe3OwURrwxTqhVY/Zu4dkQT7BPrRJclFmhRZFT0zdptjjaOy8
rCaeKuAc17+hA/SMQOvssddfPVlxNZhF4BqxNs2QWnjDdzI5mOHVb7SRV6d/bLuY
znY2gbI+ACCyBuGhJpBckh93/Q5Uo03IgLj+fcyKrHWM50NKO+ROfBEsNn+9y4i2
neNq+W/i64eEhsWVUvmVGIsViOF6UffZbJG1CqGbj/ktOCqpHADFpxvzzqvDtvwQ
`pragma protect end_protected
