// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:54 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
OAOVDnS2YNP6LaVxfrS3f1R/OY0y7b1tHs/IxbNLtlK0PjcMinZm5P9frLTVN1uj
1pzyN6Gax08epNU6fFi6cBrbmwJhqgyx2qOinBCVpvVl5K4Tv+LXCqsDpTYp78PT
lOYsBIa65f5iU3mKbS5lvGNdAG72U9VW2DIXPTKo5uY=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1456)
m23By2hy71vTs/y53n/TE4I7gWqgHO+1Zy+K5qzGXrIaUpSUAj7U+9W4Xh5uZwzi
EpLlku7oAlq/o0ev1Y5qoFfF+TS2jM3w/8emYQLXx4AbNXLUaHYIPkNeYr31OoiN
kLW++YSAmwEvHvoQm78tI3vtxy28SxvjFBYI54wo0NKzy+LoNGA0yWPiRLmsu86U
d6R5YZoJDJKBdbl/G1LfYYT+YJmmBTc8ilbIIA6JYTCyOTV5dMBywVH9zYOL8nnc
g7TUrmTdtDpD8rj58SHowidN7nwIbMc71rpS3iMcoLgnm4egohyUS897G39gAEb2
RpWZhS0BCrup6glf48DMRom3s9OoiP9+18mFT3eV5OaQMVK0s2gAn0/HivKTGLea
IRimGES78WhOANGf+bMHcAnkrTntN3b0SYNR8c7R4y8ByrwWNLQtrGlXqaXWRVQ7
DAVONtIyYEHcgxRjJv+C7d/+lFo/GqdmRgxjHeYW7bNi7TJ0NjETlJ0vwuY8LQis
jDCAjpkTh8MkN7mFpLJSYZ1Q8hze06K9qcj12QEboTvNIZMnduew6VlAjYwXzVIk
bx5FwPHqkecrSxxvs+kGF6CTN9m19NAVu3Y49H/ga+Lb0yI+JwabRK/pIrv9UMyG
PqtKfQgQv5aA2v/XLE37tmP9dbYEsOm041VXzPGCjZWapaSY9idDcz1HYHGFOT50
BoEuQAwtB2XTqlL29z5yTz5qCzm7Km0yqMd5md0pMYH/Ic0xdI0eeaDXBaVqg4Mt
GVk7Qfka561Y+cxLolo5TveoW/YsFJT2AXqKEzVpC6DQg203jqtCfK83y7oopznV
I0iUUXek0378zTkkcuN6Ca9gFJz/RH2EA2CZFYQWJAbhyikquPhpNuFWATi71N97
Fz9CdPH+BuHDufyKA66UF1ohWsBti0SReW+lKCGsPSWOPKYkk8as3zgoZU5BQ+Hu
ulsTcLqHjze79po8cpCqAO/y+QBlZjxhgHOH8sDRWtoivLmaoZD2Cf8xAauNhpnC
+7/FdGTCqDxEALHPCgITeCkCi0IC9rnDR2DFIUpntNsi/6SErX6t2s+WEJRVh2g3
V8vp1Giq7mZwKPugEr/3YegVqkODNtqLL2yQ8ZYjFs4w0UiMNh1R+fCLmbDCHL0I
uj7zbCxpD3s6UqIT3hTlmmrcwHjl+NjbkG+iOLbaP7gvxiEttphcfTGQDits8taP
tga04tg8ZrqPxR8jC2zPegt7qIOikHcdmappEbGdvtx3Wj3dBJN1cf0i5hb0aDmV
A7eBmsaI4WoQQNE+0VtyhfGEZ+yc5bvmFovRQFHaugtwwZ6m+mpqXXmS1DwKxhZl
8G82gAxmmEVdiYM4NlXNNtRjoHOG5TQAsl3NF7aI9g3t1UgHKoC1V6JGyobRAl4P
6spIfTJZSzpr3k2Vz4CEzM7g+m9xy8DpzN76uDAUOqLPa+rdmi9uq8Nfojx+2Oxe
b+5IB4gBAygcp5CiA9uO1LwQGFjfQYURls1XiotMMkeZqGJEvrkeX30oDF6+uKvg
r8L3hSuOIslBun+iT+h5wSOhCLQK3ICNJcOza16XedYAwSvRsAYHK8019AiaCCI2
gk8hnwz1MG99lPFZPned0I974sG3jFWFrRGOB4+e8ghLDSZscO8byCstA+fLtC5E
20YtyTKGnMmpZ7JB0ovuV10JQcIRodZE7zBt/dFl7A0UeojUdruzaKoi1M5Oo4s+
N+RqZC+m2v3uLg+/j8TQzCFXzk2/jXzAmhOGso6x84U6bly1deCaXcyPsNy+WFe8
LWs+hdxEV05Cf95fyMZqAeBY72KDhIyVyxJVS3JEfueXW/vVRJ17RzD5XHbS0Yo4
jE89ZSHuBRwixOAx4exhWjzmxvuRdyjIifeBbst1hQk2h7HUInNm7284EFfzx5WB
9ocH17VXeywuHAwIRJGOIA==
`pragma protect end_protected
