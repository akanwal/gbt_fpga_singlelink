// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:54 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
QBxmwIaBaIHriMkVy6nQOJLv4Yo01NpGWaz1XH3a2v9YxUNBsDUdih/VoQjlzSNC
gGdc59nZpI0THoL+O11gSaze8ND964U6UwH8fP1IAcq7JAbtVzbYyEOjFh6TzsTR
n1fDn74DvzGfqj4KYldlYCkdhP/mAYaLw3wTNKdtqnk=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3200)
+YlBMKEr3EkMTIH9V0LKMxdHZZ0WNrejsYY/yuK9Dm7DcpKkrkUJQlCQ9d2Q4DgR
JsbzFKTlfHqXRsqXvktv7nZkvnzSs68YVeYl7FI18wcuUoBzU3RxPPDJxhFYK0E7
tQASapRoxpis0sLC18X073QPHgJrYpU5VIEmEenFwklod7gcIJkRiUXZn+oEKO6/
57vtgyFpNtulPgCEFAF+EPpHqF3bGMIu4p1bLivn2xKsV8M7b2MwsZBmNggD3juE
msurWB25/Z4gpxgOgxk/w6XlOtxOaQif52yWwfxklOZqFAXzxsDZgp+zIuNaUjdU
j0SWcVI1wNSw5um+jVh4j8OPSf8gh/SZ2eY00as2yiLK8mxyFVIu/FXpPwEqrTIF
H74pCNrNzahCwBUjcpPov/p2iPKdygKym3pk0Jz0DsRLEKtYOKAD2XhcVimIfVmi
0WlNZqm5AZmu+dsn5hL2QUMunK9vyvnyTxwAmtW2XPsodc1NcgOjfYJKdwGNEbY5
ZbKQK9MB+23rO8neeiGWpb9xtIHNlWXaLEd+92p7XFy9GGegWdQhpc7zj4GRUcfN
40aTdOHVDLyJXi+nax1PsIYAJk8cqZKKJAhde1bmz1sSAnY3jSWLKwsMbQm8L3FR
P6YKgct9UZ1VhenvU5gtSS4eDxUHnBP9eN0Fva+g9D7o7lo060bgp3OrgqmDt3Li
Gk8psFZvF6P6/dXyA8S4pLxo2zO5iF0XCW9SsS/4+PZFzts1rDa5A3iE0WeIJ1VU
LrBayyXgl0kDNpuk8SX67QBSqhd2+l05yymQdSZb7wAZb8sNpoD+Mgk6mlzHoc7s
vUP8Hq3R7WSHNyIijYtKBcYwv8gKLQTjVGqNrpRFEoVvxg1yK7/DEpfUvw3o9KS5
nmoDPmIlg0ArynNr/hFgdj03fWgKKHSPzgl1ctJCp5isp1LK7zwSRrnhZsR4A57j
a7JIcgQmHuEFxx3wIXNBoI1zfTVPlCKFwm3Z+jq6T8MGz88B1FhKhoa7DWItUuHp
t2VcgnH2SZ/65q8dN0egaMFW/cL0WsvAc2DF5s8P6sZHGSVftiMeA9da/HKUkGvJ
1iMQ770G2m1YkzncrMdVodOqGHKZLod6y2qYYAt3zlStBcVtIo44FNuqg6bsLO7X
49c69H8yz+GNn/10UnLy4aK6JJzp58VF/w+piFTG8g+cKCts3d0mqTM5de7a4pwf
W26Jgk7llE2BoD85nKGQsMGld3Dv6gwzw9WW3AgldJswetWXEwK9swZ3tiMUEJ+V
o7ygy1s/cZo49sGi/iuX3d3zcgA1LY/engFiGJPMoQp8BUTGpOosQRhKqTwTkPZY
P06t69Rj6sr0rx59RaDBV+ButDUXW3ClXtcJQLYUUL2XSw0olsTnN4uQwBCMk44h
wZ7w5tSzXmOTMxUPVO8NAqrj7FE9rTPgyLEYqytWr4Jg+X87jXNUIGFI7pVfKmM0
zCCSkA5mwzASBptH/mXmgPcmzzYlq/BClhBP3BCOFqQH+9FSgCIQI71QLP6MrItj
MUWPSwvYyMB4R36GHx4NLUIlX9ridBIMiYmUyIy5kNsUQ1Tj9yuzSO0AeB1vryX3
lg0ciiyjdwYIEeCemky+Xfhm4B+x/S4DrgwuHtYFjBhTe+y8ranD3Ka0hShFd4tA
xgG1shLmVEoxJ9Be0q3btOSmjak8yIrY7szw1lh1tpp+ORxK9MZIWNH34ITGIvPB
HyyToKErGxURem1Y3VvkgiunOgqV+0j9f3iqCW7zBAK6WExbwZzeGSIdbzOQKJu6
gVdf0DKvz7C0ICssQdOvfweLonidnEI6yUa024t9l9Yx9SVGxjdjdF2cVbZR80xF
fkvKVnahbvNJxiqYM9/VSj+wj3nbtc2isUvTFk69Xz4FhkJ4F6T0HrRx8wcZgeRY
Yaa9bnL8VkXqYQ6cMvpV7bJAH60bQ6onxec7j3QKj0zrQoDr19vwpE0qPNKZL6rR
9qLoP86OOVgoZRi2G1hoL+LOfSGetRt0Pvb9LWYmpPDKwaYE1S1VXe8wpO7pYN+I
Tc9DY8MLpD+jj2MRH0Iv50jg+FS7V/RGmDehzuz06FqPY4e8yDHBu3hJRlgw5+LZ
RN4vl7nHwmUyUVfjHXVQlm+mYW3ql+Fuw7fiG1Ic0YtuWEz+gGjelOMuUXUH/H5W
9wdXEYA0YaulHKTyvCVMIBuU4n6DIC60keza/ihDxwHeECtgNtFfeudPfhC+0SPW
HV5wHUhjEjlxA/TNK+nYkDejf3NfT7eyIMKxoZZEy/JqDXxVSuI/GfzaGsVcZVl8
5aS1ioXZ2gXDRbMutBScmw+oCD1LV1eMRm7eD5OCLuFA2Hlg+wvSbXMZXQtZNUlY
lHbeQ65dyduzeGIxKvvPRblOgFq8goO1fKRor3l0nF9RrU7+v0HUHKGwhJJVHS4k
gDABB0CqNk1F7RUIM2b4K2FM58kDiWNq5LoyL8ioB9EA8nBRuPEzgqrNVHn7I2pK
KP/iXK2ZTSvl2A4bkxzB+Vw/0cV0YY5f27OijbAFQLy7DK7rkErKwJZR/1rPrZdk
/1kNtoDWl306H9gGY+2/ltdY18v8f+amvPtLpnTWzDgNx14FVzqa7WXgvlgEvcTG
GTj8LouunS2TRMZm+70ub6rQesQelM/Yyo8cDi5ebM3qk+OnAn64rwSw9nXJbY8A
kvF8Oi5I1l0X6KAjzGQZa1ivCoWZqIQw9bhCY9QNBCcaSi7bNnaUaHk34/Niyzoo
XgiCaLZiFkOwTa1rrXZGUqpgahxpQ3kvEXJLylaLQWHlVnnU4WGOAp+nIa/X/5+n
DjiytLhG1477yTt5e3HX/NdLN1PLlrk5yyX5jpr/4PWYVzTUVbhT6NvQ7vRdhYu+
ZOR0Dpcx0leeZiedMjdM4F61BDGk8T+TDwlLIV1QamS1PlJkSvKNLxnPb8dzNyV4
sXM5jJkx20jcq02pVyqSxtWsZLgaHqcnsJMqDUTEuSZWG3Ia3BHdAZyxIIt6tu5Y
k/3guVMHHLA9uoL4k0i8wXTXgLLucSk98J67lLH2dz5BfPGoeuThXTCXy+YGLapM
gy/qFf0LBoKZNNpLE9QO/r3sHL0zc6qjc9EtoWUAcawyjpdISpGWH8T1nHWz6Tuz
ASWHLf6rZEYS+Gvl90RID8WTv1jB4FPmr7QzQ7mIPs2DSNmcmbM9YJvu8cEWj7TA
kFxqIcQ3JeVuOzRBmwqHAem32Dzi6FEAo80pS8VSMAfxQLq2sxZZKQtEiSxFgZSl
E6+qQgyPyqekwy91FBan4Kp/lpK8fV9JCHXREI+e4YB9LfOFIoVopD3JvCiRUrBx
1S8Mm5Tax7YiL+7FkIVPdqagkDRbsSpvz32n/gcnnf907l5wQmW9ghAZndXDSFhX
5lb7GFcuR2/j1QJ4FCgp7mROFVzhgPWVoLiqeIWIYAq/DElNL/nu/4czgciU/0UY
yMbVjcDq3MokK9t8ap3aaseXNpytsVX8dZjS5DRZzgrISsc5iz7AoTMcMsVEh5EB
xKy9aHAP4XxqxhkhCTadYlK8bGzTicVyJpuOJAbCSqq21YNpEAsf0xwvw/hweRSd
2bSLPDQ0EiLulRd6C1xzyIVjYHdgeoCk3r85ADQDDMz2MEPV6nxsJuEJ69T+P07d
U7UGVqNK4bZsqca201Y0x69ImO5T0WYVLHawV3rIRFiYN3RV2FFZutRt/hh5TISk
mL+YiBiNUi5ayiFIckKzIPrEagvmtm5rQ/7/231dN9RE5oCueUN++QtZBnjMRFFn
JNl51xvmUXjHxy70Ti9wwb/QTRQy6IJlzYu8LcYP1dz7ygDP+1ik9iu8Gqxgp60L
n07C5JvjNVesSNKiCyM85TkVa8ugofL05MUq+ETaP0/8/r/JXIVXFLvq1tw+bgsM
PLfitm1EdesZ0HQfhKF2cSvE5Kf1chVzVI+emlOUpc99KG7txb5M0vFVcxO8pNyI
7gFWZhfELOFYwY5SmwKsPUhC9EYMuY0WbJl+Dsa3vqQC9zA27NN0hL11Y09Enqbo
ko4T3nUdE8b3Ovp556cDxZb+s9Tjfwi18ciKmEUXQkPVDZzYPm/3+wbRFXlHo6lY
lHWRMzrXXGAKz0/OWazu8xiOyQ23p5vF8JmGiX+IhYNSjypGN96fDvb2hJ4MjjWz
o1GBXYubYDu9sO1gVAVzJwgskMy5oLTyJytn262IHvX1TGF5RYEdw14MkK+KMV8s
WMrrwGuM/Vhtb3QXg6Gqw/Sc5Pg/9HCRBlrJ6lMix2E=
`pragma protect end_protected
