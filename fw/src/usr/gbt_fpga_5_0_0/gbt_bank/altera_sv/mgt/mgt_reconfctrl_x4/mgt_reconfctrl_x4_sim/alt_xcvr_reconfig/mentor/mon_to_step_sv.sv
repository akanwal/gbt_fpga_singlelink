// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:56 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
KmSa4P8pGWxlzP5SYAzUTqP5t24c0hZMo0rNb0x9xIPoNlsXTH03xiptADI91Gyb
PEJj84SUNkzEQ5KHoaRIiFNQCydAMHHTRGo7M0bZ73Jbsr49+cWJJdkCEY2U8KiH
R4vefJX1Wi201kmiFhBY2+H4yLPwQ9/UyoW6BJo+FA8=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3600)
EqgdAD+k2M01PPi7B+bz6jN+erRgQW6KwYrV5hmRHLcOxACp04HOtjvyd5tEo36Z
DHgXBH74DWPI+xg7PK4taWbyNL74xR2QP16PUpYXm8BZOomcRxLn/FVWoJA8f9R4
712ThfPqZ60/xTCDtxfPX5jvC6XZ2MsDtdClBgXXTckvRuzkBr2+foPw05aAZqmv
wWup0qNXO8n6saZTfp9wByx7c70AURqbEyi5NJg1plQvchkFrFVDI8Og0IxdzJlO
ghjD4ldhq/P7FoQVhX2xff1UWbnWosIX5HjayrZBr0yE+3tZ7FnKB7mgzXFrCmZv
8eYNp2bcOSvuJX530OHDyzqaJ5PWdV3mPiqq1ni8MR8JEDyLS3tgD47SASc8KVas
M+TYh7hxpWk0l/Sm5I8Rf9O8cglIBVNGoi6DU1ofzBAZes2p/K6EOLvszc799os4
mHg6Ur36wrECJw80kJWcou2Bwa8Tv8ZTVIXDn5CCAsZ5DdPAgkrKDFMEmL3Hb+oN
4t/JyZ1oTg8tckGvuQgesM7mXqL7lJIPR4uSneFhhohu2Iavu0L1NZSbmD5bAA54
BYYK23jqT9B0Ej6bPmOgsLUOa3NN5fQwTocqIX7tsyaNVAuWPftbJoQf8NAqjvvl
kca684nl0qjNm3gIwfXeWlv9gaCiAbPG1pNcrFJzNAnDU86RVh3MgqQyFkZ0CIIC
+Ha/NpnHwhohYafIvmD7H+MmXD2Vfj489mIZ8uffCvn+nIV6KUVc2fjSF2jOwAlx
nCKfFuwAuNQ8jbJ8GSUEQkkxWWtCVZ3lzdwVJw2zCJrpnZ3GvJwbWnFP2utNU4IZ
vKxpF9tVT21D8vh3SUazA78n5BlU40kDQWqQEbT9ZWMVrwbqp+f6pgVqmzvgpDba
ts50c6ywWqiamVgAb/vRA9iU5BK1q96p0/i+VK1T8sgMFE7oPUBEv9l/46r27+zO
8YodzZJuu1/BqKQvv/8glawai9Cso8qeL7Meb4c4PF7v6bTYhOVEyu5Pb71ya/Np
uLv3GStLQk331FAZNCvdBk8FQJWrBXqjxBn6qS+uoj/TJN+rH577zn9HWMX5dV5I
OPjpWELe/lY5vIvPfjqLDoV1QsTd2LpIPpYUqnBjOywB/2/MhEjQ2oxieV2DN7KI
HJUzTIX++Q4VKAQlfJyFm336ICnPsBHGOIMrKEio5ZvwdmHY9Ua78JyrZGmPBy6Y
bhn3NzK2DgIpLmupEY2AphoPE7oehMWIVo3djwbjhvhhju0jaFAs+j19cnfGngoK
NrcSLQARFyyx6tDxM8KRFnv/B26jcl2OFzutmJBQB/79jYHYRHJNIHMqlalrnswz
shfx2YUEMtT5dU9fapbjmsVTbq7oPk7sqaYCchR0mGz/7F+aDPEmMdvmSOsMIyGy
ctBF/wFR9q2/MZaC6Hg8xPZk/5+R6hwE8fE70C8qHC54RK9T88crtwc0/olsZ6rr
NlJoTYLsLHAfj0v8NQaozsscXopce2Wm3nhtz2N7OMOn4d1udXrWlDT/Y0+rlY6S
NRHOq2tuS2ch69TZhwz7y0f17bXcUnVwxHHDOpAVYzOrpt+DZtaTdYY8oZxUNqNf
Mth3lY3SncEplXYqY/fTgrFMQDRyG7IOChhAE+n28qMmuKtus/Fvrl4lH1TSfe19
qvhSeUYSnIV6T8WRMQybhUBTCT4QLSFYW+b6IHXzKTE2c53BbS57eZHydtfBf6Eu
Wfs5O0cb5+Dfg6peji+D7aTLAsuJQ+qfJLGsTH2JyTm1pBxgc6aZarW+RwGwzbmw
BUGaj9e3rvxg9ZS67j0DKiwkxAurZIU/dxqTLjcETGjsPZt2DmxCUaFkWhwQsAjJ
cpWBkY6BZ8FTQJPO/8B3eZe3Jk6/OuG641eQ8GDETpTTfx1r/khPg5sq2Iqasj73
Oulmc7rRMJRvxMnmMLP/Km1rGSwqhmKDn7BKP0PRXNRTbv+fj2lAxAjCuPAMQ4My
tgk/F/4kNSxZ9q7stF5oGsB5FgWqmz+vIO7n9BUOoQqR76HZdrHIo7ZpRTPuc435
FKngtL5IeBg0irEQFekFaRfE3ZvX81SBSmPOW/GvKdd0Ecw4/yJsMvoQA2CvEJzp
Kjfs0kjF80TcOJLcOaEtlwC3ArACJlKeDi0lEIzdB/V9Nn+v1B4ouQfFrbs1QmQC
4XN8ZYSjeCYbiOSTMX2pjgdHa0PKxYvDgMM2edzTxyIS0s9cZzYoxbVD44toR7XV
iarRf3pIFBkIz0prhOiVOGlPO56jcst5jdO8LCqeZ2iJicHor5E9YbR/khyC6lSh
lqqBC5Nj0zrdRTtxqQ1z9yVV27+bRbftt/qbPnBOhuoGu+owZb40mCRUJ1oenfhJ
tRYfLaJIu6XJNdRvGQADa83lnv5TByZTdAuZh1cTC2F2a2f+9STFH/EHRFRCsmjD
V2WsVOKCfXSIKL/fYD8nKmHzG/ToYiWvXeXToxts6Huifgm4vLoPucmqAHCf2832
OuGM+A5h1GH+Slmjdn8Kwk26NMUod5pXLwdNerIlrOhVjsSYygEyPtahfwHE7iZX
ik1iB6K1VWsyTyEqpJFLSH7AbOro5Y509l0lGhzlTDnvr1RcMvF12g0eIBXXEE8a
vTZ87Vb1TuqKHMfK/VIHzuKrxjc5vNqMIcZ2s4iCfZBMJy+1lvrN1SHSvcB/orB3
zabOQ7/tu9uhSUo3OVuYGPScPidHYJSoYm3SrVCdtXybGpPTVxRnbFBxj6vqwBux
+2chVOPmh3QBijoTZN09LPWcrdBO1H8OstV5HINxsscRi8bh8wnrt5TCGha0XlSk
gm0/WePY92Tp6Pj1q2XBxVqm8GGm5WzqilQWmdV6hiuUQbzccihPVUagixBCnCci
AMoXLtbZLUqGvYBaR9tW1PJku2ZqnScSiV6Zrv+IX1p9oJrKGnHpJ5UiaW57tCKe
/euuHolLFN8GUnlwoAXkmxCwxqXQJ5cjY88cp3HdrDTh9CNNOapCmGnfVWYJ5Pzi
YLFk7CKbqhk9yubGgGcs4Bwgxee+QN4+1Udh1hdWTL7kqH2Cq7dXl68b9JFWn/me
7/4M9b6eNG4vA26UJPANPeH0r46PPufCNdo/E7Wk580nyw2oDncdvmqJIQDBJxgF
LoAgtvu0qrQdjtB3yn5IgasJFlPC+Ft9KOq5kr0QdOJP04Gl0bEmEKsOfWOYfdM+
Ir+3GX1jMOPXKwNmy66rQLZXZ9mHHN/2v8EFhLsRlvmAU4wK0yneeYATiqK3dz+n
Ed2mhyWjn0ZUp5GpOouWODgiB5X/N8R+NuzeB1Lm4EJVyVa2L9Z9eRnSF0cPbqRK
CGsCnM6VrfcRP7XK3yFvAg94/T5fu0cCTyQ0Yv7pt7pAwwdMtgZdrTWl95YNjbq+
1RWYBS+z7rx15QgcOi/diClJb8w0r4Umjrjt4ZDJe9kwDrAdbAPr8zhU2SywXGXn
QA4JEb6rEINwcqA6pcyvW9BRXMCAC29P5BqvyXkG7fvsDJVc6Xxf3WeTGqN4bufM
vVzfRuoflZbMTWluxlCfIPuO+IDEzrqWzTU8ruGSl4Mcn27v+gWfZMRikHwJNi0s
hEtuDvT2SMDJAyxSVUWnjL8jZfHnpz2F7xmsiKxeMoygBrah0rsEiAs9LxF/Ir7J
XScg0tEBwQKRfnEYs3PfrRvUUja3yUVGUkMo1Ydagiw7gn3iZgvEC4wFO0/KGf8+
p1OdM5lxOg2aZ/1CWzKwii54SLcBcne7ukDW16GbxthPaK9hyj5+7iE6/kyKAsa+
qvxa+GrSJh38kuapB0IomkeS1u8anUjzbnDMYWhdffSrHi/uXQ4dxwrm/IRi3TJL
Dl2jFWN5pJD/UEVJ1yoKj7H9wTAvLkXOB4LYpUfu8Fw45/C5Z+p+dMYQ5poo5urM
ODYdzErYQ+xEtoQvQeqK8Rf7UYkYBkmalD4rNQIuLCwE2xr0ZMDvPek4ivsM7TLu
odk+L0nUUe12fqJdvNeqts9LQR6oQgItwbsRHDR/2rr2+OObN5jLxww7Fh6B22Gu
EtRq1Uty5wGcWdsPLdWPjmJKHnnUZGEdheJTZXBbIbECHmGfNxBF8h4Gxj63915f
1QYZbtCN1GZdi+DvEi2QOm4TNZVLqsnl+JU2IOw4CM9TFzBljEiEKQlSYhdVXLZq
vCuDwJNT+vSetgzGYFbVFXcLrI2W+Lifl4G/oywPH+v+9FueTlytqacXs5LCbkzt
m7Utpit5S8KYYT3v4v+m1/VASEYsMwvQ1DXzPWSzlC1FTMOr9eFnoAgjX6Wz4iSU
jQIhfVDr1FHfZANbcrOKTjMlleP9tw/n0ddk6U1ixyQkcFpG7clTsDxdaHgGANZy
7YND1leEXR7zE1VoS8/TIllPaHWrh305xUH91q82+MDBALXxic6T9HifFuwrDv0T
qTOpHU89C+VOOmh2cwACAFtNO3fX4wJsijo4Oheagh2/4cfJ/AaIUhAT041c4Ngr
OQinfnQxusk7+Y1P+tCENXX3VSwdw3rizbNYsDcnKJMMqjfwS8pl/nfviLbaocTz
BSNE81DJP1v4yPnGrjtRb6zC6OZ0a/AaLUAvy0jLkh5w+XPJGjOzbxorVd61KWhJ
fGOLYcoXFvkYvce78dEdNEszFgjWRC0BAbHN2h3BAXGoHXaOdSkeNEviHeB74ZnU
zfceTNBA0DwOGqUetsiLoOcORydcyvimidhmReDDi0kqRJl53jFVsBfW0Uqau9mi
7Rhh7wjAkAolTShVgyLPWjGqcNR98CKmMgCihnWHGvC4xsMArZdKtc5FVADD/GKl
`pragma protect end_protected
