// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:52 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
cDB/Yk2hjjjZqc1c/STteoAtO50cIYkls0WgXpK9AFZ7qUSqWqwsclAyDfd7IVLh
/vp5SoX3lLFrQISGetcT9Scvg34iCPQr8CSAK7fTTxSU5kK1qnODQiJGdiiULGUD
S5DEYZq+RDqpkbR62kvHmCZF8ygokS9yrwjVH3ckBHs=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5104)
SV6edil0o11qAyKJYPdXnTlPRukmrtg9m64xrooEM5UC0F2KfsYLW9R62RLzvrlu
+3iCIpQTtWkIm3F8GurzWzvxXR8MNz/rim9jd0ivxxWo6RiMlto9eq1Xfu/wi3qk
jL1wCOeGXRNx+XFcTdPtm5mtvrwswwTI7UV0GvgMihxyq6TFe/NIDaeWU6onOBdY
HyYpUCmCEzJ8pUNniVQyKzeQvXIc/v00QEcuMYGURgqx7gNp+hQCkWJ7M6pap2Od
RacdSRdEQebiozi/yy8F//NNTG6wkeGR4h5P87vkrVAYtO7X8ruJnXpursHJKQe3
NmEylxEl5bgDGp5RLlUangrKKWsydBZ0TBBW9JnL9zCtdLRoRQYeQvvIGr/jJhHW
YMTPhXgu2Pi7lDkG8tOYPSdFxb++XiuIM8bOWP3R8MPB/+iHXn2Hk3Nl2h75QLvj
9eX4UVEZKQI+eV46MHS/DUTb8M+wlTSQRafJC0v3AgaSVKxHOt4suDbtoBDuwvPv
BmMbczOtMuecWjwecx8RcLgJTi18ibPe/iCtsR/0TbDhzqy1wWJBzd3T6flvLhZb
RMQcTWg5VjljIPRMe5rPuyc+Xpdijj4u7AM6+rO9aRpZ4Y0Tj5thtDMEqR01eyTt
XmHgEKXxRZHffzbUMSfdBVQDqINo4hecouxUdMeDOwnzPVJeTUqY6qjDzRmNUZRC
u0gsMn88rlTPQItJVaFxCXmNfve3vfRwXDvbMpHhJ1WOj/XYx8pVDpcCLtt1dngo
sgp2jK5cUIsDQ4Cu3ld3I5vdT5wfwT7mdnuP4PDosLpJCAgOvgf94Nc+krM8EJHl
wP2QEzQ9XzkHqn0wgPobQ1jrZUbV3Sr4Ds/4bRu1pi2JKDPHgnvmq3aakfvf6UYL
XNMyb6mP39B1rspD2utyV+yUGIp2m+WLS1XA/fL5jYH7Hfi2Y4Es8ts1gAL1/YBP
SSyuh4Uf84mpivHNB/BcQ3Hb1iB4CkGg8tWseyb5lP60IcrUDRSNL/a0plYBAC/S
MxisE1U/1uI8vqZ9j57EANv3JlswFxfrxdkg+XSULZbAzzUtIrC7rSwECSzFIdXB
1t2mhyfqU1S/v+wEd/Kz3ycm/E/UQHDQPHTVeowrVNgkt9NvIVwFayjv9E20UMsh
NJG02sPOrk+Icb5FFqS09rdmTTI8Sl7JqNaSKx3AMr4xCpFIwOe/2JwpjUVPTVxf
tG3bHYwMlWTxzMLnt7s6dIY80+l/83XXtCvYhiLpw6kfvNqleWx8E3NRjRFdXXjQ
nKLPvD/jpshUN0u023K9/NVprBoNLbH7dZP2WQbtSdhm+x3l49RkWCkhDIOv3T/U
xToe270wwdWkP/AJpyNnPXnlar2T8ur6IGDWkGIPlzjIwdK3dXOQRa/ybtDprK9N
/GtUVvTR62vm5Cwx/bpf1JvBI+CBwdQ6/a4/c8VmWRvqOVvO/yjty/wbvH+2Li2G
ch9a92+cWkCtjZH0fOt5GI47XoD1ngixhcgrr1BuQfo4lP0MaDXQcsNsR1sh/VjG
qiQoT8FfOTJny7hpHtOC+I7BuIksRV2nvq/ngTSt0gvdBnGbL0qYM512RpkQTBV8
pqyF+phGRVUhLBZcSTK6v/g0obneetqvFsRgeZwQCwMWKal3QB+NSFytWYeiuVgL
8Gn7q1lh3gH2iVUqujustR5MjVCmzZYfVxm2lGUC9qjJZuk0s699es3s2uVgxguV
W1E2DterYG75S1mrBAjcyEv31syS0KWqXyyrx5HkDWJQwUXebDg0RhymUB0aXUqy
RLzxaPX8+crAXT7ILRxD7Al0pPkfpOzC+U9nbPAQasXbaEKYOlxpptcxgLhcLUBK
J9/sSs3EZusbmH27ZHxw0erNczklsHmi4gt1EVp1JzyL1sT18t+iYbDna8Ht09sI
5MtVhir/ioNj4sCMZKVqy343nNASPfYFTpIbUbbFmp3OjXDZulrQxLcoNrM0d9tS
rM+3nSZ6h0u19jYa500Wv2hvX1rY6BICuinHiEfHLQnsJPh+M1if9YnvS/UYhuV0
+RQY1KKEIBtM2iWpnHKOhGFlaO8wqK9skJonOlBtZrhuIQLseWfxYvgIQdCz7nBZ
giznRGrB0Ow0o75HaZhmhyVyfkxF+5mfVv7z40N2gSscjCRh6S6fHqKdC5Kdgd0j
q67w2dDwJBzkhqTEoAeVPXbfK2uU8QWo3lo9nTM6w/zidw+pHz3iKFrf/8/2xS9T
Y663pMxR4STKd6t2orCd7N23mpJWa6fTGpitkydpW8BdtDh8TmrRx6xIyZ2yW7C1
CvgHK03Os6Cwm85tMhrxDJjpUQwIBCbtaYIzVVLyatcWtoeZ7aU8CRiT7mO/VwI+
FE94aJ3ZtLFWZpcSfsJXGMjcSRZq4ousfDcaNYmYyQMYIjrsz2hhSRLkbCYkyVhC
TywHvfq4DKiHI7G4WHFVB9vWUsYGSDH6vJ1XjA8u+l5it5ffRq4weK/VIi14nnCo
bvlW6zzr2Y2sezLEicaLtmbHKDmKI02LvWze9rI/K4OZHW8Gg2y8Si+e2Je5K1ML
4kQ2R/leJAPQM9JNk48QS6EZldT+a3Oa4AwU4cWLee9k18p2X50QykHJnQ76LDR3
apcpHoVq1qUdsakLqwC4ji/9m8IJ6wftMP9kBatXutsjp0jHAHIRQIiu3kmz/h+c
BV2IpkkT1+q6xpxRMY8+K8blZL4va+MJgXs6twqXUGfvmdHtqSf4zuNtOudNubVW
rGD49GY+l5M+eDlrN6mYPBlVEKNM1vtGms3ZSfTYLjNZxV1TtWPnUOFkehtMu8T7
k7gIERxX31PvSn756xZoLt20pQ1ncyjQqV4TkFRtLnLH70liYpL2LxqhrzUySufq
vy12bdfuF9bUPt9TbAM5qbQN9mNbt6V0t9EGL8SS9O8cgAIYmy1YeW9yTD2IDEsO
3dgHdTQ66Sol8Txt4uKvS9uE8Yx4PZTvokl1Wro9d2OQ5uQuEu8MbOkegTAeQWk+
CWSSCYD1Uom/P5xfqXHmJzXdbSPAWyGf4dcMIRffrNt6uZoVkF3UH7X7uggzBCy9
6SQMOJGBj5woOqUDi8AjIXz6rYEOWF5INUZE8bQnT/ZSKdDQlJFAJ1v1qyO/JSzH
CcFRGmCgwZZghSGdG2pH/ckGZqvyL8qDDHVZzx1D2n260rcv02tSUa7kxcfspev9
/FTCeGgvEpv93YVtWK+qFxt1xJ2xx1WvW1YH8NqnsZ092am1DhbuicxEJQIY8tIB
J/KBSfBbjyBTweSSP0VMJv8CnYkjXfbqP7WOqrtuPTrbIWHDr/OqTGNzZXsyD4+L
oSGFQZ/VV4W+jwmmVSi+JKomx+Z69EiBmqlAlOBnP7n5z+egdSohnSKstMqQGHxw
uxJjamJOKZ0bb3LWpfttddXKMimqCIosWDyUSW4kE0UxoQx77gqF+mnlnPi3Rm+i
P5hRsQ/YkPjYKebgPr2YY8wGB5Ag2pwTxaxAIA66gA0uAa2g8BjQ79IHtvl07acS
zAEBR50zIA+Js9P7r1xuN+Vz2xECPzzCx71uznDg2ujEMpMujGczLBz6wt5Vr+5Q
JV5y+7xJ1u77ctgjHTFkvu9psCUphBymy0c1YrRghUje8dnbtYpZJEd9V7Nc/9am
c6zyytnl+JnaU8Ayk1lEsm4Im70jjToMpRsD1imjLB2jvJ4HyvgeO6UwtsISAdpW
oZLKitqbyGFSo+HRsZTCZhmVdEdPUcc0ErB/pRYr/XaVLVh6Zyxi7vGbXQOqeh6F
Fyw5JPqsCyt53UGLJpWShzbxfPHNrtG2pp82DkfLD5JT9Bf8OdheN/om5W2TuIiB
QffSY/ycZskzqqBlnco77ozFwzWlebKIrzf/NmFsvMSXcOdE9dSl6Y/3LCAEJnLM
BmGEd111R3Ad94VGWCvKTfUlYj45jalghIsixThS7mLiO6jNsbvJHXUyb+ba2FsP
+lMdDB6yoiYy5VBDYPgpx3/lJh/HrV4zNC9ZlrDs1vsW1eP5DIkWklob2KsNqDVn
4OR44ps9uZ0Fq1/Gd8cIpv+ZnWbPZOEV1GFqmLAenrSC1CrDATJFhT8doVPeZZtR
XA3Mrpl1DUqKbN0LRz+CreHS6NCFWHYziHVDjgE6Kz/DWzVls7zdiWVvmCg+l+gi
mwGnjFz3n7Q+1TSWb+Pgp9Rx7l1t6LPEmXwCyf5yC6OQ+irfkRiZEl3jpx0VtxOH
1G8KVW1XR6nhwazS7RAsSPsanw0tv7dVJipIq5v+cpe7E36pMkRO4s1Rns4qJm9L
Q+g6rJ0J4IxWsx4OOLNvY2dTYP0jTYjwmlm5r4MuF7+EELgLqBBp8JaxLZIMVJOv
DBKbz9WD3SqHC6VhJrtMJ/QpD9alUq0HDRFxiHmkRa0AFsT0m8jzK1XfI52PKQFC
ljB7SlxlyerfJ3BVVLUG88JBP42rGdHpxgBfj1Ie+do84Ty2650qU62TacDHd2tP
RbKVpIv9MlgkTIQQSabpUFGt/JOaXsrj674Lizb8VUIW+pZmBNTJ2+7YQqEnfLwM
UhvjCZbdSpJDTJD9X1ypwkc0z0i7ZZWKxfiAGtjNncURNQc2XEhRLoLXvuEILLEm
NHqQDKaMkBe05zz+5dyTVs+tnGpwlGRgaNYsOitA/eMicFvNIrr5VWuyNUwFJlQe
L+/MZQl4MwygBnC4pZrk5d3nPjcweYWYjNDVWLlHXls7ZZo9vPayf58IzaoFu+sQ
tz7CqXhSxMXAaSa+pswO7+WqPyhH5ByAItTwF3aRhhavRxKNajCKBlwc4HfRuiI0
Zp0l+Or7+nfVmBFbTJVoooZlnsJ1ijnAF+BS0tVLYM2wo3iJ9S79NZ+QIN1KCSu7
khA92QjgMqS7dtzWteUbvzA3IYydrLVsmFZDIBwWRLNEXIVTcLT5sHwfjtXJsr9W
lbbBMVP9yOxhwgh9GsFcqncCtvnzpvKa02Tm4XBTU62zpCF6qG0TmWzOThHVj/YI
/UAYF2sPWvJDN0yNe2RUcVOlPVnMwMFBCr2MHiczUhbZUEOzcJCUWdphZ/1A8hbM
m8Ymy4a5gctgXhRei8BZsZHCK+nvmuyzs7yaesfpzWGvdfnEJpHPvLoQzaNmOFhO
J4yCwX60MfaDP5NHl1GYAsfKEitQkaswWytf6F3hkM9K0brRKIr9T2Whxjc9RRB0
tohsEZPueXeTS4y7lR63hpd58/jtCtquzDnB6cLrf2+MyTOUQwJB12U4XG80ezdh
lPtHc9LtFttxvKSshkdGYEwvkFVUV2KOESmVeSpzDrkuMl7srtoAPOq0ykBSCatw
vlYl8prGaP3Y8f62AyqA2D/vlN1A/movJ5J0QbrOxDyZNrHB/PxEc8BHxhwwM4j9
HJcm6RrqdXQ53dgH0L3QE92lI/Nb3j0Rxb2FbASo4oeojXpV5WgwFQoP6MTU00GP
NnfP9iNfZ0zz8tDJzohxH2wXIAPLhxv/lxgI6a2Opx6aIiYw4pE1QkoJ4+bovqaY
4s4A/if+9tYwxoZMpEIG0RQNimIOpiJwXKO50Fuop2z3Ba6xKP7tiF6flfbK3+ho
H7pF9yNYd1OMRmnnrswzMzIroU8Tww7K0jPOyAsFRWSG905Y3whl1InS7EgxoS9T
71n7G7ebIz/+/tK9uzTz5aYa2dfZjnVpxkt1df4m6xa3/SHLLfdeFcO9+KMgoaWH
Zg9unXMzmJDXaG/Pa/IRLi8A5s3pYlg5rULn3i7d+F+yDVMH4YNqegHy+0eB+IZ1
Ztp414hNQwrBJQmgojbxRFG2fXcbYhC1+O3DwZh7So5OnFdYaiAyLFk9nhFFD4j2
zXfOrdV3thPIbRyYibEnk+Jk0bcq7oIPudWwnsXZdwrNGFgPJ+K5ua/CFCvh4o4O
iAC3pOzeMX3VmlEN8XUaL3d+hmf/7xabUPTGeQnogYE3OBYF8ZYrxDgfkM5+PMOC
EamY42KG4AheM3h+ubt/JMyP+QSjiGPnGCTVXt5LV0miPchIUc0HsTeF8pXI8L6V
tsdE4KxTI0ORkU8GE5c0BxiKiWnxcY+GqmlFYzBuZLBg+sKieq3KAw+PQ1Ase78t
VDp7eEu5jdFse1zYqI5Yn7XcP9QDdB58jygWIbF/Q3ZfUnfDMoYnHqC3zV4nWBMu
H9kGJ/oRPnQY4RJ7NOMB/hWiirxqNPnQ7yna5914su1TSoiuG8MghpJoiYXYZ+tM
7TsIwbkmxoVf9w/ApIIZ6uqF3GCyFLkc9MBhRb9rCENYl8G6osPKgM0n7oZfvQc3
vpz3oDcVtNwu8xAYzLQg+hd8Jo0de18LX3hEuVBKEo3DYwKYLw0iYDUA0yFSnaGa
lvduhalVW9WqWYxNHTABLQnpWrEHC4/4/Uq4Peb1NXD1jiFjBX3eGbl/PB8UyXv9
92iyZF5SOFKTLwMNDMOY413sHzJ+3919ugChUE6/lkdlB1YI7kgZSEPWUXm2W0Nc
org5KLOS2weOiuc//gFc3H7yH7ThjtIMjUHPukNZ1pf26QtgKvqf5yt8KW+jywJD
Luq5CGW1EGdJAjixTpbbj1s8zdZ2sEhHdzanM+2n1YrSNaaBbhXwLUkmRKp/dtU1
JO/ugu9pmCk/DtnjKYKFjWr831Xqn5Z7t1hVBKpigqzPT2HQbTZ4E4tBf+9sBT+T
r62EuQrsP1lL8gtws3mJgVTb5jC/JHMOxZ7+DeC5g0C6WnN809sAwX1yu2saPBLq
5ygWbHv5NchpHGHswcj3DXmps4Oi72oVzESN6122zn9h/iA+eiRPdb4usBbfRmrZ
sTPH/mEPx4SAAI0VUV9NAw==
`pragma protect end_protected
