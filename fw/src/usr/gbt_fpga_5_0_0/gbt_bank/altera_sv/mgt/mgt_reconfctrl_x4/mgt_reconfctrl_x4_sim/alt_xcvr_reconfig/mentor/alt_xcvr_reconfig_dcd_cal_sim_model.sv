// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:52 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
eb3MAkqbwY2LjzzzpH1ZFPLYZpE/2T91Q3Z7F22l8uFlNIeBgr8hnZYgfl3ON4MX
7vUn8xb7U0vylQfUWqbUIUrdb4PsOfIrf+GWUO86z6nzhqS0VDJY7IKbiVc/0ZXu
3xFy5UzrEKIhgatGl8chosKdbJkdglCATedn3yE2TuE=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2768)
WTvQL52yu/cB+tVkibyhBXBfLWJYJ41M7DtuU5Eg2xIWG/T3NDr3QL1cZ2oLWU6I
lazAM+uFQHysax1Afh2d5vbN/YYcKM4T2O6rEby89GdWxqoojX7rFtEdLOIyiSrV
qV4Hzn4KaqLoURMrP/CXtUZHWAqkPQRaCBdRBqWk+IB4d3oj16+wWjjg5h9NZphh
Amu+1kP7wUuI1fD02JPXV46oLKjiPn2hrp6muU5iyloeZ9UNfg0oD9J06DX7SLaG
Yv25NsiWtuqL8dXvFC5Abtjkx1sipS4D8EkgaEnSmIhU+rjviiu562r4bEx+xIxF
nmKZpuD5dIEIAR8+LsI7MqMa3AGWTWNJpgrUNKcY9LL4QPHgoRcOKbOdcldCdRmJ
YS/IAgC7dZfPRySRTLZZi91y/4cc0p8BY58+2cMqrD5aR8DzrXKe57JfoTRwW8Oi
js7h8wiDT1tLy6t43zO4HTyt1kvvpecZR0ac+f5veMcM9QHMwyL1DYS7PSM2eM7e
BE1+oL8DNi1sGfwuoow8uqtJcvwi1A5Mkaddib0Qmg44NS1jrCN2BpNzFUYmHHlk
D7ko5d1UOTZqbNRgbCqH7rNIaTZYBQZPTrqxr0JoLOLGjZRy8+kBq3QvHhcgsLTY
N4TnMmsU5XjdlEup/mesKpIfcz/NRkiKnvkTsEvtGPX4nns4JxIeLMQEVY0ihhsa
nOBFudV+CnmJ2odjh4SXwWSJJ2RlhJmL7fjujgANDIPMVl7U+sF0v5+/SV3azYZb
IYiSEAJQdHTP2PAaKV7380X1PGqbwI+0uC+l7bfPwZhWL9WGZRcAgMSwkzWz0TF+
eeiJsDc+zhDKFi6mk/Q6DHZgr1b/bUUC6sGAumLPu8NIetbAz8+dGbADr1ixdLrP
bTZyZooLcFHFSbsM1/AlDtRD5yZYw8JqBFMKkM4uHrTBGqinLZJAR7jL2mHuqBhg
vFceoZV8uWdG8A2uyl8916L3YHjSIS5q4Hwdc7uC8xYUVYVKYdECCdDzQJGR9/Y8
g7rsc4O5xHIrLx5xtd9/gtXGaYdho9JrI2Tz07ve1AYU948gpICm/q+2tqDY75wW
k4U45vYnQpCc82B1HwE+/XdohNeEUKu12+9jtfNvZaQmT8SfFN+k15qzSF5wGGaF
0HVBlfc5hpUutHs4oH57V79imESKzFlMp1ILJDqx+ynWfLd1zl02RIPwj2fBXgSZ
lOWB63Jukv0UuiLHtpOp+98Y5M7FJy2XNA1on31RCii9ykIg2mPwqBQjmEx+PGWa
P8iw82IG5j7Y4mbuf28hMBl4TRilxU5GcbQq0yeEK1xQajGdvW/VLn+uo7E1P3l/
IfPaMaukjZN+E5oQkyebNRmTDQ8eB2X2njaAvIUhgekTcg9B+7c5hvtSMc1dp8hV
Nys8eHBOrMvyO8uDc9eg2UpYswf/2HkxNXXbuAYMvM3Uts8PDdQFrpmjzKrlKTvT
oCO2Tf8i1rybiQBw4TkIXByE4wI5G7YPM/A6nZqaeQeOWATUL2PqNYNJElBo8sO2
jkP3hroYYaJTa01lr61pDFLprzzsc1OFgd6d6O5iR7rwgyGx/y4BySe6f8kP9T+t
0kDjn2B9nFE3SGGrkgIKQI/KUagUeIs0IOiZBLo6K1aFbkRa7wCvBCAgFpmu6qgs
BjS1Li/NxJ6emVOfnb4s7RzfetAoCEixGyH+r/aQpzWpuyIibHUpHFkmo0CqV2eY
wVdlIezB43+yz5A19lYrSZaw1PJIPDWiVWLAOUtdrB11gLOY6LBTZMto/EzWq0HS
YtkFNGTFAjqZk7WpjwcDf0qeXmb3edI4UbR4CSWSDPwN+C3fg6CR3bMHDPJVMZDK
YmPQ3GqUcehnhj7EKWHUzxb7YADj7MmKSHF4Nd3llUdbOtiOKln4viFanDrnJg38
muwFEw3vTBSmX+2z8AKha5CIel7T0L0DRoplXlm9zLV7g1vFQO5Gj/FMADGr5MAI
kBNQDLA4/3mdet9cfjU9ifd531UQHenUNwQfnQU4Z9lXHapu+3ZOk5yxS6IjWWnC
nb1rOyoNUlT/qCzp2No6VCz96+3nlwud7MfZDY18s3PA7wknHEDCdPnnnZvIOTh5
qQ+VawZnLxSLCWt1aLsaqKIab8inkA/WmpXfsQCfFRMhH2zlDTwGmQ6Y06WKKG4F
bQMxRbhb3+Q6vTVMSoyIxgXRgNl5gMDlGYOjZnSiV4mWmUfgAOfHj5bxFBRYV0DK
1+n1fI5rDOpGOQHpmp8x7WBrBJYiaEP0G4IEdG2RUuwqWJzpnHU7sxcRQwJJPOE4
Bc4Pkjc/pwTjNfSyp8mn2W9RA7WQSXc1dXRdgegWJyZ9pa0Yrw+BKA4OpL5Li3uc
nsU8Ew/e42W4em/ppWR6L8i9g05A4yqclvfQ1HOHz4oMzty/8HvK0ds+TcL+i2hY
EgaSU6yCLtfcQql+CqWgm4YYaqUS1eKXtn/efDO/udlb6izvVZC0ky31Z3yWgFUZ
oR+720/cAjx4d+swKD1xHNY3ggT9vhg9RYTlJrJ9DP7qch79JZXgos2sU/4sqF6N
VHC4rDVPezm9zam+WAufGLhfRS7r+BaD5TSRTg6ddcK4PTF3n6wSrlpjsB+ewxA8
M05zK3JhjHWxGZcDB8PmbWnr+20Mrh3Q/jrwddbB3WCG7RNsstezl4h+C7BTQuzP
YCtP+Adh7if1D/WWTVPtm233fUJg2QDJdVBnwHD+jp0XG07IFzDmriePl9qRqGp4
+mdDtQ8h7ZhcO0Lf/x5TKduFrFRiZiR2/qUmsWmwTPTpDWy2CI0ZAaAup11pSzRU
DFoVjgwF9xtppZL27fCCG041V3+W9nq4Sc+DZ0Mk7Qbxn2BSsSqDFseKK1yABT2d
jFRNUALdLNvIfZYYFPIixbuiirmxrRMxRmEDbEClSWw6Nh7V4iEr7okpG519jtUQ
prO5ClH3YVr0zQxppNxGmzVdLa9ouc7XB9DOH8QzPXDWpd9HLWVDFDjpIg2HA0aX
uQRbYsja8j6AROuo2WhpaJqnmh0B/2GHILCiY6+wZZU8brJJdY0Un4aEcD7seqLl
OMbz2u+pH3cJZ6JGIN0xIr6FHXYtCMRh+pauphLoaWr3m7UZo/tu52Da2SmSovgX
JQodB4WydhMI+wAQ1DDv20idX1hgrXX163KZJfIkH58D9pvjp7JrYwDf8YFm7LZp
vXXHH3uRzGulrhRGysjP0u6J0MHv3I2ZOJAYGbXe9bF6jFkLe/V0yl7yfbgLsqm7
J5/quRCSOliCDnyWJOYVF06JRsqXUWe25equsbmzpzJAe82+dLjwiRe7KguuReSa
UruZwQ7wOpuozHa8gY59yQRvAXWCHaBA0ls+KaiLoee1+NkT8XmLFOepOM8y7HPc
k6UHGs266vxhHDsv071dv9jMRRV0zAyBBhpch0VZ7mQeFFG6R9CI8fbIYwHTdGtg
k2SLD91HiAC0vhDAqjUnlgnn7+wDVaWiy+3l0WqakVSyHLnsXzouGBVs7ARZwC1c
mmry4LKwGSsQcWP/a+c2QX+gfS5bwcsC8wHvf5/0yG4/1In/nmoxXKCwMqp1zRc9
aQ0mZkcLoV15UFslUNLUoToXsOsAffqughTXx4VMfTxSDYxFn3YpRuSblGRok2GC
moLjcatDy0PS96u6eYcpv6QdrgeHxyqSl9D7Uvz7hdM=
`pragma protect end_protected
