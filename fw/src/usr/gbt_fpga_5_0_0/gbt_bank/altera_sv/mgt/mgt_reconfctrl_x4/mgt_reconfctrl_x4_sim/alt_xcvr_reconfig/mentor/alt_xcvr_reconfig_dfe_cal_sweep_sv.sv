// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:54 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
TZ+eLUNRUa+qq/XHUlmnbVS9FX4rS5SFJAk8/LRPbS0kVINR4njoTccIRKQO1Odk
Gk4RuiN0be29CecDirryKw8XJ1f1Uc2S4UY58uyrRUS9Ub1geDL4o2sw+IEtUDSD
9TPx+saT2nu9bKt+c9ytlj0OwZSjVn0oBQ5uTHZwqEo=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 11552)
j9+rRb4tm+Iib6zCpub9SoKCggD1eY4EKsLX9AljmIMeUp+ckRUUQr4wX/zQnp95
DgBTjj763g7gQoKpp1kxAcfizSiZLbByJ95vzR3MS1HIHRTzff6z/3l5pRy8y+Rt
zyrFBvkNOCIVDVRCmEZ9SBFg33E0I1JnlRRtRu3Cy8nOpchb2FCav8jVBOmHd0gj
7QdbQmpf6yAIoISFMdBlrTqg7XlC1qfLMmfU8rrwP2iDicY01p6daRWQb/YY8Qyd
tymAlef0CXhm4Jy5VJ7uyl2/RjnQnagMJJVzTkzNeSCumpaa9TX1PN+O9ClVZWji
O6jDlPmt1cRv6mQyA+ACSoFbuLQslM3wd/XDTpdlbOyNkkFyFfq011hAWSzBbHAT
GsVyd2LQYBx58JFLcnckbvLXWJ1ofKA5zR003FEHJlSLzWNIw6q6uSLCrDdNsVu+
ktCVth9BRQtjoWxuWlzpe0I/QiE2tAZua1fHlH+bQVcOcRNVsdbgC/HoZPQ3qDdx
5Xk4Fi0tGk2Y8nDdE+1f44BZiCbpdU/cA+QM+EUyD8TLIYVUSSgMJEZMFwr16Idm
6F0wxWyBboJzafjf8mTXqYNfo2ZskEYxp2fXbh35GBUFbaawCTvohE32ipXowE4v
aRbiMXeXqiUQwQlMD1/0AYWxVZi6YXM07YVbZPmDoCoio8FMoBvKigd5hWPKtPxW
O48h5YMLq8V2xA/XDrHrUfTStSrjmAqxjpK0ZGDAyJEhE4kB+CD707l3gjTlCQiv
s/kj3ONWyiM8BtizsUfEXSrdHMn2uq8jZt3RabfNSs33a4g0yKf+O9ud5d7bnIkD
LAWvtOgt+PYXt+k2B5IqcO38HprUnKmOeJtxvKGb5O3WkPsu/TskPd7ROrtWEIqg
kdm8VhzrQ3q2gexyZ7Z7HsFbsOxkuMqZqPKERiqu+tfjV93Yua6DTdM7mstthA28
z7EBu6Ic+atULaQZ1cV6A9ZFll3fcNAH+6ckbJwM/Rk4sWchx5kvaMRGdgscDttU
wzJv63q49YS1fPEusxUuBE5Cu6/a2MzjN0xsOWTlOBIakRi+jBgAolAqVJpU8N4A
GSA5IHza0KAxUQjSqTJ4xfkOLcFcILN2uEMbByiuUu3y5idJyYs8CGOjUUFIh6Ps
nQwDY12uhACSwZvuzOKyXWrRwrRGXloLc4na2QNfSg8PKfZ7aML98jO3tts9l5A+
lFzmzQnjQKJe3ZeYcxepLzgdYEu2+5w5TChs5+EOwuTqpQ3S+5AlZnk0YIK+kPzp
uNx1njoxg5+Sar6SdPmFvt1uDUsXAiZvCwOf7sbXILTtA7jM6o0D/U/4tIyzywGi
V8I9FQkksIlry9/vgFbJRES0M9421SK7IbsgEHX0XS8Qk3KW1yZoG9cN4RRM43Dg
FRt5xPnlBhcoiph1OPxoVpFfSYXqNe2NJ3KSnyWZ6ec+Ywq3rJ8aYUsdSw0aWxgU
JkI2Iq3sGyW0a10IugaOwElNCIihv5IIvT1ThCvTm79cKr2hUWpJ2li8xTx5VDce
IGCnBsZByQpduZL1VvbwQ5HDl6wn0OOezAc78xtszEIbhrKIvPcpc6tzrCk2weVM
1kFTDNIHdLCZlfFOJgJNjutMxhsfCIoKaULvmghiRc/r53v+N9n/4ySc0SXiHAuR
JqnVIt9WxZkXD9OohgVdDywDyfb5J/KM37SE5fFW/VV2tnP3/vmpQJeo853omomt
5hxVLHOEvxxrbuJzOZ5FF/MtaTJkeNwsaaFbJLpT8JNYvs/h2NTJdkhbAKF71nPF
KWL5V4oQhEIIhJHxUMizLXiv1DwDFuP+TM+mRZhvbMh+1gBtMwiJBlQQB3hSpCfv
/sdyY/1nRwIIYFR11/9xvs9ODLhbTt+nTHGU0uj/1+jNKl1yWwsYIkjpkLYzHv0c
quNeGhGXLxWzvsucBxIS/yl+7MHdGyzhBKN3Xy4KYOyo/0F9Rgi/SgnDIoGvZobT
6UCT/zo+XoP/f9liJgabWL2axIlEKCAGkylO4njo6ORa9gNKaVEwy2e/oABg5mNR
gjzn/Xdwbs0oqsi4l8iV00gpZY/eRDggGeDqxfzfuBslHQUBJX3N2/KYyd3GLAky
X6crZjOuAzjRngiRD1c/eJ4q31waspME24aaN0kn3POnbZkEh5eUlGaC3BhCKy+z
gDVZanLHjmDmsWkvfaKMVQp3xbKPii4e2/TMMsw/+/IFaxzZG1B7l2sf38iYqyvb
0HZ6oRErniCNk1Hv5aB2L8Diaek5rLMhOWKDa8lLA5ZPGAxyQiL6pJvsu19k+h63
Yb3HpLbKLhpp+CQLKhHsJbgCWJgTjCnmWgkKtnm4Q3LYIaLpL5tSj7SeML9nN7Tt
h4yRXu34LktLX82XGcFrfd20YQyydmKfWFkt2xaa+qLbve7WnMKo84TnoFmKnRax
k0cbn7BASDFoCqfSx9g3oKdddILaCeYj7giZ5SWTJ6UvV96k4yWA+9QV/W6JW+Gr
lbAlCtl7kEAeAmAyz0g38qKghGgFSQy5JjVhHbMHgckTHGE5rpHVVIGvY4Rr23Tr
VdwzXY/osNicIF5FMRSJaREQpOijPFf7bLhp0uQJUKjsO4j8SxsefUqi4NM6Uoiz
odsVkvVywhvwDEAFF3KpZFLnPTF8djR9nXMfGDis4mkX5/BZ5yChj2I1b7s13UL2
9xU74/H/vXPyO0cIcP/qh1JhF7Nm/agpSKR19P5OICNPQCHHXjhm0S7pIX0KQylO
ZRXi/p0s73GNBO97wAwWMnDrq3DM0XTqQSihSFY2JOnMwD02tVrAomgmEvYHGFUG
v6PAQ80wq7G1NQeXsFIDlfjhwm/yegoYIqkmH0ku8XJkPYK5j/6oQyst28/ZXz/a
GEBG9As1C3rPScztzpoWncCF8xXR/vcNwLAIOjQK+EkBQSRWtDg9HIH2gsrPbrla
JbRxTCsKPGgUj4c+PbB6pmeOQgNO7r9S6LGpCwe/szysz1z0Pdy72qZer3AT/APA
qYnCa5vWURaVSBmRFoZZ06QaUDJvfQclQJvHTfzVAXoj33Kfve5NFVG/v6lpjIkV
WPYUSiDlEO7Q6G2rZaXYKPf/msoXT2WFCRLdGPfFirWm9hyeOpX7jb2ogUI/CqZo
LfG/oYz7w1LlTRQzPUOmWZhW1x6JH6SgMft1xtx5ltGTlp51Bz+XrEeTPzm3xXYq
sNE8YccdnnuJp8+l82feC99hONd2I1l02w8hF5YAdPG9mp5EJciIUNiNn9bg+khg
LTnpZdBZHkkp3CwqRL9iJplwBFH01R5RDlJkFKNQCpbx5kRRh8aORlk74WEx+Ga7
ZoxWpVf0EAq+4oMADbZmQg6SC6nyenqQ4kY7YNPAqafRcSHjPtw9feA49Xl+epL8
JtMmmFanKI3d/IAnQJz1x3qH+S9ULUOg2InjcxuiKBM4z6C0tbmC9h61mGpJUoJf
1LbUjp6qt69084NiFmevsZ1U4MS1mDW/fFnjq5CAg/Sz3ViGXgjXNE4LP2jExd9t
dZLucVv3FU4IrGPyxHesPDAFaUHYH4Iv6+aMC9qPcv9nEjYh+oW5630VDl20t+Gv
q5QMumSQfk3/Xdw69rHRXGBIyaU3lAkVHvDtWObA08YOdsXdbe+Sb9gHxqjB9X1Y
cqbLO7TFUa4aPZ7kENIr707mTq+V73vgIXKMG7yTQIz2RJGgH/tzWlPWwXixAAz2
tvC14Q7eqAU7RUhgrDWKFSGMMBfFZCerTT3eVdmyrjtn0WypYRwsw+PN11lQPGTW
lpIfc8CS/M/Djae8lMwHXdg8ltWB72JwVZQP3Fi4ad4CWoMH4hPE+JdU2jhnLhvI
l7SaaRG7ZFvULpM4MvCR0l6V1R5xNgDEk3t/XVO+FyJNNFy7JHpqkid5ynzXOVOX
Ca1rzS2srzROTSHOjOKPapK1XvsOExcAp4ImDjkv8G7GgSh3v8VqCIXvY56Li/Hd
G+jliG05WBFUCiuuoeP0n+unYAfw7KTttwUo/BBIHMgVJNN6CvTIGa4Ahqcpl7FT
o7CMFLeBe3mySrRBorbRJKXOsh7Bk89Gpg22us2bbCw5lh039fMVQ+6Aovlidcv8
LRvNI6KvULv44yHu0bus3oKkSC8PZVElmTnlcyouEMRGE47wln2N75VDQ+saYanu
ZS2wlUbWnLver9zAH6HTfSCGjAHY1SKMMrCW6b5Jw8yEoIhokhReL5BqZ3Pk8Uf3
9Y6mwNiQBdyZdelhBYG7ZR09gWxrYio+NziuYZzQbZ6O3d/iWvSVbyzQbhn0+c6B
zLhEQMzY4Ch0pPqSg3AxuqnI2TJ3q3VT8frL9Pm4qCb5ua4jComdoRW62MyiNKAh
bxqvI0Pv79w81QNGAeuE3X8//ubUicLW/gg9Oaea61/bQ63GgpluNIiCbCdvV5Gj
YOyOJIQcqiun7kpRaYI2hPlWCmXHH0HCMeXOF6NJJFTe1ZippcWZq+NXE8U8h4rN
hRWp9gpNkgt4TA3DkQmJxpJx+gb3mEoIpS4RpDI4ogULZ6X8C+F6tnx0q46Mpp9f
iAGFQ4iLdt+X3aBLrquCr9Q44uuRBk1VGW5M7UDFgPSpodU1iP7wS5I3kAVZFFL/
A65fL5Ayf/s/Bhm5pHYWbKMLWo4OFIKe0T663fLN/NamKxvjAzqavOJQhxSfp/Di
Vmz1q7yjAj1slBaBXigVV9EQvk0o3WhlUfyhbT+21aZ4b/8cRwBdzuh9i1q/zLhN
N24oF0JmhICshFwXt+fYWJt++wAE4TdgZD16ut/ed4roaT3a6ZBPyVACsDcRY567
PSOeNKs3iLWa7qRRbzIjCwhvyq2W1QAUUmWPIbf/yamVwwqbsEV/uEXD3pFMl76+
S0JrFdEBL1G8eESMDrISZRtJJNMSeFbCQxUsP9MESkL3Wyb5SbtlyeF608lXcUDZ
M7Jtguk+lLl+oKI5RGngKBQZsXYLU5sPgmPJBYxxivjJS2XN2cAIytAZ58b3lCgE
e+uOVS7JmQrPa26xGISwGjziO37XMdjuGHswrLMv/F6tGJK+etkhtKoENPsJqYDT
GheUzaFLDeKwtr9AfLlntILSF9Y8F4/0VzDLnzVkmVqyk3tVatV1tGLNlOy/QiTL
DE+8Z62H1XktHLYZKMBZewr58yI6Gz4E7zWZCE3QLEcLJ8ndY17GFTmp8ec4VZmd
80X/R+16P2FC8DFVlv3u4fyZQnxsbNq5enB2exIZx4hxHgyPrL/OmJ9xkiBI9aUe
3h/9Ba9EkdBeoP/ohgHCyTxBH9Cw9XJgXbUqWDuRuJscb0+XgDKmsAwTm0MXEW8Y
DsLbEtdKWxfMT2HOqJNHnDXp0waVGktxFFukJXMoRwkATZzsKRqucnlZ4Nl1BbHc
+PBIiyQ3R6DUF59ePgZf+ARhg888JItoj60JQDjzsvVaivRxdT4q9Qxe8ea4jzC0
C31RlnSN+kGhwbbSh+WPQ6pqb3TKt3IWoaYrXvZq9ONVpo89Wur292Z4B1ZVOnNd
h6KyrkFz0Tn9WzjUbl6tgE1XoXVP3sIqjWkcJxRsyBBFK0QA4U4h8hLryAgUtEMK
kn1jbNKxPe4PA4OZself6qjeXQUj5x7CHaBpDHR/jiySHmbvaDgpOEE/C8yqnEO9
PB8NCTHj2eIoRgdN3k83jIScECbJkxIVAOqeocCfWb8YZbQMxPmcG5m+2NEV8K9P
FNpSImrYBdb5YwmD62e8RrYWmXBYVNKKHrF1IEis02xESnhyHyMFcfkmdLx2Ios+
rrEQbClKXZrbNLhQSm/LoCOat8lYeVBJYb4692XMInQDsnKLywOfN5g6D8SgmHCQ
YDtGOLESPwPBa2o4g7vE5ec67gZsGwvM4KEwdImouQ12RP+psisDDGLkq/2RZY/G
J9ZoIMvQflO8bF4yTP4A9Rxq1pI2YMVPGu4BtAFe6mN8ZG7zcwikL9x7A8trgcAj
ahyGHVGGH0PV3dLBvBqac0DFbZm93xV2noUCLVs/R3l7/gITvWUscUPHkPBFZt9D
r6mfvLK5STWnT3CYBj76i1pkWkdAwGmTjdmpBN5Wt9ODoF6xYgwow0tGnMDURQid
RNfTNrdcIr0Z/Vn1m+1pZbeAJVHNWICp8f8h2zB/UWCQmEQJQozLzDnnRClbfR5K
9TMOceGuXa5OWbf6ih+35yHCANd2Q65xpChpORk71VmSBYix6rIBx2338G6NqqOp
YmwwVoeYe8z56rNCExhg9sIRiHsN57Yj/Gq8YTvJg5Eok5KAyDOWONVnEASQIFFf
npyaPCx3MH4pSpEWsbIzdRG7d7xWaXmxgJaVyaoRe8LNrtxeBHF7LqQ5PVzEq2TW
HFtmWZxWJL/Ox0IgQM3q/2mAzJe+4fOajm+lypRjoFA8uTeUX1/a5cGOtrHIB+Rs
9/YgEI7vBdS1iaoiWqXeH/TumQgug9JvrbqrWoCYywX76nnHKlleH1L9DUdEB8l4
KZ92cpPECtIh35qGAJUNsZ1+zR40PsiVkqnS5Yf4zhvrbtRa4lUp78mIJ5mV1i9p
pFvxDwmRDvAe2HhigBPF7y/kfVJjT8S34++xaC91KLaYFvgA0MwpTRcJSt4QQvDt
LyCMh/kS35TC9KMQJDHZZSZYGG21LWwTlfysS2Quzcn7Uoc01gMKENhhZdLEHRhy
7U2FlFo0Qvby+GBg1vf0cWm5hYtvnYK/jr+qypSQPTFcSIo+R9fRCqZ3+YTDYyM2
CjNc0xVWqNJBZqlGeC16/Qu5bijWjs172CHzo9Apk8XzQE94NZilH1dl9Jz+y3ya
nfT+lGrqu2TEvFq2mmPLLNrHds+avisR+0hpUeMkJKMzfexAtBas/axttUgRsHRP
mrlFHB46kCl3g+t/83oB/0HBYwjFxoA7tno8XpITtew8Xi5Duzer3KN7WmePrVhK
58zxki6AGeCe0pKuNdzVws2Sdl9VVx4KY2A80WTEw5OvURueUm2l4iEV2kgCnph2
2zbs3gxDHakD+qU/NEdnrYKIyl+ePUpNp2ca01iu3tamnK2TEh8NZhZiy0WUC1o3
dhuchE1CLmEKSraeTRpnBGzlEaenydWLuG346/bA5/+c2/RO0LtZBGgCt/BC/70R
WOg2PJ3JzdPtMP1an00aRCnYjmWtj/NAdf2jgpx2uLSRBzuPJgMakerg1Q0MPPrx
leOKlFEKrXd5dPWZKhpsAPu+nlP5kURYkv4R0EVlzGcQQJU9cHSnxj1BBl9CBINp
zEikcBitKBtgWTlhdwSBBJj5HG4FYRNayiog82B3oHyzfd24MT7zGLNSZEzf3zT3
cJuslKzfPF4442fQEmEjGaP0F6MLyA3c2gTIfcuIH9v69D3aYrfO/s9VjzIJHAGx
2s1NS7gbMxuP6mu41NIIJmk5hLasklzBrwDpFEg61JCjTU8QXdB11R3VSO5U07zx
2poToJg+8dF7OaLZmdIRWOOvawyzepetCNHJaTW9SQkWuYvR/OhqxG3TBZlWaLOb
yvZuw/1uBrYN0IWoh7ZubjJPiHVFKO9s6VTG0wbXGhDD+LG4tr2Wc58P9+yZxztN
nnfL6U6j7VnZYS1Df7rPwRehznsKA40NS9sBb2Et1r25yU/xkEtksoeZQmCymLQZ
PohJIJy0OTVdpcTV/sC8ERzoN191BYNk7gHphjiljs0zlkFkl8gelWrIBPqZ2H0I
+WItiGfCquEVYzVD4FxopAClhIaS+TslL9V5FBpNacn3/Na4gEgenDmgigg/xmgd
ag+T6hliU2h3ZJtUJKprbYnmDythJ501MPomZrNZhD1nVLwMpBwnznsdp4VJrxh3
LT7gSSkcWPI1z97c4nR4XmM2ShQWQ1XmpR7/GsXf9lr7oUPLwPZdidVZ8gosWybh
IbrJcgvalnqHlsoOfeLAIJtMBWUXFr2PCBBatUZsjsuL9vCOZfqbhbH0kvuAQW5I
9iQuzXinDDDm6kQIJJJerzHs+4EP8+xRRGk9nhobHV8jD3YBGs2Y6lKgWsfgj6CQ
LWpYHB4AyyXhqNr3ASEFqVKiGcDcuaU4k8lCX4MM+J34bkGpQlRMDn8Otsm/MH+G
Pnt1jztOFS0woartV3qrhb3OPTooqk+lC1fK0goebYgUJie5EQVMVLkN1ldyVEoM
0wxxbiqpCyrX5zp+vLYg4oY1h8QDJLj61vlWDkC2ycXVDjNYA8JDQUPip8I7U5oc
apxQO8jNjOFmPlATdk9zj9O0b+N/uWTBoPMxe6yQqrOSPBVCC9+XiuZYyeoxx6Qc
rhexqvkNHOuRTSwC78b5Z+xcdY/xVlZBNtPtVpArvzW+Le7SXFLw3R5Pfqbc02ey
V9MvD8T2wg9grEn64ioFMhReTlg7sec/zBEptcksPkKso+x5aLsy2IdkRtGHzPuv
GHx/cC9te/kirv8Va0Agz95On6g8NkwJVuvTAxYL/Gi0sBe8Veqc2SscJAl1RMx/
VN/5ioouY+PhpEQlvmQk4+sy4UZaWZrjsNVahETctIdWNLvcL180b63qNYNdRLbP
9Z97BSQKaUFr8AUTEjanNEOmZFeEEuLZTnh0vfoZqpoEbE77YA0/Fcs9IeMatufB
jQyBP9jGDzCgwuyTOG1MdoCvP2o4ms7wQTIT9XVLq9NpQdY5YKu0mJy7ryEEWe9u
FfGOXYDkt334v9zYvbRwJplNXa+kRFisLf4LVBWAFtiJOeuhzyawaOfs+gL8XINs
PQTuJ/hrpoAwzfUjgtZ7fsVTqQj4M1ujARABuvdhC8w9D7AN5eTOy779ZC6XSUun
FzBFh7fOCyw9xnxNbmfBlO5YOO+B+Os/UG9vyEjAObrRB+YYOO1Mi1sXWjGFElLC
eJOifpCFhCOTkbcHGKYShe29nEjcqwVr4A4+UcQ/xccPMHmLVVm1UnM4asvrO73y
4ClexVEfw//5PJ4uwRTdkjEUxH+XqqmEtHu4yTRKzlGTzrx465kZOmxpPp1eFfhY
qxj3zn54l/8ZnRo2vUPLpL6VLhTdqPx3y3ou1G9G7g1WGOfUDAOohP/17erfj9E7
sG3SmVq3UW/pqwyp3J584zKKuqINPxV4jMvxy4e8KJaW6VOl0h8pn1dArArMCbAm
J2fP0/mgT65rGVIOeWf038Kgn8I7K59iegtwL+plOuZVw4vWasC812WtuCkYvo3J
AXxR+C7Leb5LuDHTlLh/raTlILW9HYi+M5PxGShOwXrNL7B87xlb396C6Z4k6pUe
c8uJOPUA7igqyyTtM1jqtM1IQaY1e6VvKIaVXZlFyJ3d+7zipfWWl2Y+x4FRimNv
D6FtKC/HucQXrDYQ6xxt2WX26mu3zuG+/5MHBz64QLiKG+J/DNJJOpi+IVCe5vzU
5yyCOX9oB+rGQ03MwMCXwobAXbCndtlnbXqZZ8rlG42SHOTwjsK2+e+WeZuen+xi
B9EWT5V4tcpsfyFJ6Yo//G23iaVf3rxwWsd3oFCb4xHn1otOjr3ygseHsgeUwGNL
PB3mMFZPtvgdZXvMm52hyYS8Bm7mU9AlIudSDA47ZkjBC/4rMkHW4f6LkL6wNqaS
VgNcd+8cMiChvcfxkijsX0XouFHGhCRsJlQnqIxUc0l7OzlW+p/5QsJ6uoexQI8p
eKtI/3TGnq/F7o7Wzp51WDD0d1nn2S8tVfFowtHguw0pG6vZfIjL6mrEDi9SyeMu
6rEyGVnW7kqMc3xBv1jy7QGU4a96X71sAEs0xAh8wRkVV5fZbSCzi6s0Tp7vHgUe
SZzPwvQqO5Jzv9uvk7IAhPwPS3mL1WfMYzS+eYJwu64JSBDiCecK2+sBGyBqO66C
OeQl4ADSneVgfh2cZEzi2SQyRcNYun0Tx7HgpLZZ47WbwEOe7lrNul6KQ1uxGOo0
wZLSUh8zLlydkRDj5xS0PcHzzDXMOxvQPl4qbV70Aqe1quA3yF9cJylIJu0WsXjM
iMDfuh2LAho99ObF9ZAve9GsgIsfe+HAsRnnQaaKYlOGevkCXZ6DIbbA+QoOONTI
zelaZcBZDUNOFsU1VGlzv9o481P3FW0XDshEkt6AeUusg71MZ47PWKQm+mTVhpoi
ctMM0s/HV9wgI+4S4/N5EXJ874cg6HKFJQvWEfNI2uPYp8JhaKxSUo9frMX535Fe
Rur412w0oc+2Flm8Lefx3VKV/Guw96biIUfTGJgxbwqU87eowhMUtySi/cbPFlar
9aJJvLMuo2dc2jHpvztdHyCgeLHr/WUOQfLNxF+oVY6+dz0ub0TJrTA3GJba5pXB
t7fPGiWvczaOXYgsF6iEfCsJkXWdRUgzUlUYmNt9Xr7cmVKh7yLxeeWFNi88aTAW
FJLV1/rFjZxvlMi6AWycQq8xYz/JOjXSSPGzFko9AN5bHgvw/tJeeK5PxVSo38/z
g3Ho49enExdEn7tKjU/+RQtEu+FYeDIAZS9A9sJwfuHnwUGUXHuvij8aYe71qACt
aHmbCzl7DKBVrc1lpjsDNxLqdc5Qf9Xq3PVbXnWDUFRUCP0oTjYkoOMpzonQRY7O
dzdih7huZR5ZKfYjt8juXk/kR/yLKI8cdw1GUDYKdZUgUKQSMfvEwZ2jChtYkRDA
bzfj5ewCaGaRT62qhJsiKHE7DtMiuROFbmCL2aIco5MXtd3/n9Zt+bW22yYldy25
/UCRNRUt5RchvUE2fMB8vfY8qgbHKhmmV8tbGPYtfj44g9S+EMbRrEoUmsun1dRt
22tfmOgLjNi2RPQJ042lIevfdTT/t+kYFif/lqAwN/3RFz5RPWQL+xqZf5bY6l5I
O5/To+lbuL53dWFgkd4+6p047otaIzUjvgi+T0M6dl/qPEYXq80mq78ale1EfJsF
1EM8Gnhq4idvu+RpoqnhCPYt7BD8/Qd46bJIx0QRHnwUXi7HnNkflpDiqicebDJ7
DTHdxZARm0S3yWpCXEYdoJ2nymXVy+z+bGjbFKmCnPiNuG1vzLz9FBorOOoEvHnz
okRO/aMYH7683EaUdBuQX12OCIoOEkQk3XPwupvPgUnVJg0gucSCRzFfLbnxhXKe
K4Gn6GpHzEU2B5HUOhpXZB6revLA5DmflcMpKdm/TVLrRmeNP0bcw9oQc1wbOXip
09Cn/S7HuC/xUzHzDccBmTTGFiSX/62Lc5+BDnDTuSnJU7O5lQKCEzwONCk2+BKS
TV2L2Ja126iLH35A3agMNHFaduuBijhk86b6JTb3ORBoQmyTl4IAsnr2pqfPN2GO
WEuH8a+blu6H7gXP1AM/CaCNXRmQJIHpV3AIot6ssTSz5I+KLCXFEWaXFO6pe5Ak
H5akooMPBmrCkZFDduF4+5e1LMDvSfa0HnvDFSJLPFyVo5bMe4wXIQts4A8qpVf+
RaQ7KEx5Y6Bp/I0jT/v1EYjopHhizHUCDrCJyBS8Kt+0F2hcZEelMmVQShe3AG6n
WH0dbydBd9qVaSXeqnazMbzADchuq5zDfBnctXOWo6LgYQoVR+H4lc2NJdj9m865
1qcfyVI856xMuYSSj94S3nzvQ37edDtvqlxKC1OTWNUdV3rDaBmAQBiV2I1YAlLL
PHpsWLYkt4Erc+LMv0nGcE2k79fIG7PAlivm0fdDfsq0hyNubDVD7YMCpnU+zhn0
ie5gSZNwPhqaTnlpKeMPYw2pZ/mvj4gNfheFomPur5ZZJeC5fF9rAcUUHK8AuO1c
R6GUnP6WMvVmZZaEkEp7+0v6RSIaLcc0hYoqeBktOQUp2RTad2CoL61AoZ5LNN9q
+RgZr74ABTBBWGxEi/N58QvU3FSow34INAEIPikkNNfCoCebibS5mjBrX0ejCTMm
zG8hYS64M+mBEfTQ8WT/DTzIZQCZBrGmIshs40kEpeXYnxRk53Xy19LQzjj62Ety
nQcVdbfFrZjgGpt+osweODEsW+anuEc3uqYcBjSYSf549mnkXXqlg+2GYqOp9zPp
j24UJiJyIi7+jFZXazYe6BGF65nIHy2OHpcXFfJ8vIqjstEq09VYhOYvtBewbCHG
bMjKsE5TMJJIufM7rq1apiHPWQCOOBj5PQBBB38bf4iDFjNbFLpc+TPmOWmuuI+z
TaMB14vvjhI+Yb0mwuTH/ckIEHkLdlYbmF3lpJ3ISj0dkgPYfV4n1Zz2i3qG+E2g
RjdUMOiUDFzAOlxn4upxEtLqpE+Y3kFQydhlKrClGnY+N0iioX+LlcjR/dMdUwGO
gkpbpJquHEtdsZmdR8enwASDsmGMX+5aSUa/s8lH2LxYY2QEinLUIkgdbCQviZBh
Z9XirlotDf51IdQxIgwpDZn7oyjKm2ulPdX3+Y9RLjRkyMJzMwkiB+ie57FyTe9h
6gYYYoCrWJu8kgcJDP/+RumVphuLgY8IcUAPFsyeCwj0nrpRz07hes+Af9jhW3yc
QN5ktA/3Yw1xX4CgVtZkCwk9cwXzur0YhIsqXCKMmYPcTgbOTqfxbFM6p9tlPSJx
oY/0SGIcEtjzfWdFr+jWgXulDUrIU9b8o/CFbJDlZX0JCWhXus67HQgxNw5beEqf
1bh/uOX6opzyESvzV4NEILZb3GQAHyb8EO/9t+LofUxVCUjvdqjG6138yt04MrCE
Y3AR+a4jiMQJ//fVTM+U8XV/p1GzNq3Y8lPVIuict3aIgrv3BSCUfj7q7+ssTUe1
5GpOcEtaPdu+JAx98Zns+stPNXI9aqknMwZJFMqC58LemUQvW9DD2miKIPFZTg5z
RKZFRHox+M4xLTpxArKleTlOGjLc1J/ItGIvXhDUBdefD89sphVSqNrecE2Cb9XS
H/LH+dM/Fj66r3zjgpFLroDANczvuGObLi1k9BWtcsAhkBZ3JaVGFYc72W6Qqu5P
88l4cyO/ADm2txibOJHeHdNBonQKhZL1GGv/wWMIOxlbTQY3S8AhmnF2+TnzoFFY
tg3LDQtzFiGQV/nN+a39dhW5cZSdovu5IiqEAVKf83q25VEFXmvGu2RJRZujghRX
B2Ye8nbrKe1HyajyHUI4oyOjusJ4kzkpYzy7bthHG6m+NAV+60biZq5hMKVOmaew
s6DEhlGrxO/X0P02A5qWO7Z4AwAI+cJFa6JjFefddyw67lxg0jk2lt3i96i8HxIh
Ni4FAaqsTFZ8xACeNUEHbMqawsSN1hDGJ4+qL5M0LrdfqWHOw3i9/CSy8WVbXkG0
R1wZQ56fDmDkf/B3q+Bj+uAilVE566FTs2NDwj+JKhDegAItqpjgeb7nP8Cg9iiG
aZ3oBmARJ4gjqG02ZJwg6KHh8B7A5ETn9KMROBbTNe0++3GDlTzCEjQ9xjT4Phbo
QWqdL6ANOh1ZlcJmSnHjRt4jLpLo5hkoPcHmBq+jO0buCZpBl+j0YwvNEjJzxqCy
aSChrVh2mPZR56mjxxJsZ9y52gyZOlw5dpZQzKzxx2axHaKpsy+Lpfz9KWFS3X2n
HjjKld0OGl0nmlwHoyAhdPlmWAyt5huVT5WPQgdEeDNay/O8ivQkTPqNpg/xWBwI
3I2bUQpJSulARCbApcEulGPptMV+lVWRTD1vS0XWtPWZM73yEiNrYg4/sFD94YfX
9na7opk/Ch9qmeSKdaBJE9zZmcsY+bPj00k2QD18Fl29MFN8xcaziM57SZGVGUrd
gaWaPLRmBhOKt6bnt1VIUZzqvztPAefu7Sammr8ELkvjzOayD3qb9y+xzDiFRBvY
GxTcjQg4RaPim/Md+3rNB0hiDHbo6j1epjdJ1OoIFWeKGLn89TR8g4J8TjRUCWvg
+evCyw/zFURwwpfXdObyInW5E5M5S6b9d6gSl24jrwJu0BAV/j1Vpi1ruPDVkFPf
d/qcPdazORIFp8ub40thnsQ6nE54yWkCWlEhT5xvKF7szk+1M/qeksbHjFmuuOHV
oZL6fXssrIYVMatGOUy/vXLinh2n8gB1PWUoURVxK+WAdU0b4MMiOFC4nu5B/pYX
c9z1gGX9Q0wmhLfVg7YLwbCuccuMMjeb0oLzMmtgZCGBq8QFwxB/KUZLc3N2PYPZ
HBXmVDBbeBr8JHn5uaFilNkD0ygzL6mLUa8cIRyDqzBgWh1VUuocpY0pr5ILI1Ic
+MHUtwke/fICwiwDMrD5rRhTSPaDqGX/nHCLqYorViy7L6mX//Qv0i3uY7MwoZdP
hhrXfGl+0MB32qxfEGmNWQPKB1JFZ0ToHfczKnIB9pXuBxwyE/5u8WHLXrBel7dn
XUAwYBGg6HTJDhcY2OmcJN2BN4w5xZDtpfKouuyqjKT86jz9mH/b7HHV7uyUvrf7
2VN0GdfSo9A7PkjIc9MaWTe/inpr8kW5ZAXLSc1mp4tqUQSW30z2+SghYWpZ52Cb
9BtZTSNpKZVBslnvvO7ptMciYdiIBeqK4a66lMh2baP2zq/ZBPpV/BQaMAqpyrBs
W2dlE3w4tCpQnL+IxBYUW1sC6Hjg1c4DLYMAVV5/isH5EB777Xzs+Y8OcZwsT0Z9
3ByPeCb3YepsXPosrOqanhTTqYSjEBmnSTp0SWoqsmzqxCMobq4jKuuEpqo5XOh9
mKRIY21gvjvI8UIbmTb4C5gWb5MvRGxArGlRyPxA+qMy+qXjY+D7gR0I35gPCa8C
t09AVHRh8AtGzuLrgDChU6d0B71m7JlLXhhpEkmuFLcScm1yK4veSawkg0UAxfBj
UJQkLB/kmTigLCu+2KXd/fEtj2fkb8uQ4avIiHpyVRQKoV9yifTuY8yTmmTi8VHu
JzO1l7/9JoeWkM0bdrVwcJCSKvRAVDAVBOQc6PJPKNDHNsHBBWgJVxNN5987Iqe8
GHkgKwvuNAMo5HcVgwCu/6UM60nZGRKYaqn1HydM7421O/XGzyLJhk1VKfFpMMdR
ABT57ikzUXqgZF+mRUcEPdYkv+KdDvNLKLZik+tqT0QtP/d7gMP9eC0TaXJNqo4X
UvsgMUrIQZpfSJLG1Q3PxifcrpFp6G6Tmsf6T+qVqBuCUd+edayPaluXkCV3mvWg
RvzJIMdWjKKwtWZ+wG0Uo/+idxjjU442CoxqX2+cZOEllkL0j4poj5K4soKu+SGQ
n9YqZDVlQFLyR3tdLH1AG6QBgZ1QacrFQSAgJYr+r7rUWD9+fFGiLa31mLurS7fZ
Q8Iwc65x/rmX9bA7GZEQHt1leFohSVUsg7Nv75lMsiB3OjCGEaM7yqrR9jNqkzUS
cZ17OkDFZGY/leZ7lROj2i7/eUPmOOTFhflmdT6HElqXizyYO0BJ9OvIjNx2laih
dc/Urk/wiayrRn+ZlIL6Ziz4/3vLoRxqtBMz6NEvRk6+h8v4uYerYilnq71NT2kL
G7gIF9f/OgzTiGfDPdDVfRir89sfAugtfCqa1lKrgQszhAB/s7Fz5XtSbiIRgnTr
7f4rW43pUVel0t1vNSYlJPgOQbWhpyFEeQhPYecT/lj+VQkWV4iYcreK1yepmDrY
HOua0DUYfUuPR177aqrXNoqGk8knYAz7w+TAHqFiC+Q=
`pragma protect end_protected
