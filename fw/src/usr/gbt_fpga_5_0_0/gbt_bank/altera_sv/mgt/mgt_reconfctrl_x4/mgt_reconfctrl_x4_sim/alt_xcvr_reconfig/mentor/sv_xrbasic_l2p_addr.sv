// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:51 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
PQbrDRdz4YzRFQFrPg0bcEJd2GfzHwMUgXobeMVmOLeY83DSrjY0r+1E3b8nSpe3
lQ/T/pG1SxLWCeb3/9PMVLMxE6kz+hNVCaZfYHHpTdauxk2Psr/Q9By5HGGLGyRi
GMi1TWJaZ2UCEq9hgLxo0xFHSElU1KgLoUlEPgzRX40=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2192)
NKXjFzaBYnpRimrOvXEYKrKXEu5lwE46NTbXxDGOBdlMVA+oa+kVO9FiHLJ4tRIv
BHS3qsxmHZKKXKhiEERDlN6MdfNhAzw7E5udoqJK+UIb/yxKO/kmtWhjIR/mfit6
iZLTgshiL8UPuj8ALVwe87PwPDRm6VyEhohTy5fqAdpDFaYsQE/+SLf3q+AmQXRS
I90yCeBXK0fagkoeldmQgFAxEG3tSumvQXSz6iWj1xQxNfIG+DNYg9sBHqAgQqdv
7tSnbvScVfTB7B7Kdv5LafK0y3bGzewqn0RONKmT94Q/YQGK9ieaNHGZlWc+FMEt
+Cr6trwqxXUa1G2/FibuIhLsZRwbloEk2KgtTZXIX8SevvQlmg9UXxTFhD90QqMF
1Nqu11gi411Y0RMjZYXeE+WlEGRe7CF1l94+F0X0RqHFDKre5o+DvfHCO15nQ+qb
xdwXQuIoC5jCWh51qt2X81Em7nW8RMWl2lXqHJfEakyAA4Ig86G0pfoGj+8sEvQ2
JEIZ6ZiD8PavXtmNtoTy2X6f5swvnWnGdixWUjgO6i45+EeTe00aeJgrwQTsD0tu
sa/LwmY/EzYmSNEuYJRxnpqHcuWvLtR7r59mEtuRfvFf/RYKfP+AO8601elKSB5l
dCb87zQW5plovLF9SYpspgcX9n8yu0knX8SgEEt89yNSCgPRqSShCZJ0eDb7af5G
ObvW+0/zjLgjPBLKVt8veiHqhGK/+zZJtlnGqIPn6ewh2q4TUsO7YuUqrDeZz4KI
OexwPcLy0dh45ikl99WxGKC22HZoCO5heGeN7Rq5Vb7Sj8MwrgxKCDb9X/4gCUmw
XfelwycL8hIGZo/GvUcDW4MUkOIzUwixgCDgXkK9GccY9gnNOPuy2U/oHyiSyVpJ
UUUnQpdugjIC00ze5T09b+I6kwBCqkpP87cEtvOCvo02w42pn1GmPKPNpG+a+1qf
ZzHNfpXznAJOtYf6uQEN47EwT5UT6y5AuCUO+mM6BO1jPLuuj8bdidj9Vn8pDHpg
79+vfUSTiywBxeLt7gR4h6z6XycKKx0Hg76YWp1NvcCh47mSqVbE3hTv81urCxg5
i/VjeijYYxRxjycWPrrhTI6Bp68a7gJQTot+B3/toj+JLNag74Ew4+zaQLnYXoSQ
0vZ/wqGU7f3KJmsklME0HJDiwkywnp3xcTF6JoC9Oho9Sav20wQHVMjljxgAtfbW
TEkKQjuhsu2ZM9wII7gmDQThmgxKVoxG2ZUZ4el9dSmFQW5LHQf/iA84bWs3NmUb
kS15Sy3gG43OfS6gx3lMnwSO+9sVQIEsk1IkdK3k0lh7+l6zNmejwNNVcf1dvQAB
ghBzYu7HmumJKDhfvSiLCyX1U6kk0jTKn1lO2p3jm4Vh4/u8PunkXtwOEw3DEF0N
OW4fcyzlbSms9YIxVdH5/WqUIoE4u/xJe/K848FMVmw50CRKcaGX/2gjpJzrsmZG
+44tAg95nxEXJ4Z7PvT6a7T20+yUEbQpgBlyJcbYbEPvCHQTF4d3tmELqdjAS/ia
luSHkItya/2V4fiDw18/LucOX8mYuDVWy44tgvmttoVvwIXEAAEoEKh4tm2WKmbn
MfQVRBZbj9beDmbVMElqJIcHnNTcRepdOxger9EHETac4ouzYjfh4Apg4EZHXZ4j
pZrn9GELvPTkS7szUwV9dVZwVeePj5As2TszXcbWAtr5Jf8OeG5sbZKaQtfsbgz4
AhSR6nqZIflbTv8wWy2N2y7pKDy/moYclPXxYjEsWVk+QqQEpjvvtaoYCGeYeAiE
98e8o3kL9OVwzjVxbYs8yBv9I7mmo10qE5qBZTuU5TVH9IqF68QbtE4yIQZykWmq
MtsUxyGmZ6Ti+yXVL9uVJYrCEEitea8ZCEhu3Q2Xs+OR4v9d7x1r7YRyddlu9DoE
tBUmldlm4OQVsS7VPQDVwmZmWmFf+gb6OfeUe57Y7lIB2HuxNLRPumfnjmvqWEV0
D5JDUKD85Nl6CpIhPBdiTHr4Qn9nXSwCLy2/Gs54eXbISSuJebCAKRJkBApVzzqu
dlqc4bT/ZJV+wiIsQs0fHIVR3I9H3nnNTbvIBVEYuMFTGUZjqRtz7xR1PhDpR6/J
I61D6TWOghv/05cjWWnKD9GlBo2zeaRHErWujimh1bgnZjDEMXj+OzEyEEyRuo6v
1sx9x4mwmDL7wZwCu/Ld4CP65ybOqjqZJLk0m1IQJf/DLFADNOVePBw84zwHHnYb
I0fki+WBHnIVYJjAJLTDn+WzlRLBRcQwVOMwz2etGqhzf8ZgcfuacxpK4Rk/IIav
MTm2naWrCLBaHeqWQADkSgHk+9bv5FICAtAxhC8W9d0+nrx+a/MarrTt/gY4s50V
VOvPA+BCiZ5fxbrxWPZLcTCJ6ZXSz/6MGIXXIdaxpvbCnCHH6ak03YaBPVtdBcky
pfexu4DciGQv9IkayOSnJ5eJz/oo5pyum176wvJsBecHsf9iMerkk0nbAeIOpO8a
VaiMvyS1yjkEs6FiRjYwodAwKWCWLLVvMq6mK0G5r2wy05yFOJGPOYLjd6fahn+2
Bxuj7e+5SsFjX6cvMIfeNjVS5LuvO1Y5w+1XC9b+Gsmpe5NjfTRhO51+P/cr+XI5
rKtkMmFdUnAhHmEh3RxQ5kFEz/rNeDLVvv/Lj1hZhVnYGxPkW2DoNEDhvrVB+7gh
pjx2HqIHHndnklgTmHTzKSiVUX4ijK2wRUJrtBev0uCCrS2djVjWJs7iRsnLOJxt
EEd0X5r5/T3Mr+7rQokwM4/aPK1o4BQyw5iDSBuE9bOrUu6D9U7x5jaSRDIQtgBv
L60jU3O/Gg2t9arfnSXwi8YNmIQg3cTJn10s/xCcbd3DqHcUFM3Upke5AD3TE9FE
4MWNb6gDyybIl7v5ZNnjD6Ov+7icjpqryPF/dGtOP/Q=
`pragma protect end_protected
