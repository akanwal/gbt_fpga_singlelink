// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:53 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
SIbaUQXO6MM+lfuRaB93qCdRuVK4uhzG1Zb1B9798KxAvp793a7cpk3EokjZSLWo
1iYoDt0+K1qtwK1yZguBzR72cMrjx0r4GkpOVogoAB/cOcOJaDAA0rbbeC23pI+6
E0WWs1NVA2/JXZ2JQnH/pTVaxLs26japdGAVA6kxbRc=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2736)
hFYgDW7jWkkTEPhtvc3ZDzaGvDVHJeGOPIkt9lnE4ixIIX/YPe0EZn4CXDxG5vSS
WvyvAWsdfxrP0aBihbY53dm8IzlHVuDGBfEcm+Us2+z9ZgRjDdBqkUHhYQPdiBXs
dPWfa7jdNCawAKbEIepLEB79tVjxbGuffrxRtb82HaWyDFc2+m7KoEZxMlLkqXMn
zEifgSMovPfU+eE7GeBpTyzAxsr5ghB5JCMRqHEToQUY0IV8xlvI3njJXtFOZywE
ck/qHeEW4OCB7iw0TgCXAG0x5MPVJ+f9q+JfUea4jMPEEOZmrSor0dchsEmSFsFs
KvZ3OgE4VBrSvrTLjnwMxcpYahIFFMtrGH2UryfpIWb5fJgmTg0bPGYcVVq+9Z4m
9AIU4q25COGukEL262z6REq/5UNSHk+3K1ffKKWDn6X/SVSEefYoQbDc3Jv4Q/Pu
BVY8GpCxnvbe2pdRfcw05igbwdRXDxTj9DqfT9vXeo/63NO+DEyNucQhkBNGkMau
G/JoNBeoSj4yNgTl4ZRZhODRZfPsILz234uS8BFzHQeYTt2nakpLg1xDf6+LxvY1
umjFOXqaQ0mlqEROsEDW+dvma2gdUhsFcYl+iWYPxS2LgMN/TATPinaOI+mWd9eT
5PltU+j6X98OINttrbNNCTISFG7CK0w80pCEta8vjCgLHpheh5o9t59aF0G5wp7X
LklAG2o0U+GKztiYToGX6r3XQp0i3FQ0lRlEhzxCVElO7loWNDTdYSb/b6lgPwpK
DihbeeH6NhEDkONVX7ZWw67Liew9NC/+Mbp05hTjlBdrcK9kLrJO/jpLjZBKEJbs
eIwiasUREYBdZ3yubpE8V4ZrJGVkqGyoDqBS1Dz82158O6zD0jU/KlvenJ+XxRIW
/w6Y1a9ae8W1xiRjnn+7g0p33/7QpwLAj6wtGAxVUWBnwAXennDdFAynZKxBAGYF
mSlK5BlAdwn07if2WY0VlItpqNuWYEjU4/zoHfO89f+xrem4u0GamPbHAxpsvQTR
U5UfSt6j0qCpIoElVfvuwCi1suMQRaQlDzaybos5OdXHwbKbqUPRGV0wgOMx/a1b
Pjc8FciYBD/8x07xJELxk5aJLZSJpgghrIEU7qLKvrcll39e2xS6ka/Gy/i9boEp
R2hZAJnssspaq5IyxmsREswqYtdqZGbMf7jI+9uqD2L3jqvfUb3Z+GwZ27Go8GxB
IuNtO61nfwFv80ryqqW79Wfht8aiVKY+oufkEA/r+Vkv9Q1WQ2/0XOJSAvVtVTeW
0AKqZbLZkeK1UPMP4VeBgsWt0RPIxvqSvxObSkynka6EV0/HY58qYY9jkQORFN01
kJwUqT/fsITHynTT6rZjL90fyJl/Wo216Bcs7Hw/Rm1EqJNBbKAVxALwxnQ0rJcm
psRDER30CjfsBT6HxO4uJEviP0beC/BjkySUGpZRlvbKmGGHegbxK2H7egYhfJnn
Jr8CO19I6GRWFVZzGQkvAyIHd1gfgM1xilaUFF/N990qHQF8wVrgsQGxY9hSNet5
8KjFxStXKTGLD9nBg7/WbS/xD938YojdUxOtwGklzvA5uIGWycJ6ug4WpyKV4HPw
9JI+lhb9zB5huKTQUyTGgRqSAyGVVP1jJvwlbc8/RN7r/CC0AiMR+t7iE9TIY0gR
wUZFSH2B+0MhhNwHkYSVdPyGHqZZqIOzPwgig7T6I0lMnPhyu0XM1CQoC3nsGhq/
2NjNdoEDxSldZhopYjo7pjmC7sYh6F9ZbL/p6qXzJqX7jxE6ddLILPQYJltFvCUX
5vEYFeOYIhFZGWPL1mpl4u2EwKQQqhDbql7P2WSkClOTKRSHlBfmGNzcXyxrtKgw
UB/RPh+awHDMTgaUVgFBFMUrsVX4bHPgRTDskaJn2t3F+/rPX0aGluFfGJV6nJ4J
cL9XdPe+Rk8xPGemBk/wRuXdNZh5XrXeZwxoB1jdDUSfwMWU3lNV/cVSDkEGCdVr
T2iX7j/FgRc2JsY2NOUQ5qvGwwgWYnQKuT/yQqmqOsjQyJKZrptw71axgx3FiPaV
p2LQs//EI3NCjFhWikB9ASVB5opUhae0e7lCc6e3LRtrAs8R3N0UWjppPHl40XaU
nd3M/dtf10tpUhvWUfgFqs0gKsZ14yAN34T/m1NXrBV4rKSCjBW5NxN5yNP1gw0Q
Lsjz1r8iOmtskYf/mbMKbNfuxByDXCH92LvDlqbnrgaR3LFE7WrynG2UTuI016U3
IoBrDxl7yIXoP7rfPJLONx2cJQN8GKNInsYbPqBKZk4ipIQZTiHf5ODuX9TLpC0w
KiBws73JOI2ITRtFm6gZvvatj/nKdEYltXqDhcJvvNoCBOByXcaujiHpDKuwkipn
TYajzBaLdzFME3ffqdvq2coKizDY0ttblrE9zga6KN5mwTz8y1sATi5cL8flPKRE
cl7C9squYt2W4vgLMQBDma7bCUZ9CKXp4Y5qvm22Qh8nXjedCKC4ClzhbMmDEesj
JiRR/Ap6LCjTiqlWm4947Z15VmGBuCFf6P0xz0BCkt00BkGKk4woggyuHajVQA7g
Rc8xTr5fXPIA3jisSgZri51iEcH9KsWQ/WSmCOrA/WuPYWznuD3WXnI3ECyumgu0
KUwpmaMeqrWBvQu7thKEYPD3djtyhvPJWetdg1cAlzsMbEohtsJZCUN4Lp6yx2YD
CrJyaVANyWBGud5mNokHGxnCBblND2TxPCMVyPeZsawpomC67AR6ZKOiR1+gC095
y320+VN5mVLXGeH8UKLsYVdcqsFwl5uGn/rdYXQnHrlYlIYaEDD8IQ4+TMy112eX
foQbzqirnIEXP46kLpgMhkDyEV+38P+LcmmcrlpDmQhKh1xXYJl49jOd5LuJBhTs
CkOQke4aAc6j6ksByFEyPLaW/7zC78X+ML4541jZscxpOJ71oVI0/09FIdUXge3S
C8BO8y9a1h4FSmJvaodXbgtuLM0AL4JWNQ7q1TrMOiB/yTIiIiCVG3MpX8YTkAC8
M7vxk4FaiGd9S5xN+Ej3HLIILmDbySRkTi2lqCEZ/QJaafsaafFkrn1Pr6RXb7W6
xx4CC3AVHcgqrdugCxPUEYqC/AoAjYNdMBUWjchO3KZq5BNqQgtfCVPa0BRDugsj
SbpFbZeOSi0bYe9le80xKVDHbGa6Ns8AhVnkG3UGBW4naVk4ZD917/3bv3tWrezv
3L/rk047fgmlf93GGE4Ad8fP4oWJDdwGt9ZszC3IYCjHG2NnLvScPrTYTrnQN8MK
u2kv78ZuC8sNGrjMk5xihi7cOeTy5NCyqUv10IwqidKpcszi3Nm8vnl4uVrfh561
ijMFRLsHCZ/7vTVEI5PfnEvK669k//HhXZACSapVIotpWLSj4d14sRcX1y0he8zj
RLcKRA/jg4XHVLopjGpsuiqz/wq8Gqf5fbaU36hkcbQYdes0lSEMtavdG8MoHO8p
iOLtDUzrli7/z27ZBg08813HHKNW9NJki00o9jIBVtaaPoYZ4cInclgFP4tzdsKO
qtFvlMPZf07Me4Ij+ekzA+ZKqbhcxleVoDx1m+dZpjoLIKiFRtYCrvLcmsFqRb37
nlPz602zIefOGeV8acwh/99eCSrYH/YUkWWT2J4orqzGmCHjMtqjcL5hFBNKmCeW
`pragma protect end_protected
